﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using BP.DA;
using BP.En;
using BP.Sys;
using BP.Web;
using BP.OA.PublicPlan;

namespace CCOA.App.PublicPlan
{
    public partial class ViewPlanCatalog : System.Web.UI.Page
    {
        /// <summary>
        /// 编号
        /// </summary>
        private string No
        {
            get
            {
                return this.Request["PK"];
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //装载数据
                PlanCatalog planCatalog = new PlanCatalog(this.No);
                ui_PCModel.Text = planCatalog.PCModel == PCModel.PlanModel ? "计划" : "阶段";
                ui_FinishPercent.Text = planCatalog.FinishPercent.ToString();
                ui_FK_DeptText.Text = planCatalog.FK_DeptText;
                ui_Name.Text = planCatalog.Name;
                ui_PersonCharge.Text = planCatalog.PersonChargeText;
                ui_PlanPartake.Text = planCatalog.PlanPartakeText;
                ui_StartDate.Text = planCatalog.StartDate;
                ui_EndDate.Text = planCatalog.EndDate;
                ui_PK_NO.Value = this.No;
                //负责人,admin,创建人
                if (WebUser.No == "admin" || planCatalog.PersonCharge == WebUser.No || planCatalog.FK_Emp == WebUser.No)
                {
                    //可以编辑，查看别人的反馈
                    ui_IsTick.Value = "2";
                }
                else if (planCatalog.PlanPartake.Split(',').Contains(WebUser.No))//参与人
                {
                    //可以编辑，查看自己的反馈
                    ui_IsTick.Value = "1";
                }
                else
                {
                    //不可编辑，可以查看所有
                    ui_IsTick.Value = "0";
                }
            }
            catch (Exception ex)
            {
            }
        }
    }
}