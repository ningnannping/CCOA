﻿//加载表格数据
function LoadGridData(pageNumber, pageSize) {
    this.pageNumber = pageNumber;
    this.pageSize = pageSize;
    var catalogNo = getArgsFromHref("PK");
    //实体名
    if (catalogNo == '') {
        $.messager.alert('提示', '传入参数有误！', 'info');
        return;
    }
    //是否可以添加反馈
    var isTickCatalog = $("#ui_IsTick").val();
    var toolbar = [{ 'text': '查看详细信息', 'iconCls': 'icon-save-close', 'handler': 'ViewPlanStage'}];
    if (isTickCatalog == "1" || isTickCatalog == "2") {
        toolbar = [{ 'text': '新增', 'iconCls': 'icon-add', 'handler': 'CreatePlanStage' }
                , { 'text': '编写', 'iconCls': 'icon-edit', 'handler': 'EditPlanStage' }
                , { 'text': '删除', 'iconCls': 'icon-delete', 'handler': 'DeletePlanStage' }
                , { 'text': '查看详细信息', 'iconCls': 'icon-edit', 'handler': 'ViewPlanStage'}];
    }
    //参数
    var params = {
        method: "getplancatalogtick",
        catalogNo: catalogNo,
        isTick: isTickCatalog,
        pageNumber: pageNumber,
        pageSize: pageSize
    };
    queryData(params, function (js, scope) {
        if (js) {
            if (js == "") js = "[]";

            var pushData = eval('(' + js + ')');
            var fitColumns = true;
            $('#planTickGrid').datagrid({
                columns: [[
                       { field: 'No', title: '编号', width: 60, hidden: true },
                       { field: 'FK_EmpText', title: '填写人', width: 100, align: 'left' },
                       { field: 'RDT', title: '填写日期', width: 100, align: 'left' },
                       { field: 'StartDate', title: '开始时间', width: 100 },
                       { field: 'EndDate', title: '结束时间', width: 100 },
                       { field: 'FinishPercent', title: '完成百分比', width: 60 }
                       ]],
                data: pushData,
                width: 'auto',
                height: 'auto',
                striped: true,
                rownumbers: true,
                singleSelect: true,
                pagination: true,
                remoteSort: false,
                toolbar: toolbar,
                fitColumns: fitColumns,
                pageNumber: scope.pageNumber,
                pageSize: scope.pageSize,
                pageList: [10, 20, 30, 40],
                onDblClickCell: function (index, field, value) {
                    ViewPlanStage();
                },
                loadMsg: '数据加载中......'
            });

            var pg = $("#planTickGrid").datagrid("getPager");
            if (pg) {
                $(pg).pagination({
                    onRefresh: function (pageNumber, pageSize) {
                        LoadGridData(pageNumber, pageSize);
                    },
                    onSelectPage: function (pageNumber, pageSize) {
                        LoadGridData(pageNumber, pageSize);
                    }
                });
            }
            $("#tb").hide();
        }
    }, this);
}
//创建记录
function CreatePlanStage() {
    var catalogNo = getArgsFromHref("PK");
    var params = {
        method: "createplancatalogtick",
        catalogNo: catalogNo
    };
    queryData(params, function (js, scope) {
        if (js) {
            var grid = $('#planTickGrid');
            var options = grid.datagrid('getPager').data("pagination").options;
            var curPage = options.pageNumber;
            LoadGridData(curPage, 10);
            //打开填写页面
            var url = "../App/PublicPlan/NewPlanCatalogContent.aspx?PK=" + js;
            AddTab("填写反馈情况", url);
        }
    });
}
//编辑记录
function EditPlanStage() {
    var row = $('#planTickGrid').datagrid('getSelected');
    if (row) {
        var url = "../App/PublicPlan/NewPlanCatalogContent.aspx?PK=" + row.No;
        AddTab("填写反馈情况", url);
    }
    else {
        $.messager.alert('提示:', '请先选择项！', 'info');
    }
}
//删除记录
function DeletePlanStage() {
    var row = $('#planTickGrid').datagrid('getSelected');
    if (row) {
        //消息提醒
        $.messager.confirm('提示', "您确定删除所选项？", function (r) {
            if (r) {
                //参数
                var params = {
                    method: "deleteplanstage",
                    stageNo: row.No
                };
                queryData(params, function (js, scope) {
                    if (js) {
                        var grid = $('#planTickGrid');
                        var options = grid.datagrid('getPager').data("pagination").options;
                        var curPage = options.pageNumber;
                        LoadGridData(curPage, 10);    
                    }
                });
            }
        });
    } else {
        $.messager.alert('提示:', '请先选择项！', 'info');
    }
}
//查看详细信息
function ViewPlanStage() {
    var row = $('#planTickGrid').datagrid('getSelected');
    if (row) {
        var url = "../App/PublicPlan/ViewPlanCatalogContent.aspx?PK=" + row.No;
        AddTab("查看反馈情况", url);
    }
    else {
        $.messager.alert('提示:', '请先选择项！', 'info');
    }
}

//默认加载
$(function ($) {
    LoadGridData(1, 10);
});