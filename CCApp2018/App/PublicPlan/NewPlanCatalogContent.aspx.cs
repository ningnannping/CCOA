﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using BP.DA;
using BP.En;
using BP.Web;
using BP.Sys;
using BP.OA.PublicPlan;

namespace CCOA.App.PublicPlan
{
    public partial class NewPlanCatalogContent : WebPage
    {
        private string CPK
        {
            get
            {
                return this.Request["PK"];
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            //页面第一次加载
            if (!this.IsPostBack)
            {
                ui_FinishPercent.Attributes.Add("onkeyup", @"Change(this);");
                ui_FinishPercent.Attributes.Add("onkeydown", "VirtyNum(this,'int')");
                ui_FinishPercent.Attributes["OnKeyPress"] += @"javascript:return  VirtyNum(this,'int');";
                if (!string.IsNullOrEmpty(this.CPK))
                {
                    BindPageControl();
                }
            }
        }

        /// <summary>
        /// 绑定页面值
        /// </summary>
        private void BindPageControl()
        {
            PlanStageContent planStage = new PlanStageContent();
            planStage.RetrieveByAttr(PlanStageContentAttr.No, this.CPK);
            //如果用户信息不存在，说明不属于任何一个人，认为是记录不存在
            if (!string.IsNullOrEmpty(planStage.FK_Emp))
            {
                //获取计划信息
                PlanCatalog planCatalog = new PlanCatalog();
                planCatalog.RetrieveByAttr(PlanCatalogAttr.No, planStage.FK_PlanStageContent);
                ui_PlanName.Text = planCatalog.Name;
                //获取上次计划信息
                PlanStageContent perStageContent = new PlanStageContent();
                QueryObject obj = new QueryObject(perStageContent);
                obj.Top = 1;
                obj.AddWhere(PlanStageContentAttr.FK_PlanStageContent, planCatalog.No);
                obj.addAnd();
                obj.AddWhere(PlanStageContentAttr.FK_Emp, WebUser.No);
                obj.addAnd();
                obj.AddWhere(PlanStageContentAttr.No, "<", planStage.No);
                obj.DoQuery();
                if (perStageContent != null && !string.IsNullOrEmpty(perStageContent.NextComplate))
                    ui_PerComplate.Text = perStageContent.NextComplate;
                else
                    ui_PerComplate.Text = "无";
                //本次计划内容
                ui_StartDate.Text = planStage.StartDate;
                ui_EndDate.Text = planStage.EndDate;
                ui_TargetContent.Text = planStage.TargetContent;
                ui_DegreeItems.Text = planStage.DegreeItem;
                ui_NextComplate.Text = planStage.NextComplate;
                ui_FinishPercent.Text = planStage.FinishPercent.ToString();
            }
            else
            {
                this.Alert("没有获取到相关记录！");
            }
        }

        /// <summary>
        /// 执行保存
        /// </summary>
        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            PlanStageContent planStage = new PlanStageContent();
            planStage.RetrieveByAttr(PlanStageContentAttr.No, this.CPK);
            //如果用户信息不存在，说明不属于任何一个人，认为是记录不存在
            if (!string.IsNullOrEmpty(planStage.FK_Emp))
            {
                if (String.IsNullOrEmpty(ui_StartDate.Text))
                {
                    this.Alert("开始时间不能为空!");
                    return;
                }
                if (String.IsNullOrEmpty(ui_EndDate.Text))
                {
                    this.Alert("结束时间不能为空!");
                    return;
                }
                DateTime StartTime = Convert.ToDateTime(String.Format("{0:yyyy-MM-dd HH:mm:ss}", ui_StartDate.Text));
                DateTime EndTime = Convert.ToDateTime(String.Format("{0:yyyy-MM-dd HH:mm:ss}", ui_EndDate.Text));
                if (StartTime > EndTime)
                {
                    this.Alert("开始时间不能晚于结束时间!");
                    return;
                }
                if (String.IsNullOrEmpty(ui_TargetContent.Text))
                {
                    this.Alert("本阶段完成工作不能为空!");
                    return;
                }
                if (String.IsNullOrEmpty(ui_FinishPercent.Text))
                {
                    this.Alert("完成百分比不能为空!");
                    return;
                }
                planStage.StartDate = ui_StartDate.Text;
                planStage.EndDate = ui_EndDate.Text;
                planStage.TargetContent = ui_TargetContent.Text;
                planStage.DegreeItem = ui_DegreeItems.Text;
                planStage.NextComplate = ui_NextComplate.Text;
                planStage.FinishPercent = Convert.ToDecimal(ui_FinishPercent.Text);
                //修改计划进度
                PlanCatalog planCatalog = new PlanCatalog();
                planCatalog.RetrieveByAttr(PlanCatalogAttr.No, planStage.FK_PlanStageContent);
                planCatalog.FinishPercent = planStage.FinishPercent;
                planCatalog.Update();

                if (planStage.Update() > 0)
                    this.Alert("保存成功！");
                else
                    this.Alert("保存失败！");
            }
        }
    }
}