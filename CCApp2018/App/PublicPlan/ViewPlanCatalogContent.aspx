﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Main/master/Site1.Master" AutoEventWireup="true"
    CodeBehind="ViewPlanCatalogContent.aspx.cs" ValidateRequest="false" ClientIDMode="Static"
    Inherits="CCOA.App.PublicPlan.ViewPlanCatalogContent" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cph_head" runat="server">
    <style type="text/css">
        body
        {
            font-size: 13px;
        }
        .doc-table
        {
            border-collapse: collapse;
            border-spacing: 0;
            margin-left: 9px;
            display: table;
            border-color: gray;
            width: 99%;
        }
        .doc-table th
        {
            background: #EEE;
        }
        .doc-table th, .doc-table td
        {
            border: 1px solid #BDDBEF;
            padding: 3px 3px;
        }
        td.title
        {
            width: 120px;
            text-align: right;
        }
        td.title3
        {
            width: 80px;
            text-align: right;
        }
        td.text
        {
            text-align: left;
            padding-left: 10px;
        }
    </style>
    <script src="../../js/js_EasyUI/jquery.easyui.min.js" type="text/javascript"></script>
    <link href="../../js/js_EasyUI/themes/default/easyui.css" rel="stylesheet" type="text/css" />
    <script src="../../js/js_EasyUI/locale/easyui-lang-zh_CN.js" type="text/javascript"></script>
    <link href="../../js/js_EasyUI/themes/icon.css" rel="stylesheet" type="text/css" />
    <script src="../../js/js_EasyUI/plugins/jquery.datebox.js" type="text/javascript"></script>
    <script src="../../js/zDialog/zDialog.js" type="text/javascript"></script>
    <link rel="stylesheet" href="../../ctrl/kindeditor/themes/default/default.css" />
    <link rel="stylesheet" href="../../ctrl/kindeditor/plugins/code/prettify.css" />
    <script type="text/javascript" charset="utf-8" src="../../ctrl/kindeditor/kindeditor.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../ctrl/kindeditor/lang/zh_CN.js"></script>
    <script type="text/javascript" charset="utf-8" src="../../ctrl/kindeditor/plugins/code/prettify.js"></script>
    <script type="text/javascript">
//        KindEditor.ready(function (K) {
//            var editor1 = K.create('#ui_TargetContent', {
//                cssPath: '../../ctrl/kindeditor/plugins/code/prettify.css',
//                uploadJson: '../../ctrl/kindeditor/asp.net/upload_json.ashx',
//                fileManagerJson: '../../ctrl/kindeditor/asp.net/file_manager_json.ashx',
//                allowFileManager: true
//            });
//            editor1.sync();
//            prettyPrint();
//        }); 
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_title" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_body" runat="server">
    <div style="margin: 10px 100px; width: 840px;">
        <table class="doc-table">
            <caption style="text-align: left; font-size: 14px; font-weight: bold;">
                <img src="../../Images/System/RptDir.gif" style="width: 18px; height: 18px;" />反馈执行情况</caption>
            <tr>
                <td class="title">
                    &nbsp;计划名称:
                </td>
                <td class="text" colspan="5">
                    <asp:Label ID="ui_PlanName" runat="server" Width="90%"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="title">
                    &nbsp;上次计划内容:
                </td>
                <td class="text" colspan="5">
                    <div style="width: 97%;height: 30px;margin: 5x 5px 5px 5px; padding: 5px 5px 5px 5px;border: dashed 1px #dddddd;">
                        &nbsp;<asp:Literal runat="server" ID="ui_PerComplate"></asp:Literal>
                    </div>
                </td>
            </tr>
            <tr>
                <td style="text-align:left; margin-left:5px; font-weight:bold;" colspan="6">
                    &nbsp;本次反馈情况如下：
                </td>
            </tr>
            <tr>
                <td class="title">
                    <span style="color: Red;">*</span>&nbsp;开始时间:
                </td>
                <td class="text">
                    &nbsp;<asp:Label ID="ui_StartDate" runat="server" Width="90%"></asp:Label>
                </td>
                <td class="title3">
                    <span style="color: Red;">*</span>&nbsp;结束时间:
                </td>
                <td class="text">
                    &nbsp;<asp:Label ID="ui_EndDate" runat="server" Width="90%"></asp:Label>
                </td>
                <td class="title3">
                    <span style="color: Red;">*</span>&nbsp;完成进度:
                </td>
                <td class="text" style="width:150px;">
                    &nbsp;<asp:Label ID="ui_FinishPercent" runat="server" Width="30px"></asp:Label>%
                </td>
            </tr>
            <tr>
                <td class="title">
                    &nbsp;<span style="color: Red;">*</span>&nbsp;本阶段完成工作:
                </td>
                <td class="text" colspan="5">
                    <div style="width: 97%;height: 100px;margin: 5x 5px 5px 5px; padding: 5px 5px 5px 5px;border: dashed 1px #dddddd; overflow:auto;">
                        &nbsp;<asp:Literal runat="server" ID="ui_TargetContent"></asp:Literal>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="title">
                    &nbsp;重难点问题:
                </td>
                <td class="text" colspan="5">
                    <div style="width: 97%;height: 100px;margin: 5x 5px 5px 5px; padding: 5px 5px 5px 5px;border: dashed 1px #dddddd; overflow:auto;">
                        &nbsp;<asp:Literal runat="server" ID="ui_DegreeItems"></asp:Literal>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="title">
                    &nbsp;下次计划完成工作:
                </td>
                <td class="text" colspan="5">
                    <div style="width: 97%;height: 100px;margin: 5x 5px 5px 5px; padding: 5px 5px 5px 5px;border: dashed 1px #dddddd; overflow:auto;">
                        &nbsp;<asp:Literal runat="server" ID="ui_NextComplate"></asp:Literal>
                    </div>
                </td>
            </tr>
        </table>
        <div style="text-align: center; margin-top: 5px;">
            <asp:HiddenField ID="ui_PK_NO" runat="server"></asp:HiddenField>
        </div>
    </div>
</asp:Content>
