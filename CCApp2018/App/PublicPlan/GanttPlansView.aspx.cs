﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using BP.En;
using BP.DA;
using BP.Web;
using BP.Sys;
using BP.WF;
using BP.OA.PublicPlan;

namespace CCOA.App.PublicPlan
{
    public partial class GanttPlansView : WebPage
    {
        /// <summary>
        /// 获取传入参数
        /// </summary>
        /// <param name="param">参数名</param>
        /// <returns></returns>
        public string getUTF8ToString(string param)
        {
            return HttpUtility.UrlDecode(Request[param], System.Text.Encoding.UTF8);
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            string method = string.Empty;
            //返回值
            string s_responsetext = string.Empty;
            if (string.IsNullOrEmpty(Request["method"]))
                return;
            if (string.IsNullOrEmpty(WebUser.No))
                return;

            method = Request["method"].ToString();
            switch (method)
            {
                case "getchildplancount"://获取子计划个数
                    s_responsetext = GetChildPlanCount();
                    break;
                case "getchartdata"://获取计划甘特图
                    s_responsetext = GetPlanChartDataBJJQ();
                    break;
                case "backpreplan"://返回上一层
                    s_responsetext = BackPrePlan();
                    break;
            }
            if (string.IsNullOrEmpty(s_responsetext))
                s_responsetext = "";
            //组装ajax字符串格式,返回调用客户端
            Response.Charset = "UTF-8";
            Response.ContentEncoding = System.Text.Encoding.UTF8;
            Response.ContentType = "text/html";
            Response.Expires = 0;
            Response.Write(s_responsetext);
            Response.End();
        }

        /// <summary>
        /// 获取子计划个数
        /// </summary>
        /// <returns></returns>
        private string GetChildPlanCount()
        {
            string parentNo = getUTF8ToString("ParentNo");
            string expendChild = getUTF8ToString("expendChild");
            if (string.IsNullOrEmpty(parentNo))
                parentNo = "0";

            string sql = "select COUNT(*) from OA_PlanCatalog where ParentNo='" + parentNo + "' and PCModel='0'";
            if (!string.IsNullOrEmpty(expendChild) && expendChild.ToLower() == "true")
            {
                PlanCatalog planCata = new PlanCatalog();
                planCata.RetrieveByAttr(PlanCatalogAttr.No, parentNo);
                sql = "select COUNT(*) from OA_PlanCatalog where TreeNo like '" + planCata.No + "%'";
            }
            return DBAccess.RunSQLReturnStringIsNull(sql, "0");
        }

        /// <summary>
        /// 返回上一层
        /// </summary>
        /// <returns></returns>
        private string BackPrePlan()
        {
            string nodeNo = getUTF8ToString("nodeNo");
            PlanCatalog catalog = new PlanCatalog();
            catalog.RetrieveByAttr(PlanCatalogAttr.No, nodeNo);
            if (catalog == null || string.IsNullOrEmpty(catalog.Name))
            {
                return "{ParentNo:'0',TreeNo:''}";
            }
            return "{ParentNo:'" + catalog.ParentNo + "',TreeNo:'" + catalog.No + "'}";
        }

        /// <summary>
        /// 获取计划甘特图数据
        /// </summary>
        /// <returns></returns>
        private string GetPlanChartDataBJJQ()
        {
            try
            {
                string parentNo = getUTF8ToString("ParentNo");
                string expendChild = getUTF8ToString("expendChild");
                //获取计划实体
                PlanCatalog parentPlan = new PlanCatalog();
                parentPlan.RetrieveByAttr(PlanCatalogAttr.No, parentNo);
                //如果是根节点
                if (!string.IsNullOrEmpty(parentNo) && parentNo == "0")
                {
                    parentPlan.RetrieveByAttr(PlanCatalogAttr.ParentNo, parentNo);
                    parentNo = parentPlan.No;
                }
                if (string.IsNullOrEmpty(parentPlan.Name))
                    return "null";

                StringBuilder append = new StringBuilder();
                append.Append("<?xml version=\"1.0\" encoding=\"gb2312\" ?>");
                append.Append("<chart dateFormat='yyyy/MM/dd' hoverCapBorderColor='2222ff' hoverCapBgColor='e1f5ff' ganttLineAlpha='80' canvasBorderColor='024455' canvasBorderThickness='0' gridBorderColor='4567aa' gridBorderAlpha='20'>");

                #region categories 表头
                //获取最大日期
                string secondName = parentPlan.Name;
                //GetSecondLevelName(parentPlan.No, ref secondName);
                append.Append("  <categories  bgColor='009999'>");
                append.Append("    <category start='2015/03/1' end='2015/09/30' align='center' name='" + secondName + "'  fontColor='ffffff' isBold='1' fontSize='16' />");
                append.Append("  </categories>");

                append.Append("  <categories  bgColor='ffffff' fontColor='1288dd'>");
                append.Append("    <category start='2015/03/1' end='2015/03/31' align='center' name='3月'  isBold='1' />");
                append.Append("    <category start='2015/04/1' end='2015/04/30' align='center' name='4月'  isBold='1' />");
                append.Append("    <category start='2015/05/1' end='2015/05/31' align='center' name='5月' isBold='1' />");
                append.Append("    <category start='2015/06/1' end='2015/06/30' align='center' name='6月' isBold='1'/>");
                append.Append("    <category start='2015/07/1' end='2015/07/31' align='center' name='7月' isBold='1' />");
                append.Append("    <category start='2015/08/1' end='2015/08/31' align='center' name='8月' isBold='1'/>");
                append.Append("    <category start='2015/09/1' end='2015/09/30' align='center' name='9月' isBold='1'/>");
                append.Append("  </categories>");

                append.Append("  <categories  bgColor='ffffff' fontColor='1288dd' fontSize='10' >");
                append.Append("    <category start='2015/03/1'  end='2015/03/10' align='center' name='上旬'  isBold='1' />");
                append.Append("    <category start='2015/03/11' end='2015/03/20' align='center' name='中旬'  isBold='1' />");
                append.Append("    <category start='2015/03/21' end='2015/03/31' align='center' name='下旬' isBold='1' />");
                append.Append("    <category start='2015/04/1'  end='2015/04/10' align='center' name='上旬'  isBold='1' />");
                append.Append("    <category start='2015/04/11' end='2015/04/20' align='center' name='中旬'  isBold='1' />");
                DateTime curDate = DateTime.Now;
                DateTime compareDateStart = DateTime.Parse("2015/04/21");
                DateTime compareDateEnd = DateTime.Parse("2015/04/30 23:59:59");
                if (curDate >= compareDateStart && curDate <= compareDateEnd)
                {
                    append.Append("    <category start='2015/04/21' end='2015/04/30' align='center' name='下旬' isBold='1' bgcolor='CCCCFF' />");
                }
                else
                {
                    append.Append("    <category start='2015/04/21' end='2015/04/30' align='center' name='下旬' isBold='1' />");
                }
                compareDateStart = DateTime.Parse("2015/05/1");
                compareDateEnd = DateTime.Parse("2015/05/10 23:59:59");
                if (curDate >= compareDateStart && curDate <= compareDateEnd)
                {
                    append.Append("    <category start='2015/05/1'  end='2015/05/10' align='center' name='上旬'  isBold='1' bgcolor='CCCCFF'/>");
                }
                else
                {
                    append.Append("    <category start='2015/05/1'  end='2015/05/10' align='center' name='上旬'  isBold='1'/>");
                }
                compareDateStart = DateTime.Parse("2015/05/11");
                compareDateEnd = DateTime.Parse("2015/05/20 23:59:59");
                if (curDate >= compareDateStart && curDate <= compareDateEnd)
                {
                    append.Append("    <category start='2015/05/11' end='2015/05/20' align='center' name='中旬'  isBold='1' bgcolor='CCCCFF' />");
                }
                else
                {
                    append.Append("    <category start='2015/05/11' end='2015/05/20' align='center' name='中旬'  isBold='1' />");
                }
                compareDateStart = DateTime.Parse("2015/05/21");
                compareDateEnd = DateTime.Parse("2015/05/31 23:59:59");
                if (curDate >= compareDateStart && curDate <= compareDateEnd)
                {
                    append.Append("    <category start='2015/05/21' end='2015/05/31' align='center' name='下旬' isBold='1' bgcolor='CCCCFF' />");
                }
                else
                {
                    append.Append("    <category start='2015/05/21' end='2015/05/31' align='center' name='下旬' isBold='1' />");
                }
                compareDateStart = DateTime.Parse("2015/06/1");
                compareDateEnd = DateTime.Parse("2015/06/10 23:59:59");
                if (curDate >= compareDateStart && curDate <= compareDateEnd)
                {
                    append.Append("    <category start='2015/06/1'  end='2015/06/10' align='center' name='上旬'  isBold='1' bgcolor='CCCCFF' />");
                }
                else
                {
                    append.Append("    <category start='2015/06/1'  end='2015/06/10' align='center' name='上旬'  isBold='1' />");
                }
                compareDateStart = DateTime.Parse("2015/06/11");
                compareDateEnd = DateTime.Parse("2015/06/20 23:59:59");
                if (curDate >= compareDateStart && curDate <= compareDateEnd)
                {
                    append.Append("    <category start='2015/06/11' end='2015/06/20' align='center' name='中旬'  isBold='1' bgcolor='CCCCFF' />");
                }
                else
                {
                    append.Append("    <category start='2015/06/11' end='2015/06/20' align='center' name='中旬'  isBold='1' />");
                }
                compareDateStart = DateTime.Parse("2015/06/21");
                compareDateEnd = DateTime.Parse("2015/06/30 23:59:59");
                if (curDate >= compareDateStart && curDate <= compareDateEnd)
                {
                    append.Append("    <category start='2015/06/21' end='2015/06/30' align='center' name='下旬' isBold='1' bgcolor='CCCCFF' />");
                }
                else
                {
                    append.Append("    <category start='2015/06/21' end='2015/06/30' align='center' name='下旬' isBold='1' />");
                }
                compareDateStart = DateTime.Parse("2015/07/1");
                compareDateEnd = DateTime.Parse("2015/07/10 23:59:59");
                if (curDate >= compareDateStart && curDate <= compareDateEnd)
                {
                    append.Append("    <category start='2015/07/1'  end='2015/07/10' align='center' name='上旬'  isBold='1' bgcolor='CCCCFF' />");
                }
                else
                {
                    append.Append("    <category start='2015/07/1'  end='2015/07/10' align='center' name='上旬'  isBold='1' />");
                }
                compareDateStart = DateTime.Parse("2015/07/11");
                compareDateEnd = DateTime.Parse("2015/07/20 23:59:59");
                if (curDate >= compareDateStart && curDate <= compareDateEnd)
                {
                    append.Append("    <category start='2015/07/11' end='2015/07/20' align='center' name='中旬'  isBold='1' bgcolor='CCCCFF' />");
                }
                else
                {
                    append.Append("    <category start='2015/07/11' end='2015/07/20' align='center' name='中旬'  isBold='1' />");
                }
                compareDateStart = DateTime.Parse("2015/07/21");
                compareDateEnd = DateTime.Parse("2015/07/31 23:59:59");
                if (curDate >= compareDateStart && curDate <= compareDateEnd)
                {
                    append.Append("    <category start='2015/07/21' end='2015/07/31' align='center' name='下旬' isBold='1' bgcolor='CCCCFF' />");
                }
                else
                {
                    append.Append("    <category start='2015/07/21' end='2015/07/31' align='center' name='下旬' isBold='1' />");
                }
                compareDateStart = DateTime.Parse("2015/08/1");
                compareDateEnd = DateTime.Parse("2015/08/10 23:59:59");
                if (curDate >= compareDateStart && curDate <= compareDateEnd)
                {
                    append.Append("    <category start='2015/08/1'  end='2015/08/10' align='center' name='上旬'  isBold='1' bgcolor='CCCCFF' />");
                }
                else
                {
                    append.Append("    <category start='2015/08/1'  end='2015/08/10' align='center' name='上旬'  isBold='1' />");
                }
                compareDateStart = DateTime.Parse("2015/08/11");
                compareDateEnd = DateTime.Parse("2015/08/20 23:59:59");
                if (curDate >= compareDateStart && curDate <= compareDateEnd)
                {
                    append.Append("    <category start='2015/08/11' end='2015/08/20' align='center' name='中旬'  isBold='1' bgcolor='CCCCFF' />");
                }
                else
                {
                    append.Append("    <category start='2015/08/11' end='2015/08/20' align='center' name='中旬'  isBold='1' />");
                }
                compareDateStart = DateTime.Parse("2015/08/21");
                compareDateEnd = DateTime.Parse("2015/08/31 23:59:59");
                if (curDate >= compareDateStart && curDate <= compareDateEnd)
                {
                    append.Append("    <category start='2015/08/21' end='2015/08/31' align='center' name='下旬' isBold='1' bgcolor='CCCCFF' />");
                }
                else
                {
                    append.Append("    <category start='2015/08/21' end='2015/08/31' align='center' name='下旬' isBold='1' />");
                }
                compareDateStart = DateTime.Parse("2015/09/1");
                compareDateEnd = DateTime.Parse("2015/09/10 23:59:59");
                if (curDate >= compareDateStart && curDate <= compareDateEnd)
                {
                    append.Append("    <category start='2015/09/1'  end='2015/09/10' align='center' name='上旬'  isBold='1' bgcolor='CCCCFF' />");
                }
                else
                {
                    append.Append("    <category start='2015/09/1'  end='2015/09/10' align='center' name='上旬'  isBold='1' />");
                }
                compareDateStart = DateTime.Parse("2015/09/11");
                compareDateEnd = DateTime.Parse("2015/09/20 23:59:59");
                if (curDate >= compareDateStart && curDate <= compareDateEnd)
                {
                    append.Append("    <category start='2015/09/11' end='2015/09/20' align='center' name='中旬'  isBold='1' bgcolor='CCCCFF' />");
                }
                else
                {
                    append.Append("    <category start='2015/09/11' end='2015/09/20' align='center' name='中旬'  isBold='1' />");
                }
                compareDateStart = DateTime.Parse("2015/09/21");
                compareDateEnd = DateTime.Parse("2015/09/30 23:59:59");
                if (curDate >= compareDateStart && curDate <= compareDateEnd)
                {
                    append.Append("    <category start='2015/09/21' end='2015/09/30' align='center' name='下旬' isBold='1' bgcolor='CCCCFF' />");
                }
                else
                {
                    append.Append("    <category start='2015/09/21' end='2015/09/30' align='center' name='下旬' isBold='1' />");
                }
                append.Append("  </categories>");
                #endregion

                #region processes 标题，开始时间，结束时间
                //子计划
                PlanCatalogs childPlans = new PlanCatalogs();
                QueryObject obj = new QueryObject(childPlans);
                //如果不要求显示阶段
                if (!string.IsNullOrEmpty(expendChild) && expendChild.ToLower() == "false")
                {
                    obj.AddWhere(PlanCatalogAttr.ParentNo, parentNo);
                    obj.addAnd();
                    obj.AddWhere(PlanCatalogAttr.PCModel, (int)PCModel.PlanModel);
                }
                else
                {
                    obj.AddWhere("OA_PlanCatalog.TreeNo like '" + parentPlan.No + "%'");
                }
                obj.addOrderBy(PlanCatalogAttr.Idx);
                obj.DoQuery();

                //processes 显示标题
                append.Append("  <processes headerText='" + parentPlan.Name + "' fontColor='000000' fontSize='10' isBold='1' isAnimated='1' bgColor='eeeeee'  headerVAlign='center' headerbgColor='4567aa' headerFontColor='ffffff' headerFontSize='12' align='left' width='200'>");
                if (childPlans.Count > 0)
                {
                    foreach (PlanCatalog planItem in childPlans)
                    {
                        append.Append(string.Format("    <process Name='{0}' link='javascript:GetChildPlanCount(\"{1}\");' id='{1}' fontSize='12' />", planItem.Name, planItem.No));
                    }
                }
                append.Append("  </processes>");

                //dataTable
                append.Append("  <dataTable showProcessName='1' nameAlign='left' fontColor='000000' fontSize='10' isBold='1' headerBgColor='00ffff' headerFontColor='4567aa' headerFontSize='12' align='left'>");

                //dataColumn开始时间
                append.Append("    <dataColumn width='60' headerfontcolor='ffffff' headerBgColor='4567aa' bgColor='eeeeee'  headerColor='ffffff' headerText='开始' isBold='0'>");
                if (childPlans.Count > 0)
                {
                    foreach (PlanCatalog planItem in childPlans)
                    {
                        append.Append(string.Format("      <text label='{0}' />", planItem.StartDate.Replace("2015-","")));
                    }
                }
                append.Append("    </dataColumn>");

                //dataColumn结束时间
                append.Append("    <dataColumn width='60' headerfontcolor='ffffff'  bgColor='eeeeee' headerbgColor='4567aa'  fontColor='000000' headerText='结束' isBold='0'>");
                if (childPlans.Count > 0)
                {
                    foreach (PlanCatalog planItem in childPlans)
                    {
                        append.Append(string.Format("      <text label='{0}' />", planItem.EndDate.Replace("2015-","")));
                    }
                }
                append.Append("    </dataColumn>");
                //总天数
                append.Append("    <dataColumn width='40' headerfontcolor='ffffff'  bgColor='eeeeee' headerbgColor='4567aa'  fontColor='000000' headerText='天数' isBold='0'>");
                if (childPlans.Count > 0)
                {
                    foreach (PlanCatalog planItem in childPlans)
                    {
                        if (string.IsNullOrEmpty(planItem.StartDate) || string.IsNullOrEmpty(planItem.EndDate))
                        {
                            append.Append("      <text label='0' />");
                            continue;
                        }
                        TimeSpan ts = DateTime.Parse(planItem.EndDate) - DateTime.Parse(planItem.StartDate);
                        int planSpaceDays = ts.Days;
                        append.Append(string.Format("      <text label='{0}' />", planSpaceDays));
                    }
                }
                append.Append("    </dataColumn>");
                append.Append("  </dataTable>");
                #endregion

                #region   tasks 计划进度和实际进度
                append.Append("  <tasks>");
                if (childPlans.Count > 0)
                {
                    string planName = "";
                    string startDate = "";
                    string endDate = "";
                    string processId = "";

                    foreach (PlanCatalog planItem in childPlans)
                    {
                        processId = planItem.No;
                        PlanCatalogs planChildStages = GetPlanChildStages(planItem.No);
                        //如果计划子项不存在阶段则计算本身进度
                        if (planChildStages == null)
                        {
                            startDate = TransDateTimeShort(planItem.StartDate);
                            endDate = TransDateTimeShort(planItem.EndDate);
                            planName = planItem.Name;
                            //计划进度
                            append.Append(string.Format("    <task name='{0}' processId='{1}' start='{2}' end='{3}' Id='{4}-1' color='4567aa' height='10' topPadding='10' animation='0'/>", planName, processId, startDate, endDate, processId));
                            continue;
                        }
                        //展示阶段内容
                        foreach (PlanCatalog planItemChild in planChildStages)
                        {
                            string color = RandColor(int.Parse(planItemChild.No));
                            startDate = TransDateTimeShort(planItemChild.StartDate);
                            endDate = TransDateTimeShort(planItemChild.EndDate);
                            planName = planItemChild.Name;
                            //计划进度
                            append.Append(string.Format("    <task name='{0}' processId='{1}' start='{2}' end='{3}' Id='{4}-{5}-1' color='{6}' height='10' topPadding='10' animation='0'/>", planName, processId, startDate, endDate, processId, planItemChild.No, color));
                        }
                    }
                }
                append.Append("  </tasks>");
                #endregion

                append.Append("</chart>");

                return append.ToString();
            }
            catch (Exception ex)
            {
            }
            return "";
        }

        /// <summary>
        /// 获取计划阶段，如果没有阶段返回Null
        /// </summary>
        /// <param name="planNo"></param>
        /// <returns></returns>
        private PlanCatalogs GetPlanChildStages(string PlanNo)
        {
            //计划的阶段
            PlanCatalogs planStages = new PlanCatalogs();
            QueryObject obj = new QueryObject(planStages);
            obj.AddWhere(PlanCatalogAttr.ParentNo, PlanNo);
            obj.addAnd();
            obj.AddWhere(PlanCatalogAttr.PCModel, (int)PCModel.StageModel);
            obj.addOrderBy(PlanCatalogAttr.Idx);
            obj.DoQuery();
            if (planStages.Count > 0)
                return planStages;

            //如果不存在阶段返回Null
            return null;
        }

        /// <summary>
        /// 获取计划开始时间和实际进度
        /// </summary>
        /// <param name="PlanNo"></param>
        /// <returns></returns>
        private PlanCatalog GetPlanStageStartDate(PlanCatalog planCatalog)
        {
            try
            {
                //实际进度
                PlanCatalog actualPlan = new PlanCatalog();
                //获取第一次反馈时间
                PlanStageContent planStage = new PlanStageContent();
                QueryObject obj = new QueryObject(planStage);
                obj.Top = 1;
                obj.AddWhere(PlanStageContentAttr.FK_PlanStageContent, planCatalog.No);
                obj.addOrderBy(PlanStageContentAttr.StartDate);
                obj.DoQuery();
                //默认值
                actualPlan.Name = planCatalog.Name;
                actualPlan.StartDate = planCatalog.StartDate;
                actualPlan.EndDate = planCatalog.EndDate;
                actualPlan.FinishPercent = planCatalog.FinishPercent;
                //获取开始时间
                if (!string.IsNullOrEmpty(planStage.StartDate))
                    actualPlan.StartDate = string.Format("{0}", TransDateTimeShort(planStage.StartDate));
                //根据进度计算结束时间
                if (!string.IsNullOrEmpty(planCatalog.EndDate))
                {
                    TimeSpan ts = DateTime.Parse(planCatalog.EndDate) - DateTime.Parse(planCatalog.StartDate);
                    int planSpaceDays = ts.Days;
                    //时间差乘以完成进去除去基数得到实际进度天数
                    decimal planAddDays = planSpaceDays * planCatalog.FinishPercent / 100;
                    DateTime endDateTime = DateTime.Parse(actualPlan.StartDate).AddDays(Convert.ToDouble(planAddDays));
                    actualPlan.EndDate = endDateTime.ToString(@"yyyy\/MM\/dd");
                }
                return actualPlan;
            }
            catch (Exception ex)
            {
            }
            return null;
        }
        /// <summary>
        /// 转换日期格式：2014/7/1
        /// </summary>
        /// <param name="dateTime"></param>
        /// <returns></returns>
        public string TransDateTimeShort(string dateTime)
        {
            try
            {
                DateTime dt = DateTime.Parse(dateTime);
                return dt.ToString(@"yyyy\/MM\/dd");
            }
            catch (Exception ex)
            {
            }
            return dateTime;
        }

        /// <summary>
        /// 获取颜色
        /// </summary>
        /// <returns></returns>
        private string RandColor(int random)
        {
            string[] colorArrary = { "3324dd", "009999", "333333", "CCCC00", "9900FF" };
            int ranIdex = random % colorArrary.Length;
            return colorArrary[ranIdex];
        }

        /// <summary>
        /// 获取计划甘特图数据
        /// </summary>
        /// <returns></returns>
        private string GetPlanChartsData()
        {
            string parentNo = getUTF8ToString("ParentNo");
            //获取计划实体
            PlanCatalog parentPlan = new PlanCatalog();
            parentPlan.RetrieveByAttr(PlanCatalogAttr.No, parentNo);
            if (string.IsNullOrEmpty(parentPlan.Name))
                return "null";

            StringBuilder append = new StringBuilder();
            append.Append("<?xml version=\"1.0\" encoding=\"gb2312\" ?>");
            append.Append("<chart dateFormat='yyyy-mm-dd' hoverCapBorderColor='2222ff' hoverCapBgColor='e1f5ff' ganttLineAlpha='80' canvasBorderColor='024455' canvasBorderThickness='0' gridBorderColor='4567aa' gridBorderAlpha='20'>");

            #region categories
            //获取最小日期
            string minStartDate = DateTime.Now.Year + "/1/1";
            minStartDate = DBAccess.RunSQLReturnStringIsNull("SELECT MIN(STARTDATE) STARTDATE FROM OA_PLANCATALOG WHERE TREENO LIKE '" + parentPlan.No + "%';", minStartDate);
            //获取最大日期
            string maxEndDate = DateTime.Now.Year + "/12/30";
            maxEndDate = DBAccess.RunSQLReturnStringIsNull("SELECT MAX(ENDDATE) ENDDATE FROM OA_PLANCATALOG WHERE TREENO LIKE '" + parentPlan.No + "%';", maxEndDate);
            string secondName = "";
            GetSecondLevelName(parentPlan.No, ref secondName);

            append.Append("  <categories  bgColor='009999'>");
            append.Append("    <category start='" + minStartDate + "' end='" + maxEndDate + "' align='center' name='" + secondName + "'  fontColor='ffffff' isBold='1' fontSize='16' />");
            append.Append("  </categories>");
            #endregion



            append.Append("</chart>");
            return "";
        }

        /// <summary>
        /// 获取二级菜单名称
        /// </summary>
        /// <param name="PlanNo"></param>
        /// <returns></returns>
        private void GetSecondLevelName(string PlanNo, ref string secondLevelName)
        {
            PlanCatalog parentPlan = new PlanCatalog();
            parentPlan.RetrieveByAttr(PlanCatalogAttr.No, PlanNo);

            //返回二级或根节点
            if (parentPlan.ParentNo != "0" && parentPlan.ParentNo != "99")
            {
                GetSecondLevelName(parentPlan.ParentNo, ref secondLevelName);
            }
            else
            {
                secondLevelName = parentPlan.Name;
            }
        }
    }
}