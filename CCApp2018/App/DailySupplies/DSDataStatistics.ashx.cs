﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BP.OA;
using BP.En;
using BP.Sys;
using BP.DS;
using System.Data;
using BP.DA;
using BP.Tools;

namespace CCOA.App.DailySupplies
{
    /// <summary>
    /// DSDataStatistics 的摘要说明
    /// </summary>
    public class DSDataStatistics : IHttpHandler
    {
        /// <summary>
        /// 封装有关个别 HTTP 请求的所有 HTTP 特定的信息
        /// </summary>
        HttpContext _Context = null;

        public string getUTF8ToString(string param)
        {
            return HttpUtility.UrlDecode(_Context.Request[param], System.Text.Encoding.UTF8);
        }

        public void ProcessRequest(HttpContext context)
        {
            _Context = context;

            //admin获取权限
            //if (BP.Web.WebUser.No == null)
            //    return;

            string method = string.Empty;
            //返回值
            string s_responsetext = string.Empty;
            if (!string.IsNullOrEmpty(_Context.Request["method"]))
                method = _Context.Request["method"].ToString();

            switch (method)
            {
                case "getnewslist"://获取新闻列表
                    s_responsetext = GetNewsList();
                    break;
            }
            if (string.IsNullOrEmpty(s_responsetext))
                s_responsetext = "";
            //组装ajax字符串格式,返回调用客户端
            _Context.Response.Charset = "UTF-8";
            _Context.Response.ContentEncoding = System.Text.Encoding.UTF8;
            _Context.Response.ContentType = "text/html";
            _Context.Response.Expires = 0;
            _Context.Response.Write(s_responsetext);
            _Context.Response.End();
        }

        /// <summary>
        /// 获取列表
        /// </summary>
        /// <returns></returns>
        public string GetNewsList()
        {
            int totalCount = 0;

            string Sort = getUTF8ToString("Sort");

            //当前页
            string pageNumber = getUTF8ToString("pageNumber");
            int iPageNumber = string.IsNullOrEmpty(pageNumber) ? 1 : Convert.ToInt32(pageNumber);
            //每页多少行
            string pageSize = getUTF8ToString("pageSize");
            int iPageSize = string.IsNullOrEmpty(pageSize) ? 9999 : Convert.ToInt32(pageSize);


            DSTakes dsTakes = new DSTakes();
            //Articles articles = new Articles();
            QueryObject obj = new QueryObject(dsTakes);
            obj.AddWhere("1=1");

            //处理js异常的抛出,如果领取表中不存在FK_Sort=Sort的记录,会抛出异常
            if (Sort == "请选择")
            {
                Sort = "0";
            }
            else
            {
                if (Sort == "按类别")
                {
                    Sort = "0";
                }
                else
                {
                    Sort = "";
                }
            }

            //string SortGetOKSQL = "SELECT * from oa_dstake WHERE FK_Sort=";
            //DataTable SortdTable = DBAccess.RunSQLReturnTable(SortGetOKSQL);
            //if (SortdTable.Rows.Count == 0)
            //{
            //    Sort = "0";
            //}

            if (Sort == "0")//如果为类型
            {
                totalCount = obj.GetCount();
                obj.addOrderByDesc(DSTakeAttr.FK_Sort);
            }
            else
            {
                //obj.AddWhere(String.Format(" and FK_Sort='{0}'", Sort));
                totalCount = obj.GetCount();
                obj.addOrderByDesc(DSTakeAttr.TakerDep);
            }


            //totalCount = obj.GetCount();
            //obj.addOrderByDesc(DSTakeAttr.FK_Sort);
            obj.DoQuery(DSTakeAttr.OID, iPageSize, iPageNumber);

            return Entitis2Json.ConvertEntitis2GridJsonOnlyData(dsTakes);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}