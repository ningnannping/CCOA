﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using BP.OA;

namespace CCOA.App.DailyRecord
{
    public partial class NewSheet : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {


              
                #region 初始化数据.
                int oid = 0;
                string oidStr = this.Request.QueryString["OID"];
                if (string.IsNullOrEmpty(oidStr) == false)
                    oid = int.Parse(oidStr);
                BP.OA.DRSheet en = new BP.OA.DRSheet();
                en.OID = oid;
                if (oid != 0)
                    en.Retrieve();
                this.TB_Title.Text = en.Title;
               
                this.TB_Doc.Text= en.GetValDocText();
               
                

                if (en.DRSta == BP.OA.DRSta.Private)
                    this.RB_0.Checked = true;

                if (en.DRSta == BP.OA.DRSta.Share)
                    this.RB_1.Checked = true;

                if (en.DRSta == BP.OA.DRSta.Public)
                    this.RB_2.Checked = true;



                #endregion 初始化数据.
            }

        }

        //限制共享日志修改
        public string NotEdit()
        {
            string sSql = String.Format("select *from OA_DRSheet where DRSta=1 and OID='{0}'");
            return BP.DA.DBAccess.RunSQLReturnString(sSql);

        }

        //限制当天分享日志次数
        public string IsGetCom()
        {
            string sSql = String.Format("select * from OA_DRSheet where DateDiff(dd,RDT,getdate())=0 and  DRSta=1 and Rec='{0}' ", BP.Web.WebUser.No);

               return   BP.DA.DBAccess.RunSQLReturnString(sSql);

        }
        BP.OA.DRSheet en; //得到他的实体
        protected void btnSub_Click(object sender, EventArgs e)
        {
            if (IsGetCom() ==null || this.RB_0.Checked || this.RB_2.Checked || this.RB_3.Checked || this.RB_4.Checked)
            {
                if (string.IsNullOrEmpty(Request.QueryString["OID"]) == false)
                {
                    en = new BP.OA.DRSheet(Int32.Parse(Request.QueryString["OID"]));
                }
                else
                {
                    en = new BP.OA.DRSheet();
                }

                en = BP.Sys.PubClass.CopyFromRequest(en) as BP.OA.DRSheet;
                //   en.CheckPhysicsTable();
                int sta = 0;
                int rta = 0;
                if (this.RB_0.Checked)
                {
                    sta = 0;
             
                }
                if (this.RB_1.Checked)
                {
                    sta = 1;
          
                }
                if (this.RB_2.Checked)
                {
                    sta = 2;
                
                }
                if (this.RB_3.Checked)
                {
                   
                    rta = 3;
                }
                if (this.RB_4.Checked)
                {
                 
                    rta = 4;
                }
                 en.DRShe=(BP.OA.DRShe)rta;
                en.DRSta = (BP.OA.DRSta)sta; 
                en.Save();


                Response.Redirect("MySheet.aspx");

            }
            else
            {
                Response.Write("<script>alert('亲，一天只能共享一次噢，或共享日志不能编辑！')</script>");
            }
                 
        }
    }
}
