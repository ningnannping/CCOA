﻿function replaceTrim(val) {//去除空格
    return val.replace(/[ ]/g, "");
}
//统一权限接口 true  false
var curNodeId = null;
var FK_Emp = "";

//初始化
$(function () {
    $("#pageloading").show();
    //加载菜单
    LoadDeptTree();
});

//加载菜单
function LoadDeptTree() {
    Application.data.getDeptTree(0, showDeptTreeCallBack, this);
}

//展示部门树
function showDeptTreeCallBack(js, scope) {
    if (js == "") js = [];
    var pushData = eval('(' + js + ')');

    //加载部门目录  qinqin
    $("#deptTree").tree({
        data: pushData,
        collapsed: true,
        lines: true,
        onClick: function (node) {
            $("#deptTree").tree("expand", node.target);
            deptNodeTreeAction(node);
        }
    });
    loadRoot()
    //关闭等待页面
    $("#pageloading").hide();
}

//默认显示根节点信息
function loadRoot() {
    var node;
    if (curNodeId) {
        node = $('#deptTree').tree('find', curNodeId);
    }
    else {
        node = $('#deptTree').tree('getRoot');
        curNodeId = node.id;
    }

    $('#deptTree').tree('select', node.target);
    $('#deptTree').tree('expandTo', node.target);
    $('#deptTree').tree('expand', node.target);

    $('#searchText').val('');
    LoadDataGridAdmin(1, 20);
}

function deptNodeTreeAction(node) {
    $('#deptTree').tree('select', node.target);
    curNodeId = node.id;
    $('#searchText').val('');

    LoadDataGridAdmin(1, 20);
}

//加载人员列表
function LoadDataGridAdmin(pageNumber, pageSize) {
    $('#empGrid').datagrid('loadData', { total: 0, rows: [] }); //清空下方DateGrid

    var searchText = $('#searchText').val();
    Application.data.LoadDataGridDeptEmp(curNodeId, orderBy, replaceTrim(searchText), pageNumber, pageSize, function (js, scope) {
        if (js) {
            if (js == "") js = [];
            var pushData = eval('(' + js + ')');

            $('#empGrid').datagrid({
                data: pushData,
                width: 'auto',
                rownumbers: true,
                pagination: true,
                singleSelect: true,
                selectOnCheck: false,
                loadMsg: '数据加载中......',
                pageNumber: pageNumber,
                pageSize: pageSize,
                pageList: [20, 30, 40, 50],
                columns: [[
                       { field: 'NO', title: '帐号', sortable: true, width: 100, align: 'left' },
                       { field: 'NAME', title: '人员姓名', sortable: true, width: 200, align: 'center' },
                       { field: 'DEPTNAME', title: '所在部门', sortable: true, width: 200, align: 'center' },
                       { field: 'DUTYNAME', title: '职务', sortable: true, width: 160, align: 'center' },
//                       { field: 'STATIONNAME', title: '岗位', sortable: true, width: 160, align: 'center' },
                       { field: 'TEL', title: '电话', sortable: true, width: 160, align: 'center' },
                       { field: 'EMAIL', title: 'E-Mail', sortable: true, width: 160, align: 'center' }
                       ]],
                onClickRow: function (rowIndex, rowData) {
                    FK_Emp = rowData.NO;
                }
            });

            //分页
            var pg = $("#empGrid").datagrid("getPager");
            if (pg) {
                $(pg).pagination({
                    onRefresh: function (pageNumber, pageSize) {
                        LoadDataGridAdmin(pageNumber, pageSize);
                    },
                    onSelectPage: function (pageNumber, pageSize) {
                        LoadDataGridAdmin(pageNumber, pageSize);
                    }
                });
            }
        }
    }, this);
}


//模糊查询
function SearchByEmpNoOrName(pageNumber, pageSize) {
    var searchText = $('#searchText').val();
    if (searchText == "") {
        $.messager.alert("提示", "请输入查询内容，支持账号、姓名、工号和电话!", "info");
        return;
    }
    $('#empGrid').datagrid('loadData', { total: 0, rows: [] }); //清空下方DateGrid
    Application.data.searchByEmpNoOrName(replaceTrim(searchText), orderBy, pageNumber, pageSize, function (js, scope) {
        if (js) {
            if (js == "") js = [];
            var pushData = eval('(' + js + ')');

            $('#empGrid').datagrid({
                data: pushData,
                width: 'auto',
                rownumbers: true,
                pagination: true,
                singleSelect: true,
                selectOnCheck: false,
                loadMsg: '数据加载中......',
                pageNumber: pageNumber,
                pageSize: pageSize,
                pageList: [20, 30, 40, 50],
                columns: [[
                       { field: 'NO', title: '帐号', sortable: true, width: 100, align: 'left' },
                       { field: 'NAME', title: '人员姓名', sortable: true, width: 200, align: 'center' },
                       { field: 'DEPTNAME', title: '所在部门', sortable: true, width: 200, align: 'center' },
                       { field: 'DUTYNAME', title: '职务', sortable: true, width: 160, align: 'center' },
//                       { field: 'STATIONNAME', title: '岗位', sortable: true, width: 160, align: 'center' },
                       { field: 'TEL', title: '电话', sortable: true, width: 160, align: 'center' },
                       { field: 'EMAIL', title: 'E-Mail', sortable: true, width: 160, align: 'center' }
                       ]],
                onClickRow: function (rowIndex, rowData) {
                    var node = $('#deptTree').tree('find', rowData.FK_DEPT);
                    if (node) {
                        $('#deptTree').tree('expandTo', node.target);
                        $('#deptTree').tree('select', node.target);
                    }
                    FK_Emp = rowData.NO;
                    curNodeId = rowData.FK_DEPT;
                }
            });

            //分页
            var pg = $("#empGrid").datagrid("getPager");
            if (pg) {
                $(pg).pagination({
                    onRefresh: function (pageNumber, pageSize) {
                        SearchByEmpNoOrName(pageNumber, pageSize);
                    },
                    onSelectPage: function (pageNumber, pageSize) {
                        SearchByEmpNoOrName(pageNumber, pageSize);
                    }
                });
            }
        }
    }, this);
}


//排序
var sortAgain = 0;
var orderBy = '';
function LoadGridOrderBy(lbtn) {
    var isASC = sortAgain % 2 != 0;
    orderBy = isASC ? "Name ASC" : "Name DESC";
    $(lbtn).linkbutton({ iconCls: isASC ? "icon-down" : "icon-up", text: isASC ? "按姓名降序" : "按姓名升序" });
    sortAgain += 1;

    LoadDataGridAdmin(1, 20);
}