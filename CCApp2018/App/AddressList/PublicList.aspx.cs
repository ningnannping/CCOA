﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BP.OA;
using BP.En;
using System.Data;

namespace CCOA.App.AddressList
{
    public partial class PublicList : System.Web.UI.Page
    {
        #region 属性.
        public int PageIdx
        {
            get
            {
                try
                {
                    return int.Parse(this.Request.QueryString["PageIdx"]);
                }
                catch
                {
                    return 1;
                }
            }
        }
        #endregion 属性.
        protected void Page_Load(object sender, EventArgs e)
        {
                  int currPageIdx = this.PageIdx;
                  int pageSize = 12; //页面记录条数.
                
                BP.OA.ALEmps ens = new ALEmps();
                BP.En.QueryObject qo = new QueryObject(ens);
            
                this.Pub1.Clear();
                this.Pub1.BindPageIdx(qo.GetCount(),
                    pageSize, currPageIdx, "PublicList.aspx?1 =2&3=xx");
                qo.DoQuery("No", pageSize, currPageIdx);

                DataTable dt = ens.ToDataTableField();
                rep_List.DataSource = dt;
                rep_List.DataBind();
            
           

           
        }

        /// <summary>
        /// 通过ID获取他的值
        /// </summary>
        #region

        private static DataTable _AllDepts = null;

        public static DataTable GetAllDepts()
        {
            if (_AllDepts == null)
            {
                string sSql = "Select * from Port_ALDept";
                _AllDepts = BP.DA.DBAccess.RunSQLReturnTable(sSql);
            }
            return _AllDepts;
        }

        public static string GetDeptNames(string depts)
        {
            List<String> l_depts = new List<string>();
            foreach (DataRow dr in GetAllDepts().Rows)
            {
                if (String.Format(",{0},", depts).Contains(String.Format(",{0},", Convert.ToString(dr["No"]))))
                    l_depts.Add(String.Format("{0}", dr["Name"]));
            }
            return String.Join(",", l_depts.ToArray());
        }

        /// <summary>
        /// 取得发布者为用户还是部门
        /// </summary>
        /// <param name="evalUser"></param>
        /// <param name="evalDept"></param>
        /// <returns></returns>
        /// 
        public string GetDeptNames(object userNos)
        {
            String users = String.Format("{0}", userNos);
            return GetDeptNames(users);
        }
        #endregion


    }

}

