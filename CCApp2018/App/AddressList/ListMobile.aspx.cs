﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BP.Web;
using BP.DA;
using System.Data;
namespace CCOA.App.AddressList
{
    public partial class ListMobile : System.Web.UI.Page
    {
        public string getUTF8ToString(string param)
        {
            return HttpUtility.UrlDecode(Request[param], System.Text.Encoding.UTF8);

        }
        protected void Page_Load(object sender, EventArgs e)
        {
            string method = string.Empty;
            string s_responsetext = string.Empty;
            if (string.IsNullOrEmpty(Request["method"]))
            {
                return;
            }
            method = Request["method"].ToString();
            switch (method)
            {
                case "getList":
                    s_responsetext = getList();
                    break;
                //case"WEISHENQING":
                //    s_responsetext = WEISHENQING();
                //    break;
            }
            if (string.IsNullOrEmpty(s_responsetext))
                s_responsetext = "";
            Response.Charset = "UTF-8";
            Response.ContentEncoding = System.Text.Encoding.UTF8;
            Response.ContentType = "text/html";
            Response.Expires = 0;
            Response.Write(s_responsetext);
            Response.End();

        }
        private string getList()
        {
            string IT_TXL = getUTF8ToString("IT_TXL");
            string sql = "";

            string pageNumber = getUTF8ToString("pageNumber");
            int iPageNumber = string.IsNullOrEmpty(pageNumber) ? 1 : Convert.ToInt32(pageNumber);
            string pageSize = getUTF8ToString("pageSize");
            int iPageSize = string.IsNullOrEmpty(pageSize) ? 9999 : Convert.ToInt32(pageSize);
            if (IT_TXL != "")
            {
                sql = "select * from OA_ALEmp where 1=1 and name='" + IT_TXL + "'";
            }
            else
            {
                sql = "select * from OA_ALEmp";
            }


            return DBPaging(" ( " + sql + " ) dbSourse ", iPageNumber, iPageSize, "No", "");
        }


        private string DBPaging(string dataSource, int pageNumber, int pageSize, string key, string orderKey)
        {
            string sql = "";
            string orderByStr = "";

            if (!string.IsNullOrEmpty(orderKey))
                orderByStr = " ORDER BY " + orderKey;

            switch (DBAccess.AppCenterDBType)
            {
                case BP.DA.DBType.Oracle:
                    int beginIndex = (pageNumber - 1) * pageSize + 1;
                    int endIndex = pageNumber * pageSize;

                    sql = "SELECT * FROM ( SELECT A.*, ROWNUM RN " +
                        "FROM (SELECT * FROM  " + dataSource + orderByStr + ") A WHERE ROWNUM <= " + endIndex + " ) WHERE RN >=" + beginIndex;
                    break;
                case DBType.MSSQL:
                    sql = "SELECT TOP " + pageSize + " * FROM " + dataSource + " WHERE " + key + " NOT IN  ("
                    + "SELECT TOP (" + pageSize + "*(" + pageNumber + "-1)) " + key + " FROM " + dataSource + " )" + orderByStr;
                    break;
                default:
                    throw new Exception("暂不支持您的数据库类型.");
            }

            DataTable DTable = DBAccess.RunSQLReturnTable(sql);

            int totalCount = DBAccess.RunSQLReturnCOUNT("select " + key + " from " + dataSource);

            return DataTableConvertJson.DataTable2Json(DTable, totalCount);
        }
    }
}