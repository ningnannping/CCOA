﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BP.OA.HR;
using System.IO;
using BP.Port;
using BP.Web.Controls;
namespace CCOA.App.HR
{
    public partial class DaoRuExecel : System.Web.UI.Page
    {
        TB tb = new TB();
        protected void Page_Load(object sender, EventArgs e)
        {
            Lab la = new Lab();
            la.Text = "月份：";
            this.Pub1.Add(la);
            tb.ID = "TB_DaoRu";
            tb.ShowType = TBType.Date;
            tb.Attributes["onfocus"] = "WdatePicker();";
            this.Pub1.Add(tb);
        }
        protected void BT_DaoRu_Click(object sender, EventArgs e)
        {
            if (this.fu.HasFile)
            {
                //保存上传文件
                string filename = this.fu.PostedFile.FileName;
                string extension = (new FileInfo(filename)).Extension;
                string newfilename = System.DateTime.Now.ToString("yyyyMMddHHmmss") + extension;
                string path = Server.MapPath("/DataUser/userloadfile/");
                this.fu.PostedFile.SaveAs(path + newfilename);
                //导入execel
                try
                {
                    DataTable dt = BP.DA.DBLoad.ReadExcelFileToDataTable(path + newfilename);
                string s = dt.Columns[4].ColumnName.ToString() + " like '" + "%" + DateTime.Parse(tb.Text.ToString()).Month + "%" + "'";
                dt.Select(s);
                GongJiJin gj = new GongJiJin();
                foreach (DataRow row in dt.Rows)
                {
                    BP.Port.Depts des = new Depts();
                    des.RetrieveAll();
                    gj.FK_Dept = ((Dept)des.Filter("Name", row[1].ToString())).No.ToString();
                    gj.FK_DanWei = row[2].ToString();
                    BP.OA.HR.HRRecords hrrs = new BP.OA.HR.HRRecords();
                    hrrs.RetrieveAll();
                    gj.FK_Name = ((HRRecord)hrrs.Filter("Name", row[3].ToString())).No.ToString();
                    gj.Mouth = row[4].ToString();
                    gj.XinJiShu =float.Parse( row[5].ToString());
                    gj.DanWeiJiaoCun =float.Parse(  row[6].ToString());
                    gj.GeRenJiaoCun = float.Parse( row[7].ToString());
                    gj.GeRenZhangHao = row[8].ToString();
                    gj.RDT = row[9].ToString();
                    gj.BeiZhu = row[10].ToString();
                    gj.Save();
                }
                Response.Redirect("/WF/Comm/Search.aspx?EnsName=BP.OA.HR.GongJiJins");
                }
                catch (Exception)
                {
                     BP.Sys.PubClass.Alert("文件的格式必须是xls,并且符合模板要求！");
                     return;
                }
            }
        }
    }
}