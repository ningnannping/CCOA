﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NotepaperList.aspx.cs"
    Inherits="CCOA.App.NotepaperList" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="../../Js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script src="../../Js/zDialog/zDialog.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="/js/js_EasyUI/themes/default/easyui.css" />
    <link rel="stylesheet" type="text/css" href="/js/js_EasyUI/themes/icon.css" />
    <link rel="stylesheet" type="text/css" href="../../CSS/GridviewPager.css" />
    <script type="text/javascript" src="/js/js_EasyUI/jquery.easyui.min.js"></script>
    <script src="/js/js_EasyUI/plugins/jquery.menubutton.js" type="text/javascript"></script>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:GridView ID="gv_Notepaper" CssClass="grid" runat="server" AutoGenerateColumns="false"
            Width="98%">
            <Columns>
                <asp:TemplateField HeaderText="序号" InsertVisible="False">
                    <ItemStyle HorizontalAlign="Center" />
                    <HeaderStyle HorizontalAlign="Center" Width="5%" />
                    <ItemTemplate>
                        <%# (this.AspNetPager1.CurrentPageIndex - 1) * this.AspNetPager1.PageSize + this.gv_Notepaper.Rows.Count + 1%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="便笺名称" ItemStyle-Width="350px">
                    <ItemTemplate>
                        <%#Eval("Name") %>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="撰写时间" ItemStyle-Width="150px">
                    <ItemTemplate>
                        <%# Convert.ToDateTime(Eval("RDT")).ToString("yyyy年MM月dd日 HH:mm:ss")%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="管理" ItemStyle-Width="150px" ItemStyle-HorizontalAlign="Center">
                    <ItemTemplate>
                        <div style="float:left;">
                            <a href="NoticeEdit.aspx?OID=<%# Eval("No") %>">编辑</a>
                        </div>
                        <div style="float:left; margin-left:10px;">
                            <asp:LinkButton ID="Btn_Del" runat="server" CausesValidation="False" OnClick="Del_Notepaper"
                                OnClientClick="return confirm('是否真的要删除这条便签吗？');" CommandName='<% #Eval("No") %>'
                                Text="删除" />
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate></ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
    <table cellpadding="0" cellspacing="0" align="center" width="99%" class="border">
        <tr>
            <td align="left" colspan="2">
                <webdiyer:AspNetPager ID="AspNetPager1" CssClass="paginator" CurrentPageButtonClass="cpb"
                    runat="server" AlwaysShow="false" FirstPageText="首页" LastPageText="尾页" NextPageText="下一页"
                    PageSize="5" PrevPageText="上一页" ShowCustomInfoSection="Left" ShowInputBox="Never"
                    CustomInfoTextAlign="Left" LayoutType="Table" OnPageChanged="AspNetPager1_PageChanged">
                </webdiyer:AspNetPager>
            </td>
        </tr>
    </table>
    </form>
</body>
</html>
