﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BP.OA.Meeting;

namespace CCOA.App.Meeting
{
    public partial class ResSelect : System.Web.UI.Page
    {
        string[] NOArry;
        protected void Page_Load(object sender, EventArgs e)
        {
            string nos = Request.QueryString["no"];
            if (!string.IsNullOrEmpty(nos))
            {
                NOArry = nos.Split(',');
            }
            RoomResources reses = new RoomResources();
            reses.RetrieveAll();
            Repeater1.DataSource = reses;
            Repeater1.DataBind();
        }
        public string ChekcState(object obj)
        {
            if (obj != null && NOArry != null && NOArry.Contains(obj.ToString()))
            {
                return "checked='checked'";
            }
            else
            {
                return string.Empty;
            }
        }
    }
}