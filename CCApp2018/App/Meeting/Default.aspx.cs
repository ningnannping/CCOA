﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

namespace CCOA.App.Meeting
{
    public partial class Default : System.Web.UI.Page
    {

        #region 属性.
        public string ShowType
        {
            get
            {
                return this.Request.QueryString["ShowType"];
            }
        }
        public string CurrDate
        {
            get
            {
                string s = this.Request.QueryString["CurrDate"];
                if (string.IsNullOrEmpty(s) || s == "undefined")
                    return BP.DA.DataType.CurrentData;
                return s;
            }
        }
        #endregion 属性.
        private DataTable _DTNoCount = null;
        public DataTable DTNoCount
        {
            get
            {
                if (_DTNoCount == null)
                {
                    string strSql = "select a.No,Fk_Room,DateFrom,DateTo from OA_Room a,OA_RoomOrding b where a.No=b.FK_Room";
                    _DTNoCount = BP.DA.DBAccess.RunSQLReturnTable(strSql);
                }
                return _DTNoCount;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack == false)
            {
                BP.OA.Meeting.Rooms rooms = new BP.OA.Meeting.Rooms();
                rooms.RetrieveAll();
                Repeater1.DataSource = rooms;
                Repeater1.DataBind();
            }
        }

        /// <summary>
        /// 会议室使用状态转换。
        /// </summary>
        /// <param name="o"></param>
        /// <returns></returns>
        public string StateConverter(object o)
        {
            return (Convert.ToString((MeetingRoomState)o));
        }
        /// <summary>
        /// 临时变量用来存放检测过的会议室预定数量
        /// </summary>
        int TemCount = 0;
        public string CheckVisible()
        {
            if (TemCount > 0)
                return "inline";
            else
                return "none";
        }
        /// <summary>
        /// 检测会议室预定数量
        /// </summary>
        /// <returns></returns>
        public string CheckOrderNum(object no)
        {
            int i = 0;
            DataRow[] drs = DTNoCount.Select("no=" + Convert.ToString(no));
            foreach (DataRow dr in drs)
            {
                DateTime dtCurr = Convert.ToDateTime(CurrDate).Date;
                DateTime dtFrom = Convert.ToDateTime(dr["DateFrom"]).Date;
                DateTime dtTo = Convert.ToDateTime(dr["DateTo"]).Date;
                if (dtFrom <= dtCurr && dtTo >= dtCurr)
                {
                    i++;
                }
            }
            TemCount = i;
            return i.ToString();
        }
        //<%#GetOrderList( DataBinder.Eval(Container.DataItem,"No"))%>
        public string GetOrderList(object no)
        {
            string result = string.Empty;
            DataRow[] drs = DTNoCount.Select("no=" + Convert.ToString(no));
            foreach (DataRow dr in drs)
            {
                DateTime dtCurr = Convert.ToDateTime(CurrDate);
                DateTime dtFrom = Convert.ToDateTime(dr["DateFrom"]);
                DateTime dtTo = Convert.ToDateTime(dr["DateTo"]);
                if (dtFrom.Date <= dtCurr.Date && dtTo.Date >= dtCurr.Date)
                {
                    result += dtFrom + "--" + dtTo + "<br/>";
                }
            }
            return result;
        }
    }
    public enum MeetingRoomState
    {
        未预定 = 0,
        已预订 = 1,
        使用中 = 2,
        未知 = 3
    }
}