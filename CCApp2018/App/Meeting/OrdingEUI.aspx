﻿<%@ Page Title="" Language="C#" MasterPageFile="../AppMaster/AppSite.Master" AutoEventWireup="true"
    CodeBehind="OrdingEUI.aspx.cs" Inherits="CCOA.App.Meeting.OrdingEUI1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
    <script src="../../Js/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
    <script language="javascript" type="text/javascript">
        var roomNo = "";
        //发起会议
        function Sponsor(para, state) {
            if (state == "预定中") {
                var url = "SponsorMeeting.aspx?oid=" + para;
                DoOpenCommonWindow(url, 700, 520, "发起会议");
            }
            else {
                ShowMessage("会议" + state + "不能发起会议！");
            }
        }
        //会议纪要
        function Summary(para, state) {
            if (state == "已结束") {
                var url = "Summary.aspx?oid=" + para;
                DoOpenCommonWindow(url, 820, 520, "会议纪要");
            }
            else {
                ShowMessage("会议" + state + "不能添加会议纪要！");
            }
        }
        //会议变更
        function Modify(para, state) {
            if (state == "预定中" || state == "待召开") {
                var url = "Order.aspx?oid=" + para;
                DoOpenCommonWindow(url, 650, 520, "会议变更");
            }
            else {
                ShowMessage("会议" + state + "不能变更会议！");
            }
        }
        //结束会议
        function Stop(para, state) {
            if (state == "正在进行") {
                ChangeState(para, 3);
            }
            else {
                ShowMessage("会议" + state + "不能结束会议！");
            }
        }
        //取消会议
        function Cancel(para, state) {
            if (state == "预定中" || state == "待召开") {
                ChangeState(para, 2);
            }
            else {
                ShowMessage("会议" + state + "不能取消会议！");
            }
        }
        //状态
        function ChangeState(para, state) {
            var params = {
                mtd: "ChangeState",
                OID: para,
                state: state
            };
            queryData(params, function (js, scope) {
                ShowMessage(js);
                var date = $('#d15').val();
                var row = $('#newsGrid').datagrid('getSelected');
                if (row) {
                    LoadOrdingGridData(row.No, date);
                }
            }, this);
        }
        var diag = new Dialog();
        //打开窗口
        function DoOpen(url, height, width, title) {
            diag.Width = width;
            diag.Height = height;
            diag.Title = title;
            diag.URL = url;
            diag.show();
        }
        //加载grid后回调函数
        function LoadDataGridCallBack(js, scorp) {
            $("#pageloading").hide();
            if (js == "") js = "[]";
            var pushData = eval('(' + js + ')');
            $('#newsGrid').datagrid({
                columns: [[
                    { field: 'NO', title: '编号', width: 60, hidden: true },
                    { field: 'Name', title: '会议室名称', width: 60 },
                    { field: "State", title: "状态", width: 80, formatter: function (value, rec) {
                        var date = $('#d15').val();
                        $.ajax({
                            type: "post", //使用GET或POST方法访问后台
                            dataType: "text", //返回json格式的数据
                            contentType: "application/json; charset=utf-8",
                            url: "OrdingEUI.aspx/CheckOrderNum", //要访问的后台地址
                            data: "{'no':" + "'" + rec.No + "','CurrDate':" + "'" + date + "'}", //要发送的数据
                            async: false,
                            cache: false,
                            error: function (XMLHttpRequest, errorThrown) {
                                $("body").html("<b>访问页面出错，请联系管理员。<b>");
                            },
                            success: function (msg) {//msg为返回的数据，在这里做数据绑定
                                value = msg
                            }
                        });
                        value = value.substring(6, value.length - 2);
                        return "已有" + " <span style='color:red'>" + value + "</span>" + "个预定";
                    }
                    }, { field: 'Title', title: '操作', width: 300, formatter: function (value, rec) {
                        return "<a href='javascript:void(0)' onclick=\"DoOpenDialog('" + rec.No + "')\">预定</a>";
                    }
                    }
                ]],
                data: pushData,
                width: 'auto',
                height: 'auto',
                striped: true,
                rownumbers: true,
                singleSelect: true,
                pagination: false,
                remoteSort: false,
                fitColumns: true,
                idField: "No",
                pageList: [20, 30, 40, 50],
                onClickRow: function (rowIndex, rowData) {
                    var date = $('#d15').val();
                    roomNo = rowData.No;
                    LoadOrdingGridData(roomNo, date);
                },
                loadMsg: '数据加载中......'
            });
        }
        //加载grid
        function LoadGridData(pageNumber, pageSize) {
            var pageNumber = pageNumber;
            var pageSize = pageSize;
            var params = {
                mtd: "getmeetingroomlist"
            };
            queryData(params, LoadDataGridCallBack, this);
        }
        //加载预定会议详细
        function LoadOrdingGridData(FK_RoomNo, curDate) {
            var params = {
                mtd: "getordinglist",
                FK_RoomNo: FK_RoomNo,
                curDate: curDate
            };
            queryData(params, function (js, scope) {
                $("#pageloading").hide();
                var empno = $("#txtEmpNo").val();
                if (js == "") js = "[]";
                var pushData = eval('(' + js + ')');
                $('#gridOrding').datagrid({
                    columns: [[
                    { field: 'OID', title: '编号', width: 60, hidden: true },
                    { field: 'TITLE', title: '标题', width: 120 },
                    { field: 'ORDERNAME', title: '预定人', width: 60 },
                    { field: 'DEPTNAME', title: '预定部门', width: 60 },
                    { field: 'DATEFROM', title: '开始时间', width: 80 },
                    { field: 'DATETO', title: '结束时间', width: 80 },
                    { field: 'TOEMPNAMES', title: '参与人员', width: 80 },
                    { field: 'TOSTATIONNAMES', title: '参与岗位', width: 80 },
                    { field: 'TODEPTNAMES', title: '参与部门', width: 80 },
                    { field: 'STATE', title: '状态', width: 60 },
                    { field: 'MTRESOURCE', title: '资源', width: 100 },
                    { field: 'MANGE', title: '操作', width: 200, formatter: function (value, rec) {
                        var href = "<a href=javascript:Sponsor('" + rec.OID + "','" + rec.STATE + "');>会议发起</a>&nbsp;&nbsp;"
                        + "<a href=javascript:Summary('" + rec.OID + "','" + rec.STATE + "')>会议纪要</a>&nbsp;&nbsp;"
                        + "<a href=javascript:Modify('" + rec.OID + "','" + rec.STATE + "')>会议变更</a>&nbsp;&nbsp;"
                        + "<a href=javascript:Stop('" + rec.OID + "','" + rec.STATE + "')>结束会议</a>&nbsp;&nbsp;"
                        //具有"取消会议"权限的人有：预定会议人，admin
                        if ((rec.FK_EMP == empno) || (empno == "admin")) {
                            href += "<a href=javascript:Cancel('" + rec.OID + "','" + rec.STATE + "')>取消会议</a>";
                        } else if (rec.ROOMADMIN.indexOf(empno) > 0)//会议室管理员之一
                         {
                            href += "<a href=javascript:Cancel('" + rec.OID + "','" + rec.STATE + "')>取消会议</a>";
                        }
                        return href;
                    }
                    }
                ]],
                    data: pushData,
                    width: 'auto',
                    height: 'auto',
                    striped: true,
                    rownumbers: true,
                    singleSelect: true,
                    pagination: false,
                    remoteSort: false,
                    fitColumns: true,
                    pageList: [20, 30, 40, 50],
                    onDblClickRow: function (rowIndex, rowData) {

                    },
                    loadMsg: '数据加载中......'
                });
            }, this);
        }

        //打开预定窗口
        function DoOpenDialog(id) {
            //预定窗口
            var url = "Order.aspx?id=" + id;
            $('#openIframe')[0].src = url;
            commonWin = $('#dialogWindow').window({
                title: "预定",
                width: 650,
                height: 520,
                top: 2,
                left: (document.body.clientWidth - 650) * 0.5,
                shadow: true,
                modal: true,
                iconCls: 'icon-add',
                closed: true,
                maximizable: true,
                collapsible: true
            });
            commonWin.window('open');
        }
        //刷新会议室列表
        function ReFreashRoomGrid() {
            $('#dialogWindow').dialog('close');
            LoadGridData();
            if (roomNo) {
                $('#ensGrid').datagrid('selectRecord', roomNo);
                var date = $('#d15').val();
                LoadOrdingGridData(roomNo, date);
            }
        }
        //打开公共页面
        var commonWin = null;
        function DoOpenCommonWindow(url, width, height, title) {
            $('#openIframe')[0].src = url;
            commonWin = $('#dialogWindow').window({
                title: title,
                width: width,
                height: height,
                top: 2,
                left: (document.body.clientWidth - width) * 0.5,
                shadow: true,
                modal: true,
                iconCls: 'icon-add',
                closed: true,
                maximizable: true,
                collapsible: true
            });
            commonWin.window('open');
        }
        //刷新预定列表
        function RefreashOrdingGrid() {
            if (commonWin) {
                commonWin.window('close');
                if (roomNo) {
                    var date = $('#d15').val();
                    LoadOrdingGridData(roomNo, date);
                }
            }
        }
        //弹出消息
        function ShowMessage(msg) {
            $.messager.alert('提示', msg);
        }

        //初始化
        $(function () {
            $("#pageloading").show();
            LoadGridData();
        });
        //公共方法
        function queryData(param, callback, scope, method, showErrMsg) {
            if (!method) method = 'GET';
            $.ajax({
                type: method, //使用GET或POST方法访问后台
                dataType: "text", //返回json格式的数据
                contentType: "application/json; charset=utf-8",
                url: "Ording.ashx", //要访问的后台地址
                data: param, //要发送的数据
                async: false,
                cache: false,
                complete: function () { }, //AJAX请求完成时隐藏loading提示
                error: function (XMLHttpRequest, errorThrown) {
                    $("body").html("<b>访问页面出错，请联系管理员。<b>");
                    //callback(XMLHttpRequest);
                },
                success: function (msg) {//msg为返回的数据，在这里做数据绑定
                    var data = msg;
                    callback(data, scope);
                }
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <input type="hidden" id="txtEmpNo" name="txtEmpNo" value="<%=BP.Web.WebUser.No%>" />
    <div data-options="region:'center'" border="false" style="margin: 0; padding: 0;
        overflow: hidden;">
        <div id="pageloading">
        </div>
        <div id="tb" style="padding: 6px; height: 28px;">
            选择日期：
            <input class="Wdate" type="text" id="d15" value='<%=DateTime.Now.ToString("yyyy-MM-dd")%>'
                onfocus="WdatePicker({isShowClear:false,readOnly:true})" />
            <a id="DoQueryByKey" href="#" class="easyui-linkbutton" data-options="plain:true,iconCls:'icon-search'"
                onclick="LoadGridData()">查询</a>
        </div>
        <table id="newsGrid" toolbar="#tb" class="easyui-datagrid">
        </table>
        <div id="divCon" style="width: 100%;">
            <div id="ordingTb" style="padding: 6px; height: 16px;">
                会议室预订详情(选择上面的会议室，显示详细的预定情况。)
            </div>
            <table id="gridOrding" toolbar="#ordingTb" class="easyui-datagrid">
            </table>
        </div>
    </div>
    <div id="dialogWindow" class="easyui-window" minimizable="false" closed="true" modal="true"
        title="窗口" style="overflow: hidden;">
        <iframe scrolling="auto" id='openIframe' frameborder="0" src="" style="width: 100%;
            height: 100%;"></iframe>
    </div>
</asp:Content>
