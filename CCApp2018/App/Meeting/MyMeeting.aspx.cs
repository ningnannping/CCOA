﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BP.Web;

namespace CCOA.App.Meeting
{
    public partial class MyMeeting : System.Web.UI.Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
            string strSql = "SELECT b.Title,CompereEmpNo,d.Name as Dept,DateFrom,DateTo,c.Name as Room,State,a.OID ";
            strSql += "FROM OA_RoomOrding a, OA_SponsorMeeting b, OA_Room c,Port_Dept d ";
            strSql += "WHERE a.OID=b.FK_SponsorOID and b.SponsorDeptNo=d.No and a.Fk_Room =c.No ";
            string part = "and ParticipantsEmpNo like('%," + WebUser.No + ",%')";//我参与的会议。
            string ording = "and a.FK_Emp='" + WebUser.No + "'";//我预定的。
            string sponsor = "and   b.FK_Emp='" + WebUser.No + "'";//我发起的。

            if (!Page.IsPostBack)
            {
                //默认未发起，我预定的。
                strSql = strSql + ording;
                DataTable dt = SetMeetingState(BP.DA.DBAccess.RunSQLReturnTable(strSql));
                Repeater1.DataSource = this.ToDataTable(dt.Select("State='未发起'"));
                Repeater1.DataBind();
            }
            else
            {
                string state1 = ddlState1.Value;
                string state2 = ddlState2.Value;//我的会议 预定，发起，参与
                switch (state2)
                {
                    case "0"://我预定的
                        strSql = strSql + ording;
                        break;
                    case "1":
                        strSql = strSql + sponsor;//我发起的
                        break;
                    case "2":
                        strSql = strSql + part;//我参与的
                        break;
                }
                DataTable dt = SetMeetingState(BP.DA.DBAccess.RunSQLReturnTable(strSql));
                Repeater1.DataSource = this.ToDataTable(dt.Select("State='" + state1 + "'"));
                Repeater1.DataBind();
            }
        }

        public DataTable ToDataTable(DataRow[] rows)
        {
            if (rows == null || rows.Length == 0)
                return null;
            DataTable tmp = rows[0].Table.Clone();
            foreach (DataRow row in rows)
                tmp.Rows.Add(row.ItemArray);
            return tmp;
        }

        public DataTable SetMeetingState(DataTable dt)
        {
            foreach (DataRow dr in dt.Rows)
            {
                dr["State"] = GetMeetingState(dr["DateFrom"], dr["DateTo"], dr["State"]);
            }
            return dt;
        }
        public string GetMeetingState(object dateFrom, object dateTo, object state)
        {
            return MyHelper.GetMeetingState(dateFrom, dateTo, state);
        }
    }
}