﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BP.Port;
using BP.OA.Meeting;
using BP.En;
using BP.OA.UI;
using BP.Web;

namespace CCOA.App.Meeting
{
    public partial class SponsorMeeting : System.Web.UI.Page
    {

        public MeetingTypes mtypes = new MeetingTypes();
        public Depts depts = new Depts();
        public Rooms rooms = new Rooms();
        public RoomOrding Ording = null;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack)
            {
                SaveSponsor();
            }
            else
            {
                GetOrding();
                mtypes.RetrieveAll();
                depts.RetrieveAll();
                rooms.RetrieveAll();
            }
        }
        public void GetOrding()
        {
            //填充数据
            if (Request.QueryString["OID"] != null)
            {
                BP.OA.Meeting.RoomOrdings ordings = new RoomOrdings();
                ordings.Retrieve("OID", Request.QueryString["OID"].ToString());
                Ording = ordings[0] as RoomOrding;
            }
        }

        /// <summary>
        /// 保存发起
        /// </summary>
        private void SaveSponsor()
        {
            BP.OA.Meeting.SponsorMeeting sponsor = PageCommon.GetEntity(this, new BP.OA.Meeting.SponsorMeeting()) as BP.OA.Meeting.SponsorMeeting;
            if (string.IsNullOrEmpty(sponsor.Content))
            {
                Common.Alert("请输入会议内容！", this);
                return;
            }
            //发起人
            sponsor.Fk_Emp = WebUser.No;
            object o = Request.Files["Attachment"];
            //上传附件并返回文件名
            sponsor.Attachment = Common.Upload(o);

            SponsorMeetings sponsors = new SponsorMeetings();
            sponsors.RetrieveByAttr(SponsorMeetingAttr.FK_SponsorOID, sponsor.FK_SponsorOID);
            if (sponsors == null || sponsors.Count == 0) sponsor.Insert();

            //更新预定表的状态字段；
            BP.OA.Meeting.RoomOrding ording = new RoomOrding();
            ording.RetrieveByAttr(RoomOrdingAttr.OID, sponsor.FK_SponsorOID);
            ording.State = "1";
            ording.Update();
            ClientScript.RegisterStartupScript(this.GetType(), "refresh", "alert('发起成功！');RefreshParent()", true);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ens"></param>
        public void GetOptions(EntitiesNoName ens)
        {
            string option = "<option value='{0}'>{1}</option>\r\n";
            foreach (EntityNoName en in ens)
            {
                Response.Write(string.Format(option, en.No, en.Name));
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="resNo"></param>
        /// <returns></returns>
        public string GetResName(object resNo)
        {
            return MyHelper.GetResName(resNo).Trim();
        }
    }
}