﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" CodeBehind="SelectOrding.aspx.cs"
    Inherits="CCOA.App.Meeting.SelectOrding" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
    <link href="../../CSS/table.css" rel="stylesheet" type="text/css" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script src="../../Js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script src="../../Js/zDialog/zDialog.js" type="text/javascript"></script>
    <script src="../../Js/zDialog/zDrag.js" type="text/javascript"></script>
    <script src="../../Js/Trim.js" type="text/javascript"></script>
    <style type="text/css">
        .p_next_all
        {
            background-color: Red;
        }
    </style>
    <script type="text/javascript">
        function openDia(e) {
            var diag = new Dialog();
            diag.Width = 800;
            diag.Height = 500;
            diag.Title = "内容页为外部连接的窗口";
            diag.URL = e;
            diag.show();
        }
        function GetText() {
            var strVal = $("input[name='check']:checked").val();
            var eles = $("[id$=" + strVal + "]");
            var result = strVal + ";";
            for (i = 0; i < eles.length; i++) {
                result += trim(eles[i].innerText) + ";";
            }
            return result;
        }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <h3>
        预定选择</h3>
    <asp:Repeater ID="Repeater1" runat="server">
        <HeaderTemplate>
            <table style="width: 100%" class="table">
                <tr>
                    <th>
                        选择
                    </th>
                    <th>
                        会议名称
                    </th>
                    <th>
                        预定人
                    </th>
                    <th>
                        预定部门
                    </th>
                    <th>
                        会议室名称
                    </th>
                    <th>
                        预定时段
                    </th>
                    <th>
                        资源
                    </th>
                </tr>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td>
                    <input name="check" type="radio" value='<%#DataBinder.Eval(Container.DataItem,"OID")%>' />
                </td>
                <td>
                    <span id='<%#"check0"+Eval("OID")%>'>
                        <%#DataBinder.Eval(Container.DataItem,"Title")%>
                    </span>
                </td>
                <td>
                    <span id='<%#"check1"+Eval("OID")%>'>
                        <%#DataBinder.Eval(Container.DataItem,"Fk_EmpName")%>
                    </span><span id='<%#"check2"+Eval("OID")%>' style="display: none">
                        <%#DataBinder.Eval(Container.DataItem,"Fk_Emp")%>
                    </span>
                </td>
                <td>
                    <span id='<%#"check3"+Eval("OID")%>'>
                        <%#DataBinder.Eval(Container.DataItem, "Fk_DeptName")%>
                    </span><span id='<%#"check4"+Eval("OID")%>' style="display: none">
                        <%#DataBinder.Eval(Container.DataItem, "Fk_Dept")%>
                    </span>
                </td>
                <td>
                    <span id='<%#"check5"+Eval("OID")%>'>
                        <%#DataBinder.Eval(Container.DataItem, "FK_RoomName")%>
                    </span><span id='<%#"check6"+Eval("OID")%>' style="display: none">
                        <%#DataBinder.Eval(Container.DataItem, "FK_Room")%>
                    </span>
                </td>
                <td>
                    <span>【<%#DataBinder.Eval(Container.DataItem,"DateFrom")%>】-- 【<%#DataBinder.Eval(Container.DataItem,"DateTo")%>】</span>
                    <span id='<%#"check7"+Eval("OID")%>' style="display: none">
                        <%#DataBinder.Eval(Container.DataItem,"DateFrom")%>
                    </span><span id='<%#"check8"+Eval("OID")%>' style="display: none">
                        <%#DataBinder.Eval(Container.DataItem,"DateTo")%>
                    </span>
                </td>
                <td>
                    <span id='<%#"check9"+Eval("OID")%>'>
                        <%#GetResource(DataBinder.Eval(Container.DataItem, "MTResource"))%>
                    </span><span id='<%#"check10"+Eval("OID")%>' style="display: none">
                        <%#DataBinder.Eval(Container.DataItem, "MTResource")%>
                    </span>
                </td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
            </table>
        </FooterTemplate>
    </asp:Repeater>
    </form>
</body>
</html>
