﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;

namespace CCOA.App.Meeting
{
    public partial class OrderDetail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string strOp = Request.QueryString["op"];//操作方式 0：删除。
            string id = Request.QueryString["id"];//会议室ID
            string strOID = Request.QueryString["OID"];//会议室预定编号
            string date = Request.QueryString["date"];//预定日期
            //如有OP则删除
            if (!string.IsNullOrEmpty(strOp))
            {
                //根据OID删除预定信息。
                if (strOp == "del" && !string.IsNullOrEmpty(strOID))
                {
                    string strSql = "delete from OA_RoomOrding where OID={0};";
                    BP.DA.DBAccess.RunSQL(string.Format(strSql, strOID));
                    Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", "alert('删除成功！')", true);
                }
            }
            if (!string.IsNullOrEmpty(id) && !string.IsNullOrEmpty(date))
            {
                StringBuilder sbSql = new StringBuilder();
                sbSql.Append("select OID ,DateFrom ,DateTo,Title ,Note ,Fk_Emp,Fk_Dept,MTResource ");
                sbSql.Append("b.Name as OrderName,c.Name as RoomName ");
                sbSql.Append("from OA_RoomOrding a,Port_Emp b,OA_Room c ");
                sbSql.Append("where a.FK_Emp=b.No and a.FK_Room=c.No and  FK_Room='{0}'");
                sbSql.Append(" and ");
                sbSql.Append("DATEDIFF(day,CONVERT(varchar(100), DTFrom, 23) ,'{1}')>=0");
                sbSql.Append(" and ");
                sbSql.Append("DATEDIFF(day,CONVERT(varchar(100), DTTo, 23) ,'{1}')<=0");
                DataTable dt = BP.DA.DBAccess.RunSQLReturnTable(string.Format(sbSql.ToString(), id, date));
                Repeater1.DataSource = dt;
                Repeater1.DataBind();
            }
        }

        public string CheckState(object dtFrom, object oid)
        {
            string result = string.Empty;
            string strHtml = "<img class='imgsta' src='../../Images/icon/{0}' alt='{1}'/>";
            string strA = "<a onclick='javascript:return ShowAlert()' href='OrderDetail.aspx?id={0}&oid={1}&op=del'>{2}</a>";
            string para = Request.QueryString["id"];
            DateTime dt = Convert.ToDateTime(dtFrom);
            if (dt > DateTime.Now)
            {
                string str5 = "<span>（删除）</span>";
                string str1 = string.Format(strHtml, "green.png", "未开始");
                string str2 = string.Format(strHtml, "delete.png", "取消预定");
                string str3 = string.Format(strA, para, oid.ToString(), str2 + str5);
                string str4 = "<span>（尚未开始）</span>";
                return str1 + str4 + str3;
            }
            else
            {
                result = string.Format(strHtml, "red.png", "已开始");
                result += "<span>（已经开始）</span>";
                return result;
            }
        }
    }
}