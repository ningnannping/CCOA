﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using BP.En;
using BP.DA;
using BP.Sys;
using BP.Web;
using BP.OA;
using BP.OA.BBS;

namespace CCOA.App.BBS
{
    public partial class thread : System.Web.UI.Page
    {
        /// <summary>
        /// 板块编号
        /// </summary>
        public string FK_Class
        {
            get
            {
                return StringFormat.GetQuerystring("FK_Class");
            }
        }
        /// <summary>
        /// 主题编号
        /// </summary>
        public string FK_Motif
        {
            get
            {
                return StringFormat.GetQuerystring("FK_Motif");
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Localize htmNav = (Localize)this.Page.Master.FindControl("htm_Nav");
            BBSClass bbsClass = new BBSClass(FK_Class);
            htmNav.Text = "<a href=\"default.aspx\" class=\"ghome\">论坛首页</a> > <a href=\"list.aspx?FK_Class=" + FK_Class + "\" class=\"ghome\">" + bbsClass.Name + "</a> > <a href=\"#\" class=\"ghome\">主题回复</a>";

            if (!IsPostBack)
            {
                int page = 0;
                if (FK_Class == "" || !StringFormat.IsNumber(page.ToString()))
                {
                    Response.Redirect("Default.aspx");
                }
                if (FK_Motif == "" || !StringFormat.IsNumber(page.ToString()))
                {
                    Response.Redirect("Default.aspx");
                }

                if (!StringFormat.IsNumber(StringFormat.GetQuerystring("page")))
                {
                    page = 1;
                }
                else
                {
                    page = int.Parse(StringFormat.GetQuerystring("page"));
                }

                //主题
                BBSMotif motif = new BBSMotif();
                motif.RetrieveByAttr(BBSMotifAttr.No, FK_Motif);
                //修改浏览次数
                motif.Hits = motif.Hits + 1;
                motif.Update();

                htm_fTitle.Text = "<span><font>发布时间：" + motif.RDT.ToString("yyyy-MM-dd HH:mm:ss") + "</font> <em>[楼主]</em></span><strong>" + motif.Name + "</strong>";
                htm_fContent.Text = StringFormat.Invert(motif.Contents);
                htm_fAuthor.Text = GetUserInfo(motif.FK_Emp);
                Author_Manage.Text = GetAuthorPower(FK_Motif, "motif");
                //处理回复
                int pagesize = 30; //分页数
                int allCount = motif.Replays; //回复总数
                int allpage = motif.Replays / pagesize; //全部页数
                if ((allCount % pagesize) > 0)
                {
                    allpage++;
                }

                if (page > allpage)
                {
                    page = allpage;
                }
                PageTop.InnerHtml = "<input name=\"\" type=\"button\" class=\"but_new\" value=\"\" onclick=\"window.location='post.aspx?FK_Class=" + FK_Class + "'\" onmouseover=\"this.className = 'but_new but_new_h'\" onmouseout=\"this.className = 'but_new'\" />";
                PageTop.InnerHtml += "<input name=\"\" type=\"button\" class=\"but_reply\" value=\"\" onclick=\"window.location='Reply.aspx?FK_Motif=" + FK_Motif + "&FK_Class=" + FK_Class + "'\" onmouseover=\"this.className = 'but_reply but_reply_h'\" onmouseout=\"this.className = 'but_reply'\"  />";
                PageTop.InnerHtml += "<span>回复数：<font class=\"forange\">" + allCount + "</font>篇  查看数：<font class=\"forange\">" + motif.Hits + "</font>次</span>";
                PageTop.InnerHtml += "<span class=\"pgs\">";

                if (page > 1)
                {
                    PageTop.InnerHtml += "<a class=\"pages_next\" href=\"thread.aspx?FK_Class=" + FK_Class + "&FK_Motif=" + FK_Motif + "&page=" + (page - 1).ToString() + "\">上一页</a>";
                }
                for (int i = 2; i > 0; i--)
                {
                    if (page - i > 0)
                    {
                        PageTop.InnerHtml += "<a href='thread.aspx?FK_Class=" + FK_Class + "&FK_Motif=" + FK_Motif + "&page=" + (page - i).ToString() + "'>" + (page - i).ToString() + "</a>";
                    }
                }
                PageTop.InnerHtml += "<a class='pcurr' >" + page.ToString() + "</a>";

                for (int i = 1; i < 3; i++)
                {
                    if (page + i <= allpage)
                    {
                        PageTop.InnerHtml += "<a href='thread.aspx?FK_Class=" + FK_Class + "&FK_Motif=" + FK_Motif + "&page=" + (page + i).ToString() + "'>" + (page + i).ToString() + "</a>";
                    }
                }

                if (page < allpage)
                {
                    PageTop.InnerHtml += "<a class=\"pages_next\" href=\"thread.aspx?FK_Class=" + FK_Class + "&FK_Motif=" + FK_Motif + "&page=" + (page + 1).ToString() + "\">下一页</a>";
                }

                PageTop.InnerHtml += "</span>";

                PageDown.InnerHtml = PageTop.InnerHtml;

                BBSReplays replays = new BBSReplays();
                QueryObject objInfo = new QueryObject(replays);
                objInfo.AddWhere(BBSReplayAttr.FK_Motif, FK_Motif);
                objInfo.DoQuery(BBSReplayAttr.No, pagesize, page);

                datalistR.DataSource = replays;
                datalistR.DataBind();
            }
        }
        /// <summary>
        /// 获取操作人
        /// </summary>
        /// <param name="userid"></param>
        /// <returns></returns>
        protected string GetAuthorPower(string mID, string mType)
        {
            if (mType == "motif")
            {
                BBSMotif motif = new BBSMotif(mID);
                //如果为发布人
                if (motif.FK_Emp == WebUser.No)
                {
                    string reVal = "<a href=\"Post.aspx?FK_Motif=" + FK_Motif + "\" title=\"编辑我的主题\"><img alt=''  class=\"aotherpost\" src='images/pencil.png' align='middle' />编辑</a>";
                    reVal += "&nbsp;&nbsp;&nbsp;&nbsp;<a href='#' onclick=\"DelTarget('motif','" + FK_Class + "','" + FK_Motif + "','')\" title=\"删除我的主题\"><img alt='' class=\"aotherpost\" src='images/Delete.png' align='middle' />删除</a>";
                    return reVal;
                }
            }
            else if (mType == "replay")
            {
                BBSReplay bbsReplay = new BBSReplay();
                bbsReplay.Retrieve(BBSReplayAttr.No, mID);
                //如果为发布人
                if (bbsReplay.FK_Emp == WebUser.No)
                {
                    string reVal = "<a href=\"Reply.aspx?FK_Class=" + FK_Class + "&FK_Motif=" + FK_Motif + "&FK_Replay=" + mID + "\" title=\"编辑我的回帖\"><img alt=''  class=\"aotherpost\" src='images/pencil.png' align='middle' />编辑</a>";
                    reVal += "&nbsp;&nbsp;&nbsp;&nbsp;<a href='#' onclick=\"DelTarget('replay','" + FK_Class + "','" + FK_Motif + "','" + mID + "')\" title=\"删除我的回帖\"><img alt='' class=\"aotherpost\" src='images/Delete.png' align='middle' />删除</a>";
                    return reVal;
                }
            }
            return "";
        }

        protected string GetUserInfo(string userid)
        {
            BBSUserInfo userInfo = new BBSUserInfo();
            userInfo.RetrieveByAttr(BBSUserInfoAttr.FK_Emp, userid);

            string reStr = "";
            reStr += "<div class=\"authorinfo\">\n";
            reStr += "  <span class=\"arrow\"></span>\n";
            reStr += "  <div class=\"author_photo\">\n";
            reStr += "    <div class=\"author_photo_in\"><span class=\"author_photo_bg\"></span>\n";
            reStr += "      <img src=\"" + userInfo.UserImg + "\" onerror=\"this.src = 'images/default.gif'\" />\n";
            reStr += "    </div>\n";
            reStr += "  </div>\n";
            reStr += "         <p class=\"uname\"><a href=\"userCenter.aspx?FK_Emp=" + userInfo.FK_Emp + "\">" + userInfo.FK_EmpText + "</a></p>";
            reStr += "  <dl>\n";
            reStr += "    <dt>主题：</dt><dd><strong>" + userInfo.ThemeCount + "</strong></dd>\n";
            reStr += "    <dt>经验：</dt><dd><strong>" + userInfo.Exp + "</strong></dd>\n";
            reStr += "    <dt>积分：</dt><dd><strong>" + userInfo.Integral + "</strong></dd>\n";
            reStr += "    <dt>金币：</dt><dd><strong>" + userInfo.Gold + "</strong></dd>\n";
            reStr += "  </dl>\n";
            reStr += "</div>\n";

            return reStr;
        }

        protected string FormatString(string str)
        {
            return StringFormat.Invert(str);
        }
    }
}