﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using BP.En;
using BP.DA;
using BP.OA;
using BP.OA.BBS;

namespace CCOA.App.BBS
{
    public partial class List : System.Web.UI.Page
    {
        /// <summary>
        /// 板块编号
        /// </summary>
        private string FK_Class
        {
            get
            {
                string cid = StringFormat.GetQuerystring("FK_Class");
                if (string.IsNullOrEmpty(cid)) Response.Redirect("default.aspx");

                return cid;
            }
        }
        /// <summary>
        /// 共几页
        /// </summary>
        private int AllPage
        {
            get;
            set;
        }
        /// <summary>
        /// 第几页
        /// </summary>
        private int CurPage
        {
            get
            {
                int page = 1;
                if (StringFormat.IsNumber(StringFormat.GetQuerystring("p")))
                {
                    page = int.Parse(StringFormat.GetQuerystring("p"));
                }
                if (page < 1)
                {
                    page = 1;
                }
                if (AllPage != 0 && page > AllPage)
                {
                    page = AllPage;
                }
                return page;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                int pagesize = 10;

                BBSMotifs motifs = new BBSMotifs();
                //创建查询对象，并将空实例集合传入查询对象
                QueryObject objInfo = new QueryObject(motifs);
                //添加查询条件
                objInfo.AddWhere(BBSMotifAttr.FK_Class, FK_Class);
                int AllCount = objInfo.GetCount();
                //执行查询符合条件的当前页数据
                objInfo.DoQuery(BBSMotifAttr.No, pagesize, CurPage);

                
                //计算页数
                int allPage = AllCount / pagesize;
                if ((AllCount % pagesize) > 0)
                {
                    allPage++;
                }

                if (AllCount == 0)
                {
                    allPage = 1;
                }

                PageTopShow.InnerHtml = "<input  type=\"button\" class=\"but_new\" value=\"\" onclick=\"window.location='Post.aspx?FK_Class=" + FK_Class + "'\" onmouseover=\"this.className='but_new  but_new_h'\" onmouseout=\"this.className='but_new'\" />\n";
                //分页控制
                PageTopShow.InnerHtml += "<span>总贴数：<font class=\"forange\">" + AllCount.ToString() + "</font>篇</span>\n<span class=\"pgs\">";
                if (CurPage > 1)
                {
                    PageTopShow.InnerHtml += "<a class=\"pages_next\" href=\"List.aspx?FK_Class=" + FK_Class + "&p=" + (CurPage - 1).ToString() + "\">上一页</a>\n";
                }
                //上导航2页
                for (int i = 2; i > 0; i--)
                {
                    if ((CurPage - i) > 0)
                    {
                        PageTopShow.InnerHtml += "<a  href=\"List.aspx?FK_Class=" + FK_Class + "&p=" + (CurPage - i).ToString() + "\" >" + (CurPage - i).ToString() + "</a>\n";
                    }
                }
                //当前页
                PageTopShow.InnerHtml += "<a class='pcurr' >" + CurPage.ToString() + "</a>\n";

                //下导航2页
                for (int i = 1; i < 3; i++)
                {
                    if ((CurPage + i) <= allPage)
                    {
                        PageTopShow.InnerHtml += "<a href=\"List.aspx?FK_Class=" + FK_Class + "&p=" + (CurPage + i).ToString() + "\" >" + (CurPage + i).ToString() + "</a>\n";
                    }
                }
                //下一页
                if ((CurPage + 1) <= allPage)
                {
                    PageTopShow.InnerHtml += "<a class=\"pages_next\" href=\"List.aspx?FK_Class=" + FK_Class + "&p=" + (CurPage + 1).ToString() + "\">下一页</a></span>\n";
                }

                PageTopShow.InnerHtml += "</span>";
                PageTopShow.InnerHtml += "<span>第<input type=\"text\" size=\"3\" class='ipt_pg' />页&nbsp;<a style=\"cursor: pointer\" class='btn_go' onclick=\"javascript:var value = parseInt(this.previousSibling.previousSibling.value); var page=(value>" + allPage + ") ? " + allPage + " : value;  location='list.aspx?FK_Class=" + FK_Class + "&p='+page+''; return false;\">GO</a></span>";

                PageDownShow.InnerHtml = PageTopShow.InnerHtml;


                datalistF.DataSource = motifs;
                datalistF.DataBind();
            }
        }

        protected string StateImg(int hits, int replays)
        {
            if (replays <= 10)
            {
                return "<img src=\"images/i_nor.gif\">";
            }
            else
            {
                if (hits > 200 && replays > 50)
                {
                    return "<img src=\"images/i_hot.gif\">";
                }
                else
                {
                    return "<img src=\"images/i_new.gif\">";
                }
            }
        }
    }
}