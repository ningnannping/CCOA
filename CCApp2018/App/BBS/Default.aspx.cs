﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using BP.En;
using BP.OA.BBS;

namespace CCOA.App.BBS
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                BBSClasss bbsClasss = new BBSClasss();
                bbsClasss.RetrieveAll();

                ListForum.InnerHtml = "";
                string url = "";
                int idx = -1;
                int cols = 2;
                foreach (BBSClass item in bbsClasss)
                {
                    url = "list.aspx?FK_Class=" + item.No;
                    idx++;
                    if (idx == 0)
                        ListForum.InnerHtml += "<ul>\n";
                    
                    ListForum.InnerHtml += "<li>\n";
                    ListForum.InnerHtml += "  <dl>\n";
                    ListForum.InnerHtml += "    <dt><a href=\"" + url + "\"><img alt=\"" + item.Name + "\" src=\"" + item.ImgUrl + "\" onerror='nofindImg();' width='64' height='73' /></a></dt>\n";
                    ListForum.InnerHtml += "    <dd><strong><a href=\"" + url + "\">" + item.Name + "</a></strong>\n";
                    ListForum.InnerHtml += "      <p>" + item.ReMark + "</p>\n";
                    ListForum.InnerHtml += "      <a class=\"btn_enter\" href=\"" + url + "\">进入板块</a>\n";
                    ListForum.InnerHtml += "    </dd>\n";
                    ListForum.InnerHtml += "  </dl>\n";
                    ListForum.InnerHtml += "</li>\n";
                    
                    if (idx == cols - 1)
                    {
                        idx = -1;
                        ListForum.InnerHtml += "</ul>\n";
                    }
                }
                while (idx != -1)
                {
                    idx++;
                    if (idx == cols - 1)
                    {
                        idx = -1;
                        ListForum.InnerHtml += "</ul>\n";
                    }
                }
            }
        }
    }
}