﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using BP.En;
using BP.DA;
using BP.Web;
using BP.Sys;
using BP.OA;
using BP.OA.BBS;

namespace CCOA.App.BBS
{
    public partial class Reply : WebPage
    {
        /// <summary>
        /// 版块编号
        /// </summary>
        public string FK_Class
        {
            get
            {
                return StringFormat.GetQuerystring("FK_Class");
            }
        }
        /// <summary>
        /// 主题编号
        /// </summary>
        public string FK_Motif
        {
            get
            {
                return StringFormat.GetQuerystring("FK_Motif");
            }
        }
        /// <summary>
        /// 回帖编号
        /// </summary>
        public string FK_Replay
        {
            get
            {
                return StringFormat.GetQuerystring("FK_Replay");
            }
        }
        /// <summary>
        /// 对外接口，用于改变Session名称，支持WebConfig配置
        /// </summary>
        private string CheckCode_SessionName
        {
            get
            {
                string code = Convert.ToString(ConfigurationManager.AppSettings["CheckCode_SessionName"]);
                if (String.IsNullOrEmpty(code))
                    return "CheckCode";
                else
                    return code;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                try
                {
                    if (string.IsNullOrEmpty(FK_Replay))
                    {
                        BBSMotif motif = new BBSMotif();
                        motif.RetrieveByAttr(BBSMotifAttr.No, FK_Motif);
                        if (motif.Name == "")
                        {
                            this.Alert("出错了。");
                            return;
                        }
                        txt_title.Text = "Re:" + motif.Name;
                    }
                    else
                    {
                        BBSReplay replay = new BBSReplay();
                        replay.RetrieveByAttr(BBSReplayAttr.No, FK_Replay);

                        txt_title.Text = replay.Name;
                        editorContent.Value= replay.Contents;
                    }
                    
                }
                catch
                {
                    this.Alert("出错了。");
                }
            }
        }

        protected void btn_Save_Click(object sender, EventArgs e)
        {
            //string code = this.txt_vali.Text;
            //验证用户变量
            //if (HttpContext.Current.Session[this.CheckCode_SessionName] != null
            //    && Convert.ToString(HttpContext.Current.Session[this.CheckCode_SessionName]) != code)
            //{
            //    this.Alert("验证码不对");
            //    return;
            //}

            string title = txt_title.Text;
            string content = editorContent.Value;
            if (title.Length < 1 || title.Length > 200)
            {
                this.Alert("标题不能为空，且不能超过100字！");
                return;
            }
            if (content.Length < 1)
            {
                this.Alert("内容不能为空！");
                return;
            }

            int i = 0;
            if (string.IsNullOrEmpty(FK_Replay))
            {
                BBSReplay replay = new BBSReplay();
                replay.FK_Motif = FK_Motif;
                replay.Name = title;
                replay.Contents = content;
                replay.FK_Emp = WebUser.No;
                replay.AddTime = DateTime.Now;
                replay.LastEditTime = DateTime.Now;

                i = replay.Insert();
                replay.Contents = content;
                replay.Update();
            }
            else
            {
                BBSReplay replay = new BBSReplay(FK_Replay);
                replay.Name = title;
                replay.Contents = content;
                replay.LastEditTime = DateTime.Now;
                i = replay.Update();
            }
            if (i > 0)
                Response.Redirect("thread.aspx?FK_Class=" + FK_Class + "&FK_Motif=" + FK_Motif);

        }

        protected void btn_Back_Click(object sender, EventArgs e)
        {
            Response.Redirect("thread.aspx?FK_Class=" + FK_Class + "&FK_Motif=" + FK_Motif);
        }
    }
}