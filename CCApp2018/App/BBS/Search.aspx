﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App/BBS/BBSPage.Master" AutoEventWireup="true"
    CodeBehind="Search.aspx.cs" Inherits="CCOA.App.BBS.Search" %>

<%@ Register Assembly="AspNetPager" Namespace="Wuqi.Webdiyer" TagPrefix="webdiyer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <ul class="sorttag clearfix" style="margin-top: 8px;">
        <li class="ntop"></li>
        <li class="ncurr"><a href="">搜索内容</a></li>
        <li class="nbott"></li>
    </ul>
    <div class="threadtype">
        <asp:HiddenField ID="hidden_keywords" runat="server" />
        <table align="center" border="0" cellspacing="0" cellpadding="0">
            <tbody>
                <tr>
                    <th width="55%">
                        标题
                    </th>
                    <th width="15%">
                        版块
                    </th>
                    <th width="15%">
                        查看/回复
                    </th>
                    <th width="15%">
                        最后回复
                    </th>
                </tr>
            </tbody>
        </table>
    </div>
    <!-- 主题分类end -->
    <div class="threadlist">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tbody>
                <asp:DataList ID="datalistF" RepeatLayout="Flow" runat="server" Width="100%" ShowFooter="False"
                    ShowHeader="False" RepeatDirection="Horizontal">
                    <ItemTemplate>
                        <tr class="hover">
                            <td width="55%">
                                <a href="thread.aspx?FK_Class=<%#Eval("FK_Class") %>&FK_Motif=<%#Eval("No") %>">
                                    <%#Eval("Name")%></a>
                            </td>
                            <td width="15%">
                                <%#GetClassName(Eval("FK_Class"))%>
                            </td>
                            <td width="15%">
                                <%#Eval("Hits")%>/<%#Eval("Replays")%>
                            </td>
                            <td width="15%">
                                <a href="userCenter.aspx?uid=<%#Eval("LastReEmp") %>">
                                    <%#Eval("LastReEmpText")%></a>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:DataList>
                <tr>
                    <td colspan="5">
                        <div class="fixsel" id="downHtml" runat="server" align="center">
                            <webdiyer:AspNetPager ID="AspNetPager1" CssClass="paginator" CurrentPageButtonClass="cpb"
                                runat="server" AlwaysShow="false" FirstPageText="首页" LastPageText="尾页" NextPageText="下一页"
                                PageSize="10" PrevPageText="上一页" ShowCustomInfoSection="Left" ShowInputBox="Never"
                                CustomInfoTextAlign="Left" LayoutType="Table" OnPageChanged="AspNetPager1_PageChanged">
                            </webdiyer:AspNetPager>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</asp:Content>
