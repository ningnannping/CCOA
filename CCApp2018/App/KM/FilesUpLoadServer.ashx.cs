﻿using System;
using System.Collections.Generic;
using System.Web;
using System.IO;

using BP.KM;

namespace CCOA.App.KM
{
    /// <summary>
    /// FilesUpLoad 的摘要说明
    /// </summary>
    public class FilesUpLoadServer : IHttpHandler
    {
        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/plain";
            context.Response.Charset = "utf-8";

            string uploadType = context.Request["UploadType"];
            switch (uploadType)
            {
                case "upload"://上传
                    UpLoadFile(context);
                    break;
                case "download"://下载
                    DownLoadFile(context);
                    break;
            }

        }

        //上传文件
        private void UpLoadFile(HttpContext context)
        {
            //获取上传文件队列  
            HttpPostedFile oFile = context.Request.Files["Filedata"];
            if (oFile != null)
            {
                string folderID = context.Request["FolderID"];
                if (!string.IsNullOrEmpty(folderID))
                {
                    string topDir = context.Request["folder"];  // 获取uploadify的folder配置，在此示例中，客户端配置了上传到 Files/ 文件夹
                    string fileExt = Path.GetExtension(oFile.FileName).Substring(1).ToLower();
                    // 检测并创建目录:当月上传的文件放到以当月命名的文件夹中，例如2011年11月的文件放到网站根目录下的 /Files/201111 里面
                    string dateFolder = HttpContext.Current.Server.MapPath(topDir) + "\\" + DateTime.Now.Date.ToString("yyyyMM");
                    string uploadPath = topDir + "/" + DateTime.Now.Date.ToString("yyyyMM");
                    if (!Directory.Exists(dateFolder))  // 检测是否存在磁盘目录
                    {
                        Directory.CreateDirectory(dateFolder);  // 不存在的情况下，创建这个文件目录 例如 C:/wwwroot/Files/201111/
                    }

                    // 使用Guid命名文件，确保每次文件名不会重复
                    string guidFileName = Guid.NewGuid() +"."+ fileExt;
                    uploadPath = uploadPath + "/" + guidFileName;
                    // 保存文件，注意这个可是完整路径，例如C:/wwwroot/Files/201111/92b2ce5b-88af-405e-8262-d04b552f48cf.jpg
                    oFile.SaveAs(dateFolder + "\\" + guidFileName);

                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    //////// TODO 在此，您可以添加自己的业务逻辑，比如保存这个文件信息到数据库
                    //
                    BP.KM.Tree sort_Tree = new BP.KM.Tree(folderID);
                    sort_Tree.No = folderID;
                    sort_Tree.RetrieveFromDBSources();
                    //文件信息
                    BP.KM.FileInfo addFile = new BP.KM.FileInfo();
                    addFile.Title = oFile.FileName;
                    addFile.KeyWords = oFile.FileName;
                    addFile.RefTreeNo = folderID;
                    addFile.UploadPath = uploadPath;
                    addFile.FK_Emp = BP.Web.WebUser.No;
                    addFile.FK_Dept = BP.Web.WebUser.FK_Dept;
                    addFile.EDTER = BP.Web.WebUser.No;
                    addFile.FileExt = fileExt;
                    addFile.FileSize = oFile.ContentLength > 0 ? oFile.ContentLength / 1024 : 0;
                    addFile.IsShare = sort_Tree.IsShare;
                    addFile.FileStatus = sort_Tree.FileStatus;
                    addFile.KMTreeCtrlWay = sort_Tree.KMTreeCtrlWay;
                    addFile.Insert();
                    ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
                    //如果为指定人共享
                    if (sort_Tree.FileStatus == BP.KM.FileStatus.somePersons && sort_Tree.IsShare == "1")
                    {
                        //继承父项共享
                        TreeEmps treeEmps = new TreeEmps();
                        treeEmps.RetrieveByAttr(TreeEmpAttr.RefTreeNo, sort_Tree.No);
                        foreach (TreeEmp item in treeEmps)
                        {
                            TreeEmp cTreeEmp = new TreeEmp();
                            cTreeEmp.RefTreeNo = addFile.MyPK;
                            cTreeEmp.FK_Emp = item.FK_Emp;
                            cTreeEmp.Insert();
                        }
                    }
                    // 上面的所有操作顺利完成，你就完成了一个文件的上传（和保存信息到数据库），返回成功，在此我返回1，表示上传了一个文件
                    context.Response.Write(folderID);
                    context.Response.End();
                }
            }
            context.Response.Write("0");
        }

        /// <summary>
        /// 下载文件
        /// </summary>
        /// <param name="context"></param>
        private void DownLoadFile(HttpContext context)
        {
            string MyPK = context.Request["MyPK"];
                        
            BP.KM.FileInfo fileInfo = new BP.KM.FileInfo();
            fileInfo.RetrieveByAttr(BP.KM.FileInfoAttr.MyPK, MyPK);

            if (fileInfo != null && fileInfo.IsDownload != "1")
            {
                string fileName = fileInfo.Title;//客户端保存的文件名
                string filePath = context.Server.MapPath(fileInfo.UploadPath);//路径

                if (File.Exists(filePath))
                {
                    //修改下载次数
                    fileInfo.DownLoadTimes = fileInfo.DownLoadTimes + 1;
                    fileInfo.Update();

                    //以字符流的形式下载文件
                    FileStream fs = new FileStream(filePath, FileMode.Open);
                    byte[] bytes = new byte[(int)fs.Length];
                    fs.Read(bytes, 0, bytes.Length);
                    fs.Close();

                    context.Response.ContentType = "application/octet-stream";
                    //通知浏览器下载文件而不是打开
                    context.Response.AddHeader("Content-Disposition", "attachment; filename=" + HttpUtility.UrlEncode(fileName, System.Text.Encoding.UTF8));
                    context.Response.BinaryWrite(bytes);
                    context.Response.Flush();
                    context.Response.End();
                }
                context.Response.Write("文件不存在。");
            }
            else
            {
                context.Response.Write("不允许下载。");
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}