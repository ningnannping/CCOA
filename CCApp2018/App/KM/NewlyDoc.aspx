﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="NewlyDoc.aspx.cs" Inherits="CCOA.App.KM.NewlyDoc" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href="../../Js/Js_EasyUI/themes/default/easyui.css" rel="stylesheet" type="text/css" />
    <link href="../../Js/Js_EasyUI/themes/default/tree.css" rel="stylesheet" type="text/css" />
    <link href="../../Js/Js_EasyUI/themes/default/datagrid.css" rel="stylesheet" type="text/css" />
    <link href="../../Js/Js_EasyUI/themes/icon.css" rel="stylesheet" type="text/css" />
    <script src="../../Js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script src="../../Js/Js_EasyUI/jquery.easyui.min.js" type="text/javascript"></script>
    <script src="../../Js/Js_EasyUI/locale/easyui-lang-zh_CN.js" type="text/javascript" charset="UTF-8"></script>
    <script src="js/AppData.js" type="text/javascript"></script>
    <script src="js/NewlyDoc.js" type="text/javascript"></script>
</head>
<body class="easyui-layout">
    <div data-options="region:'center'" style="overflow: hidden;">
        <div class="panel-header" style="height: 20px;">
            <div style="float: left; vertical-align: middle; margin-top: 6px;">
                <img alt="" align="middle" height="16" width="16" src="../../Images/Home.gif" />当前路径：
                <span id="folderPath"><a href='#'>最新文档</a></span>
            </div>
            <div style="float: right; vertical-align: middle;">
                <input type="text" id="TB_Search" />&nbsp;&nbsp;<a href="#" class="easyui-linkbutton"
                    data-options="iconCls:'icon-search'" onclick="MyDocSearch()">搜索</a></div>
        </div>
        <div style="overflow: auto;width:100%; height: 90%;">
            <table id="docGrid" class="easyui-datagrid">
            </table>
        </div>
    </div>
    <div id="newDocDialog">
        <table>
            <tr>
                <td>
                    文件名：
                </td>
                <td>
                    <input type="text" id="TB_FileName" style="width: 500px;" />
                </td>
            </tr>
            <tr>
                <td>
                    关键词：
                </td>
                <td>
                    <input type="text" id="TB_KeyWord" style="width: 500px;" />
                </td>
            </tr>
            <tr>
                <td>文件大小：</td>
                <td><input type="text" id="TB_FileSize" style="width: 500px;" /></td>
            </tr>
            <tr>
                <td>
                    简介：
                </td>
                <td>
                    <textarea cols="100" rows="15" id="TB_Doc" style="width: 500px"></textarea>
                </td>
            </tr>
        </table>
    </div>
</body>
</html>
