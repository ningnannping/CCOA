﻿KM = {}
OA = {}

OADataFactory = function () {
    this.data = new KM.Data();
}

jQuery(function ($) {
    OA = new OADataFactory();
});
//数据访问
KM.Data = function () {
    this.url = "Serv/KMDataService.ashx";
    //获取知识树
    this.getKMSorts = function (ParentNo, isLoadChild, callback, scope) {
        var tUrl = this.url;
        var params = {
            method: "getkmsorts",
            ParentNo: ParentNo,
            isLoadChild: isLoadChild
        };
        queryData(tUrl, params, callback, scope);
    }
    //知识节点树操作
    this.treeSortManage = function (dowhat, sortNo, callback, scope) {
        var tUrl = this.url;
        var params = {
            method: "treesortmanage",
            dowhat: dowhat,
            sortNo: sortNo
        };
        queryData(tUrl, params, callback, scope);
    }
    //修改知识
    this.updateSortName = function (sortNo, sortName, callback, scope) {
        var tUrl = this.url;
        var params = {
            method: "updatesortname",
            sortNo: sortNo,
            sortName: encodeURI(sortName)
        };
        queryData(tUrl, params, callback, scope);
    }
    //获取我的文档
    this.getMyDoc = function (TreeNo, content, callback, scope) {
        var tUrl = this.url;
        var params = {
            method: "getmydoc",
            TreeNo: TreeNo,
            content: content
        };
        queryData(tUrl, params, callback, scope);
    }
    //上传文件
    this.addMyDoc = function (FileName, KeyWord, Doc, RefTreeNo, callback, scope) {
        var tUrl = this.url;
        var params = {
            method: "addmydoc",
            FileName: encodeURI(FileName),
            KeyWord: encodeURI(KeyWord),
            Doc: encodeURI(Doc),
            RefTreeNo: RefTreeNo
        };
        queryData(tUrl, params, callback, scope);
    }
    //修改文件属性
    this.editMyDoc = function (MyPK, KeyWord, Doc, callback, scope) {
        var tUrl = this.url;
        var params = {
            method: "editmydoc",
            MyPK: MyPK,
            KeyWord: encodeURI(KeyWord),
            Doc: encodeURI(Doc)
        };
        queryData(tUrl, params, callback, scope);
    }
    //添加目录
    this.addFolder = function (TreeNo, FolderName, callback, scope) {
        var tUrl = this.url;
        var params = {
            method: "addfolder",
            TreeNo: TreeNo,
            FolderName: encodeURI(FolderName)
        };
        queryData(tUrl, params, callback, scope);
    }
    //分配权限，获取模版数据
    this.getTemplateData = function (TreeNo, model, callback, scope) {
        var tUrl = this.url;
        var params = {
            method: "gettemplatedata",
            TreeNo: TreeNo,
            model: model
        };
        queryData(tUrl, params, callback, scope);
    }
    //保存权限
    this.saveSortRights = function (TreeNo, ckNos, model, saveChildNode, callback, scope) {
        var tUrl = this.url;
        var params = {
            method: "savesortrights",
            TreeNo: TreeNo,
            ckNos: ckNos,
            model: model,
            saveChildNode: saveChildNode
        };
        queryPostData(tUrl, params, callback, scope);
    }
    //获取最新文档
    this.getNewLyDoc = function (content, callback, scope) {
        var tUrl = this.url;
        var params = {
            method: "getnewlydoc",
            content: content
        };
        queryData(tUrl, params, callback, scope);
    }
    //删除文件或文件夹
    this.delDoc = function (MyPK, IsFolder, callback, scope) {
        var tUrl = this.url;
        var params = {
            method: "deldoc",
            MyPK: MyPK,
            IsFolder: IsFolder
        };
        queryData(tUrl, params, callback, scope);
    }
    //修改阅读次数
    this.ReadTimeEdit = function (MyPK, callback, scope) {
        var tUrl = this.url;
        var params = {
            method: "readtimeedit",
            MyPK: MyPK
        };
        queryData(tUrl, params, callback, scope);
    }
    //获取共享信息
    this.getShareInfo = function (MyPK, callback, scope) {
        var tUrl = this.url;
        var params = {
            method: "getshareinfo",
            MyPK: MyPK
        };
        queryData(tUrl, params, callback, scope);
    }
    //共享文件
    this.shareFileManage = function (ShareType, IsShare, IsAllEmps, SharePersons, MyPK, callback, scope) {
        var tUrl = this.url;
        var params = {
            method: "sharefilemanage",
            ShareType: ShareType,
            IsShare: IsShare,
            IsAllEmps: IsAllEmps,
            SharePersons: SharePersons,
            MyPK: MyPK
        };
        queryData(tUrl, params, callback, scope);
    }
    //同事共享文档
    this.getEmpShareFiles = function (content, callback, scope) {
        var tUrl = this.url;
        var params = {
            method: "getempsharefiles",
            content: content
        };
        queryData(tUrl, params, callback, scope);
    }
    //获取单位知识管理数据
    this.getCompanyDoc = function (TreeNo, content, callback, scope) {
        var tUrl = this.url;
        var params = {
            method: "getcompanydoc",
            TreeNo: TreeNo,
            content: content
        };
        queryData(tUrl, params, callback, scope);
    }
    //获取有权限查看的单位知识数据
    this.getCompanyDocForPerson = function (TreeNo, content, callback, scope) {
        var tUrl = this.url;
        var params = {
            method: "getcompanydocforperson",
            TreeNo: TreeNo,
            content: content
        };
        queryData(tUrl, params, callback, scope);
    }
    //公共方法
    function queryData(url, param, callback, scope, method, showErrMsg) {
        if (!method) method = 'GET';
        $.ajax({
            type: method, //使用GET或POST方法访问后台
            dataType: "text", //返回json格式的数据
            contentType: "application/json; charset=utf-8",
            url: url, //要访问的后台地址
            data: param, //要发送的数据
            async: true,
            cache: false,
            complete: function () { }, //AJAX请求完成时隐藏loading提示
            error: function (XMLHttpRequest, errorThrown) {
                callback(XMLHttpRequest);
            },
            success: function (msg) {//msg为返回的数据，在这里做数据绑定
                var data = msg;
                callback(data, scope);
            }
        });
    }

    //公共方法
    function queryPostData(url, param, callback, scope) {
        $.post(url, param, callback);
    }
}