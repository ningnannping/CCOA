﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using BP.Web;

namespace CCOA.App.Attendance
{
    public partial class WorkRegister : System.Web.UI.Page
    {
        private DataTable _DTDetails = null;
        /// <summary>
        /// 登记详细信息
        /// </summary>
        public DataTable DTDetails
        {
            get
            {
                if (_DTDetails == null)
                {
                    _DTDetails = BP.OA.Attendance.WorkRegisterDetail.GetWorkRegisterDetails(WebUser.No);
                }
                return _DTDetails;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(Request.QueryString["OID"]))
            {
                //保存登记信息
            }
            repeater1.DataSource = BP.OA.Attendance.WorkRegister.GetWorkRegister();
            repeater1.DataBind();
        }
        /// <summary>
        /// 根据ＯＩＤ检测是否已经打卡
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public string CheckState(object obj)
        {
            DataRow[] dr = DTDetails.Select("OID=" + Convert.ToString(obj));
            if (dr != null && dr.Count() > 0)
            {
                return dr[0]["RegDateTime"].ToString();
            }
            return "未登记";
        }
        /// <summary>
        /// 判断当前时间是否可以上下班登记
        /// </summary>
        /// <param name="canRegisterTime"></param>
        /// <param name="ahead"></param>
        /// <param name="behind"></param>
        /// <param name="registerType"></param>
        /// <returns></returns>
        public string CheckState(object canRegisterTime, object ahead, object behind, object registerType, object OID)
        {
            DateTime dt = Convert.ToDateTime(canRegisterTime);
            bool b1 = DateTime.Now >= dt.AddMinutes(0 - Convert.ToInt32(ahead));
            bool b2 = DateTime.Now <= dt.AddMinutes(Convert.ToInt32(behind));
            if (b1 && b2)
            {
               // return "<a href='?OID=" + OID.ToString() + "'>" + Convert.ToString(registerType) + "</a>";
            }
            return "不在登记时间段 ";
        }
    }
}