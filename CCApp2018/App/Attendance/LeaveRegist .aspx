﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="LeaveRegist .aspx.cs" Inherits="CCOA.App.Attendance.LeaveRegist" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="../../Js/jquery-1.7.2.min.js" type="text/javascript"></script>
    <script src="../../Js/My97DatePicker/WdatePicker.js" type="text/javascript"></script>
    <script src="../../Js/My97DatePicker/lang/en.js" type="text/javascript"></script>
    <script src="../../Js/Validate/formValidator-4.0.1.min.js" type="text/javascript"></script>
    <script src="../../Js/Validate/formValidatorRegex.js" type="text/javascript"></script>
    <script src="../../Js/Validate/site.js" type="text/javascript"></script>
    <link href="../../Js/Validate/style/validator.css" rel="stylesheet" type="text/css" />
    <script src="../../Js/Validate/DateTimeMask.js" type="text/javascript"></script>
    <link href="../../CSS/table.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .td2
        {
            text-align: left;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server" action="LeaveRegist .aspx" method="post" target="iframe1">
    <table class="TableBlock">
        <tr>
            <td>
                请假人：
            </td>
            <td class="td2">
                <input type="text" readonly="readonly" name="FK_Emp" />
            </td>
        </tr>
        <tr>
            <td>
                请假原因：
            </td>
            <td class="td2">
                <textarea name="LEAVE_TYPE" cols="60" rows="3" name="Note"></textarea>
            </td>
        </tr>
        <tr>
            <td>
                请假时间：
            </td>
            <td class="td2">
                从<input type="text" value="" name="DateTimeFrom" />
                到
                <input type="text" value="" name="DateTimeTo" />
            </td>
        </tr>
        <tr>
            <td>
                请假类型：
            </td>
            <td class="td2">
                <select name="LeaveType">
                    <option value="1">事假</option>
                    <option value="2">病假</option>
                    <option value="3">年假</option>
                    <option value="9">其他</option>
                </select>
            </td>
        </tr>
        <tr>
            <td>
                审批人：
            </td>
            <td class="td2">
                <select name="FK_EmpCheck">
                    <option value="admin" selected="selected">系统管理员</option>
                </select>
            </td>
        </tr>
        <tr align="center" class="TableControl">
            <td colspan="2" nowrap>
                <input type="submit" value="请假" title="请假登记" />&nbsp;&nbsp;
                <input type="button" value="返回" />&nbsp;&nbsp;
            </td>
        </tr>
    </table>
    </form>
    <iframe id="iframe1" name="iframe1" style="display: none" />
</body>
</html>
