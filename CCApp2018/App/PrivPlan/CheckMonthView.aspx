﻿<%@ Page Title="" Language="C#" MasterPageFile="../../Main/master/Site1.Master" AutoEventWireup="true"
    CodeBehind="CheckMonthView.aspx.cs" Inherits="CCOA.App.PrivPlan.CheckMonthView" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cph_head" runat="server">
    <style type="text/css">
        body
        {
            font-size: 13px;
        }
        div#nav a
        {
            border: solid 1px #777;
            padding: 1px 1px 1px 1px;
            margin: 3px 3px 3px 3px;
            background-color: #eee;
            font-weight: lighter;
            color: #555;
        }
        #cc, #dd
        {
            width: 250;
            border-style: solid;
            border-color: #aaaaaa;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_title" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_body" runat="server">
    <div style="float: left; margin-right: 100px; text-align: justify;">
        部门：
        <asp:DropDownList ID="ddlDept" runat="server" AutoPostBack="True" 
            onselectedindexchanged="ddlDept_SelectedIndexChanged">
        </asp:DropDownList>
        用户名： <asp:DropDownList ID="ddlEmp" runat="server" AutoPostBack="True">
        </asp:DropDownList>
        <asp:Button ID="btnCheck" runat="server" Text="查询" onclick="btnCheck_Click" />
    </div>
    <div id="nav" style="padding: 5px 5px 5px 5px; border-bottom: solid 1px #888;">
        <asp:LinkButton runat="server" ID="lbReturn" Text="返回" OnClick="lbReturn_Click"></asp:LinkButton>
        <asp:LinkButton runat="server" ID="lbPreviousMonth" Text="前一月" OnClick="lbPreviousMonth_Click"></asp:LinkButton>
        <asp:Label runat="server" ID="lblDate" Font-Size="18px" Font-Bold="true" ForeColor="#B8860B"></asp:Label>
        <asp:LinkButton runat="server" ID="lbThisMonth" Text="本月" OnClick="lbThisMonth_Click"></asp:LinkButton>
        <asp:LinkButton runat="server" ID="lbNextMonth" Text="后一月" OnClick="lbNextMonth_Click"></asp:LinkButton>
    </div>
    <asp:Repeater runat="server" ID="rptMonth">
        <ItemTemplate>
            <div style="padding: 5px 5px 5px 5px; margin: 5px 5px 5px 5px; border-bottom: dashed 1px #aaa;">
                <div style="width: 50px; height: 50px; float: left; padding-top: 20px;">
                    <span style="font-size: 15px; color: #B8860B; text-decoration: <%# Convert.ToDateTime(Eval("Day")).DayOfWeek== DayOfWeek.Sunday||Convert.ToDateTime(Eval("Day")).DayOfWeek== DayOfWeek.Saturday?"line-through":"none" %>;
                        font-weight: bold;">
                        <%# String.Format("{0:dd}日<br/>{0:ddd}",Convert.ToDateTime(Eval("Day"))) %>
                    </span>
                </div>
                <div style="width: 300px; float: left;">
                    <fieldset style="padding: 10px 10px 10px 10px; min-height: 50px;">
                        <legend>今日计划</legend>
                        <%# Eval("Tasks") %>
                    </fieldset>
                </div>
                <div style="width: 300px; float: left;">
                    <fieldset style="padding: 10px 10px 10px 10px; min-height: 50px;">
                        <legend>目标达成</legend>
                        <%# Eval("Reasons") %>
                    </fieldset>
                </div>
                <div style="width: 300px; float: left;">
                    <fieldset style="padding: 10px 10px 10px 10px; min-height: 50px;">
                        <legend>方式改进</legend>
                        <%# Eval("Improves") %>
                    </fieldset>
                </div>
                <div style="clear: both;">
                </div>
            </div>
        </ItemTemplate>
    </asp:Repeater>
</asp:Content>
