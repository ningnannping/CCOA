﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using BP.OA.PrivPlan;
namespace CCOA.App.PrivPlan
{
    public partial class MonthView : System.Web.UI.Page
    {
        #region //1.公共接口(Public Interface)
        //权限控制
        private string FuncNo = null;
        private DateTime rDate
        {
            get
            {
                string sDate = Convert.ToString(Request.QueryString["Date"]);
                if (String.IsNullOrEmpty(sDate))
                    return DateTime.Now;
                else
                    return Convert.ToDateTime(sDate);
            }
        }
        private string CurUserNo
        {
            get { return BP.Web.WebUser.No; }
        }
        #endregion

        #region //2.页面装载(Page Load)
        protected void Page_Init(object sender, EventArgs e)
        {
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.IsPostBack)
            {
                this.lblDate.Text = String.Format("&nbsp;&nbsp;{0:yyyy年MM月}", this.rDate);
                this.lbThisMonth.ForeColor = String.Format("{0:yyyy-MM}", this.rDate) == String.Format("{0:yyyy-MM}", DateTime.Now) ? System.Drawing.Color.Red : System.Drawing.Color.FromArgb(5, 5, 5);
                this.LoadData();
            }
        }
        private void LoadData()
        {
            DataTable dtPrivPlan = BP.DA.DBAccess.RunSQLReturnTable(String.Format("select OID,PlanDate,Checked from OA_PrivPlan where PlanDate like '{0:yyyy-MM}%' and FK_UserNo='{1}'", this.rDate, BP.Web.WebUser.No));
            DataTable dtPrivPlanItem = BP.DA.DBAccess.RunSQLReturnTable(String.Format("select Succeed,Task,Reason,Improve,FK_PrivPlan,Sort from OA_PrivPlanItem where FK_PrivPlan in (select OID from OA_PrivPlan where PlanDate like '{0:yyyy-MM}%' and FK_UserNo='{1}') order by Sort", this.rDate, BP.Web.WebUser.No));
            dtPrivPlanItem.Columns.Add("SucceedTitle");
            foreach (DataRow dr in dtPrivPlanItem.Rows)
            {
                dr["SucceedTitle"] = Convert.ToInt32(dr["Succeed"]) == 1 ? "成功" : "<span style='color:red;'>失败</span>";
            }
            DataTable dt = new DataTable();
            dt.Columns.Add("Day");
            dt.Columns.Add("Tasks");
            dt.Columns.Add("Reasons");
            dt.Columns.Add("Improves");
            DateTime dStart = DateTime.Parse(String.Format("{0:yyyy-MM-01}", this.rDate));
            DateTime dEnd = dStart.AddMonths(1).AddDays(-1);
            for (DateTime d = dStart; (d - dEnd).Days <= 0; d=d.AddDays(1))
            {
                DataRow dr = dt.Rows.Add();
                dr["Day"]=d;
                
                //string s_d = String.Format("{0:yyyy-MM-dd}", d); 
                //get drPP
                DataRow[] drsPP = dtPrivPlan.Select(String.Format("PlanDate='{0:yyyy-MM-dd}'", d));
                if (drsPP.Length == 0) continue;
                DataRow drPP=drsPP[0];
                //get drsPPI
                DataRow[] drsPPI = dtPrivPlanItem.Select(String.Format("FK_PrivPlan='{0}'",drPP["OID"]));
                //get Tasks
                dr["Tasks"] = BP.OA.Main.GetValueListFromDataRowArray(drsPPI,"（{2}）{0}.{1}","<br/>","Sort,Task,SucceedTitle");
                dr["Reasons"] = BP.OA.Main.GetValueListFromDataRowArray(drsPPI, "（{2}）{0}.{1}", "<br/>", "Sort,Reason,SucceedTitle");
                dr["Improves"] = BP.OA.Main.GetValueListFromDataRowArray(drsPPI, "（{2}）{0}.{1}", "<br/>", "Sort,Improve,SucceedTitle");
            }
            this.rptMonth.DataSource = dt;
            this.rptMonth.DataBind();
        }

        /*
        private string GetSomeFieldsByDay(DataTable dt,String day,String fieldName)
        {
            DataRow[] drs = dt.Select("");
        }*/
        #endregion

        #region //3.页面事件(Page Event)
        protected void lbReturn_Click(object sender, EventArgs e)
        {
            this.Response.Redirect(String.Format("MyPlan.aspx?Date={0:yyyy-MM-dd}", this.rDate));
        }  
        #endregion  

        protected void lbPreviousMonth_Click(object sender, EventArgs e)
        {
            this.Response.Redirect(String.Format("MonthView.aspx?Date={0:yyyy-MM-dd}", this.rDate.AddMonths(-1)), true);
        }

        protected void lbNextMonth_Click(object sender, EventArgs e)
        {
            this.Response.Redirect(String.Format("MonthView.aspx?Date={0:yyyy-MM-dd}", this.rDate.AddMonths(1)), true);
        }

        protected void lbThisMonth_Click(object sender, EventArgs e)
        {
            this.Response.Redirect(String.Format("MonthView.aspx?Date={0:yyyy-MM-dd}", DateTime.Now), true);
        }
     }
}