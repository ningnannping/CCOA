﻿<%@ Page Title="" Language="C#" ClientIDMode="Static" MasterPageFile="~/Main/master/Site1.Master" AutoEventWireup="true" CodeBehind="MyPlan.aspx.cs" Inherits="CCOA.App.PrivPlan.MyPlan" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cph_head" runat="server">
<style type="text/css">
       body{font-size:13px;}
       div#nav a{ border:solid 1px #777; padding:1px 1px 1px 1px; margin:3px 3px 3px 3px; background-color:#eee;  font-weight:lighter; color:#555;}
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cph_title" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="cph_body" runat="server">
    <div id="nav" style="padding: 5px 5px 5px 5px; border-bottom: solid 1px #888;">
        <asp:LinkButton runat="server" ID="lbPreviousDay" Text="前一天" OnClick="lbPreviousDay_Click"></asp:LinkButton>
        <asp:Label runat="server" ID="lblDate" Font-Size="18px" Font-Bold="true" ForeColor="#B8860B"></asp:Label>
        <asp:LinkButton runat="server" ID="lbToday" Text="今天" OnClick="lbToday_Click"></asp:LinkButton>
        <asp:LinkButton runat="server" ID="lbNextDay" Text="后一天" OnClick="lbNextDay_Click"></asp:LinkButton>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:HyperLink runat="server" ID="hlEdit" Text="编制计划"></asp:HyperLink>
        <asp:HyperLink runat="server" ID="hlSum" Text="今日总结"></asp:HyperLink>
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:HyperLink runat="server" ID="hlWeek" Text="周视图" Visible="false"></asp:HyperLink>
        <asp:HyperLink runat="server" ID="hlMonth" Text="月视图"></asp:HyperLink>
    </div>
    <p runat="server" id="pNoTodayPlan">
        <asp:Label runat="server" ID="lblMsg" Text="你今天还没有制定计划！"></asp:Label>&nbsp;<asp:HyperLink runat="server" ID="hlEdit1" Text="编制今日计划"></asp:HyperLink></p>
    <div runat="server" id="divCommon">
        <div style="margin: 10px 10px 10px 10px; padding: 10px 10px 10px 10px; border: solid 1px #888;
            background-color: #FFF68F; font-size: 13px;">
            1、计划必须在<%=this.CreatePlanTimeLine %>以前进行制定或修改，必须在<%=this.SumPlanTimeLine %>以后进行总结。
            <br />
            2、过去的计划不允许再进行修改！
            </div>
        <div style="padding: 5px 5px 5px 5px;">
            <div style="">
                <p style="font-size: 16px; font-weight: bold;">
                    日计划&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label runat="server" ID="lbDayState" ForeColor="Red"></asp:Label></p> 
                <p style="padding: 5px 5px 5px 5px; border: dashed #aaa 1px; height: 50px; background-color: #efefef;
                    float: left; width: 50%;">
                    <asp:Literal runat="server" ID="liTodayDesc"></asp:Literal></p>
                <p style="padding: 5px 5px 5px 5px; width: 130px; height: 50px; font-size: 18px;
                    color: red; font-weight: bolder; float: left; display:none;">
                    <asp:Literal runat="server" ID="liScore"></asp:Literal>
                </p>
                 <div style="clear:both"></div>
            </div>
           
        </div>
        <asp:Repeater runat="server" ID="rptTaskItems">
            <ItemTemplate>
                <div>
                    <div style="padding: 10px 10px 10px 10px;">
                        <%# GetTitleNo(Eval("Sort")) %>&nbsp;&nbsp;
                        <%# Convert.ToInt32(Eval("Type"))==0?"可选任务":"主要任务"%>
                        &nbsp;&nbsp; 耗时:<%# Eval("Hours") %>小时 &nbsp;&nbsp;结果：<%# Convert.ToInt32(Eval("Succeed"))==1?"成功":"失败" %>
                    </div>
                    <fieldset style="padding: 10px 10px 10px 10px; height: 40px; width:30%;float:left;">
                        <legend>任务要求</legend>
                        <%# Eval("Task") %></fieldset>
                        <fieldset style="padding: 10px 10px 10px 10px; height: 40px; width:30%;float:left;">
                        <legend><%# GetResultTitle(Eval("Succeed"),Eval("Checked")) %></legend>
                        <%# Eval("Reason") %></fieldset>
                        <fieldset style="padding: 10px 10px 10px 10px; height: 40px; width:30%;float:left;">
                        <legend>方式改进</legend>
                        <%# Eval("Improve") %></fieldset>
                        <div style="clear:both;"></div>
                </div>
            </ItemTemplate>
            <SeparatorTemplate>
                <br />
                <br />
            </SeparatorTemplate>
        </asp:Repeater>
    </div>
</asp:Content>
