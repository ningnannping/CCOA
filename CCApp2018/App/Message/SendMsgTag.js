﻿(function ($) {

    function onUnselect(target, record) {
        var opts = getOptions(target);
        opts.onUnselect.call("", record);
    }

    function append(target, datas, remove) {
        var opts = getOptions(target);
        var container = $(target).find(".message_div-container");
        if (remove) {
            container.children("div").remove();
        }
        var valueField = opts.valueField;
        var textField = opts.textField;
        for (var i = 0; i < datas.length; i++) {
            var data = datas[i];
            if (!contains(target, data, valueField)) {
                var tag = $(' <div class="addr_base addr_normal" style="float:left;white-space:nowrap;"unselectable="on"><div>');
                var html = "";
                tag.data(data);
                html +="<b>"+data[textField]+"</b>";
                html +=" <span>&lt;"+data[valueField]+"&gt;</span>";
                html +="<span class='semicolon'>;</span>";
                html +="<a href='javascript:;' class='addr_del' name='del'><i class='fa fa-times' data-role='remove'></i></a>";
                tag.html(html);
                container.append(tag);
                tag.delegate("a", "click", function (e) {
                    var record = $(this).parent().data();
                    $(this).parent().remove();
                    opts.onUnselect.call("", record);
                });
            }
        }
    }

    function loadData(target, datas) {
        append(target, datas, false);
    }

    function clear(target) {
        $(target).find(".message_div-container div").remove();
    }

    function setValues(target, values) {
        append(target, datas);
    }

    function getText(target) {
        var opts = getOptions(target);
        var textField = opts.textField;
        var text = [];
        $(target).find(".message_div-container div").each(function () {
            text.push($(this).data()[textField]);
        });
        return text.join(",");
    }

    function getValue(target) {
        var opts = getOptions(target);
        var valueField = opts.valueField;
        var text = [];
        $(target).find(".message_div-container div").each(function () {
            text.push($(this).data()[valueField]);
        });
        return text.join(",");
    }

    function getOptions(target) {
        return $.data(target, "sendMagTag").options;
    }

    function contains(target, data, valueField) {
        var flag = false;
        $(target).find(".message_div-container div").each(function () {
            if (data[valueField] == $(this).data()[valueField]) {
                flag = true;
                return;
            }
        });
        return flag;
    }

    function create(target) {
        var opts = getOptions(target);
        var html = "";
        html += '<div class="col-xs-10 main-container">';
        html += ' <div style="position: absolute; color: rgb(160, 160, 160); padding-top: 1px; display: none;"></div>';
        html += '<div id="stuff" style="display: inline; width: 1px;"></div>';
        html += '</div>';
        html += '</div>';
        $(target).html(html);
    }
    function setSize(target) {
        var opts = getOptions(target);
        var t = $(target);
        if (opts.fit == true) {
            var p = t.parent();
            opts.width = p.width();
        }
        var c = t.find('.main-container');
        c._outerWidth(opts.width);
        c._outerHeight(opts.height);
        t.find("#stuff")._outerHeight(opts.height);
    }

    $.fn.sendMsgTag = function (options, params) {
        if (typeof options == 'string') {
            return $.fn.sendMsgTag.methods[options](this, params);
        }
        options = options || {};
        return this.each(function () {
            var state = $.data(this, "sendMagTag");
            if (state) {
                $.extend(state.options, options);
            } else {
                state = $.data(this, 'sendMagTag', {
                    options: $.extend({}, $.fn.sendMsgTag.defaults, $.fn.sendMsgTag.parseOptions(this), options)
                });
               // create(this);
            }
            setSize(this);
        });
    };

    $.fn.sendMsgTag.methods = {
        setValues: function (jq, values) {
            return jq.each(function () {
                setValues(this, values);
            });
        },
        getValue: function (jq) {
            return getValue(jq[0]);
        },
        getText: function (jq) {
            return getText(jq[0]);
        },
        clear: function (jq) {
            return jq.each(function () {
                clear(this);
            });
        },
        loadData: function (jq, values) {
            return jq.each(function () {
                loadData(this, values);
            });
        }
    };

    $.fn.sendMsgTag.parseOptions = function (target) {
        var t = $(target);
        return $.extend({}, $.parser.parseOptions(target, ["width", "data", {
            "fit": "boolean",
            "valueField": "string",
            "textField": "string"
        }]));
    };

    $.fn.sendMsgTag.defaults = {
        "width": "100%",
        "fit": true,
        "valueField": "No",
        "textField": "Name",
        "onUnselect": function (record) {
        }
    };

})(jQuery);
