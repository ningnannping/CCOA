﻿function initSubTable(index, row, $detail,msgType) {
    //获取邮件的MyPK
    var MyPK = row.MyPK;

    //获取信息详情
    var en;
    if (msgType == 1) {
        en = new Entity("BP.OA.Message.InBox", MyPK);
        //修改邮件的状态 已读、 阅读时间
        en.InBoxState = 1;
        if (en.OpenTime == null || en.OpenTime == undefined || en.OpenTime == "") {
            en.OpenTime = dateFtt("yyyy-MM-dd hh:mm", new Date());
            en.Update();
        }
    }
    if (msgType == 0)
        en = new Entity("BP.OA.Message.SendBox", MyPK);

    var cur_table = $detail.html('<div></div>').find('div');
    //加载table页面
    var html = "";
    html += '<div id="InBoxMsg" style="margin-left:30px;margin-top:10px;">';
    html += '<div class="titleInfo">';
    html += '<div style="text-align: left; padding-left: 10px; margin-bottom:10px;">';
    html += '<div style="float: left;"><span ></span></div>';
    html += '<div style="float: left; padding: 1px 1px 1px 1px; width: 80%;height:17px; overflow:visible;">';
    var senderName = en.SenderName;
    if (senderName == null || senderName == "" || senderName == undefined) {
        var emp = new Entity("BP.Port.Emp", en.Sender);
        senderName = emp.Name;
    }
    html += '<p style="font-size: 16px; font-weight: bolder; ">' + senderName + ' | ' + en.Title + '</p>';
    html += '</div>';
    html += '<div style="clear: both;"></div>';
    html += '</div>';
    html += '<div style="text-align: left; padding-left: 10px; margin-bottom:5px;">';
    html += '<div style="float: left;"><span style="color:black">发件人：</span></div>';
    html += '<div id="sender" style="float: left; width: 80%;"><span class="label" style="color:black">' + senderName + "&lt;" + en.Sender + "&gt;" + '</span></div>';
    html += '<div style="clear: both;"></div>';
    html += '</div>';

    html += '<div style="text-align: left; padding-left: 10px; margin-bottom:5px;">';
    html += '<div style="float: left;"><span style="color:black">收件人：</span></div>';
    html += '<div  style="float: left;  width: 80%;"><span id="receviers" class="label" style="color:black"></span></div>';
    html += '<div style="clear: both;"></div>';
    html += '</div>';
    html += '<div style="text-align: left; padding-left: 10px; padding-bottom:5px;">';
    html += '<div style="float: left;"><span  style="color:black">抄送人：</span></div>';
    html += '<div style="float: left; width: 80%;"><span id="ccers" class="label" style="color:black"></span></div>';
    html += '<div style="clear: both;"></div>';
    html += '</div>';
    var sendTime = en.SendTime;
    var ttDate = sendTime.replace(/(\d{4}).(\d{1,2}).(\d{1,2}).+/mg, '$1年$2月$3日') + sendTime.substring(10);
    html += '<div style="text-align: left; padding-left: 10px; padding-bottom:10px;">';
    html += '<div style="float: left;"><span  style="color:black">发送时间：</span></div>';
    html += '<div id="sendTime" style="float: left; padding: 1px 1px 1px 1px; width: 80%;height:17px; overflow:visible;">'+ttDate + "(" + getWeekDay(sendTime) + ")"+'</div>';
    html += '<div style="clear: both;"></div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '</div>';
    html += '<div class="contentInfo" style="margin-left:30px">';
    var docs = en.Docs;
    docs = docs.replace(/~/g, "");
    html += '<div id="docs" class="Article" style="margin: 10x 10px 10px 10px; padding: 10px 10px 10px 10px; border: dashed 1px #dddddd;min-height:240px;">' + docs + '</div>';
    html += '</div>';
    html += '</div>';

    html += '<div style="margin-left:30px;margin-top:10px">';
    html += '<div class="btn-group">';
    html += '<button type="button" class="btn btn-default " onclick="ReplyMsg(\''+en.MyPK+'\')">回复</button>';
    html += '</div>';
    html += '<div class="btn-group" style="margin-left:5px">';
    html += '<button type="button" class="btn btn-default " onclick="ForwardedMsg(\'' + en.MyPK + '\')">转发</button>';
    html += '</div>';
    html += '<div class="btn-group" style="margin-left:5px">';
    html += '<button type="button" class="btn btn-default " onclick="NoChangSendMsg(\'' + en.MyPK + '\')">原文件转发</button>';
    html += '</div>';
    html += '<br />';
    html += '</div>';


    cur_table.append(html);

    //获取收件人的信息
    var handler = new HttpHandler("BP.OA.HttpHandler.App_Message");
    handler.AddPara("Emps", en.Receiver);
    var data = handler.DoMethodReturnString("SelectEmps");
    if (data.indexOf("err@") != -1) {
        alert(data);
        return;
    }
    data = JSON.parse(data);
    var container = $("#receviers");
    $.each(data, function (i, emp) {
        var html = "";
        if (emp.No == new WebUser().No)
            html += "<b>我</b>";
        else
            html += "<b>" + emp.Name + "</b>";
        html += " <span>&lt;" + emp.No + "&gt;</span>";
        html += "<span class='semicolon'>;</span>";
        if (emp.No == new WebUser().No)
            container.prepend(html);
        else
            container.append(html);
    });

    //获取抄送人的信息
    var ccers = en.CCers;
    if (ccers == null || ccers == "" || ccers == undefined)
        return;
    handler.AddPara("Emps", en.CCers);
    var data = handler.DoMethodReturnString("SelectEmps");
    if (data.indexOf("err@") != -1) {
        alert(data);
        return;
    }

    data = JSON.parse(data);
    container = $("#ccers");

    $.each(data, function (i, emp) {
        var html = "";
        if (emp.No == new WebUser().No)
            html += "<b>我</b>";
        else
            html += "<b>" + emp.Name + "</b>";
        html += " <span>&lt;" + emp.No + "&gt;</span>";
        html += "<span class='semicolon'>;</span>";
        if (emp.No == new WebUser().No)
            container.prepend(html);
        else
            container.append(html);
    });

}
//回复
function ReplyMsg(MyPK) {
    window.location.href = 'SendMsg.htm?ResourceType=0&PKVal=' + MyPK;
}

//转发
function ForwardedMsg(MyPK) {
    window.location.href = 'SendMsg.htm?ResourceType=1&PKVal=' + MyPK;
}

function NoChangSendMsg(MyPK) {
    //弹出提示窗口，输入接收人
    $("#myModal").modal('show');
    $("#InBox_PK").val(MyPK);
}

function Send() {
    var MyPK = $("#InBox_PK").val();
    //获取信息详情
    var boxEn = new Entity("BP.OA.Message.InBox", MyPK);
    recevier = $("#TB_Recevier").val();
    if (receviers.toString().indexOf(';') != -1) {
        $("#msg").html("一次只能转发给一人");
        return;
    }


    //获取接受者信息
    var en = new Entity("BP.OA.Message.InBox");
    en.Sender = boxEn.Sender;
    en.SenderName = boxEn.SenderName + "(由" + new WebUser().Name + "转发)";
    en.Receiver = boxEn.Receiver;
    en.SendTime = dateFtt("yyyy-MM-dd hh:ss",new Data());
    en.Title = boxEn.Title;
    en.Docs = boxEn.Docs;
    en.MyPK = boxEn.MyPK + "_" + recevier;
    en.UserNo = recevier;
    en.RefMsg = sendEn.MyPK;
    en.InBoxState = 0;
    en.MsgType = 0;
    en.Save();

}
function Close() {
    $("#myModal").modal('hide');
}