use CCOA;

-- 系统;
DELETE FROM GPM_App WHERE No='CCOA' ;
INSERT INTO GPM_App(No,Name,AppModel,Url,MyFileName,MyFilePath,MyFileExt,WebPath,FK_AppSort,RefMenuNo) VALUES ('CCOA','驰骋OA',0,'http://www.chichengoa.org/App/login/login.ashx','ccoa.png','Path','GIF','/DataUser/BP.GPM.STem/CCOA.png','01','2002');

-- 删除所有的菜单.
DELETE FROM GPM_Menu WHERE FK_App='CCOA';

-- root;
INSERT INTO GPM_Menu(FK_App,No,ParentNo,Name,MenuType,Url,IsEnable) VALUES ('CCOA','2000','0','驰骋OA',1,'',1);

-- CCOA 流程管理;
INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD01', '流程管理', '2000',1, 3, 'CCOA', '', 0,'',1);
INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD0101', '发起', 'HD01', 1, 4, 'CCOA', '/WF/Start.htm',  0,'',1);
INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD0102', '待办', 'HD01', 2, 4, 'CCOA', '/WF/Todolist.htm',  0,'',1);
INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD0103', '已办', 'HD01', 3, 4, 'CCOA', '/WF/Runing.htm',  0,'',1);
INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD0104', '查询', 'HD01', 3, 4, 'CCOA', '/WF/Search.htm',  0,'',1);

-- CCOA 项目管理;
INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD02', '项目管理', '2000',9, 3, 'CCOA', '', 0,'',1);
INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD0201', '客户项目组管理', 'HD02', 9, 4, 'CCOA', '../WF/Comm/SearchBS.htm?EnsName=BP.CCOA.CustomerPrjs',  1,'',1);
INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD0202', '客户联系人', 'HD02', 9, 4, 'CCOA', '../WF/Comm/SearchBS.htm?EnsName=BP.CCOA.Linkers',  0,'',1);
INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD0204', '项目台账', 'HD02', 9, 4, 'CCOA', '../WF/Comm/SearchBS.htm?EnsName=BP.CCOA.Prjs',  0,'',1);
--INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD0205', '项目组', 'HD02', 9, 4, 'CCOA', '../WF/Comm/SearchBS.htm?EnsName=BP.CCOA.EmpPrjExts',  0,'',1);

INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD0207', '我的项目', 'HD02', 9, 4, 'CCOA', '/App/MyPrj.htm',  0,'',1);
INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD0208', '飞机/部件台账', 'HD02', 9, 4, 'CCOA', '../WF/Comm/SearchBS.htm?EnsName=BP.CCOA.Aircrafts',  0,'',1);
INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD0210', '飞机/部件台账分析', 'HD02', 9, 4, 'CCOA', '../WF/Comm/Group.htm?EnsName=BP.CCOA.Aircrafts',  0,'',1);
INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD0221', '航空代码', 'HD02', 9, 4, 'CCOA', '../WF/Comm/SearchBS.htm?EnsName=BP.CCOA.CustomerCodes',  0,'',1);
INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD0223', '人员工作分布', 'HD02', 9, 4, 'CCOA', '/App/EmpWorking.htm',  0,'',1);

-- INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD0203', '项目台账2', 'HD02', 9, 4, 'CCOA', '../WF/Comm/SearchBS.htm?EnsName=BP.PRJ.Prjs',  0,'',1);
 
--CCOA 工艺管理;
--INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD03', '工艺管理', '2000',9, 3, 'CCOA', '', 0,'',1);
--INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD0301', '工艺类别树', 'HD03', 9, 4, 'CCOA', '/WF/Comm/Tree.htm?EnsName=BP.CCOA.TechnicTrees',  1,'',1);
--INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD0302', '工艺', 'HD03', 9, 4, 'CCOA', '/WF/Comm/SearchBS.htm?EnsName=BP.CCOA.Technics',  0,'',1);

--INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD0303', '个人荣誉信息', 'HD03', 9, 4, 'CCOA', 'SPAnalyzeHD.aspx',  0,'',1);
--INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD0304', '个人资质信息', 'HD03', 9, 4, 'CCOA', 'SPAnalyzeHD.aspx',  0,'',1);
--INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD0305', '内部监管信息', 'HD03', 9, 4, 'CCOA', 'SPAnalyzeHD.aspx',  0,'',1);

-- CCOA NRC模版管理;
INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD04', 'NRC模版管理', '2000',1, 3, 'CCOA', '', 0,'',1);
INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD0401', '模版树', 'HD04', 1, 4, 'CCOA', '/WF/Comm/Tree.htm?EnsName=BP.CCOA.TemplateNRCTrees',  0,'',1);
INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD0402', '模版台账', 'HD04', 2, 4, 'CCOA', '/WF/Comm/SearchBS.htm?EnsName=BP.CCOA.TemplateNRCs',  0,'',1);
INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD0403', '台账树', 'HD04', 2, 4, 'CCOA', '/WF/Comm/TreeEns.htm?TreeEnsName=BP.CCOA.TemplateNRCTrees&EnsName=BP.CCOA.TemplateNRCs&RefPK=FK_Template',  0,'',1);
INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD0404', 'NRC流程', 'HD04', 2, 4, 'CCOA', '/WF/Comm/SearchBS.htm?SearchBS=BP.CCOA.TemplateNRCTrees&EnsName=BP.CCOA.ND1RptTemplates&RefPK=FK_Template',  0,'',1);

 
 -- CCOA SWS模版管理;
INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD10', 'SWS模版管理', '2000',1, 3, 'CCOA', '', 0,'',1);
INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD1011', '模版树', 'HD10', 1, 4, 'CCOA', '/WF/Comm/Tree.htm?EnsName=BP.CCOA.TemplateSWSTrees',  0,'',1);
INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD1012', '模版台账', 'HD10', 2, 4, 'CCOA', '/WF/Comm/SearchBS.htm?EnsName=BP.CCOA.TemplateSWSs',  0,'',1);
INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD1013', '台账树', 'HD10', 2, 4, 'CCOA', '/WF/Comm/TreeEns.htm?TreeEnsName=BP.CCOA.TemplateSWSTrees&EnsName=BP.CCOA.TemplateSWSs&RefPK=FK_Template',  0,'',1);
 
-- CCOA 物料管理;
INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD11', '物料管理', '2000',1, 3, 'CCOA', '', 0,'',1);
INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD1101', '物料树', 'HD11', 1, 4, 'CCOA', '/WF/Comm/Tree.htm?EnsName=BP.CCOA.MaterielTrees',  0,'',1);
INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD1102', '物料台账', 'HD11', 2, 4, 'CCOA', '/WF/Comm/SearchBS.htm?EnsName=BP.CCOA.Materiels',  0,'',1);
INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD1103', '台账树', 'HD11', 2, 4, 'CCOA', '/WF/Comm/TreeEns.htm?TreeEnsName=BP.CCOA.MaterielTrees&EnsName=BP.CCOA.Materiels&RefPK=FK_Materiel',  0,'',1);

-- CCOA 查询分析;
INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD05', '查询分析', '2000',1, 3, 'CCOA', '', 0,'',1);
INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD0501', 'NRC台账', 'HD05', 1, 4, 'CCOA', '/WF/Comm/SearchBS.htm?EnsName=BP.CCOA.ND1Rpts',  0,'',1);
INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD0502', 'NRC分析', 'HD05', 2, 4, 'CCOA', '/WF/Comm/Group.htm?EnsName=BP.CCOA.ND1Rpts',  0,'',1);

-- CCOA 系统管理;
INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD99', '组织结构管理', '2000',1, 3, 'CCOA', '', 0,'',1);
INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD9900', '部门树', 'HD99', 2, 4, 'CCOA', '/WF/Comm/Tree.htm?EnsName=BP.CCOA.Depts',  0,'',1);
INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD9901', '部门台账', 'HD99', 2, 4, 'CCOA', '/WF/Comm/SearchBS.htm?EnsName=BP.CCOA.Depts',  0,'',1);

INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD9909', '职务', 'HD99', 2, 4, 'CCOA', '/WF/Comm/SearchBS.htm?EnsName=BP.CCOA.Dutys',  0,'',1);

INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD9902', '人员台账', 'HD99', 2, 4, 'CCOA', '/WF/Comm/SearchBS.htm?EnsName=BP.CCOA.Emps',  0,'',1);

INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD9903', '权限类别', 'HD99', 1, 4, 'CCOA', '/WF/Comm/SearchBS.htm?EnsName=BP.CCOA.SQLBs',  0,'',1);
INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD9904', '权限台帐', 'HD99', 1, 4, 'CCOA', '/WF/Comm/SearchBS.htm?EnsName=BP.CCOA.SQs',  0,'',1);
INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD9905', '人员授权台账', 'HD99', 1, 4, 'CCOA', '/WF/Comm/SearchBS.htm?EnsName=BP.CCOA.EmpSQLDtlExts',  0,'',1);

INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD9910', '组织结构', 'HD99', 2, 4, 'CCOA', '/GPM/Organization.htm',  0,'',1);
INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD9911', '岗位类型', 'HD99', 1, 4, 'CCOA', '/WF/Comm/SearchBS.htm?EnsName=BP.GPM.StationTypes',  0,'',1);
INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD9912', '岗位', 'HD99', 1, 4, 'CCOA', '/WF/Comm/SearchBS.htm?EnsName=BP.GPM.Stations',  0,'',1);

-- CCOA 系统/菜单/权限;
INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD98', '系统/菜单/权限', '2000',1, 3, 'CCOA', '', 0,'',1);
INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD9801', '系统类别', 'HD98', 2, 4, 'CCOA', '/WF/Comm/SearchBS.htm?EnsName=BP.GPM.AppSorts',  0,'',1);
INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD9802', '系统台账', 'HD98', 2, 4, 'CCOA', '/WF/Comm/SearchBS.htm?EnsName=BP.GPM.Apps',  0,'',1);
INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD9803', '权限组菜单', 'HD98', 1, 4, 'CCOA', '/WF/Comm/SearchBS.htm?EnsName=BP.GPM.Groups',  0,'',1);
INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD9804', '岗位菜单', 'HD98', 1, 4, 'CCOA', '/WF/Comm/SearchBS.htm?EnsName=BP.GPM.StationExts',  0,'',1);
INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD9805', '人员菜单', 'HD98', 2, 4, 'CCOA', '/WF/Comm/SearchBS.htm?EnsName=BP.GPM.GPMEmps',  0,'',1);
INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD9806', '菜单台账', 'HD98', 1, 4, 'CCOA', '/WF/Comm/SearchBS.htm?EnsName=BP.GPM.Menus',  0,'',1);
INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD9807', '菜单树', 'HD98', 1, 4, 'CCOA', '/WF/Comm/Tree.htm?EnsName=BP.GPM.Menus',  0,'',1);

-- CCOA 基本信息管理;
INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD97', '基本信息管理', '2000',1, 3, 'CCOA', '', 0,'',1);
INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD9701', '适航当局表', 'HD97', 1, 4, 'CCOA', '/WF/Comm/SearchBS.htm?EnsName=BP.CCOA.AAs',  0,'',1);

INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD9702', '飞机部件类型', 'HD97', 1, 4, 'CCOA', '/WF/Comm/SearchBS.htm?EnsName=BP.CCOA.AircraftTypes',  0,'',1);
INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD9703', '发动机类型', 'HD97', 1, 4, 'CCOA', '/WF/Comm/SearchBS.htm?EnsName=BP.CCOA.EngineTypes',  0,'',1);
INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD9704', '工种', 'HD97', 1, 4, 'CCOA', '/WF/Comm/SearchBS.htm?EnsName=BP.CCOA.Trades',  0,'',1);
INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD9705', 'NDT方法类型级别', 'HD97', 1, 4, 'CCOA', '/WF/Comm/SearchBS.htm?EnsName=BP.CCOA.NDTMethods',  0,'',1);
INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD9706', '技术人员类Technic信息', 'HD97', 1, 4, 'CCOA', '/WF/Comm/SearchBS.htm?EnsName=BP.CCOA.TECDtls',  0,'',1);
INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD9707', '地面设备操作类GEO信息', 'HD97', 1, 4, 'CCOA', '/WF/Comm/SearchBS.htm?EnsName=BP.CCOA.GEODtls',  0,'',1);

-- CCOA PPC信息管理;
INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD96', 'PPC管理', '2000',1, 3, 'CCOA', '', 0,'',1);
INSERT INTO GPM_Menu (No, Name, ParentNo, Idx, MenuType, FK_App, Url, OpenWay,Icon,MenuCtrlWay) VALUES ('HD9601', 'SAP-NRC登记号', 'HD96', 1, 4, 'CCOA', '/WF/Comm/SearchBS.htm?EnsName=BP.CCOA.SAPNRCBillNos',  0,'',1);


UPDATE GPM_Menu SET IsEnable=1;