﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web.UI;
using BP.En;

namespace BP.OA.UI
{
    public class PageCommon
    {
        /// <summary>
        /// 把页面数据填充到实体
        /// </summary>
        /// <param name="page"></param>
        /// <param name="en"></param>
        /// <returns></returns>
        public static Entity GetEntity(Page page, Entity en)
        {
            //属性集合
            Attrs attrs = en.EnMap.Attrs;
            if (attrs != null && attrs.Count > 0)
            {
                foreach (Attr attr in attrs)
                {
                    if (attr.Key == "OID" || attr.Key == "No" || attr.Key == "MyPK")
                        continue;
                    var reVal = page.Request.Form[attr.Key];
                    if (reVal != null && !string.IsNullOrEmpty(reVal.ToString()))
                    {
                        en.SetValByKey(attr.Key, reVal);
                    }
                }
            }
            return en;
        }
        /// <summary>
        /// 新增数据
        /// </summary>
        /// <param name="page">页面</param>
        /// <param name="en">实体</param>
        /// <returns></returns>
        public static int Insert(Page page, Entity en)
        {
            //属性集合
            Attrs attrs = en.EnMap.Attrs;
            if (attrs != null && attrs.Count > 0)
            {
                foreach (Attr attr in attrs)
                {
                    if (attr.Key == "OID" || attr.Key == "No" || attr.Key == "MyPK")
                        continue;
                    var reVal = page.Request.Form[attr.Key];
                    if (reVal != null && !string.IsNullOrEmpty(reVal.ToString()))
                    {
                        en.SetValByKey(attr.Key, reVal);
                    }
                }
                return en.Insert();
            }
            return 0;
        }
        /// <summary>
        /// 修改
        /// </summary>
        /// <param name="page"></param>
        /// <param name="en"></param>
        /// <returns></returns>
        public static int Update(Page page, Entity en)
        {
            //属性集合
            Attrs attrs = en.EnMap.Attrs;
            if (attrs != null && attrs.Count > 0)
            {
                foreach (Attr attr in attrs)
                {
                    if (attr.Key == "OID" || attr.Key == "No" || attr.Key == "MyPK")
                        continue;
                    var reVal = page.Request.Form[attr.Key];
                    if (reVal != null && !string.IsNullOrEmpty(reVal.ToString()))
                    {
                        en.SetValByKey(attr.Key, reVal);
                    }
                }
                return en.Update();
            }
            return 0;
        }
    }
}
