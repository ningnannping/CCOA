﻿using System;
using System.Data;
using BP.DA;
using BP.En;
using BP.Web;

namespace BP.OA
{
    /// <summary>
    /// 新闻类别属性
    /// </summary>
    public class NewsSortAttr : EntityNoNameAttr
    {
    }
    /// <summary>
    /// 新闻类别
    /// </summary>
    public class NewsSort : EntityNoName
    {
        #region 属性
        #endregion

        #region 构造函数
        /// <summary>
        /// 新闻类别
        /// </summary>
        public NewsSort() { }
        /// <summary>
        /// 新闻类别
        /// </summary>
        /// <param name="no">编号</param>
        public NewsSort(string no) : base(no) { }
        #endregion

        #region 重写方法
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                uac.OpenAll();
                return uac;
            }
        }
        /// <summary>
        /// Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map();
                map.PhysicsTable = "OA_NewsSort";
                map.EnDesc = "新闻类别";   // 实体的描述.
                map.CodeStruct = "2";
                map.IsAllowRepeatName = false;
                map.IsAutoGenerNo = true;

                map.AddTBStringPK(NewsSortAttr.No, null, "编号", true, true, 2, 2, 20);
                map.AddTBString(NewsSortAttr.Name, null, "名称", true, false, 1, 100, 30);

                this._enMap = map;
                return this._enMap;
            }
        }
        #endregion
    }
    /// <summary>
    ///得到集合
    /// </summary>
    public class NewsSorts : EntitiesNoName
    {
        /// <summary>
        /// 得到一个新实体
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new NewsSort();
            }
        }
        /// <summary>
        /// 新闻类别集合
        /// </summary>
        public NewsSorts()
        {
        }
    }
}