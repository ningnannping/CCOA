﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.DA;
using BP.En;
using BP.Web;

namespace BP.OA
{
    /// <summary>
    /// 新闻文章属性
    /// </summary>
    public class NewsAttr : EntityOIDAttr
    {
        public const String FK_UserNo = "FK_UserNo";
        public const String FK_NewsSort = "FK_NewsSort";
        public const String RDT = "RDT";
        public const String Title = "Title";
        public const String KeyWords = "KeyWords";
        public const String NewsSource = "NewsSource";
        public const String Doc = "Doc";
        public const String BrowseCount = "BrowseCount";
        public const String AttachFile = "AttachFile";
        public const String Idx = "Idx";
    }
    /// <summary>
    /// 新闻文章
    /// </summary>
    public partial class News : EntityOID
    {
        #region 属性
        public string FK_UserNo
        {
            get { return this.GetValStrByKey(NewsAttr.FK_UserNo); }
            set { this.SetValByKey(NewsAttr.FK_UserNo, value); }
        }
        public string FK_NewsSort
        {
            get { return this.GetValStrByKey(NewsAttr.FK_NewsSort); }
            set { this.SetValByKey(NewsAttr.FK_NewsSort, value); }
        }
        public string NewsSortText
        {
            get { return this.GetValRefTextByKey(NewsAttr.FK_NewsSort); }
        }
        /// <summary>
        /// 增加时间
        /// </summary>
        public string RDT
        {
            get { return this.GetValStrByKey(NewsAttr.RDT); }
            set { this.SetValByKey(NewsAttr.RDT, value); }
        }
        public String Title
        {
            get { return this.GetValStrByKey(NewsAttr.Title); }
            set { this.SetValByKey(NewsAttr.Title, value); }
        }
        public String NewsSource
        {
            get { return this.GetValStrByKey(NewsAttr.NewsSource); }
            set { this.SetValByKey(NewsAttr.NewsSource, value); }
        }
        public String KeyWords
        {
            get { return this.GetValStrByKey(NewsAttr.KeyWords); }
            set { this.SetValByKey(NewsAttr.KeyWords, value); }
        }
        public string Doc
        {
            get
            {//新闻内容不能为空的前提
                string doc = this.GetValStrByKey(NewsAttr.Doc);
                if (doc == "" || doc.Length == 0)
                    return this.GetBigTextFromDB("DocFile");
                else
                    return doc;
            }
            set
            {
                if (value.Length > 2000)
                {
                    this.SaveBigTxtToDB("DocFile", value);
                    this.SetValByKey(NewsAttr.Doc, "");
                }
                else
                {
                    this.SetValByKey(NewsAttr.Doc, value);
                }

            }
        }
        public int BrowseCount
        {
            get { return this.GetValIntByKey(NewsAttr.BrowseCount); }
            set { this.SetValByKey(NewsAttr.BrowseCount, value); }
        }
        public string AttachFile
        {
            get { return this.GetValStrByKey(NewsAttr.AttachFile); }
            set { this.SetValByKey(NewsAttr.AttachFile, value); }
        }
        public DateTime Idx
        {
            get { return this.GetValDateTime(NewsAttr.Idx); }
            set { this.SetValByKey(NewsAttr.Idx, value); }
        }
        #endregion

        #region 构造函数
        /// <summary>
        /// 新闻文章
        /// </summary>
        public News() { }
        /// <summary>
        /// 新闻文章
        /// </summary>
        /// <param name="no">编号</param>
        public News(int no) : base(no) { }
        #endregion

        #region 重写方法
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                uac.OpenForSysAdmin();
                return uac;
            }
        }
        /// <summary>
        /// Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map();
                map.PhysicsTable = "OA_News";
                map.EnDesc = "新闻文章";                   // 实体的描述.
                map.CodeStruct = "2";
                map.AddTBIntPKOID();
                map.AddTBString(NewsAttr.FK_UserNo, null, "发布人", true, false, 2, 50, 120);
                map.AddDDLEntities(NewsAttr.FK_NewsSort, "0", "新闻目录", new NewsSorts(), true);
                map.AddTBString(NewsAttr.Title, null, "标题", true, false, 0, 100, 300, true);
                map.AddTBString(NewsAttr.KeyWords, null, "关键字", true, false, 0, 100, 100);
                map.AddTBString(NewsAttr.NewsSource, null, "来源", true, false, 0, 100, 100);

                //map.AddTBStringDoc(NewsAttr.Doc, null, "新闻内容", true, false, true);

                map.AddTBStringDoc(); // (NewsAttr.Doc, null, "新闻内容", true, false, true);


                map.AddTBInt(NewsAttr.BrowseCount, 0, "浏览量", true, false);
                map.AddTBDate(NewsAttr.RDT, null, "添加时间", true, false);
                map.AddTBInt(NewsAttr.Idx, 0, "顺序号", false, false);
                map.AddMyFile("附件");

                map.AddSearchAttr(NewsAttr.FK_NewsSort); //类别.

                this._enMap = map;
                return this._enMap;
            }
        }
        #endregion

    }
    /// <summary>
    ///新闻文章s
    /// </summary>
    public class Newss : EntitiesOID
    {
        /// <summary>
        /// 得到一个新实体
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new News();
            }
        }
        /// <summary>
        /// 新闻文章集合
        /// </summary>
        public Newss()
        {
        }
    }
}
