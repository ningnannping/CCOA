﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.En;

namespace BP.OA.HR
{
    //OID， FK_合同编号，合同人员， 登记人， 登记时间，原有无固定期限， 原合同开始日期， 原合同结束日期， 改后有无固定期限， 改后合同开始日期， 改后合同结束日期， 备注

    /// <summary>
    /// 劳动合同变更与续签 属性
    /// </summary>
    public class HRContractModifyAttr : EntityOIDAttr
    {
        #region 基本属性
        /// <summary>
        /// FK_合同编号
        /// </summary>
        public const string FK_HRContract = "FK_HRContract";
        /// <summary>
        /// FK_人事档案编号
        /// </summary>
        public const string FK_HRRecord = "FK_HRRecord";
        /// <summary>
        /// 登记人
        /// </summary>
        public const string DengJiRen = "DengJiRen";
        /// <summary>
        /// 登记时间
        /// </summary>
        public const string DengJiDT = "DengJiDT";
        /// <summary>
        /// 原合同有无固定期限
        /// </summary>
        public const string OldGuDingQiXian = "OldGuDingQiXian";
        /// <summary>
        /// 原合同开始日期
        /// </summary>
        public const string OldStartDT = "OldStartDT";
        /// <summary>
        /// 原合同结束日期
        /// </summary>
        public const string OldEndDT = "OldEndDT";
        /// <summary>
        /// 改后有无固定期限
        /// </summary>
        public const string NewGuDingQiXian = "NewGuDingQiXian";
        /// <summary>
        /// 改后合同开始日期
        /// </summary>
        public const string NewStartDT = "NewStartDT";
        /// <summary>
        /// 改后合同结束日期
        /// </summary>
        public const string NewEndDT = "NewEndDT";
        /// <summary>
        /// 备注
        /// </summary>
        public const string Note = "Note";

        #endregion
    }

    /// <summary>
    /// 劳动合同变更与续签
    /// </summary>
    public class HRContractModify : EntityOID
    {
        #region 基本属性
        /// <summary>
        /// FK_合同编号
        /// </summary>
        public string FK_HRContract
        {
            get { return this.GetValStringByKey(HRContractModifyAttr.FK_HRContract); }
            set { this.SetValByKey(HRContractModifyAttr.FK_HRContract, value); }
        }
        /// <summary>
        /// FK_人事档案编号
        /// </summary>
        public string FK_HRRecord
        {
            get { return this.GetValStringByKey(HRContractModifyAttr.FK_HRRecord); }
            set { this.SetValByKey(HRContractModifyAttr.FK_HRRecord, value); }
        }
        /// <summary>
        /// 登记人
        /// </summary>
        public string DengJiRen
        {
            get { return this.GetValStringByKey(HRContractModifyAttr.DengJiRen); }
            set { this.SetValByKey(HRContractModifyAttr.DengJiRen, value); }
        }
        /// <summary>
        /// 登记时间
        /// </summary>
        public string DengJiDT
        {
            get { return this.GetValStringByKey(HRContractModifyAttr.DengJiDT); }
            set { this.SetValByKey(HRContractModifyAttr.DengJiDT, value); }
        }
        /// <summary>
        /// 原合同有无固定期限
        /// </summary>
        public string OldGuDingQiXian
        {
            get { return this.GetValStringByKey(HRContractModifyAttr.OldGuDingQiXian); }
            set { this.SetValByKey(HRContractModifyAttr.OldGuDingQiXian, value); }
        }
        /// <summary>
        /// 原合同开始日期
        /// </summary>
        public string OldStartDT
        {
            get { return this.GetValStringByKey(HRContractModifyAttr.OldStartDT); }
            set { this.SetValByKey(HRContractModifyAttr.OldStartDT, value); }
        }
        /// <summary>
        /// 原合同结束日期
        /// </summary>
        public string OldEndDT
        {
            get { return this.GetValStringByKey(HRContractModifyAttr.OldEndDT); }
            set { this.SetValByKey(HRContractModifyAttr.OldEndDT, value); }
        }
        /// <summary>
        /// 改后有无固定期限
        /// </summary>
        public string NewGuDingQiXian
        {
            get { return this.GetValStringByKey(HRContractModifyAttr.NewGuDingQiXian); }
            set { this.SetValByKey(HRContractModifyAttr.NewGuDingQiXian, value); }
        }
        /// <summary>
        /// 改后合同开始日期
        /// </summary>
        public string NewStartDT
        {
            get { return this.GetValStringByKey(HRContractModifyAttr.NewStartDT); }
            set { this.SetValByKey(HRContractModifyAttr.NewStartDT, value); }
        }
        /// <summary>
        /// 改后合同结束日期
        /// </summary>
        public string NewEndDT
        {
            get { return this.GetValStringByKey(HRContractModifyAttr.NewEndDT); }
            set { this.SetValByKey(HRContractModifyAttr.NewEndDT, value); }
        }
        /// <summary>
        /// 备注
        /// </summary>
        public string Note
        {
            get { return this.GetValStringByKey(HRContractModifyAttr.Note); }
            set { this.SetValByKey(HRContractModifyAttr.Note, value); }
        }

        #endregion

        #region 构造方法

        public HRContractModify()
        {

        }

        public HRContractModify(int oid)
            : base(oid)
        {

        }
        #endregion

        #region 重写父类的方法
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map("OA_HRContractModify");
                map.EnDesc = "劳动合同变更与续签";
                //编号
                map.AddTBIntPKOID();
                //字段
                map.AddTBString(HRContractRemoveAttr.FK_HRContract, null, "合同编号", true, true, 0, 50, 100);
                map.AddDDLEntities(HRContractModifyAttr.FK_HRRecord, null, "合同人员", new BP.OA.HR.HRRecords(), true);
                map.AddDDLEntities(HRContractModifyAttr.FK_HRRecord, null, "登记人", new BP.OA.HR.HRRecords(), true);
                map.AddTBDate(HRContractModifyAttr.DengJiDT, null, "登记时间", true, false);
                map.AddDDLSysEnum(HRContractModifyAttr.OldGuDingQiXian, 0, "原合同有无固定期限", true, true, HRContractModifyAttr.OldGuDingQiXian, "@0=有@1=无", true);
                map.AddTBDate(HRContractModifyAttr.OldStartDT, null, "原合同开始日期", true, false);
                map.AddTBDate(HRContractModifyAttr.OldEndDT, null, "原合同截至日期", true, false);
                map.AddDDLSysEnum(HRContractModifyAttr.NewGuDingQiXian, 0, "改后有无固定期限", true, true, HRContractModifyAttr.NewGuDingQiXian, "@0=有@1=无", true);
                map.AddTBDate(HRContractModifyAttr.NewStartDT, null, "改后合同开始日期", true, false);
                map.AddTBDate(HRContractModifyAttr.NewEndDT, null, "改后合同截至日期", true, false);
                map.AddTBStringDoc(HRContractModifyAttr.Note, null, "备注", true, false, true);
                //查询条件
                //map.AddSearchAttr(HRContractModifyAttr.FK_HRContract);
                map.AddSearchAttr(HRContractModifyAttr.FK_HRRecord);
                this._enMap = map;
                return this._enMap;
            }
        }
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                if (Web.WebUser.No == "admin")
                {
                    uac.OpenAll();
                    return uac;
                }

                uac.IsInsert = false;
                uac.IsUpdate = true;
                uac.IsDelete = false;
                uac.IsView = true;

                return uac;
            }
        }
        //protected override bool beforeUpdateInsertAction()
        //{
        //    BP.OA.HR.HRRecord emp =new BP.OA.HR.HRRecord(this.FK_HRRecord);
        //    BP.OA.HR.HRContracts hrcs = new BP.OA.HR.HRContracts();
        //    HRContract hrc=    (HRContract)hrcs.Filter("FK_HRRecord",emp.No);
        //    this.FK_HRContract = hrc.No;
        //    this.OldGuDingQiXian = hrc.GuDingQiXian;
        //    this.OldStartDT = hrc.StartDT;
        //    this.OldEndDT = hrc.EndDT;
        //    //this.Insert();
        //    return base.beforeUpdateInsertAction();
        //}
        protected override void afterInsertUpdateAction()
        {
            //HRContract hrc = new HRContract(this.FK_HRContract);
            //hrc.GuDingQiXian = this.NewGuDingQiXian;
            //hrc.StartDT = this.NewStartDT;
            //hrc.EndDT = this.NewEndDT;
            //hrc.Update();
            base.afterInsertUpdateAction();
        }
        #endregion
    }
    /// <summary>
    /// 劳动合同变更与续签s
    /// </summary>
    public class HRContractModifys : EntitiesOID
    {
        #region 构造方法

        public HRContractModifys()
        {

        }
        #endregion

        #region 重写父类的方法
        public override Entity GetNewEntity
        {
            get { return new HRContractModify(); }
        }
        #endregion
    }
}
