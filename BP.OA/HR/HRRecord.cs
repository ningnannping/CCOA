﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.En;
using BP.Port;


namespace BP.OA.HR
{

    /// <summary>
    /// 人事档案 属性
    /// </summary>
    public class HRRecordAttr : EntityNoNameAttr
    {
        #region 基本属性
        /// <summary>
        /// 入职时间
        /// </summary>
        public const string InCompanyDT = "InCompanyDT";
        /// <summary>
        /// 性别
        /// </summary>
        public const string Gender = "Gender";
        /// <summary>
        /// 出生日期
        /// </summary>
        public const string BirthDT = "BirthDT";
        /// <summary>
        /// 民族
        /// </summary>
        public const string MinZu = "MinZu";
        /// <summary>
        /// 籍贯
        /// </summary>
        public const string JiGuan = "JiGuan";
        /// <summary>
        /// 户口所在地
        /// </summary>
        public const string HuKouAddr = "HuKouAddr";
        /// <summary>
        /// 政治面貌
        /// </summary>
        public const string ZhengZhiMianMao = "ZhengZhiMianMao";
        /// <summary>
        /// 创建时间
        /// </summary>
        public const string JiaRuDT = "JiaRuDT";
        /// <summary>
        /// 参加工作时间
        /// </summary>
        public const string WorkDT = "WorkDT";
        /// <summary>
        /// 部门
        /// </summary>
        public const string FK_Dept = "FK_Dept";
       
        /// <summary>
        /// 职称
        /// </summary>
        public const string ZhiCheng = "ZhiCheng";
        /// <summary>
        /// 从事专业
        /// </summary>
        public const string CongShiZhuanYe = "CongShiZhuanYe";
        /// <summary>
        /// 文化程度
        /// </summary>
        public const string WenHuaChengDu = "WenHuaChengDu";
        /// <summary>
        /// 毕业院校
        /// </summary>
        public const string BiYeSchool = "BiYeSchool";
        /// <summary>
        /// 所学专业
        /// </summary>
        public const string SuoXueZhuanYe = "SuoXueZhuanYe";
        /// <summary>
        /// 毕业时间
        /// </summary>
        public const string BiYeDT = "BiYeDT";
        /// <summary>
        /// 身份证号码
        /// </summary>
        public const string IDCard = "IDCard";
        /// <summary>
        /// 原工作单位
        /// </summary>
        public const string YuanCompany = "YuanCompany";
        /// <summary>
        /// 备注
        /// </summary>
        public const string Note = "Note";
        /// <summary>
        /// 在职状态
        /// </summary>
        public const string ZhuangTai = "ZhuangTai";
        #endregion
    }
    /// <summary>
    /// 人事档案
    /// </summary>
    public class HRRecord : EntityNoName
    {
        #region 基本属性
        /// <summary>
        /// 入职时间
        /// </summary>
        public string InCompanyDT
        {
            get { return this.GetValStringByKey(HRRecordAttr.InCompanyDT); }
            set { this.SetValByKey(HRRecordAttr.InCompanyDT, value); }
        }

        /// <summary>
        /// 性别编号
        /// </summary>
        public int Gender
        {
            get { return this.GetValIntByKey(HRRecordAttr.Gender); }
            set { this.SetValByKey(HRRecordAttr.Gender, value); }
        }
        /// <summary>
        /// 性别名称
        /// </summary>
        public string GenderText
        {
            get { return this.GetValRefTextByKey(HRRecordAttr.Gender); }
        }
        /// <summary>
        /// 出生日期
        /// </summary>
        public string BirthDT
        {
            get { return this.GetValStringByKey(HRRecordAttr.BirthDT); }
            set { this.SetValByKey(HRRecordAttr.BirthDT, value); }
        }
        /// <summary>
        /// 民族
        /// </summary>
        public string MinZu
        {
            get { return this.GetValStringByKey(HRRecordAttr.MinZu); }
            set { this.SetValByKey(HRRecordAttr.MinZu, value); }
        }
        /// <summary>
        /// 籍贯
        /// </summary>
        public string JiGuan
        {
            get { return this.GetValStringByKey(HRRecordAttr.JiGuan); }
            set { this.SetValByKey(HRRecordAttr.JiGuan, value); }
        }
        /// <summary>
        /// 户口所在地
        /// </summary>
        public string HuKouAddr
        {
            get { return this.GetValStringByKey(HRRecordAttr.HuKouAddr); }
            set { this.SetValByKey(HRRecordAttr.HuKouAddr, value); }
        }
        /// <summary>
        /// 政治面貌
        /// </summary>
        public string ZhengZhiMianMao
        {
            get { return this.GetValStringByKey(HRRecordAttr.ZhengZhiMianMao); }
            set { this.SetValByKey(HRRecordAttr.ZhengZhiMianMao, value); }
        }
        /// <summary>
        /// 创建时间
        /// </summary>
        public string JiaRuDT
        {
            get { return this.GetValStringByKey(HRRecordAttr.JiaRuDT); }
            set { this.SetValByKey(HRRecordAttr.JiaRuDT, value); }
        }
        /// <summary>
        /// 参加工作时间
        /// </summary>
        public string WorkDT
        {
            get { return this.GetValStringByKey(HRRecordAttr.WorkDT); }
            set { this.SetValByKey(HRRecordAttr.WorkDT, value); }
        }
        /// <summary>
        /// 部门
        /// </summary>
        public string FK_Dept
        {
            get { return this.GetValStringByKey(HRRecordAttr.FK_Dept); }
            set { this.SetValByKey(HRRecordAttr.FK_Dept, value); }
        }
     
        /// <summary>
        /// 职称
        /// </summary>
        public string ZhiCheng
        {
            get { return this.GetValStringByKey(HRRecordAttr.ZhiCheng); }
            set { this.SetValByKey(HRRecordAttr.ZhiCheng, value); }
        }
        /// <summary>
        /// 从事专业
        /// </summary>
        public string CongShiZhuanYe
        {
            get { return this.GetValStringByKey(HRRecordAttr.CongShiZhuanYe); }
            set { this.SetValByKey(HRRecordAttr.CongShiZhuanYe, value); }
        }
        /// <summary>
        /// 文化程度
        /// </summary>
        public string WenHuaChengDu
        {
            get { return this.GetValStringByKey(HRRecordAttr.WenHuaChengDu); }
            set { this.SetValByKey(HRRecordAttr.WenHuaChengDu, value); }
        }
        /// <summary>
        /// 毕业院校
        /// </summary>
        public string BiYeSchool
        {
            get { return this.GetValStringByKey(HRRecordAttr.BiYeSchool); }
            set { this.SetValByKey(HRRecordAttr.BiYeSchool, value); }
        }
        /// <summary>
        /// 所学专业
        /// </summary>
        public string SuoXueZhuanYe
        {
            get { return this.GetValStringByKey(HRRecordAttr.SuoXueZhuanYe); }
            set { this.SetValByKey(HRRecordAttr.SuoXueZhuanYe, value); }
        }
        /// <summary>
        /// 毕业时间
        /// </summary>
        public string BiYeDT
        {
            get { return this.GetValStringByKey(HRRecordAttr.BiYeDT); }
            set { this.SetValByKey(HRRecordAttr.BiYeDT, value); }
        }
        /// <summary>
        /// 身份证号码
        /// </summary>
        public string IDCard
        {
            get { return this.GetValStringByKey(HRRecordAttr.IDCard); }
            set { this.SetValByKey(HRRecordAttr.IDCard, value); }
        }
        /// <summary>
        /// 原工作单位
        /// </summary>
        public string YuanCompany
        {
            get { return this.GetValStringByKey(HRRecordAttr.YuanCompany); }
            set { this.SetValByKey(HRRecordAttr.YuanCompany, value); }
        }
        /// <summary>
        /// 备注
        /// </summary>
        public string Note
        {
            get { return this.GetValStringByKey(HRRecordAttr.Note); }
            set { this.SetValByKey(HRRecordAttr.Note, value); }
        }
        /// <summary>
        /// 在职状态
        /// </summary>
        public string ZhuangTai
        {
            get { return this.GetValStringByKey(HRRecordAttr.ZhuangTai); }
            set { this.SetValByKey(HRRecordAttr.ZhuangTai, value); }
        }
        #endregion
        #region 构造函数

        public HRRecord()
        {

        }

        public HRRecord(string no)
            : base(no)
        {

        }
        #endregion
        #region 重写父类方法
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                if (Web.WebUser.No == "admin" || Web.WebUser.Name == this.Name)
                { uac.OpenAll(); }
                else
                {
                    uac.Readonly();
                }
                return uac;
            }
        }
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                {
                    return this._enMap;
                }
                Map map = new Map("OA_HRRecord");
                map.EnDesc = "人事档案";
                //自动编号
                map.CodeStruct = "4";
                map.IsAutoGenerNo = true;
                map.AddTBStringPK(HRRecordAttr.No, null, "编号", true, true, 1, 40, 70);
                //字段
                map.AddTBString(HRRecordAttr.Name, null, "姓名", true, false, 0, 50, 100);
                map.AddTBDate(HRRecordAttr.InCompanyDT, null, "入职时间", true, false);
                map.AddDDLSysEnum(HRRecordAttr.Gender, 0, "性别", true, true, HRRecordAttr.Gender, "@0=男@1=女");
                map.AddTBDate(HRRecordAttr.BirthDT, null, "出生日期", true, false);
                map.AddTBString(HRRecordAttr.MinZu, "汉族", "民族", true, false, 0, 50, 100);
                map.AddTBString(HRRecordAttr.JiGuan, null, "籍贯", true, false, 0, 200, 100);
                map.AddTBString(HRRecordAttr.HuKouAddr, null, "户口所在地", true, false, 0, 200, 100);
                map.AddTBString(HRRecordAttr.IDCard, null, "身份证号码", true, false, 0, 18, 100);
                map.AddTBDate(HRRecordAttr.JiaRuDT, DateTime.Now.ToString("yyyy-MM-dd"), "创建时间", true, true);
                map.AddDDLSysEnum(HRRecordAttr.ZhuangTai, 0, "在职状态", true, false, HRRecordAttr.ZhuangTai, "@0=在职@1=离职");
                map.AddDDLEntities(HRRecordAttr.FK_Dept, null, "部门", new BP.Port.Depts(), true);
               // map.AddDDLEntities(HRRecordAttr.FK_Duty, null, "职务", new BP.GPM.Dutys(), true);
                map.AddTBString(HRRecordAttr.ZhiCheng, null, "职称", true, false, 0, 100, 100);
                map.AddTBString(HRRecordAttr.CongShiZhuanYe, null, "从事专业", true, false, 0, 100, 100);
                map.AddTBString(HRRecordAttr.WenHuaChengDu, null, "文化程度", true, false, 0, 20, 100);
                map.AddTBString(HRRecordAttr.ZhengZhiMianMao, null, "政治面貌", true, false, 0, 20, 100);
                map.AddTBString(HRRecordAttr.BiYeSchool, null, "毕业院校", true, false, 0, 100, 100);
                map.AddTBString(HRRecordAttr.SuoXueZhuanYe, null, "所学专业", true, false, 0, 100, 100);
                map.AddTBDate(HRRecordAttr.BiYeDT, null, "毕业时间", true, false);
                map.AddTBDate(HRRecordAttr.WorkDT, null, "参加工作时间", true, false);
                map.AddTBString(HRRecordAttr.YuanCompany, null, "原工作单位", true, false, 0, 200, 100);
                map.AddTBStringDoc(HRRecordAttr.Note, null, "备注", true, false, true);

                //查询条件
                //map.AddSearchAttr(HRRecordAttr.FK_Dept);
                RefMethod rm = new RefMethod();
                rm = new RefMethod();
                rm.Title = "工作经历";
                rm.ClassMethodName = this.ToString() + ".DoWorkExperience";
                map.AddRefMethod(rm);
                rm = new RefMethod();
                rm.Title = "签订合同";
                rm.ClassMethodName = this.ToString() + ".DoHRContract";
                map.AddRefMethod(rm);
                rm = new RefMethod();
                rm.Title = "变更合同";
                rm.ClassMethodName = this.ToString() + ".DoHRContractModify";
                map.AddRefMethod(rm);
                //rm = new RefMethod();
                //rm.Title = "离职申请";
                //rm.ClassMethodName = this.ToString() + ".DoHRContractRemove";
                map.AddRefMethod(rm);
                rm = new RefMethod();
                rm.Title = "个人福利";
                rm.ClassMethodName = this.ToString() + ".DoFuLi";
                map.AddRefMethod(rm);
                rm = new RefMethod();
                rm.Title = "公积金";
                rm.ClassMethodName = this.ToString() + ".DoGongJiJin";
                map.AddRefMethod(rm);
                this._enMap = map;
                return this._enMap;
            }
        }
        #endregion

        #region 方法

        /// <summary>
        /// 工作经历
        /// </summary>
        /// <returns></returns>
        public string DoWorkExperience()
        {
            return "/WF/Comm/En.htm?EnsName=BP.OA.HR.JingLis&FK_Name="+this.No;
        }
        /// <summary>
        /// 人员调动
        /// </summary>
        /// <returns></returns>
        public string DoTransfer()
        {
            return "/WF/Comm/En.htm?EnsName=BP.OA.HR.HRTransfers&FK_HRRecord=" + this.No;
        }
        /// <summary>
        /// 变更合同
        /// </summary>
        /// <returns></returns>
        public string DoHRContractModify()
        {
            HRContracts hrs = new HRContracts();
            hrs.RetrieveAll();
            BP.OA.HR.HRContract hrc = new HRContract();
            hrc = (HRContract)hrs.Filter("FK_HRRecord", this.No);
            if (hrc != null)
            {
                return "/WF/Comm/En.htm?EnsName=BP.OA.HR.HRContractModifys&FK_HRContract=" + hrc.No + "&FK_HrRecord=" + hrc.FK_HRRecord
                    + "&OldGuDingQiXian=" + hrc.GuDingQiXian + "&OldStartDT=" + hrc.StartDT + "&OldEndDT=" + hrc.EndDT + "&NewGuDingQiXian=" + hrc.GuDingQiXian + "&Note=" + hrc.Note;
            }
            return "没有签订合同！";
            //else
            //{
            //    return "/Comm/En.htm?EnsName=BP.OA.HR.HRContracts&FK_HRRecord=" + this.No, 800, 800);
            //}
        }
        /// <summary>
        /// 签订合同
        /// </summary>
        /// <returns></returns>
        public string DoHRContract()
        {
            HRContracts hrs = new HRContracts();
            hrs.RetrieveAll();
            BP.OA.HR.HRContract hrc = new HRContract();
            hrc = (HRContract)hrs.Filter("FK_HRRecord", this.No);
            if (hrc != null)
            {
                return "/WF/Comm/En.htm?EnsName=BP.OA.HR.HRContracts&No=" + hrc.No;
            }
            else
            {
                return "/WF/Comm/En.htm?EnsName=BP.OA.HR.HRContracts&FK_HRRecord=" + this.No;
            }
            return null;
        }
        ///// <summary>
        ///// 离职申请
        ///// </summary>
        ///// <returns></returns>
        //public void DoHRContractRemove()
        //{
        //    HRContracts hrs = new HRContracts();
        //    hrs.RetrieveAll();
        //    BP.OA.HR.HRContract hrc = new HRContract();
        //    hrc = (HRContract)hrs.Filter("FK_HRRecord", this.No);
        //    if (hrc == null)
        //    {
        //        BP.Sys.PubClass.Alert("没有签订劳动合同！");
        //        return;
        //    }
        //    if (API.HR_ContractRemove_FlowIsEnable == true)
        //    {
        //        return "/WF/MyFlow.htm?FK_Flow=HRContractRemove", 800, 800);
        //        return;
        //    }
        //    //else
        //    //{
        //    //    return "/Comm/En.htm?EnsName=BP.OA.HR.HRContractRemoves&FK_HRContract=" + hrc.No + "&FK_HrRecord=" + hrc.FK_HRRecord

        //    //          + "&OldGuDingQiXian=" + hrc.GuDingQiXian + "&StartDT=" + hrc.StartDT + "&EndDT=" + hrc.EndDT + "&DengJiDT=" + hrc.DengJiDT, 800, 800);
        //    //}
        //    //HRContracts hrs = new HRContracts();
        //    //hrs.RetrieveAll();
        //    //BP.OA.HR.HRContract hrc = new HRContract();
        //    //hrc = (HRContract)hrs.Filter("FK_HRRecord", this.No);
        //    //return "/WF/MyFlow.htm?FK_Flow=HRContractRemove&FK_HRRecord=" + hrc.No, 800, 800);
        //}
        /// <summary>
        /// 个人福利
        /// </summary>
        /// <returns></returns>
        public string DoFuLi()
        {
            FuLis hrs = new FuLis();
            hrs.RetrieveAll();
            FuLi hrc = (FuLi)hrs.Filter("FK_Name", this.No);

            if (hrc != null)
            {
                return "/WF/Comm/En.htm?EnsName=BP.OA.HR.FuLis&OID=" + hrc.OID;
                return null;
            }
            return "/WF/Comm/En.htm?EnsName=BP.OA.HR.FuLis&FK_Name=" + this.No;
            return null;
        }
        /// <summary>
        /// 公积金
        /// </summary>
        /// <returns></returns>
        public string DoGongJiJin()
        {
            GongJiJins hrs = new GongJiJins();
            hrs.RetrieveAll();
            GongJiJin hrc = (GongJiJin)hrs.Filter("FK_Name", this.No);
            if (hrc != null)
            {
                return "/WF/Comm/En.htm?EnsName=BP.OA.HR.GongJiJins&OID=" + hrc.OID ;
                return null;
            }
            return "/WF/Comm/En.htm?EnsName=BP.OA.HR.GongJiJins&FK_Name=" + this.No;
            return null;
        }
        /// <summary>
        /// 获取集合
        /// </summary>
        public override Entities GetNewEntities
        {
            get { return new HRRecords(); }
        }

        #endregion
    }

    /// <summary>
    /// 人事档案s
    /// </summary>
    public class HRRecords : EntitiesNoName
    {
        #region 构造函数

        public HRRecords()
        {

        }
        #endregion

        #region 重写父类方法
        public override Entity GetNewEntity
        {
            get { return new HRRecord(); }
        }
        #endregion

    }
}
