﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.En;

namespace BP.OA.HR
{
    //OID， FK_合同编号，合同人员， 登记人， 登记时间， 有无固定期限， 合同开始日期， 合同结束日期， 合同解除日期， 备注

    /// <summary>
    /// 劳动合同解除 属性
    /// </summary>
    public class HRContractRemoveAttr : EntityOIDAttr
    {
        #region 基本属性

        /// <summary>
        /// FK_合同编号
        /// </summary>
        public const string FK_HRContract = "FK_HRContract";
        /// <summary>
        /// FK_人事档案编号
        /// </summary>
        public const string FK_HRRecord = "FK_HRRecord";
        /// <summary>
        /// 登记人
        /// </summary>
        public const string DengJiRen = "DengJiRen";
        /// <summary>
        /// 登记时间
        /// </summary>
        public const string DengJiDT = "DengJiDT";
        /// <summary>
        /// 有无固定期限
        /// </summary>
        public const string GuDingQiXian = "GuDingQiXian";
        /// <summary>
        /// 合同开始日期
        /// </summary>
        public const string StartDT = "StartDT";
        /// <summary>
        /// 合同结束日期
        /// </summary>
        public const string EndDT = "EndDT";
        /// <summary>
        /// 合同解除日期
        /// </summary>
        public const string RemoveDT = "RemoveDT";
        /// <summary>
        /// 备注
        /// </summary>
        public const string Note = "Note";

        #endregion
    }

    /// <summary>
    /// 劳动合同解除
    /// </summary>
    public class HRContractRemove : EntityOID
    {
        #region 基本属性
        /// <summary>
        /// FK_合同编号
        /// </summary>
        public string FK_HRContract
        {
            get { return this.GetValStringByKey(HRContractRemoveAttr.FK_HRContract); }
            set { this.SetValByKey(HRContractRemoveAttr.FK_HRContract, value); }
        }
        /// <summary>
        /// FK_人事档案编号
        /// </summary>
        public string FK_HRRecord
        {
            get { return this.GetValStringByKey(HRContractRemoveAttr.FK_HRRecord); }
            set { this.SetValByKey(HRContractRemoveAttr.FK_HRRecord, value); }
        }
        /// <summary>
        /// 登记人
        /// </summary>
        public string DengJiRen
        {
            get { return this.GetValStringByKey(HRContractRemoveAttr.DengJiRen); }
            set { this.SetValByKey(HRContractRemoveAttr.DengJiRen, value); }
        }
        /// <summary>
        /// 登记时间
        /// </summary>
        public string DengJiDT
        {
            get { return this.GetValStringByKey(HRContractRemoveAttr.DengJiDT); }
            set { this.SetValByKey(HRContractRemoveAttr.DengJiDT, value); }
        }
        /// <summary>
        /// 有无固定期限
        /// </summary>
        public string GuDingQiXian
        {
            get { return this.GetValStringByKey(HRContractRemoveAttr.GuDingQiXian); }
            set { this.SetValByKey(HRContractRemoveAttr.GuDingQiXian, value); }
        }
        /// <summary>
        /// 合同开始日期
        /// </summary>
        public string StartDT
        {
            get { return this.GetValStringByKey(HRContractRemoveAttr.StartDT); }
            set { this.SetValByKey(HRContractRemoveAttr.StartDT, value); }
        }
        /// <summary>
        /// 合同结束日期
        /// </summary>
        public string EndDT
        {
            get { return this.GetValStringByKey(HRContractRemoveAttr.EndDT); }
            set { this.SetValByKey(HRContractRemoveAttr.EndDT, value); }
        }
        /// <summary>
        /// 合同解除日期
        /// </summary>
        public string RemoveDT
        {
            get { return this.GetValStringByKey(HRContractRemoveAttr.RemoveDT); }
            set { this.SetValByKey(HRContractRemoveAttr.RemoveDT, value); }
        }
        /// <summary>
        /// 备注
        /// </summary>
        public string Note
        {
            get { return this.GetValStringByKey(HRContractRemoveAttr.Note); }
            set { this.SetValByKey(HRContractRemoveAttr.Note, value); }
        }
        #endregion
        #region 构造方法
        public HRContractRemove()
        {

        }

        public HRContractRemove(int oid)
            : base(oid)
        {

        }
        #endregion

        #region 重写父类的方法
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                {
                    return this._enMap;
                }
                Map map = new Map("OA_HRContractRemove");
                map.EnDesc = "劳动合同解除";
                //编号
                map.AddTBIntPKOID();
                //字段
                map.AddTBString(HRContractRemoveAttr.FK_HRContract, null, "合同编号", true, true, 0, 50, 100);
                map.AddDDLEntities(HRContractRemoveAttr.FK_HRRecord, null, "姓名", new BP.OA.HR.HRRecords(), true);
                map.AddDDLEntities(HRContractRemoveAttr.FK_HRRecord, null, "登记人", new BP.OA.HR.HRRecords(), true);
                map.AddTBDateTime(HRContractRemoveAttr.DengJiDT, null, "登记时间", true, false);
                map.AddDDLSysEnum(HRContractRemoveAttr.GuDingQiXian, 0, "有无固定期限", true, false, HRContractRemoveAttr.GuDingQiXian, "@0=有@1=无");
                map.AddTBDate(HRContractRemoveAttr.StartDT, null, "合同开始日期", true, true);
                map.AddTBDate(HRContractRemoveAttr.EndDT, null, "合同截至日期", true, true);
                map.AddTBDate(HRContractRemoveAttr.RemoveDT, null, "合同解除日期", true, false);
                map.AddTBStringDoc(HRContractRemoveAttr.Note, null, "备注", true, false, true);
                //设置查询条件
                //map.AddSearchAttr(HRContractRemoveAttr.FK_HRContract);
                map.AddSearchAttr(HRContractRemoveAttr.FK_HRRecord);
                this._enMap = map;
                return this._enMap;
            }
        }
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                uac.Readonly();
                return uac;
            }
        }
        protected override void afterInsert()
        {
            HRContract hrc = new HRContract(this.FK_HRContract);
            hrc.Delete();
            base.afterInsert();
        }
        protected override bool beforeUpdateInsertAction()
        {
            //BP.OA.HR.HRRecord emp = new BP.OA.HR.HRRecord(this.FK_HRRecord);
            //BP.OA.HR.HRContracts hrcs = new BP.OA.HR.HRContracts();
            //HRContract hrc = (HRContract)hrcs.Filter("FK_HRRecord", emp.No);
            //this.GuDingQiXian = hrc.GuDingQiXian;
            //this.StartDT = hrc.StartDT;
            //this.EndDT = hrc.EndDT;
            //this.Insert();
            return base.beforeUpdateInsertAction();
        }
        #endregion
    }
    /// <summary>
    /// 劳动合同解除s
    /// </summary>
    public class HRContractRemoves : EntitiesOID
    {
        #region 构造方法

        public HRContractRemoves()
        {

        }
        #endregion

        #region 重写父类的方法
        public override Entity GetNewEntity
        {
            get { return new HRContractRemove(); }
        }
        #endregion
    }
}
