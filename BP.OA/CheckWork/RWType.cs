﻿using System;
using System.Data;
using BP.DA;
using BP.En;
using BP.Web;

namespace BP.OA.CheckWork
{
    /// <summary>
    /// 考勤类型基本信息表
    /// </summary>
    public class RWTypeAttr : EntityNoNameAttr
    {       
        /// <summary>
        /// 规定时间
        /// </summary>
        public const string RegularTime = "RegularTime";
    }
    public class RWType : EntityNoName
    {
        /// <summary>
        /// 规定时间
        /// </summary>
        public string RegularTime
        {
            get { return this.GetValStrByKey(RWTypeAttr.RegularTime); }
            set { this.SetValByKey(RWTypeAttr.RegularTime, value); }
        }
        /// <summary>
        /// Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map();
                map.PhysicsTable = "CW_RWType";
                map.EnDesc = "考勤类型";   // 实体的描述.
               
                //基本信息

                map.AddTBStringPK(RWTypeAttr.No, null, "编号", true, true, 3, 3, 3);
                map.AddTBString(RWTypeAttr.Name, null, "名称", true, false, 0, 300, 30, true);
                map.AddTBString(RWTypeAttr.RegularTime, null, "规定时间", true, false, 0, 300, 30, true);
              
                this._enMap = map;
                return this._enMap;
            }

        }
    }
          /// <summary>
    /// 签到类型
    /// </summary>
    public class RWTypes : EntitiesNoName
    {
        /// <summary>
        /// 签到状态s
        /// </summary>
        public RWTypes() { }
        /// <summary>
        /// 得到它的 Entity 
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new RWType();
            }
        }
    }
}
