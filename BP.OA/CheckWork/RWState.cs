﻿using System;
using System.Collections;
using BP.DA;
using BP.En;


namespace BP.OA.CheckWork
{
    /// <summary>
    /// 签到状态 基本信息表
    /// </summary>
    public class RWStateAttr : EntityNoNameAttr
    {
    }
    /// <summary>
    ///  签到状态
    /// </summary>
    public class RWState : EntityNoName
    {
        #region 属性
        #endregion

        #region 构造方法
        /// <summary>
        /// 签到状态
        /// </summary>
        public RWState()
        {
        }
        /// <summary>
        /// 签到状态
        /// </summary>
        /// <param name="_No"></param>
        public RWState(string _No) : base(_No) { }
        #endregion

        /// <summary>
        /// 签到状态Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;
                Map map = new Map("CW_RWState");
                map.EnDesc = "签到状态";

                map.AddTBStringPK(RWTypeAttr.No, null, "编号", true, true, 3, 3, 3);
                map.AddTBString(RWStateAttr.Name, null, "名称", true, false, 1, 50, 20);

                this._enMap = map;
                return this._enMap;
            }
        }
    }
    /// <summary>
    /// 签到状态
    /// </summary>
    public class RWStates : EntitiesNoName
    {
        /// <summary>
        /// 签到状态s
        /// </summary>
        public RWStates() { }
        /// <summary>
        /// 得到它的 Entity 
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new RWState();
            }
        }
    }
}
