﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace BP.OA.RCSWGL
{
    public class PWGLFEE:BP.WF.FlowEventBase
    {
         #region 构造.
        /// <summary>
        /// 保养 流程事件实体
        /// </summary>
        public PWGLFEE()
        {
        }
        #endregion 构造.

        #region 重写属性.
        public override string FlowMark
        {
            get { return API.RCSWGL_PWGL_FlowMark; }
        }
        #endregion 重写属性.

        #region 重写节点运动事件.
        /// <summary>
        /// 删除后
        /// </summary>
        /// <returns></returns>
        public override string AfterFlowDel()
        {
            return null;
        }
        /// <summary>
        /// 删除前
        /// </summary>
        /// <returns></returns>
        public override string BeforeFlowDel()
        {
            return null;
        }
        /// <summary>
        /// 结束后
        /// </summary>
        /// <returns></returns>
        public override string FlowOverAfter()
        {
            ////把领用信息写入api.
            //BP.OA.DS.API.DailySupplie_Take(this.GetValStr("OA_DSMain"),
            //  this.GetValInt("ShuLiang"),
            //  BP.Web.WebUser.Name,
            //  this.GetValStr("BeiZhu"));

            API.RCSWGL_PWGL(this.GetValStr("SQR"), this.GetValStr("CCRQ"), this.GetValInt("DPLX"),
                 this.GetValStr("MDD"), this.GetValStr("LDSP_Note"), this.GetValStr("DPGLY"),
                this.GetValStr("HBH"),this.GetValInt("JG"),this.GetValStr("QCRQ"),
                this.GetValStr("DPRQ"), this.GetValStr("BZ"), this.GetValStr("SSNY"), this.WorkID);

            

            return "写入成功....";
        }
        /// <summary>
        /// 结束前
        /// </summary>
        /// <returns></returns>
        public override string FlowOverBefore()
        {
            return null;
        }
        #endregion 重写事件，完成业务逻辑.
    }
}
