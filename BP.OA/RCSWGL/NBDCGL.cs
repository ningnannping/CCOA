﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.En;
using BP.OA;
using BP.Port;
using BP.Web;

namespace BP.OA.RCSWGL
{

    /// <summary>
    /// 内部人员订餐管理属性
    /// </summary>
    public class NBDCGLAttr : EntityOIDAttr 
    {
        /// <summary>
        ///上报状态
        /// </summary>
        public const string ZT = "ZT";
        /// <summary>
        /// 部门
        /// </summary>
        public const string FK_Dept = "FK_Dept";
        /// <summary>
        /// 员工
        /// </summary>
        public const string FK_Emp = "FK_Emp";
        /// <summary>
        /// 接待部门
        /// </summary>
        public const string FK_JDBM = "FK_JDBM";
        /// <summary>
        /// 人数
        /// </summary>
        public const string RS = "RS";
        /// <summary>
        /// 午餐
        /// </summary>
        public const string WuC = "WuC";
        /// <summary>
        /// 午餐单价
        /// </summary>
        public const string WuCDJ = "WuCDJ";
        /// <summary>
        /// 晚餐
        /// </summary>
        public const string WanC = "WanC";
        /// <summary>
        /// 晚餐单价
        /// </summary>
        public const string WanCDJ = "WanCDJ";
        /// <summary>
        /// 餐费
        /// </summary>
        public const string CF = "CF";
        /// <summary>
        /// 订餐日期
        /// </summary>
        public const string DCRQ = "DCRQ";
        /// <summary>
        /// 所属年月
        /// </summary>
        public const string FK_NY = "FK_NY";
        /// <summary>
        /// 结算状态
        /// </summary>
        public const string JSZT = "JSZT";

       
    }
    /// <summary>
    /// 订餐管理 
    /// </summary>
    public class NBDCGL : EntityOID
    {
        #region 构造方法
        /// <summary>
        /// 无参构造
        /// </summary>
        public NBDCGL(){}
        /// <summary>
        /// 有参构造
        /// </summary>
        /// <param name="no"></param>
        public NBDCGL(int no) : base(no) { }
        #endregion

        #region 属性
        /// <summary>
        /// 订餐日期
        /// </summary>
        public string DCRQ {
            get {
                return this.GetValStrByKey(NBDCGLAttr.DCRQ);
            }
            set {
                this.SetValByKey(NBDCGLAttr.DCRQ,value);
            }
        }
        /// <summary>
        /// 接待部门
        /// </summary>
        public string FK_JDBM {
            get {
                return this.GetValStrByKey(NBDCGLAttr.FK_JDBM);
            }
            set {
                this.SetValByKey(NBDCGLAttr.FK_JDBM,value);
            }
        }
        /// <summary>
        /// 人数
        /// </summary>
        public decimal RS
        {
            get
            {
                return this.GetValDecimalByKey(NBDCGLAttr.RS);
            }
            set
            {
                this.SetValByKey(NBDCGLAttr.RS, value);
            }
        }
        /// <summary>
        /// 午餐单价
        /// </summary>
        public decimal WuCDJ
        {
            get
            {
                return this.GetValDecimalByKey(NBDCGLAttr.WuCDJ);
            }
            set
            {
                this.SetValByKey(NBDCGLAttr.WuCDJ, value);
            }
        }
        /// <summary>
        /// 晚餐单价
        /// </summary>
        public decimal WanCDJ
        {
            get
            {
                return this.GetValDecimalByKey(NBDCGLAttr.WanCDJ);
            }
            set
            {
                this.SetValByKey(NBDCGLAttr.WanCDJ, value);
            }
        }
        /// <summary>
        /// 餐费
        /// </summary>
        public decimal CF
        {
            get
            {
                return this.GetValDecimalByKey(NBDCGLAttr.CF);
            }
            set
            {
                this.SetValByKey(NBDCGLAttr.CF, value);
            }
        }




        #endregion

        #region 枚举属性
        /// <summary>
        /// 结算状态属性
        /// </summary>
        public int JSZT {
            get {
                return this.GetValIntByKey(NBDCGLAttr.JSZT);
            }
            set {
                this.SetValByKey(NBDCGLAttr.JSZT,value);
            }
        }
        /// <summary>
        /// 结算状态的名称
        /// </summary>
        public string JSZTText{
            get {
                return this.GetValRefTextByKey(NBDCGLAttr.JSZT);
            }
        }
        /// <summary>
        /// 午餐 属性
        /// </summary>
        public int WuC
        {
            get
            {
                return this.GetValIntByKey(NBDCGLAttr.WuC);
            }
            set
            {
                this.SetValByKey(NBDCGLAttr.WuC, value);
            }
        }
        /// <summary>
        /// 午餐名称
        /// </summary>
        public string WuCText
        {
            get
            {
                return this.GetValRefTextByKey(NBDCGLAttr.WuC);
            }
        }
        /// <summary>
        /// 晚餐 属性
        /// </summary>
        public int WanC
        {
            get
            {
                return this.GetValIntByKey(NBDCGLAttr.WanC);
            }
            set
            {
                this.SetValByKey(NBDCGLAttr.WanC, value);
            }
        }
        /// <summary>
        /// 晚餐名称
        /// </summary>
        public string WanCText
        {
            get
            {
                return this.GetValRefTextByKey(NBDCGLAttr.WanC);
            }
        }
        #endregion

        #region  外键属性
        /// <summary>
        /// 部门
        /// </summary>
        public string FK_Dept {
            get {
                return this.GetValStrByKey(NBDCGLAttr.FK_Dept);
            }
            set {
                this.SetValByKey(NBDCGLAttr.FK_Dept,value);
            }
        }
        /// <summary>
        /// 人员
        /// </summary>
        public string FK_Emp {
            get {
                return this.GetValStrByKey(NBDCGLAttr.FK_Emp);
            }
            set {
                this.SetValByKey(NBDCGLAttr.FK_Emp,value);
            }
        }
        /// <summary>
        /// 所属年月
        /// </summary>
        public string FK_NY
        {
            get
            {
                return this.GetValStrByKey(NBDCGLAttr.FK_NY);
            }
            set
            {
                this.SetValByKey(NBDCGLAttr.FK_NY, value);
            }
        }
        #endregion

        public override Map EnMap
        {
            get {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map("OA_DCGL");
                map.EnDesc = "内部人员订餐管理";

                FK_Dept = BP.Web.WebUser.FK_Dept;
                FK_Emp = BP.Web.WebUser.No;

                map.DepositaryOfEntity = BP.DA.Depositary.None;//存储位置
                map.DepositaryOfMap = BP.DA.Depositary.None;

               //增加OID主键字段。
                map.AddTBIntPKOID();

                map.AddDDLEntities(NBDCGLAttr.FK_Dept, null, "部门",new Depts(),true);
                map.AddDDLEntities(NBDCGLAttr.FK_Emp, null, "人员", new Emps(), true);

                map.AddDDLSysEnum(NBDCGLAttr.WuC, 0, "午餐", true, true, NBDCGLAttr.WuC, "@0=未定@1=已定");
                map.AddTBDecimal(NBDCGLAttr.WuCDJ, 0, "午餐单价", true, false);
                map.AddDDLSysEnum(NBDCGLAttr.WanC, 0, "晚餐", true, true, NBDCGLAttr.WanC, "@0=未定@1=已定");
                map.AddTBDecimal(NBDCGLAttr.WanCDJ, 0, "晚餐单价", true, false);
                map.AddTBDecimal(NBDCGLAttr.CF, 0, "餐费", true, true);

                map.AddTBDate(NBDCGLAttr.DCRQ,null,"订餐日期",true,true);
                map.AddDDLEntities(NBDCGLAttr.FK_NY,null,"所属年月",new Pub.NYs(),true);
               
                map.AddDDLSysEnum(NBDCGLAttr.JSZT, 0, "结算状态", true, true, NBDCGLAttr.JSZT,"@0=未结算@1=已结算");

                
                

                ///设置查询条件
                map.AddSearchAttr(NBDCGLAttr.FK_Dept);
                map.AddSearchAttr(NBDCGLAttr.FK_Emp);
                map.AddSearchAttr(NBDCGLAttr.FK_NY);

                this._enMap = map;
                return this._enMap;

            }
        }


        #region 重写方法
        protected override bool beforeInsert()
        {
            this.DCRQ = DateTime.Now.ToString();
            return base.beforeInsert();
        }
        protected override bool beforeUpdateInsertAction()
        {
            this.DCRQ = DateTime.Now.ToString();

            decimal temp1 = Decimal.Add(this.WanCDJ, this.WuCDJ);
            this.CF = temp1;

            return base.beforeUpdateInsertAction();
        }
        protected override bool beforeUpdate()
        {
            this.DCRQ = DateTime.Now.ToString();
            return base.beforeUpdate();
        }
        protected override void afterInsertUpdateAction()
        {
            this.DCRQ = DateTime.Now.ToString();
            
            //WLRYSB wlsb = new WLRYSB();
            //NBDCGL DC = new NBDCGL();
            /////将外来人员的上报表中的信息同步到订餐管理的表中
            //DC.DCRQ = wlsb.WDCRQ;
            //DC.FK_JDBM = wlsb.FK_JDBM;
            //DC.RS = wlsb.RS;
            //DC.WuC = wlsb.WC;
            //DC.WanC = wlsb.WANC;

            //DC.Insert();

            base.afterInsertUpdateAction();
        }
        #endregion


    }

    /// <summary>
    /// 订餐管理 entity
    /// </summary>
    public class NBDCGLs : SimpleNoNames
    {
        
        /// <summary>
        /// 订餐管理s
        /// </summary>
        public NBDCGLs() { }
       

        /// <summary>
        /// 得到它的entity
        /// </summary>
        public override Entity GetNewEntity
        {
            get {
                return new NBDCGL();
            }
        }


    }
}
