﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.En;
using BP.OA;
using BP.Port;
using BP.Web;

namespace BP.OA.RCSWGL
{

    /// <summary>
    /// 上报状态
    /// </summary>
    public enum ZT
    {
        /// <summary>
        /// 已上报
        /// </summary>
        YSB,
        /// <summary>
        /// 未上报
        /// </summary>
        WSB
    }
    /// <summary>
    /// 结算状态
    /// </summary>
    public enum JSZT { 
        /// <summary>
        /// 已结算
        /// </summary>
        YJS,
        /// <summary>
        /// 未结算
        /// </summary>
        WJS
    }
    /// <summary>
    /// 午餐
    /// </summary>
    public enum WuC
    {
        /// <summary>
        /// 未定
        /// </summary>
        WD,
        /// <summary>
        /// 已定
        /// </summary>
        YD
    }
    /// <summary>
    /// 晚餐
    /// </summary>
    public enum WanC
    {
        /// <summary>
        /// 未定
        /// </summary>
        WD,
        /// <summary>
        /// 已定
        /// </summary>
        YD
    }
    /// <summary>
    /// 订餐管理属性
    /// </summary>
    public class DCGLAttr : EntityOIDAttr 
    {
        /// <summary>
        ///上报状态
        /// </summary>
        public const string ZT = "ZT";
        /// <summary>
        /// 部门
        /// </summary>
        public const string FK_Dept = "FK_Dept";
        /// <summary>
        /// 员工
        /// </summary>
        public const string FK_Emp = "FK_Emp";
        /// <summary>
        /// 接待部门
        /// </summary>
        public const string FK_JDBM = "FK_JDBM";
        /// <summary>
        /// 人数
        /// </summary>
        public const string RS = "RS";
        /// <summary>
        /// 午餐
        /// </summary>
        public const string WuC = "WuC";
        /// <summary>
        /// 午餐单价
        /// </summary>
        public const string WuCDJ = "WuCDJ";
        /// <summary>
        /// 晚餐
        /// </summary>
        public const string WanC = "WanC";
        /// <summary>
        /// 晚餐单价
        /// </summary>
        public const string WanCDJ = "WanCDJ";
        /// <summary>
        /// 餐费
        /// </summary>
        public const string CF = "CF";
        /// <summary>
        /// 订餐日期
        /// </summary>
        public const string DCRQ = "DCRQ";
        /// <summary>
        /// 所属年月
        /// </summary>
        public const string FK_NY = "FK_NY";
        /// <summary>
        /// 结算状态
        /// </summary>
        public const string JSZT = "JSZT";

        public const string WorkId = "WorkId";
    }
    /// <summary>
    /// 订餐管理 
    /// </summary>
    public class DCGL : EntityOID
    {
        #region 构造方法
        /// <summary>
        /// 无参构造
        /// </summary>
        public DCGL(){}
        /// <summary>
        /// 有参构造
        /// </summary>
        /// <param name="no"></param>
        public DCGL(int no) : base(no) { }
        #endregion

        #region 属性
        /// <summary>
        /// 订餐日期
        /// </summary>
        public string DCRQ {
            get {
                return this.GetValStrByKey(DCGLAttr.DCRQ);
            }
            set {
                this.SetValByKey(DCGLAttr.DCRQ,value);
            }
        }
        /// <summary>
        /// 接待部门
        /// </summary>
        public string FK_JDBM {
            get {
                return this.GetValStrByKey(DCGLAttr.FK_JDBM);
            }
            set {
                this.SetValByKey(DCGLAttr.FK_JDBM,value);
            }
        }
        /// <summary>
        /// 人数
        /// </summary>
        public decimal RS
        {
            get
            {
                return this.GetValDecimalByKey(DCGLAttr.RS);
            }
            set
            {
                this.SetValByKey(DCGLAttr.RS, value);
            }
        }
        /// <summary>
        /// 午餐单价
        /// </summary>
        public decimal WuCDJ
        {
            get
            {
                return this.GetValDecimalByKey(DCGLAttr.WuCDJ);
            }
            set
            {
                this.SetValByKey(DCGLAttr.WuCDJ, value);
            }
        }
        /// <summary>
        /// 晚餐单价
        /// </summary>
        public decimal WanCDJ
        {
            get
            {
                return this.GetValDecimalByKey(DCGLAttr.WanCDJ);
            }
            set
            {
                this.SetValByKey(DCGLAttr.WanCDJ, value);
            }
        }
        /// <summary>
        /// 餐费
        /// </summary>
        public decimal CF
        {
            get
            {
                return this.GetValDecimalByKey(DCGLAttr.CF);
            }
            set
            {
                this.SetValByKey(DCGLAttr.CF, value);
            }
        }




        #endregion

        #region 枚举属性
        /// <summary>
        /// 结算状态属性
        /// </summary>
        public int JSZT {
            get {
                return this.GetValIntByKey(DCGLAttr.JSZT);
            }
            set {
                this.SetValByKey(DCGLAttr.JSZT,value);
            }
        }
        /// <summary>
        /// 结算状态的名称
        /// </summary>
        public string JSZTText{
            get {
                return this.GetValRefTextByKey(DCGLAttr.JSZT);
            }
        }
        /// <summary>
        /// 午餐 属性
        /// </summary>
        public int WuC
        {
            get
            {
                return this.GetValIntByKey(DCGLAttr.WuC);
            }
            set
            {
                this.SetValByKey(DCGLAttr.WuC, value);
            }
        }
        /// <summary>
        /// 午餐名称
        /// </summary>
        public string WuCText
        {
            get
            {
                return this.GetValRefTextByKey(DCGLAttr.WuC);
            }
        }
        /// <summary>
        /// 晚餐 属性
        /// </summary>
        public int WanC
        {
            get
            {
                return this.GetValIntByKey(DCGLAttr.WanC);
            }
            set
            {
                this.SetValByKey(DCGLAttr.WanC, value);
            }
        }
        /// <summary>
        /// 晚餐名称
        /// </summary>
        public string WanCText
        {
            get
            {
                return this.GetValRefTextByKey(DCGLAttr.WanC);
            }
        }
        #endregion

        #region  外键属性
        /// <summary>
        /// 部门
        /// </summary>
        public string FK_Dept {
            get {
                return this.GetValStrByKey(DCGLAttr.FK_Dept);
            }
            set {
                this.SetValByKey(DCGLAttr.FK_Dept,value);
            }
        }
        /// <summary>
        /// 人员
        /// </summary>
        public string FK_Emp {
            get {
                return this.GetValStrByKey(DCGLAttr.FK_Emp);
            }
            set {
                this.SetValByKey(DCGLAttr.FK_Emp,value);
            }
        }
        /// <summary>
        /// 所属年月
        /// </summary>
        public string FK_NY
        {
            get
            {
                return this.GetValStrByKey(DCGLAttr.FK_NY);
            }
            set
            {
                this.SetValByKey(DCGLAttr.FK_NY, value);
            }
        }
        #endregion

        public override Map EnMap
        {
            get {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map("OA_DCGL");
                map.EnDesc = "订餐管理";

                FK_Dept = BP.Web.WebUser.FK_Dept;
                FK_Emp = BP.Web.WebUser.No;

                map.DepositaryOfEntity = BP.DA.Depositary.None;//存储位置
                map.DepositaryOfMap = BP.DA.Depositary.None;

               //增加OID主键字段。
                map.AddTBIntPKOID();

                //map.AddTBStringPK(DCGLAttr.No,null,"编号",true,false,0,100,20);

                map.AddDDLEntities(DCGLAttr.FK_Dept, null, "部门",new Depts(),true);
                map.AddDDLEntities(DCGLAttr.FK_Emp, null, "人员", new Emps(), true);

                map.AddTBString(DCGLAttr.FK_JDBM, null, "接待部门", true, false, 0, 100, 20);
                map.AddTBString(DCGLAttr.RS, null, "人数", true, false, 0, 100, 20);

                map.AddDDLSysEnum(DCGLAttr.WuC, 0, "午餐", true, true, DCGLAttr.WuC, "@0=未定@1=已定");
                map.AddTBDecimal(DCGLAttr.WuCDJ, 0, "午餐单价", true, false);
                map.AddDDLSysEnum(DCGLAttr.WanC, 0, "晚餐", true, true, DCGLAttr.WanC, "@0=未定@1=已定");
                map.AddTBDecimal(DCGLAttr.WanCDJ, 0, "晚餐单价", true, false);
                map.AddTBDecimal(DCGLAttr.CF, 0, "餐费", true, true);

                map.AddTBDate(DCGLAttr.DCRQ,null,"订餐日期",true,true);
                map.AddDDLEntities(DCGLAttr.FK_NY,null,"所属年月",new Pub.NYs(),true);
                map.AddDDLSysEnum(DCGLAttr.ZT, 0, "上报状态", true, true, DCGLAttr.ZT, "@0=未结算@1=已结算");
                map.AddDDLSysEnum(DCGLAttr.JSZT, 0, "结算状态", true, true, DCGLAttr.JSZT,"@0=未结算@1=已结算");


                ///设置查询条件
                map.AddSearchAttr(DCGLAttr.FK_Dept);
                map.AddSearchAttr(DCGLAttr.FK_Emp);
                map.AddSearchAttr(DCGLAttr.FK_NY);

                this._enMap = map;
                return this._enMap;

            }
        }

       

        #region 重写方法
        protected override bool beforeInsert()
        {
            this.DCRQ = DateTime.Now.ToString();
            return base.beforeInsert();
        }
        protected override bool beforeUpdateInsertAction()
        {
            this.DCRQ = DateTime.Now.ToString();
            return base.beforeUpdateInsertAction();
        }
        protected override bool beforeUpdate()
        {
            this.DCRQ = DateTime.Now.ToString();
            return base.beforeUpdate();
        }
        protected override void afterInsertUpdateAction()
        {
            this.DCRQ = DateTime.Now.ToString();
            

            base.afterInsertUpdateAction();
        }
        #endregion


    }

    /// <summary>
    /// 订餐管理 entity
    /// </summary>
    public class DCGLs : SimpleNoNames
    {
        
        /// <summary>
        /// 订餐管理s
        /// </summary>
        public DCGLs() { }
       

        /// <summary>
        /// 得到它的entity
        /// </summary>
        public override Entity GetNewEntity
        {
            get {
                return new DCGL();
            }
        }


    }
}
