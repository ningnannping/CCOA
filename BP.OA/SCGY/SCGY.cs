﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.En;
using BP.OA;
using BP.Port;
using BP.Web;

namespace BP.OA
{
    /// <summary>
    /// 流程状态
    /// </summary>
    public enum LCZT { 
        /// <summary>
        /// 申请
        /// </summary>
        SQ,
        /// <summary>
        /// 流程中
        /// </summary>
        LCZ,
        /// <summary>
        /// 流程结束
        /// </summary>
        LCJS
    }

    /// <summary>
    /// 生产工艺的属性
    /// </summary>
    public class SCGYAttr : EntityOIDAttr {
        /// <summary>
        /// 标题
        /// </summary>
        public const string BT = "BT";
        /// <summary>
        /// 请求人
        /// </summary>
        public const string QQR = "QQR";
        /// <summary>
        /// 分公司
        /// </summary>
        public const string FK_FGS = "OP_Dept";
        /// <summary>
        /// 流程状态
        /// </summary>
        public const string LCZT = "LCZT";

        public const string WFState = "WFState";

        public const string FlowEndNode = "FlowEndNode";

        public const string WorkId = "WorkId";
    }

  /// <summary>
    /// 生产工艺
    /// </summary>
   public class SCGY:EntityOID
   {
       #region 属性
       /// <summary>
       /// 标题
       /// </summary>
       public string BT
       {
           get
           {
               return this.GetValStrByKey(SCGYAttr.BT);
           }
           set
           {
               this.SetValByKey(SCGYAttr.BT, value);
           }
       }
       /// <summary>
       /// 请求人
       /// </summary>
       public string QQR
       {
           get
           {
               return this.GetValStrByKey(SCGYAttr.QQR);
           }
           set
           {
               this.SetValByKey(SCGYAttr.QQR, value);
           }
       }
       /// <summary>
       /// WorkId
       /// </summary>
       public Int64 WorkId
       {
           get
           {
               return this.GetValInt64ByKey(SCGYAttr.WorkId);
           }
           set
           {
               this.SetValByKey(SCGYAttr.WorkId, value);
           }
       }
       /// <summary>
       /// WFState
       /// </summary>
       public int WFState {
           get {
               return this.GetValIntByKey(SCGYAttr.WFState);
           }
           set {
               this.SetValByKey(SCGYAttr.WFState,value);
           }
       }

       public string FlowEndNode
       {
           get
           {
               return this.GetValStrByKey(SCGYAttr.FlowEndNode);
           }
           set
           {
               this.SetValByKey(SCGYAttr.FlowEndNode, value);
           }
       }


       #endregion

       #region 构造方法
       public SCGY() { }
       #endregion

       #region 枚举属性
       /// <summary>
       /// 流程状态
       /// </summary>
       public int LCZT
       {
           get
           {
               return this.GetValIntByKey(SCGYAttr.LCZT);
           }
           set
           {
               this.SetValByKey(SCGYAttr.LCZT, value);
           }
       }
       /// <summary>
       /// 流程状态
       /// </summary>
       public string LCZTText
       {
           get
           {
               return this.GetValRefTextByKey(SCGYAttr.LCZT);
           }
       }
       #endregion

       #region 外键属性
       /// <summary>
       /// 分公司
       /// </summary>
       public string FK_FGS
       {
           get
           {
               return this.GetValStrByKey(SCGYAttr.FK_FGS);
           }
           set
           {
               this.SetValByKey(SCGYAttr.FK_FGS, value);
           }
       }
       #endregion


       public override Map EnMap
       {
           get {
               if (this._enMap != null)
                   return this._enMap;

               Map map = new Map("ND29Rpt");
               map.EnDesc = "生产工艺";

               map.DepositaryOfEntity = BP.DA.Depositary.None;
               map.DepositaryOfMap = BP.DA.Depositary.None;

               map.AddTBIntPKOID();

               map.AddTBString(SCGYAttr.BT,null,"标题",true,false,0,100,20);
               map.AddTBString(SCGYAttr.QQR, null, "请求人", true, false, 0, 100, 20);
               map.AddDDLEntities(SCGYAttr.FK_FGS, null, "分公司",new OP_Depts(), true);
               map.AddTBString(SCGYAttr.WFState,null,"",false,true,0,100,30);
               map.AddTBString(SCGYAttr.FlowEndNode,null,"",false,true,0,100,30);
               
               map.AddDDLSysEnum(SCGYAttr.LCZT, 0, "流程状态", true, true, SCGYAttr.LCZT, "@0=申请@1=流程中@2=流程结束");
               //查询条件
               map.AddSearchAttr(SCGYAttr.LCZT);

               this._enMap = map;
               return this._enMap;



           }
       }

         #region 重写方法
       protected override bool beforeInsert()
       {
            return base.beforeInsert();
       }
       protected override bool beforeUpdate()
       {
            return base.beforeUpdate();
       }
       protected override bool beforeUpdateInsertAction()
       {
           //if(this.WFState.Equals("3")){
           //    // 流程结束
           //    this.LCZT = (int)BP.OA.LCZT.LCJS;
           //}else if (this.WFState.Equals("2") && this.FlowEndNode.Equals("2902"))
           //{
           //    // 申请
           //    this.LCZT = (int)BP.OA.LCZT.SQ;
           //}else {
           //    // 流程中
           //    this.LCZT = (int)BP.OA.LCZT.LCZ;
           //}
           return base.beforeUpdateInsertAction();
       }
       protected override void afterInsertUpdateAction()
       {


           base.afterInsertUpdateAction();
       }
       #endregion
    }
    /// <summary>
    /// 生产工艺 s
    /// </summary>
   public class SCGYs : EntitiesOID
   {
       #region 构造方法
       public SCGYs() { }
       #endregion
       /// <summary>
       /// 得到它的entity
       /// </summary>
       public override Entity GetNewEntity
       {
           get {
               return new SCGY();
           }
       }
   }
}
