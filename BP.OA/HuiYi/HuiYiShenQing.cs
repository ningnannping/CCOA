﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.DA;
using BP.En;
using BP.Port;
namespace BP.OA.HuiYi
{

    public enum SFWX
    {
        //维修中
        S,
        //不在维修中
        N

    }
    public class HuiYiShenQingAttr : EntityNoNameAttr
    {
        /// <summary>
        /// 会议主题
        /// </summary>
        public const string HYZT = "HYZT";
        /// <summary>
        /// 会议类型
        /// </summary>
        public const string HYLX = "HYLX";
        /// <summary>
        ///申请部门
        /// </summary>
        public const string SQBM = "SQBM";
        /// <summary>
        ///发起人
        /// </summary>
        public const string FQR = "FQR";
        /// <summary>
        ///记录人
        /// </summary>
        public const string JLR = "JLR";
        /// <summary>
        /// 登记日期
        /// </summary>
        public const string DJRQ = "DJRQ";
        /// <summary>
        /// 参会人数
        /// </summary>
        public const string CHRS = "CHRS";
        /// <summary>
        /// 起始时间
        /// </summary>
        public const string QSSJ = "QSSJ";
        /// <summary>
        /// 终止时间
        /// </summary>
        public const string ZZSJ = "ZZSJ";
        ///// <summary>
        ///// 会议室
        ///// </summary>
        //public const string HYS = "HYS";
        /// <summary>
        /// 布置要求
        /// </summary>
        public const string BZYQ = "BZYQ";
        /// <summary>
        /// 布置状态
        /// </summary>
        public const string BZZT = "BZZT";

        /// <summary>
        /// 会议结论
        /// </summary>
        public const string HYJL = "HYJL";
        /// <summary>
        /// 备注
        /// </summary>
        public const string BZ = "BZ";
      
       
        /// <summary>
        /// WorkID
        /// </summary>
        public const string WorkID = "WorkID";
        /// <summary>
        /// 会议室外键
        /// </summary>
        public const string FK_HuiYiShi = "FK_HuiYiShi";
    }



    /// <summary>
    /// 会议申请实体类
    /// </summary>
    public class HuiYiShenQing : EntityNoName
    {
        #region 属性
       

        /// <summary>
        /// 会议类型
        /// </summary>
        public string HYLX
        {
            get
            {
                return this.GetValStrByKey(HuiYiShenQingAttr.HYLX);
            }
            set
            {
                this.SetValByKey(HuiYiShenQingAttr.HYLX, value);
            }
        }
        public Int64 WorkID
        {
            get
            {
                return this.GetValInt64ByKey(HuiYiShenQingAttr.WorkID);
            }
            set
            {
                this.SetValByKey(HuiYiShenQingAttr.WorkID, value);
            }
        }
        /// <summary>
        /// 会议主题
        /// </summary>

        public string HYZT
        {
            get
            {
                return this.GetValStrByKey(HuiYiShenQingAttr.HYZT);
            }
            set
            {
                this.SetValByKey(HuiYiShenQingAttr.HYZT, value);
            }
        }

        /// <summary>
        /// 外键
        /// </summary>

        public string FK_HuiYiShi
        {
            get
            {
                return this.GetValStrByKey(HuiYiShenQingAttr.FK_HuiYiShi);
            }
            set
            {
                this.SetValByKey(HuiYiShenQingAttr.FK_HuiYiShi, value);
            }
        }

        /// <summary>
        /// 申请部门
        /// </summary>
        public string SQBM
        {
            get
            {
                return this.GetValStrByKey(HuiYiShenQingAttr.SQBM);
            }
            set
            {
                this.SetValByKey(HuiYiShenQingAttr.SQBM, value);
            }
        }
        /// <summary>
        /// 发起人
        /// </summary>
        public string FQR
        {
            get
            {
                return this.GetValStrByKey(HuiYiShenQingAttr.FQR);
            }
            set
            {
                this.SetValByKey(HuiYiShenQingAttr.FQR, value);
            }
        }
        /// <summary>
        /// 记录人
        /// </summary>
        public string JLR
        {
            get
            {
                return this.GetValStrByKey(HuiYiShenQingAttr.JLR);
            }
            set
            {
                this.SetValByKey(HuiYiShenQingAttr.JLR, value);
            }
        }
        /// <summary>
        /// 登记日期
        /// </summary>
        public string DJRQ
        {
            get
            {
                return this.GetValStrByKey(HuiYiShenQingAttr.DJRQ);
            }
            set
            {
                this.SetValByKey(HuiYiShenQingAttr.DJRQ, value);
            }
        }
        /// <summary>
        /// 参会人数
        /// </summary>
        public decimal CHRS
        {
            get
            {
                return this.GetValDecimalByKey(HuiYiShenQingAttr.CHRS);
            }
            set
            {
                this.SetValByKey(HuiYiShenQingAttr.CHRS, value);
            }
        }

        /// <summary>
        ///   起始时间
        /// </summary>
        public string  QSSJ
        {
            get
            {
                return this.GetValStrByKey(HuiYiShenQingAttr.QSSJ);
            }
            set
            {
                this.SetValByKey(HuiYiShenQingAttr.QSSJ, value);
            }
        }

        /// <summary>
        ///   结束时间
        /// </summary>
        public string  ZZSJ
        {
            get
            {
                return this.GetValStrByKey(HuiYiShenQingAttr.ZZSJ);
            }
            set
            {
                this.SetValByKey(HuiYiShenQingAttr.ZZSJ, value);
            }
        }
        ///// <summary>
        /////   会议室
        ///// </summary>
        //public string HYS
        //{
        //    get
        //    {
        //        return this.GetValStrByKey(HuiYiShenQingAttr.HYS);
        //    }
        //    set
        //    {
        //        this.SetValByKey(HuiYiShenQingAttr.HYS, value);
        //    }
        //}
        /// <summary>
        ///  布置要求
        /// </summary>
        public string BZYQ
        {
            get
            {
                return this.GetValStrByKey(HuiYiShenQingAttr.BZYQ);
            }
            set
            {
                this.SetValByKey(HuiYiShenQingAttr.BZYQ, value);
            }
        }

        /// <summary>
        /// 布置状态
        /// </summary>
        public string BZZT
        {
            get
            {
                return this.GetValStrByKey(HuiYiShenQingAttr.BZZT);
            }
            set
            {
                this.SetValByKey(HuiYiShenQingAttr.BZZT, value);
            }
        }
        /// <summary>
        /// 会议结论
        /// </summary>
        public string HYJL
        {
            get
            {
                return this.GetValStrByKey(HuiYiShenQingAttr.HYJL);
            }
            set
            {
                this.SetValByKey(HuiYiShenQingAttr.HYJL, value);
            }
        }
        /// <summary>
        /// 备注
        /// </summary>
        public string BZ
        {
            get
            {
                return this.GetValStrByKey(HuiYiShenQingAttr.BZ);
            }
            set
            {
                this.SetValByKey(HuiYiShenQingAttr.BZ, value);
            }
        }
       
        
        #endregion 属性
        #region 权限控制属性.

        #endregion 权限控制属性.

        #region 构造方法
        /// <summary>
        /// 投标信息
        /// </summary>
        public HuiYiShenQing()
        {
        }
        /// <summary>
        /// 投标信息
        /// </summary>
        /// <param name="_No"></param>
        public HuiYiShenQing(string oid) : base(oid) { }
        #endregion

        #region 重写方法
        protected override bool beforeInsert()
        {

            return base.beforeInsert();
        }

        protected override bool beforeUpdate()
        {

            return base.beforeUpdate();
        }
       
        


        #endregion 重写方法

        /// <summary>
        /// 投标信息Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map("OA_HuiYiShiShenQing");
                map.EnDesc = "会议室申请记录";
                //map.IsAutoGenerNo = true;
                map.CodeStruct = "3"; //三位编号001开始
                map.AddTBStringPK(HuiYiShenQingAttr.No, null, "编号", false, true, 0, 100, 40);
              //  map.AddDDLEntities(HuiYiShenQingAttr.FK_HT, null, "合同编号", new HeTongDengJis(), true);
               // map.AddTBString(HuiYiShenQingAttr.Name, null, "会议室编号", false, true, 0, 100, 30);
                map.AddTBString(HuiYiShenQingAttr.Name, null, "会议室", false, false, 0, 100, 40);
                map.AddDDLEntities(HuiYiShenQingAttr.FK_HuiYiShi,null,"会议室",new HuiYiShis(),true);
               //// map.AddTBString(HuiYiShenQingAttr.GLBM, null, "管理部门", true, false, 0, 100, 30);
               //// map.AddTBString(HuiYiShenQingAttr.FZR, null, "负责人", true, false, 0, 100, 30);
               //// map.AddTBDecimal(HuiYiShenQingAttr.RNRS, 0, "容纳人数", true, false);
               ////// map.AddDDLSysEnum(HuiYiShenQingAttr.SFWX, 0, "是否维修", true, true, HuiYiShenQingAttr.SFWX, "@0=是@1=否");
               //// map.AddTBString(HuiYiShenQingAttr.FWMJ, null, "房屋面积", true, false, 0, 100, 40);
               //// map.AddTBString(HuiYiShenQingAttr.SS, null, "设施", true, false, 0, 100, 40);
                //map.AddDDLSysEnum(HuiYiShenQingAttr.HYLX, 0, "类型", true, true, HuiYiShenQingAttr.HYLX, "@0=实体@1=视频");
                map.AddTBStringDoc(HuiYiShenQingAttr.HYZT, null, "会议主题", true, false, true);
                map.AddTBString(HuiYiShenQingAttr.HYLX, null, "会议类型", true, false, 0, 100, 30);
                map.AddTBString(HuiYiShenQingAttr.SQBM, null, "申请部门", true, false, 0, 100, 30);
                map.AddTBString(HuiYiShenQingAttr.FQR, null, "发起人", true, false, 0, 100, 30);
                map.AddTBString(HuiYiShenQingAttr.JLR, null, "记录人", true, false, 0, 100, 30);
                //map.AddTBString(HuiYiShenQingAttr.HYS, null, "会议室", true, false, 0, 100, 30);
                map.AddTBDate(HuiYiShenQingAttr.DJRQ, "登记日期", true, false);

                //map.AddTBString(HuiYiShenQingAttr.CHRS, null, "参会人数", false, true, 0, 100, 30);
                map.AddTBDecimal(HuiYiShenQingAttr.CHRS, 0, "参会人数", true, false);
                map.AddTBDateTime(HuiYiShenQingAttr.QSSJ, "起始时间", true, false);
                map.AddTBDateTime(HuiYiShenQingAttr.ZZSJ, "终止时间", true, false);
               
                map.AddTBString(HuiYiShenQingAttr.BZYQ, null, "布置要求", true, false, 0, 100, 40);
                map.AddTBString(HuiYiShenQingAttr.BZZT, null, "布置状态", true, false, 0, 100, 40);
                map.AddTBStringDoc(HuiYiShenQingAttr.HYJL, null, "会议结论", true, false,true);
              
                map.AddTBStringDoc(HuiYiShenQingAttr.BZ, null, "备注", true, false, true);

                map.AddTBInt(WeiXiuAttr.isWeiXiu, 0, "是否是维修", false, true);
               
                this._enMap = map;
                return this._enMap;
            }
        }
        



    }
    /// <summary>
    /// 收据信息
    /// </summary>
    public class HuiYiShenQings : SimpleNoNames
    {
        /// <summary>
        /// 收据信息s
        /// </summary>
        public HuiYiShenQings() { }
        /// <summary>
        /// 得到它的 Entity 
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new HuiYiShenQing();
            }
        }
    }
}

