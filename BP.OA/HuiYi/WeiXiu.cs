﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.DA;
using BP.En;
using BP.Port;
namespace BP.OA.HuiYi
{

   
    public class WeiXiuAttr : EntityNoNameAttr
    {
        /// <summary>
        /// 会议室
        /// </summary>
        public const string HYS = "HYS";
        /// <summary>
        /// 维修开始时间
        /// </summary>
        public const string QSSJ = "QSSJ";
        /// <summary>
        ///维修结束时间
        /// </summary>
        public const string ZZSJ = "ZZSJ";
       
        /// <summary>
        /// 备注
        /// </summary>
        public const string BZ = "BZ";

        public const string isWeiXiu = "isWeiXiu";
        /// <summary>
        /// 外键
        /// </summary>
        public const string FK_HuiYiShi = "FK_HuiYiShi";
    }



    /// <summary>
    /// 维修实体类
    /// </summary>
    public class WeiXiu : EntityNoName
    {
        #region 属性
      
        /// <summary>
        /// 会议室
        /// </summary>
        public string HYS
        {
            get
            {
                return this.GetValStrByKey(WeiXiuAttr.HYS);
            }
            set
            {
                this.SetValByKey(WeiXiuAttr.HYS, value);
            }
        }
       
      
        /// <summary>
        /// 维修开始时间
        /// </summary>
        public DateTime QSSJ
        {
            get
            {
                return this.GetValDateTime(WeiXiuAttr.QSSJ);
            }
            set
            {
                this.SetValByKey(WeiXiuAttr.QSSJ, value);
            }
        }

        /// <summary>
        /// 维修结束时间
        /// </summary>
        public DateTime ZZSJ
        {
            get
            {
                return this.GetValDateTime(WeiXiuAttr.ZZSJ);
            }
            set
            {
                this.SetValByKey(WeiXiuAttr.ZZSJ, value);
            }
        }
       
        /// <summary>
        /// 备注
        /// </summary>
        public string BZ
        {
            get
            {
                return this.GetValStrByKey(WeiXiuAttr.BZ);
            }
            set
            {
                this.SetValByKey(WeiXiuAttr.BZ, value);
            }
        }
        /// <summary>
        /// 外键
        /// </summary>
        public string FK_HuiYiShi
        {
            get
            {
                return this.GetValStrByKey(WeiXiuAttr.FK_HuiYiShi);
            }
            set
            {
                this.SetValByKey(WeiXiuAttr.FK_HuiYiShi, value);
            }
        }
       
        #endregion 属性
        #region 权限控制属性.

        #endregion 权限控制属性.

        #region 构造方法
        /// <summary>
        /// 维修信息
        /// </summary>
        public WeiXiu()
        {
        }
        /// <summary>
        /// 维修信息
        /// </summary>
        /// <param name="_No"></param>
        public WeiXiu(string oid) : base(oid) { }
        #endregion

        #region 重写方法
        protected override bool beforeInsert()
        {

            return base.beforeInsert();
        }

        protected override bool beforeUpdate()
        {

            return base.beforeUpdate();
        }
        protected override bool beforeUpdateInsertAction()
        {
            return base.beforeUpdateInsertAction();
        }


        #endregion 重写方法

        /// <summary>
        /// 维修信息Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map("OA_HuiYiShiWeiXiuView");
                map.EnDesc = "会议室维修";
                //map.IsAutoGenerNo = true;
                map.CodeStruct = "3"; //三位编号001开始
                map.AddTBStringPK(WeiXiuAttr.No, null, "编号", false, true, 0, 100, 40);
                map.AddDDLEntities(WeiXiuAttr.FK_HuiYiShi, null, "会议室", new HuiYiShis(), true);
                map.AddTBString(WeiXiuAttr.Name, null, "", false, false, 0, 100, 40);


                map.AddTBDateTime(WeiXiuAttr.QSSJ, "维修起始时间", true, false);
                map.AddTBDateTime(WeiXiuAttr.ZZSJ, "维修结束时间", true, false);

                map.AddTBInt(WeiXiuAttr.isWeiXiu,1,"是否是维修",false,true);
                map.AddTBStringDoc(WeiXiuAttr.BZ, null, "备注", true, false, true);

                this._enMap = map;
                return this._enMap;
            }
        }




    }
    /// <summary>
    /// 维修信息
    /// </summary>
    public class WeiXius : SimpleNoNames
    {
        /// <summary>
        /// 维修信息s
        /// </summary>
        public WeiXius() { }
        /// <summary>
        /// 得到它的 Entity 
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new WeiXiu();
            }
        }
    }
}

