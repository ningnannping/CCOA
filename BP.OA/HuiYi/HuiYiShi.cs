﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.DA;
using BP.En;
using BP.Port;
namespace BP.OA.HuiYi
{
    //public enum LeiXing
    //{
    //    //视频
    //    SP,
    //    //实体
    //    ST

    //}
    public class HuiYiShiAttr : EntityNoNameAttr
    {
        /// <summary>
        /// 会议室号码
        /// </summary>
        public const string HYSHM = "HYSHM";
        /// <summary>
        /// 管理部门
        /// </summary>
        public const string GLBM = "GLBM";
        /// <summary>
        ///负责人
        /// </summary>
        public const string FZR = "FZR";
        /// <summary>
        ///容纳人数
        /// </summary>
        public const string RNRS = "RNRS";
        /// <summary>
        ///设施
        /// </summary>
        public const string SS = "SS";
        /// <summary>
        /// 房间面积
        /// </summary>
        public const string FWMJ = "FWMJ";
        /// <summary>
        /// 类型
        /// </summary>
        //public const string LeiXing = "LeiXing";

        /// <summary>
        /// 备注
        /// </summary>
        public const string BZ = "BZ";

    }



    /// <summary>
    /// 会议室实体类
    /// </summary>
    public class HuiYiShi : EntityNoName
    {
        #region 属性
        //public string LeiXing
        //{
        //    get
        //    {
        //        return this.GetValStrByKey(HuiYiShiAttr.LeiXing);
        //    }
        //    set
        //    {
        //        this.SetValByKey(HuiYiShiAttr.LeiXing, value);
        //    }
        //}
        /// <summary>
        /// 会议室号码
        /// </summary>

        public string HYSHM
        {
            get
            {
                return this.GetValStrByKey(HuiYiShiAttr.HYSHM);
            }
            set
            {
                this.SetValByKey(HuiYiShiAttr.HYSHM, value);
            }
        }

        /// <summary>
        /// 管理部门
        /// </summary>

        public string GLBM
        {
            get
            {
                return this.GetValStrByKey(HuiYiShiAttr.GLBM);
            }
            set
            {
                this.SetValByKey(HuiYiShiAttr.GLBM, value);
            }
        }

        /// <summary>
        /// 负责人
        /// </summary>
        public string FZR
        {
            get
            {
                return this.GetValStrByKey(HuiYiShiAttr.FZR);
            }
            set
            {
                this.SetValByKey(HuiYiShiAttr.FZR, value);
            }
        }
        /// <summary>
        /// 容纳人数
        /// </summary>
        public decimal RNRS
        {
            get
            {
                return this.GetValDecimalByKey(HuiYiShiAttr.RNRS);
            }
            set
            {
                this.SetValByKey(HuiYiShiAttr.RNRS, value);
            }
        }
        /// <summary>
        /// 房屋面积
        /// </summary>
        public string FWMJ
        {
            get
            {
                return this.GetValStrByKey(HuiYiShiAttr.FWMJ);
            }
            set
            {
                this.SetValByKey(HuiYiShiAttr.FWMJ, value);
            }
        }
        /// <summary>
        /// 设施
        /// </summary>
        public string 设施
        {
            get
            {
                return this.GetValStrByKey(HuiYiShiAttr.SS);
            }
            set
            {
                this.SetValByKey(HuiYiShiAttr.SS, value);
            }
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string BZ
        {
            get
            {
                return this.GetValStrByKey(HuiYiShiAttr.BZ);
            }
            set
            {
                this.SetValByKey(HuiYiShiAttr.BZ, value);
            }
        }

        #endregion 属性
        #region 权限控制属性.

        #endregion 权限控制属性.

        #region 构造方法
        /// <summary>
        /// 会议室信息
        /// </summary>
        public HuiYiShi()
        {
        }
        /// <summary>
        /// 会议室信息
        /// </summary>
        /// <param name="_No"></param>
        public HuiYiShi(string oid) : base(oid) { }
        #endregion

        #region 重写方法
        protected override bool beforeInsert()
        {

            return base.beforeInsert();
        }

        protected override bool beforeUpdate()
        {

            return base.beforeUpdate();
        }



        #endregion 重写方法

        /// <summary>
        /// 会议室信息Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map("OA_HuiYiShi");
                map.EnDesc = "会议室登记与查询";
                map.IsAutoGenerNo = true;
                map.CodeStruct = "3"; //三位编号001开始

                map.AddTBStringPK(HuiYiShiAttr.No, null, "编号", false, true, 0, 100, 40);
                map.AddTBString(HuiYiShiAttr.Name, null, "会议室", true, false, 0, 100, 30);
               // map.AddTBString(HuiYiShiAttr.HYSHM, null, "会议室号码", true, false, 0, 100, 30);
                map.AddTBString(HuiYiShiAttr.GLBM, null, "管理部门", true, false, 0, 100, 30);
                map.AddTBString(HuiYiShiAttr.FZR, null, "负责人", true, false, 0, 100, 30);
                map.AddTBDecimal(HuiYiShiAttr.RNRS, 0, "容纳人数", true, false);
               
                map.AddTBString(HuiYiShiAttr.FWMJ, null, "会议室面积", true, false, 0, 100, 30);
                map.AddTBString(HuiYiShiAttr.SS, null, "设施", true, false, 0, 100, 40);
               // map.AddTBString(HuiYiShiAttr.LeiXing, null, "类型", true, false, 0, 100, 40);
               // map.AddDDLSysEnum(HuiYiShiAttr.LeiXing, 0, "类型", true, true, HuiYiShiAttr.LeiXing, "@0=实体@1=视频");
                map.AddTBStringDoc(HuiYiShiAttr.BZ, null, "备注", true, false, true);

                this._enMap = map;
                return this._enMap;
            }
        }




    }
    /// <summary>
    /// 收据信息
    /// </summary>
    public class HuiYiShis : SimpleNoNames
    {
        /// <summary>
        /// 收据信息s
        /// </summary>
        public HuiYiShis() { }
        /// <summary>
        /// 得到它的 Entity 
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new HuiYiShi();
            }
        }
    }
}

