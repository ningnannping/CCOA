﻿using System;
using System.Collections;
using System.Data;
using BP.DA;
using BP.DTS;
using BP.En;
using BP.Web;
using BP.Sys;
using BP.WF;
using BP.Port;

namespace BP.OA.HuiYi
{
    /// <summary>
    /// 车辆年检 流程事件实体
    /// </summary>
    public class HuiYiShenQingFEE : BP.WF.FlowEventBase
    {
        #region 构造.
        /// <summary>
        /// 会议室申请 流程事件实体
        /// </summary>
        public HuiYiShenQingFEE()
        {
        }
        #endregion 构造.

        #region 重写属性.
        public override string FlowMark
        {
            get { return HuiYiAPI.HuiYi_FlowMark; }
        }
        #endregion 重写属性.

        #region 重写节点运动事件.
        /// <summary>
        /// 删除后
        /// </summary>
        /// <returns></returns>
        public override string AfterFlowDel()
        {
            return null;
        }
        /// <summary>
        /// 删除前
        /// </summary>
        /// <returns></returns>
        public override string BeforeFlowDel()
        {
            return null;
        }
        /// <summary>
        /// 结束后
        /// </summary>
        /// <returns></returns>
        public override string FlowOverAfter()
        {


            HuiYiAPI.HuiYiShiShenPi(this.GetValStr("HYZT"), this.GetValStr("HYLX"),
                this.GetValStr("SQBM"), this.GetValStr("FQR"), this.GetValStr("JLR"), this.GetValStr("DJRQ"),
                this.GetValDecimal("CHRS"), this.GetValStr("QSSJ"), this.GetValStr("ZZSJ"),
                //this.GetValStr("NoFJ"),
                this.GetValStr("FK_HuiYiShi"), this.GetValStr("BZYQ"), this.GetValStr("BZZT"), this.GetValStr("HYJL"),
                 this.WorkID);

            return "写入成功....";

        }
        /// <summary>
        /// 结束前
        /// </summary>
        /// <returns></returns>
        public override string FlowOverBefore()
        {
            return null;
        }
        #endregion 重写事件，完成业务逻辑.
    }
}
