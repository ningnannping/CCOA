﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using BP.DA;
using BP.En;
using System.Data;
using BP.WF.Port;

namespace BP.OA.PublicPlan
{
    /// <summary>
    /// 计划状态
    /// </summary>
    public enum PlanState
    {
        /// <summary>
        /// 未启动
        /// </summary>
        Normal = 0,
        /// <summary>
        /// 启动
        /// </summary>
        SartUp = 1,
        /// <summary>
        /// 结束
        /// </summary>
        EndOf = 2
    }
    /// <summary>
    /// 类型
    /// </summary>
    public enum PCModel
    {
        /// <summary>
        /// 计划
        /// </summary>
        PlanModel=0,
        /// <summary>
        /// 阶段
        /// </summary>
        StageModel=1
    }

    /// <summary>
    /// 计划和阶段目录表
    /// </summary>
    public class PlanCatalogAttr : EntityTreeAttr
    {
        /// <summary>
        /// 创建人
        /// </summary>
        public const String FK_Emp = "FK_Emp";
        /// <summary>
        /// 记录日期
        /// </summary>
        public const String RDT = "RDT";
        /// <summary>
        /// 部门
        /// </summary>
        public const String FK_Dept = "FK_Dept";
        /// <summary>
        /// 负责人
        /// </summary>
        public const String PersonCharge = "PersonCharge";
        /// <summary>
        /// 参与人员编号
        /// </summary>
        public const String PlanPartake = "PlanPartake";
        /// <summary>
        /// 参与人员文本
        /// </summary>
        public const String PlanPartakeText = "PlanPartakeText";
        /// <summary>
        /// 计划开始时间
        /// </summary>
        public const String StartDate = "StartDate";
        /// <summary>
        /// 计划结束时间
        /// </summary>
        public const String EndDate = "EndDate";
        /// <summary>
        /// 状态
        /// </summary>
        public const String PlanState = "PlanState";
        /// <summary>
        /// 类型
        /// </summary>
        public const String PCModel = "PCModel";
        /// <summary>
        /// 完成百分比
        /// </summary>
        public const String FinishPercent = "FinishPercent";
        /// <summary>
        /// 启动日期
        /// </summary>
        public const String StartUpDate = "StartUpDate";
        /// <summary>
        /// 实际结束日期
        /// </summary>
        public const String RelEndDate = "RelEndDate";
        /// <summary>
        /// 简述
        /// </summary>
        public const String SimpleContents = "SimpleContents";
        /// <summary>
        /// 目标要求
        /// </summary>
        public const String TargetContents = "TargetContents";
    }
    /// <summary>
    /// 计划和阶段目录表
    /// </summary>
    public partial class PlanCatalog : EntityTree
    {
        #region 属性
        /// <summary>
        /// 创建人
        /// </summary>
        public string FK_Emp
        {
            get
            {
                return this.GetValStrByKey(PlanCatalogAttr.FK_Emp);
            }
            set
            {
                this.SetValByKey(PlanCatalogAttr.FK_Emp, value);
            }
        }
        /// <summary>
        /// 创建人姓名
        /// </summary>
        public string FK_EmpText
        {
            get
            {
                return this.GetValRefTextByKey(PlanCatalogAttr.FK_Emp);
            }
        }
        /// <summary>
        /// 记录日期
        /// </summary>
        public DateTime RDT
        {
            get
            {
                return this.GetValDateTime(PlanCatalogAttr.RDT);
            }
            set
            {
                this.SetValByKey(PlanCatalogAttr.RDT, value);
            }
        }
        /// <summary>
        /// 部门
        /// </summary>
        public string FK_Dept
        {
            get
            {
                return this.GetValStrByKey(PlanCatalogAttr.FK_Dept);
            }
            set
            {
                this.SetValByKey(PlanCatalogAttr.FK_Dept, value);
            }
        }
        /// <summary>
        /// 部门名称
        /// </summary>
        public string FK_DeptText
        {
            get
            {
                return this.GetValRefTextByKey(PlanCatalogAttr.FK_Dept);
            }
        }
        /// <summary>
        /// 负责人
        /// </summary>
        public string PersonCharge
        {
            get
            {
                return this.GetValStrByKey(PlanCatalogAttr.PersonCharge);
            }
            set
            {
                this.SetValByKey(PlanCatalogAttr.PersonCharge, value);
            }
        }
        /// <summary>
        /// 负责人
        /// </summary>
        public string PersonChargeText
        {
            get
            {
                return this.GetValRefTextByKey(PlanCatalogAttr.PersonCharge);
            }
        }
        /// <summary>
        /// 参与人员编号
        /// </summary>
        public string PlanPartake
        {
            get
            {
                return this.GetValStrByKey(PlanCatalogAttr.PlanPartake);
            }
            set
            {
                this.SetValByKey(PlanCatalogAttr.PlanPartake, value);
            }
        }
        /// <summary>
        /// 参与人员文本
        /// </summary>
        public string PlanPartakeText
        {
            get
            {
                return this.GetValStrByKey(PlanCatalogAttr.PlanPartakeText);
            }
            set
            {
                this.SetValByKey(PlanCatalogAttr.PlanPartakeText, value);
            }
        }
        /// <summary>
        /// 计划开始时间
        /// </summary>
        public string StartDate
        {
            get
            {
                return this.GetValStrByKey(PlanCatalogAttr.StartDate);
            }
            set
            {
                this.SetValByKey(PlanCatalogAttr.StartDate, value);
            }
        }
        /// <summary>
        /// 计划结束时间
        /// </summary>
        public string EndDate
        {
            get
            {
                return this.GetValStrByKey(PlanCatalogAttr.EndDate);
            }
            set
            {
                this.SetValByKey(PlanCatalogAttr.EndDate, value);
            }
        }
        /// <summary>
        /// 状态
        /// </summary>
        public PlanState PlanState
        {
            get
            {
                return (PlanState)this.GetValIntByKey(PlanCatalogAttr.PlanState);
            }
            set
            {
                this.SetValByKey(PlanCatalogAttr.PlanState, (decimal)value);
            }
        }
        /// <summary>
        /// 状态文本
        /// </summary>
        public string PlanStateText
        {
            get
            {
                return this.GetValRefTextByKey(PlanCatalogAttr.PlanState);
            }
        }
        /// <summary>
        /// 类型
        /// </summary>
        public PCModel PCModel
        {
            get
            {
                return (PCModel)this.GetValIntByKey(PlanCatalogAttr.PCModel);
            }
            set
            {
                this.SetValByKey(PlanCatalogAttr.PCModel, (decimal)value);
            }
        }
        /// <summary>
        /// 完成百分比
        /// </summary>
        public decimal FinishPercent
        {
            get
            {
                return this.GetValDecimalByKey(PlanCatalogAttr.FinishPercent);
            }
            set
            {
                this.SetValByKey(PlanCatalogAttr.FinishPercent, value);
            }
        }
        /// <summary>
        /// 简述
        /// </summary>
        public string SimpleContents
        {
            get
            {
                return this.GetValStrByKey(PlanCatalogAttr.SimpleContents);
            }
            set
            {
                this.SetValByKey(PlanCatalogAttr.SimpleContents, value);
            }
        }
        /// <summary>
        /// 目标要求
        /// </summary>
        public string TargetContents
        {
            get
            {
                return this.GetValStrByKey(PlanCatalogAttr.TargetContents);
            }
            set
            {
                this.SetValByKey(PlanCatalogAttr.TargetContents, value);
            }
        }
        /// <summary>
        /// 启动日期
        /// </summary>
        public string StartUpDate
        {
            get
            {
                return this.GetValStrByKey(PlanCatalogAttr.StartUpDate);
            }
            set
            {
                this.SetValByKey(PlanCatalogAttr.StartUpDate, value);
            }
        }
        /// <summary>
        /// 实际结束日期
        /// </summary>
        public string RelEndDate
        {
            get
            {
                return this.GetValStrByKey(PlanCatalogAttr.RelEndDate);
            }
            set
            {
                this.SetValByKey(PlanCatalogAttr.RelEndDate, value);
            }
        }
        #endregion

        #region 构造函数
        /// <summary>
        /// 计划和阶段目录表
        /// </summary>
        public PlanCatalog() { }

        /// <summary>
        /// 计划和阶段目录表
        /// </summary>
        public PlanCatalog(string _No) : base(_No) { }
        #endregion

        #region 重写方法
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                uac.OpenAll();
                return uac;
            }
        }
        /// <summary>
        /// Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map();
                map.PhysicsTable = "OA_PlanCatalog";
                map.EnDesc = "计划和阶段目录表";
                map.DepositaryOfEntity = Depositary.Application;
                map.DepositaryOfMap = Depositary.Application;
                map.IsAutoGenerNo = true;
                map.CodeStruct = "2";
                map.AddTBStringPK(PlanCatalogAttr.No, null, "计划编号", true, true, 1, 10, 80);
                map.AddTBString(PlanCatalogAttr.ParentNo, null, "父计划编号", false, false, 0, 100, 30);
                map.AddTBString(PlanCatalogAttr.Name, null, "名称", true, false, 1, 1000, 100);
               // map.AddTBString(PlanCatalogAttr.TreeNo, null, "树编号", false, false, 0, 200, 100);
                map.AddDDLEntities(PlanCatalogAttr.FK_Emp, null, "创建人", new Emps(), true);
                map.AddTBDateTime(PlanCatalogAttr.RDT, "创建日期", false, false);
                map.AddDDLEntities(PlanCatalogAttr.FK_Dept, null, "负责部门", new Depts(), true);
                map.AddDDLEntities(PlanCatalogAttr.PersonCharge, null, "负责人", new Emps(), true);
                map.AddTBString(PlanCatalogAttr.PlanPartake, null, "参与人员", true, false, 1, 1000, 300);
                map.AddTBString(PlanCatalogAttr.PlanPartakeText, null, "参与人员", true, false, 1, 1000, 300);
                map.AddTBDateTime(PlanCatalogAttr.StartDate, null, "开始时间", true, false);
                map.AddTBDateTime(PlanCatalogAttr.EndDate, null, "结束时间", true, false);
                map.AddDDLSysEnum(PlanCatalogAttr.PlanState, 0, "计划状态", true, true, PlanCatalogAttr.PlanState, "@0=未启动@1=启动@2=结束");
                map.AddDDLSysEnum(PlanCatalogAttr.PCModel, 0, "类型", true, true, PlanCatalogAttr.PCModel, "@0=计划@1=阶段");
                map.AddTBDateTime(PlanCatalogAttr.StartUpDate, null, "启动日期", false, false);
                map.AddTBDateTime(PlanCatalogAttr.RelEndDate, null, "实际结束日期", false, false);
                map.AddTBInt(PlanCatalogAttr.FinishPercent, 0, "完成百分比", true, false);
                map.AddTBStringDoc(PlanCatalogAttr.SimpleContents, null, "简述", true, false, true);
                map.AddTBStringDoc(PlanCatalogAttr.TargetContents, null, "目标要求", true, false, true);
                map.AddTBString(PlanCatalogAttr.ICON, "icon-plan-0", "图标", true, false, 0, 100, 100);
                map.AddTBInt(PlanCatalogAttr.Idx, 0, "Idx", false, false);
               // map.AddTBInt(PlanCatalogAttr.IsDir, 0, "IsDir", false, false);

                this._enMap = map;
                return this._enMap;
            }
        }
        #endregion

        /// <summary>
        /// 计划的查看权限
        /// </summary>
        /// <param name="FK_Emp">登录账号</param>
        /// <returns></returns>
        public bool CheckLookRight(string FK_Emp)
        {
            //判断是不是负责人
            if (this.PersonCharge == FK_Emp)
                return true;
            //判断是不是参与人
            if (this.PlanPartake.Contains(FK_Emp))
                return true;
            //判断是不是计划所属部门的人            
            
            return false;
        }
        /// <summary>
        /// 计划的拥有权限
        /// </summary>
        /// <returns></returns>
        public bool CheckOwner(string FK_Emp) 
        {

            return false;
        }
    }
    /// <summary>
    ///计划和阶段目录表集合
    /// </summary>
    public class PlanCatalogs : EntitiesTree
    {
        /// <summary>
        /// 得到一个新实体
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new PlanCatalog();
            }
        }
        /// <summary>
        /// 计划和阶段目录表集合
        /// </summary>
        public PlanCatalogs()
        {
        }
    }
}
