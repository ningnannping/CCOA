using System;
using System.Data;
using System.Collections;
using BP.DA;
using BP.En;
using BP.Port;

namespace BP.OA.Meeting
{
    /// <summary>
    /// 预定信息属性
    /// </summary>
    public class RoomOrdingAttr : EntityOIDAttr
    {
        //会议标题	
        public const string Title = "Title";
        //申请人	 周朋
        public const string Fk_Emp = "Fk_Emp";
        //申请部门	 集团总部
        public const string Fk_Dept = "Fk_Dept";
        //会议室	
        public const string Fk_Room = "Fk_Room";
        //开始时间	
        public const string DateFrom = "DateFrom";
        //结束时间	
        public const string DateTo = "DateTo";
        //会议室资源	 添加会议室资源
        public const string MTResource = "MTResource";
        //参与人员 hzm
        public const string ToEmps = "ToEmps";
        public const string ToEmpNames = "ToEmpNames";
        //参与岗位 hzm
        public const string ToStations = "ToStations";
        public const string ToStationNames = "ToStationNames";
        //参与部门 hzm
        public const string ToDepts = "ToDepts";
        public const string ToDeptNames = "ToDeptNames";

        //备注
        public const string Note = "Note";
        //状态
        public const string State = "State";
    }
    /// <summary>
    ///  预定信息
    /// </summary>
    public class RoomOrding : EntityOID
    {
        #region 封装属性

        //会议标题	
        public string Title
        {
            get
            {
                return this.GetValStrByKey(RoomOrdingAttr.Title);
            }
            set
            {
                this.SetValByKey(RoomOrdingAttr.Title, value);
            }
        }
        //申请人	 周朋
        public string Fk_Emp
        {
            get
            {
                return this.GetValStrByKey(RoomOrdingAttr.Fk_Emp);
            }
            set
            {
                this.SetValByKey(RoomOrdingAttr.Fk_Emp, value);
            }
        }
        public string Fk_EmpName
        {
            get
            {
                return this.GetValRefTextByKey(RoomOrdingAttr.Fk_Emp);
            }
        }
        //申请部门	 集团总部
        public string Fk_Dept
        {
            get
            {
                return this.GetValStrByKey(RoomOrdingAttr.Fk_Dept);
            }
            set
            {
                this.SetValByKey(RoomOrdingAttr.Fk_Dept, value);
            }
        }
        public string Fk_DeptName
        {
            get
            {
                return this.GetValRefTextByKey(RoomOrdingAttr.Fk_Dept);
            }
        }
        //会议室	
        public string Fk_Room
        {
            get
            {
                return this.GetValStrByKey(RoomOrdingAttr.Fk_Room);
            }
            set
            {
                this.SetValByKey(RoomOrdingAttr.Fk_Room, value);
            }
        }
        public string Fk_RoomName
        {
            get
            {
                return this.GetValRefTextByKey(RoomOrdingAttr.Fk_Room);
            }
        }
        //开始时间	
        public string DateFrom
        {
            get
            {
                return this.GetValStrByKey(RoomOrdingAttr.DateFrom);
            }
            set
            {
                this.SetValByKey(RoomOrdingAttr.DateFrom, value);
            }
        }
        //结束时间	
        public string DateTo
        {
            get
            {
                return this.GetValStrByKey(RoomOrdingAttr.DateTo);
            }
            set
            {
                this.SetValByKey(RoomOrdingAttr.DateTo, value);
            }
        }
        //参与人员
        public string ToEmps
        {
            get
            {
                return this.GetValStrByKey(RoomOrdingAttr.ToEmps);
            }
            set
            {
                this.SetValByKey(RoomOrdingAttr.ToEmps, value);
            }
        }
        public string ToEmpNames
        {
            get
            {
                return this.GetValStrByKey(RoomOrdingAttr.ToEmpNames);
            }
            set
            {
                this.SetValByKey(RoomOrdingAttr.ToEmpNames, value);
            }
        }
        //参与岗位
        public string ToStations
        {
            get
            {
                return this.GetValStrByKey(RoomOrdingAttr.ToStations);
            }
            set
            {
                this.SetValByKey(RoomOrdingAttr.ToStations, value);
            }
        }
        public string ToStationNames
        {
            get
            {
                return this.GetValStrByKey(RoomOrdingAttr.ToStationNames);
            }
            set
            {
                this.SetValByKey(RoomOrdingAttr.ToStationNames, value);
            }
        }
        //参与部门
        public string ToDepts
        {
            get
            {
                return this.GetValStrByKey(RoomOrdingAttr.ToDepts);
            }
            set
            {
                this.SetValByKey(RoomOrdingAttr.ToDepts, value);
            }
        }
        public string ToDeptNames
        {
            get
            {
                return this.GetValStrByKey(RoomOrdingAttr.ToDeptNames);
            }
            set
            {
                this.SetValByKey(RoomOrdingAttr.ToDeptNames, value);
            }
        }

        //会议室资源	 添加会议室资源
        public string MTResource
        {
            get
            {
                return this.GetValStrByKey(RoomOrdingAttr.MTResource);
            }
            set
            {
                this.SetValByKey(RoomOrdingAttr.MTResource, value);
            }
        }
        //备注
        public string Note
        {
            get
            {
                return this.GetValStrByKey(RoomOrdingAttr.Note);
            }
            set
            {
                this.SetValByKey(RoomOrdingAttr.Note, value);
            }
        }
        public string State
        {
            get { return this.GetValStringByKey(RoomOrdingAttr.State); }
            set { this.SetValByKey(RoomOrdingAttr.State, value); }
        }
        #endregion

        #region 权限控制属性.
        #endregion 权限控制属性.

        #region 构造方法
        /// <summary>
        /// 预定信息
        /// </summary>
        public RoomOrding()
        {
        }
        /// <summary>
        /// 预定信息
        /// </summary>
        /// <param name="_No"></param>
        public RoomOrding(int _No) : base(_No) { }
        #endregion

        /// <summary>
        /// 预定信息Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;
                Map map = new Map("OA_RoomOrding");
                map.EnDesc = "预定信息";
                map.CodeStruct = "9";
                map.DepositaryOfEntity = Depositary.Application;
                map.DepositaryOfMap = Depositary.Application;
                map.AddTBIntPKOID();
                map.AddTBString(RoomOrdingAttr.Title, string.Empty, "会议议题", true, false, 0, 100, 30);
                map.AddDDLEntitiesPK(RoomOrdingAttr.Fk_Emp, string.Empty, "预定人", new Emps(), true);
                map.AddDDLEntitiesPK(RoomOrdingAttr.Fk_Dept, string.Empty, "申请部门", new Depts(), true);
                map.AddDDLEntitiesPK(RoomOrdingAttr.Fk_Room, string.Empty, "会议室", new Rooms(), true);
                map.AddTBDateTime(RoomOrdingAttr.DateFrom, null, "时间从", true, false);
                map.AddTBDateTime(RoomOrdingAttr.DateTo, null, "到", true, false);
                map.AddTBString(RoomOrdingAttr.MTResource, string.Empty, "会议资源", true, false, 0, 500, 200);
                map.AddTBString(RoomOrdingAttr.ToEmps, string.Empty, "参与人员", true, false, 0, 500, 200);
                map.AddTBString(RoomOrdingAttr.ToStations, string.Empty, "参与岗位", true, false, 0, 500, 200);
                map.AddTBString(RoomOrdingAttr.ToDepts, string.Empty, "参与部门", true, false, 0, 500, 200);
                map.AddTBString(RoomOrdingAttr.ToEmpNames, string.Empty, "参与人员", true, false, 0, 500, 200);
                map.AddTBString(RoomOrdingAttr.ToStationNames, string.Empty, "参与岗位", true, false, 0, 500, 200);
                map.AddTBString(RoomOrdingAttr.ToDeptNames, string.Empty, "参与部门", true, false, 0, 500, 200);
                map.AddTBString(RoomOrdingAttr.State, string.Empty, "状态", true, false, 0, 1, 20);//0:预定，1,已发起，2已取消，3已结束
                map.AddTBString(RoomOrdingAttr.Note, string.Empty, "备注", true, false, 0, 500, 200);
                this._enMap = map;
                return this._enMap;
            }
        }

        #region 重写方法
        protected override bool beforeInsert()
        {
            return base.beforeInsert();
        }
        #endregion 重写方法


    }
    /// <summary>
    /// 预定信息
    /// </summary>
    public class RoomOrdings : EntitiesOID
    {
        /// <summary>
        /// 预定信息s
        /// </summary>
        public RoomOrdings() { }
        /// <summary>
        /// 得到它的 Entity 
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new RoomOrding();
            }
        }
    }
}
