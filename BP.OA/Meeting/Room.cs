using System;
using System.Data;
using System.Collections;
using BP.DA;
using BP.En;
using BP.Port;

namespace BP.OA.Meeting
{
    /// <summary>
    /// 会议室状态(枚举)
    /// </summary>
    public enum RoomSta
    {
        /// <summary>
        /// 正常
        /// </summary>
        正常,
        /// <summary>
        /// 维修中
        /// </summary>
        维修中
    }
    /// <summary>
    /// 会议室属性
    /// </summary>
    public class RoomAttr : EntityNoNameAttr
    {
        /// <summary>
        /// 状态
        /// </summary>
        public const string RoomSta = "RoomSta";
        /// <summary>
        /// 保管人
        /// </summary>
        public const string FK_Emp = "FK_Emp";
        /// <summary>
        /// 真实姓名
        /// </summary>
        public const string Fk_EmpName = "Fk_EmpName";
        /// <summary>
        /// 容量
        /// </summary>
        public const string Capacity = "Capacity";
        /// <summary>
        /// 会议室资源
        /// </summary>
        public const string RoomResource = "RoomResource";
        /// <summary>
        /// 会议室资源name
        /// </summary>
        public const string ResourceName = "ResourceName";
        /// <summary>
        /// 会议室说明
        /// </summary>
        public const string Note = "Note";
        /// <summary>
        /// 可以使用 的权限
        /// </summary>
        public const string Authority = "Authority";

    }
    /// <summary>
    ///  会议室
    /// </summary>
    public class Room : EntityNoName
    {
        #region 属性
        /// <summary>
        /// 状态
        /// </summary>
        public RoomSta RoomSta
        {
            get
            {
                return (RoomSta)this.GetValIntByKey(RoomAttr.RoomSta);
            }
            set
            {
                this.SetValByKey(RoomAttr.RoomSta, (int)value);
            }
        }       
        /// <summary>
        /// 保管人
        /// </summary>
        public string FK_Emp
        {
            get
            {
                return this.GetValStrByKey(RoomAttr.FK_Emp);
            }
            set
            {
                this.SetValByKey(RoomAttr.FK_Emp, value);
            }
        }
        public string Fk_EmpName
        {
            get
            {
                return this.GetValStrByKey(RoomAttr.Fk_EmpName);
            }
            set
            {
                this.SetValByKey(RoomAttr.Fk_EmpName, value);
            }
            //get
            //{
            //    string empNames = "";
            //    string[] empNos = this.FK_Emp.Split(',');
            //    foreach(string str in empNos)
            //    {
            //        if (string.IsNullOrEmpty(str)) continue;
            //        BP.Port.Emp emp = new Emp(str);
            //        empNames += emp.Name+",";
            //    }
            //    return empNames;
            //}
        }
        /// <summary>
        /// 容量
        /// </summary>
        public int Capacity
        {
            get
            {
                return this.GetValIntByKey(RoomAttr.Capacity);
            }
            set
            {
                this.SetValByKey(RoomAttr.Capacity, value);
            }

        }
        /// <summary>
        /// 会议室资源
        /// </summary>
        public string RoomResource
        {
            get
            {
                return this.GetValStrByKey(RoomAttr.RoomResource);
            }
            set
            {
                this.SetValByKey(RoomAttr.RoomResource, value);
            }

        }
        public string ResourceName
        {
            get
            {
                return this.GetValStrByKey(RoomAttr.ResourceName);
            }
            set
            {
                this.SetValByKey(RoomAttr.ResourceName, value);
            }

        }
        /// <summary>
        /// 会议室说明
        /// </summary>
        public string Note
        {
            get
            {
                return this.GetValStrByKey(RoomAttr.Note);
            }
            set
            {
                this.SetValByKey(RoomAttr.Note, value);
            }
        }
        /// <summary>
        /// 可以使用 的权限
        /// </summary>
        public string Authority
        {
            get
            {
                return this.GetValStrByKey(RoomAttr.Authority);
            }
            set
            {
                this.SetValByKey(RoomAttr.Authority, value);
            }
        }
        #endregion 属性


        #region 构造方法
        /// <summary>
        /// 会议室
        /// </summary>
        public Room()
        {
        }
        /// <summary>
        /// 会议室
        /// </summary>
        /// <param name="_No"></param>
        public Room(string _No) : base(_No) { }
        #endregion

        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();

                uac.IsUpdate = true;
                uac.IsDelete = true;
                uac.IsInsert = true;
                uac.IsView = true;
                return uac;
            }
        }
        /// <summary>
        /// 会议室Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map("OA_Room");
                map.EnDesc = "会议室";
                map.CodeStruct = "3";
                map.IsAllowRepeatName = false;
                map.DepositaryOfEntity = Depositary.Application;
                map.DepositaryOfMap = Depositary.Application;

                map.AddTBStringPK(RoomAttr.No, string.Empty, "编号", true, true, 3, 3, 3);
                map.AddTBString(RoomAttr.Name, string.Empty, "名称", true, false, 1, 100, 30, false);
                map.AddDDLSysEnum(RoomAttr.RoomSta, 0, "状态", true, true, RoomAttr.RoomSta,
                    "@0=正常@1=维护中");
                map.AddDDLEntities(RoomAttr.FK_Emp, null, "保管人", new BP.Port.Emps(), true);
                map.AddTBString(RoomAttr.Fk_EmpName, string.Empty, "真实名称", false, false, 0, 100, 30, false);
                map.AddTBString(RoomAttr.Capacity, string.Empty, "容量", true, false, 2, 5, 10, false);
                map.AddTBString(RoomAttr.RoomResource, string.Empty, "会议室资源", true, false, 0, 100, 30, false);
                map.AddTBString(RoomAttr.ResourceName, string.Empty, "会议室资源名字", false, false, 0, 100, 30, false);
                map.AddTBString(RoomAttr.Note, string.Empty, "会议室说明", true, false, 0, 100, 30, false);
                map.AddTBString(RoomAttr.Authority, string.Empty, "可以使用的权限", true, false, 0, 100, 30, false);

                //查询条件
                map.AddSearchAttr(RoomAttr.RoomSta);
                map.AddSearchAttr(RoomAttr.FK_Emp);

                this._enMap = map;
                return this._enMap;
            }
        }
        protected override bool beforeUpdateInsertAction()
        {
            this.Fk_EmpName = CCOA.MyHelper.GetEmpName(this.FK_Emp);
            this.ResourceName = CCOA.MyHelper.GetResName(this.RoomResource);
            //FixMan ds = new FixMan(this.FK_FixMan);
            ////复制主表的属性.
            //this.FixNum = ds.No;
            //this.ZipSpec = ds.ZipSpec;
            //this.FK_FixAssBigCatagory = ds.FK_FixAssBigCatagory;
            //this.FK_FixAssBigCatSon = ds.FK_FixAssBigCatSon;
            return base.beforeUpdateInsertAction();
        }
    }
    
    /// <summary>
    /// 会议室
    /// </summary>
    public class Rooms : EntitiesNoName
    {
        /// <summary>
        /// 会议室s
        /// </summary>
        public Rooms() { }
        /// <summary>
        /// 得到它的 Entity 
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new Room();
            }
        }
    }
}
