﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
namespace BP.OA
{
    public class Debug
    {
        public const string SCRIPT_BEGIN = "<Script language= 'Javascript' type='text/javascript'>";
        public const string SCRIPT_END = "</Script>";
        public static void Alert(Page page,string mess)
        {
            if (string.IsNullOrEmpty(mess))
                return;

            mess = mess.Replace("@@", "@");
            mess = mess.Replace("@@", "@");

            mess = mess.Replace("'", "＇");

            mess = mess.Replace("\"", "＇");

            mess = mess.Replace("\"", "＂");

            mess = mess.Replace(";", "；");
            mess = mess.Replace(")", "）");
            mess = mess.Replace("(", "（");

            mess = mess.Replace(",", "，");
            mess = mess.Replace(":", "：");

            mess = mess.Replace("<", "［");
            mess = mess.Replace(">", "］");

            mess = mess.Replace("[", "［");
            mess = mess.Replace("]", "］");

            mess = mess.Replace("@", "\\n@");
            string script = "<script language=JavaScript>alert('" + mess + "');</script>";
            System.Web.UI.WebControls.Literal li = new System.Web.UI.WebControls.Literal();
            li.Text = script;
            page.Controls.Add(li);
        }
        public static void Confirm(Page sPage, string sTitle, string sTrueUrl, string sFalseUrl)
        {
            if (sTrueUrl == "")
            {
                sTrueUrl = sPage.Request.Url.PathAndQuery;
            }
            if (sFalseUrl == "")
            {
                sFalseUrl = sPage.Request.Url.PathAndQuery;
            }
            Literal li = new Literal();
            string sScript = ((((SCRIPT_BEGIN + "\nif(confirm('" + sTitle + "'))") + "\n  " + ((sTrueUrl != "") ? ("location.href='" + sTrueUrl + "'") : ";")) + "\nelse") + "\n  " + ((sFalseUrl != "") ? ("location.href='" + sFalseUrl + "'") : ";")) + "\n" + SCRIPT_END;
            li.Text = sScript;
            sPage.Controls.Add(li);
        }
    }
}
