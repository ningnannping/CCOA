using System;
using System.Collections;
using BP.DA;
using BP.En;
using BP.Port;

namespace BP.KM
{
    /// <summary>
    /// 抄送到岗位属性	  
    /// </summary>
    public class TreeStationAttr
    {
        /// <summary>
        /// 知识树
        /// </summary>
        public const string RefTreeNo = "RefTreeNo";
        /// <summary>
        /// 工作岗位
        /// </summary>
        public const string FK_Station = "FK_Station";
    }
    /// <summary>
    /// 抄送到岗位
    /// 知识树的工作岗位有两部分组成.	 
    /// 记录了从一个知识树到其他的多个知识树.
    /// 也记录了到这个知识树的其他的知识树.
    /// </summary>
    public class TreeStation : EntityMM
    {
        #region 基本属性
        /// <summary>
        /// UI界面上的访问控制
        /// </summary>
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                uac.OpenAll();
                return uac;
            }
        }
        /// <summary>
        ///知识树
        /// </summary>
        public string RefTreeNo
        {
            get
            {
                return this.GetValStrByKey(TreeStationAttr.RefTreeNo);
            }
            set
            {
                this.SetValByKey(TreeStationAttr.RefTreeNo, value);
            }
        }
        /// <summary>
        /// 岗位名称
        /// </summary>
        public string FK_StationT
        {
            get
            {
                return this.GetValRefTextByKey(TreeStationAttr.FK_Station);
            }
        }
        /// <summary>
        /// 工作岗位
        /// </summary>
        public string FK_Station
        {
            get
            {
                return this.GetValStringByKey(TreeStationAttr.FK_Station);
            }
            set
            {
                this.SetValByKey(TreeStationAttr.FK_Station, value);
            }
        }
        #endregion

        #region 构造方法
        /// <summary>
        /// 抄送到岗位
        /// </summary>
        public TreeStation() { }
        /// <summary>
        /// 重写基类方法
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map("KM_TreeStation");
                map.EnDesc = "抄送岗位";

                map.DepositaryOfEntity = Depositary.None;
                map.DepositaryOfMap = Depositary.Application;

                map.AddTBStringPK(TreeEmpAttr.RefTreeNo, null, "树", true, true, 1, 100, 100);
                map.AddDDLEntitiesPK(TreeStationAttr.FK_Station, null, "工作岗位", new Stations(), true);
              
                this._enMap = map;

                return this._enMap;
            }
        }
        #endregion
    }
    /// <summary>
    /// 抄送到岗位
    /// </summary>
    public class TreeStations : EntitiesMM
    {
        /// <summary>
        /// 他的工作岗位
        /// </summary>
        public Stations HisStations
        {
            get
            {
                Stations ens = new Stations();
                foreach (TreeStation ns in this)
                {
                    ens.AddEntity(new Station(ns.FK_Station));
                }
                return ens;
            }
        }
        
        /// <summary>
        /// 抄送到岗位
        /// </summary>
        public TreeStations() { }
        /// <summary>
        /// 抄送到岗位
        /// </summary>
        /// <param name="nodeID">知识树ID</param>
        public TreeStations(int nodeID)
        {
            QueryObject qo = new QueryObject(this);
            qo.AddWhere(TreeStationAttr.RefTreeNo, nodeID);
            qo.DoQuery();
        }
        /// <summary>
        /// 抄送到岗位
        /// </summary>
        /// <param name="StationNo">StationNo </param>
        public TreeStations(string StationNo)
        {
            QueryObject qo = new QueryObject(this);
            qo.AddWhere(TreeStationAttr.FK_Station, StationNo);
            qo.DoQuery();
        }
        /// <summary>
        /// 得到它的 Entity 
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new TreeStation();
            }
        }
        /// <summary>
        /// 转向此知识树的集合的Nodes
        /// </summary>
        /// <param name="nodeID">此知识树的ID</param>
        /// <returns>转向此知识树的集合的Nodes (FromNodes)</returns> 
        public Stations GetHisStations(int nodeID)
        {
            QueryObject qo = new QueryObject(this);
            qo.AddWhere(TreeStationAttr.RefTreeNo, nodeID);
            qo.DoQuery();

            Stations ens = new Stations();
            foreach (TreeStation en in this)
            {
                ens.AddEntity(new Station(en.FK_Station));
            }
            return ens;
        }
    }
}
