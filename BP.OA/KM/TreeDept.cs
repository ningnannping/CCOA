using System;
using System.Collections;
using BP.DA;
using BP.En;
using BP.Port;

namespace BP.KM
{
	/// <summary>
	/// 抄送部门属性	  
	/// </summary>
	public class TreeDeptAttr
	{
		/// <summary>
		/// 知识树
		/// </summary>
		public const string RefTreeNo="RefTreeNo";
		/// <summary>
		/// 工作部门
		/// </summary>
		public const string FK_Dept="FK_Dept";
	}
	/// <summary>
	/// 抄送部门
	/// 知识树的工作部门有两部分组成.	 
	/// 记录了从一个知识树到其他的多个知识树.
	/// 也记录了到这个知识树的其他的知识树.
	/// </summary>
	public class TreeDept :EntityMM
	{
		#region 基本属性
		/// <summary>
		///知识树
		/// </summary>
		public string  RefTreeNo
		{
			get
			{
				return this.GetValStrByKey(TreeDeptAttr.RefTreeNo);
			}
			set
			{
				this.SetValByKey(TreeDeptAttr.RefTreeNo,value);
			}
		}
		/// <summary>
		/// 工作部门
		/// </summary>
		public string FK_Dept
		{
			get
			{
				return this.GetValStringByKey(TreeDeptAttr.FK_Dept);
			}
			set
			{
				this.SetValByKey(TreeDeptAttr.FK_Dept,value);
			}
		}
		#endregion 

		#region 构造方法
		/// <summary>
		/// 抄送部门
		/// </summary>
		public TreeDept(){}
		/// <summary>
		/// 重写基类方法
		/// </summary>
		public override Map EnMap
		{
			get
			{
				if (this._enMap!=null) 
					return this._enMap;
				
				Map map = new Map("KM_TreeDept");				 
				map.EnDesc="权限部门";

				map.DepositaryOfEntity=Depositary.None;
				map.DepositaryOfMap=Depositary.Application;

                map.AddTBStringPK(TreeEmpAttr.RefTreeNo, null, "树", true, true, 1, 100, 100);
				map.AddDDLEntitiesPK( TreeDeptAttr.FK_Dept,null,"部门",new Depts(),true);

				this._enMap=map;
				 
				return this._enMap;
			}
		}
		#endregion

	}
	/// <summary>
	/// 权限部门
	/// </summary>
    public class TreeDepts : EntitiesMM
    {
        /// <summary>
        /// 他的工作部门
        /// </summary>
        public Stations HisStations
        {
            get
            {
                Stations ens = new Stations();
                foreach (TreeDept ns in this)
                {
                    ens.AddEntity(new Station(ns.FK_Dept));
                }
                return ens;
            }
        }
        /// <summary>
        /// 抄送部门
        /// </summary>
        public TreeDepts() { }
        /// <summary>
        /// 抄送部门
        /// </summary>
        /// <param name="NodeID">知识树ID</param>
        public TreeDepts(int NodeID)
        {
            QueryObject qo = new QueryObject(this);
            qo.AddWhere(TreeDeptAttr.RefTreeNo, NodeID);
            qo.DoQuery();
        }
        /// <summary>
        /// 抄送部门
        /// </summary>
        /// <param name="StationNo">StationNo </param>
        public TreeDepts(string StationNo)
        {
            QueryObject qo = new QueryObject(this);
            qo.AddWhere(TreeDeptAttr.FK_Dept, StationNo);
            qo.DoQuery();
        }
        /// <summary>
        /// 得到它的 Entity 
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new TreeDept();
            }
        }
        /// <summary>
        /// 转向此知识树的集合的Nodes
        /// </summary>
        /// <param name="nodeID">此知识树的ID</param>
        /// <returns>转向此知识树的集合的Nodes (FromNodes)</returns> 
        public Stations GetHisStations(int nodeID)
        {
            QueryObject qo = new QueryObject(this);
            qo.AddWhere(TreeDeptAttr.RefTreeNo, nodeID);
            qo.DoQuery();

            Stations ens = new Stations();
            foreach (TreeDept en in this)
            {
                ens.AddEntity(new Station(en.FK_Dept));
            }
            return ens;
        }

    }
}
