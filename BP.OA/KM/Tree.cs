using System;
using System.Data;
using System.Collections;
using BP.DA;
using BP.En;
using BP.Port;

namespace BP.KM
{
    /// <summary>
    /// 权限控制方式
    /// </summary>
    public enum KMTreeCtrlWay
    {
        /// <summary>
        /// 任何人
        /// </summary>
        Anyone,
        /// <summary>
        /// 继承
        /// </summary>
        Inherit,
        /// <summary>
        /// 按岗位
        /// </summary>
        ByStation,
        /// <summary>
        /// 按部门
        /// </summary>
        ByDept,
        /// <summary>
        /// 按照人员
        /// </summary>
        ByEmp,
        /// <summary>
        /// 按照部门与岗位的交集
        /// </summary>
        ByDeptAndStation,
        /// <summary>
        /// 按SQL
        /// </summary>
        BySQL
    }
    public enum FileStatus
    {
        /// <summary>
        /// 私有
        /// </summary>
        self,
        /// <summary>
        /// 共享给全体人员
        /// </summary>
        anyPersons,
        /// <summary>
        /// 共享给指定人
        /// </summary>
        somePersons,
        /// <summary>
        /// 删除
        /// </summary>
        deleted
    }
    /// <summary>
    /// 知识树属性
    /// </summary>
    public class TreeAttr : EntityTreeAttr
    {
        /// <summary>
        /// 控制方式
        /// </summary>
        public const string KMTreeCtrlWay = "KMTreeCtrlWay";
        /// <summary>
        /// 数量
        /// </summary>
        public const string NumOfInfos = "NumOfInfos";
        /// <summary>
        /// 发布人
        /// </summary>
        public const string FK_Emp = "FK_Emp";
        /// <summary>
        /// 创建日期
        /// </summary>
        public const string RDT = "RDT";
        /// <summary>
        /// 是否启用
        /// </summary>
        public const string IsEnable = "IsEnable";
        /// <summary>
        /// 文件状态
        /// </summary>
        public const string FileStatus = "FileStatus";
        /// <summary>
        /// 是否共享
        /// </summary>
        public const string IsShare = "IsShare";
        /// <summary>
        /// 文件大小
        /// </summary>
        public const string FileSize = "FileSize";
    }
    /// <summary>
    ///  知识树
    /// </summary>
    public class Tree : EntityTree
    {
        #region 属性.
        /// <summary>
        /// 个数
        /// </summary>
        public int NumOfInfos
        {
            get
            {
                return this.GetValIntByKey(TreeAttr.NumOfInfos);
            }
            set
            {
                this.SetValByKey(TreeAttr.NumOfInfos, value);
            }
        }
        /// <summary>
        /// 控制方式
        /// </summary>
        public KMTreeCtrlWay KMTreeCtrlWay
        {
            get
            {
                return (KMTreeCtrlWay)this.GetValIntByKey(TreeAttr.KMTreeCtrlWay);
            }
            set
            {
                this.SetValByKey(TreeAttr.KMTreeCtrlWay, (int)value);
            }
        }
        /// <summary>
        /// 发布人
        /// </summary>
        public string FK_Emp
        {
            get
            {
                return this.GetValStrByKey(TreeAttr.FK_Emp);
            }
            set
            {
                this.SetValByKey(TreeAttr.FK_Emp, value);
            }
        }
        /// <summary>
        /// 创建时间
        /// </summary>
        public string RDT
        {
            get
            {
                return this.GetValStrByKey(TreeAttr.RDT);
            }
            set
            {
                this.SetValByKey(TreeAttr.RDT, value);
            }
        }
        /// <summary>
        /// 文件状态
        /// </summary>
        public FileStatus FileStatus
        {
            get
            {
                return (FileStatus)this.GetValIntByKey(TreeAttr.FileStatus);
            }
            set
            {
                this.SetValByKey(TreeAttr.FileStatus, (int)value);
            }
        }
        /// <summary>
        /// 是否启用
        /// </summary>
        public string IsEnable
        {
            get
            {
                return this.GetValStrByKey(TreeAttr.IsEnable);
            }
            set
            {
                this.SetValByKey(TreeAttr.IsEnable, value);
            }
        }
        /// <summary>
        /// 是否共享
        /// </summary>
        public string IsShare
        {
            get
            {
                return this.GetValStrByKey(TreeAttr.IsShare);
            }
            set
            {
                this.SetValByKey(TreeAttr.IsShare, value);
            }
        }
        /// <summary>
        /// 文件大小
        /// </summary>
        public float FileSize
        {
            get
            {
                return this.GetValFloatByKey(TreeAttr.FileSize);
            }
            set
            {
                this.SetValByKey(TreeAttr.FileSize, value);
            }
        }
        #endregion 属性.

        #region 构造方法
        /// <summary>
        /// 知识树
        /// </summary>
        public Tree()
        {

        }
        /// <summary>
        /// 知识树
        /// </summary>
        /// <param name="_No"></param>
        public Tree(string _No) : base(_No) { }
        #endregion

        /// <summary>
        /// 知识树Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map("KM_Tree");
                map.EnDesc = "知识树";
                map.CodeStruct = "4";

                map.DepositaryOfEntity = Depositary.Application;
                map.DepositaryOfMap = Depositary.Application;
                map.IsAutoGenerNo = true;

                map.AddTBStringPK(TreeAttr.No, null, "编号", true, true, 4, 4, 4);

                map.AddTBString(TreeAttr.Name, null, "名称", true, false, 0, 100, 30);
                map.AddTBString(TreeAttr.ParentNo, null, "父节点编号", true, false, 0, 100, 30);
               // map.AddTBString(TreeAttr.TreeNo, null, "树编号", false, false, 0, 100, 30);

               // map.AddTBString(TreeAttr.ICON, null, "图标", false, false, 0, 100, 30);

                map.AddTBString(TreeAttr.FK_Emp, null, "发布人", true, false, 0, 100, 30);
                map.AddTBDateTime(TreeAttr.RDT, "创建日期", false, false);
                map.AddTBInt(TreeAttr.Idx, 0, "Idx", false, false);
              //  map.AddTBInt(TreeAttr.IsDir, 0, "IsDir", false, false);

                map.AddDDLSysEnum(TreeAttr.KMTreeCtrlWay, 0, "该节点访问控制方式", true, true,
                    "KMTreeCtrlWay","@0=任何人@1=继承@2=按岗位@3=按部门@4=按人员@5=按岗位与部门的交集@6=按SQL");

                map.AddTBStringDoc(TreeAttr.CtrlWayPara, null, "控制参数",true,false,true);

                map.AddTBInt(TreeAttr.NumOfInfos, 0, "文章数量", false, false);
                map.AddTBInt(TreeAttr.IsEnable, 1, "是否启用", false, false);
                map.AddDDLSysEnum(TreeAttr.FileStatus, 0, "文件状态", true, true, TreeAttr.FileStatus, "@0=私有@1=共享给全体人员@2=共享给指定人@3=删除");
                map.AddBoolean(TreeAttr.IsShare, false, "是否共享", true, false);
                map.AddTBFloat(TreeAttr.FileSize, 0, "文件大小", true, false);

                // 相关功能。
                map.AttrsOfOneVSM.Add(new BP.Port.Stations(), new BP.Port.Stations(),
                    TreeStationAttr.RefTreeNo, TreeStationAttr.FK_Station,
                    DeptAttr.Name, DeptAttr.No, "岗位");

                map.AttrsOfOneVSM.Add(new BP.Port.Depts(), new BP.Port.Depts(), TreeDeptAttr.RefTreeNo, TreeDeptAttr.FK_Dept, DeptAttr.Name,
                DeptAttr.No, "部门");

                map.AttrsOfOneVSM.Add(new BP.Port.Emps(), new BP.Port.Emps(), TreeEmpAttr.RefTreeNo, TreeEmpAttr.FK_Emp, DeptAttr.Name,
                    DeptAttr.No, "人员");

                RefMethod rm = new RefMethod();
                rm.Title = "增加下级";
                rm.ClassMethodName=this.ToString()+".DoCreateSubNode"; 
                map.AddRefMethod(rm);

                rm = new RefMethod();
                rm.Title = "增加同级";
                rm.ClassMethodName = this.ToString() + ".DoCreateSameLevelNode";
                map.AddRefMethod(rm);

                this._enMap = map;
                return this._enMap;
            }
        }

        #region 重写方法
        protected override bool beforeInsert()
        {
            this.FK_Emp = BP.Web.WebUser.No;
            return base.beforeInsert();
        }
        #endregion 重写方法
    }
    /// <summary>
    /// 知识树
    /// </summary>
    public class Trees : SimpleNoNames
    {
        /// <summary>
        /// 知识树s
        /// </summary>
        public Trees() { }
        /// <summary>
        /// 得到它的 Entity 
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new Tree();
            }
        }
    }
}
