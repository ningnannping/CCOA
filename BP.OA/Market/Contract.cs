using System;
using System.Data;
using System.Collections;
using BP.DA;
using BP.En;
using BP.Port;

namespace BP.OA.Market
{

    /// <summary>
    /// 合同
    /// </summary>
    public class ContractAttr : EntityNoNameAttr
    {
        /// <summary>
        /// 
        /// </summary>
        public const string Title = "Title";
        /// <summary>
        /// 合同编号
        /// </summary>
        public const string Htbh = "Htbh";
        /// <summary>
        /// 合同名称
        /// </summary>
        public const string Htmc = "Htmc";
        /// <summary>
        /// 项目
        /// </summary>
        public const string FK_XM = "FK_XM";
        /// <summary>
        /// 类型
        /// </summary>
        public const string LX = "LX";
        /// <summary>
        /// 总金额
        /// </summary>
        public const string Ze = "Ze";
        /// <summary>
        /// 建安金额
        /// </summary>
        public const string Jaje = "Jaje";
        /// <summary>
        /// 设计金额
        /// </summary>
        public const string Sjje = "Sjje";
        /// <summary>
        /// 设备金额
        /// </summary>
        public const string Sbje = "Sbje";
        /// <summary>
        /// 其他金额
        /// </summary>
        public const string Qtje = "Qtje";
        /// <summary>
        /// 签署日期
        /// </summary>
        public const string Qsrq = "Qsrq";
        /// <summary>
        /// 合同文件
        /// </summary>
        public const string Htwj = "Htwj";
        /// <summary>
        /// 结算类型
        /// </summary>
        public const string JSLX = "JSLX";


    }
    /// <summary>
    ///  合同登记实体类
    /// </summary>
    public class Contract : EntityNoName
    {
        #region 属性
        /// <summary>
        /// 标题
        /// </summary>
        public string Title
        {
            get
            {
                return this.GetValStrByKey(ContractAttr.Title);
            }
            set
            {
                this.SetValByKey(ContractAttr.Title, value);
            }
        }
     
        #endregion 属性

        #region 权限控制属性.
        
        #endregion 权限控制属性.

        #region 构造方法
        /// <summary>
        /// 投标信息
        /// </summary>
        public Contract()
        {
        }
        /// <summary>
        /// 投标信息
        /// </summary>
        /// <param name="_No"></param>
        public Contract(string _No) : base(_No) { }
        #endregion

        #region 重写方法
        protected override bool beforeInsert()
        {

            return base.beforeInsert();
        }

        protected override bool beforeUpdate()
        {

            return base.beforeUpdate();
        }
        #endregion 重写方法

        /// <summary>
        /// 投标信息Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map("OA_Contract");
                map.EnDesc = "合同登记";
                map.IsAutoGenerNo = true;
                map.CodeStruct = "3"; //四位编号0001开始
                map.AddTBStringPK(ContractAttr.No, null, "编号", false, true, 3, 3, 3);
                map.AddTBString(ContractAttr.Htbh, null, "合同编号", true, false, 0, 100, 3);
                map.AddTBString(ContractAttr.Htmc, null, "合同名称", true, false, 0, 100, 3);
                map.AddTBString(ContractAttr.FK_XM, null, "项目", true, false, 0, 100, 3);
                map.AddDDLSysEnum(ContractAttr.LX, 1, "类型", true, true, ContractAttr.LX, "@1=总包@2=设计@3=运营");
                map.AddTBMoney(ContractAttr.Ze, 0, "总额", true, false);
                map.AddTBMoney(ContractAttr.Jaje, 0, "建安金额", true, false);
                map.AddTBMoney(ContractAttr.Sjje, 0, "设计金额", true, false);
                map.AddTBMoney(ContractAttr.Sbje, 0, "设备总额", true, false);
                map.AddTBMoney(ContractAttr.Qtje, 0, "其他总额", true, false);
                map.AddTBDate(ContractAttr.Qsrq, null, "签署日期", true, false);
                map.AddTBString(ContractAttr.Htwj,null,"合同文件",true,false,0,100,3);
                map.AddDDLSysEnum(ContractAttr.JSLX, 1, "结算类型", true, true, ContractAttr.JSLX, "@1=总包@2=设计@3=运营");
                this._enMap = map;
                return this._enMap;
            }
        }
       
    }
    /// <summary>
    /// 投标信息
    /// </summary>
    public class Contracts : SimpleNoNames
    {
        /// <summary>
        /// 汽车信息s
        /// </summary>
        public Contracts() { }
        /// <summary>
        /// 得到它的 Entity 
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new Contract();
            }
        }
    }
}
