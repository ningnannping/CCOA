﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.DA;
using BP.En;
using BP.Port;
namespace BP.OA.Market
{
   public class WaiXieHuiAttr: EntityNoNameAttr
    {
        /// <summary>
        /// 名称
        /// </summary>
       public const string MingCheng = "MingCheng";
       /// <summary>
       /// 联系人
       /// </summary>
       public const string LianXiRen = "LianXiRen";
       /// <summary>
       /// 联系电话
       /// </summary>
       public const string LianXiDianHua = "LianXiDianHua";
       /// <summary>
       ///联系地址
       /// </summary>
       public const string LianXiDiZhi = "LianXiDiZhi";
       /// <summary>
       ///传真
       /// </summary>
       public const string ChuanZhen = "ChuanZhen";
       /// <summary>
       /// Email
       /// </summary>
       public const string Email = "Email";
       /// <summary>
       /// 备注
       /// </summary>
       public const string BeiZhu = "BeiZhu";
    }



   /// <summary>
   /// 公司资质实体类
   /// </summary>
   public class WaiXieHui : EntityNoName
   {
       #region 属性
       /// <summary>
       /// 名称
       /// </summary>
       public string MingCheng
       {
           get
           {
               return this.GetValStrByKey(WaiXieHuiAttr.MingCheng);
           }
           set
           {
               this.SetValByKey(WaiXieHuiAttr.MingCheng, value);
           }
       }
      /// <summary>
        /// 联系人
        /// </summary>
        public string LianXiRen
        {
            get
            {
                return this.GetValStrByKey(WaiXieHuiAttr.LianXiRen);
            }
            set
            {
                this.SetValByKey(WaiXieHuiAttr.LianXiRen, value);
            }
        }
        /// <summary>
        /// 联系电话
        /// </summary>
        public string LianXiDianHua
        {
            get
            {
                return this.GetValStrByKey(WaiXieHuiAttr.LianXiDianHua);
            }
            set
            {
                this.SetValByKey(WaiXieHuiAttr.LianXiDianHua, value);
            }
        }
        /// <summary>
        /// 联系地址
        /// </summary>
        public string LianXiDiZhi
        {
            get
            {
                return this.GetValStrByKey(WaiXieHuiAttr.LianXiDiZhi);
            }
            set
            {
                this.SetValByKey(WaiXieHuiAttr.LianXiDiZhi, value);
            }
        }
        /// <summary>
        /// 传真
        /// </summary>
        public string ChuanZhen
        {
            get
            {
                return this.GetValStrByKey(WaiXieHuiAttr.ChuanZhen);
            }
            set
            {
                this.SetValByKey(WaiXieHuiAttr.ChuanZhen, value);
            }
        }
        /// <summary>
        /// Email
        /// </summary>
        public string Email
        {
            get
            {
                return this.GetValStrByKey(WaiXieHuiAttr.Email);
            }
            set
            {
                this.SetValByKey(WaiXieHuiAttr.Email, value);
            }
        }
        /// <summary>
        /// 备注
        /// </summary>
        public string BeiZhu
        {
            get
            {
                return this.GetValStrByKey(WaiXieHuiAttr.BeiZhu);
            }
            set
            {
                this.SetValByKey(WaiXieHuiAttr.BeiZhu, value);
            }
        }
       #endregion 属性
       #region 权限控制属性.

       #endregion 权限控制属性.

       #region 构造方法
       /// <summary>
       /// 投标信息
       /// </summary>
       public WaiXieHui()
       {
       }
       /// <summary>
       /// 投标信息
       /// </summary>
       /// <param name="_No"></param>
       public WaiXieHui(string _No) : base(_No) { }
       #endregion

       #region 重写方法
       protected override bool beforeInsert()
       {

           return base.beforeInsert();
       }

       protected override bool beforeUpdate()
       {

           return base.beforeUpdate();
       }
       #endregion 重写方法

       /// <summary>
       /// 投标信息Map
       /// </summary>
       public override Map EnMap
       {
           get
           {
               if (this._enMap != null)
                   return this._enMap;

               Map map = new Map("OA_WaiXieHui");
               map.EnDesc = "外协会信息维护";
               //map.IsAutoGenerNo = true;
               map.CodeStruct = "3"; //三位编号001开始
               map.AddTBStringPK(WaiXieHuiAttr.No, null, "编号", true, true, 0, 100, 40);
               map.AddTBString(WaiXieHuiAttr.MingCheng, null, "名称", true, false, 0, 100, 40);
               map.AddTBString(WaiXieHuiAttr.LianXiRen, null, "联系人", true, false, 0, 100, 40);
               map.AddTBString(WaiXieHuiAttr.LianXiDiZhi, null, "联系地址", true, false, 0, 100, 40);
               map.AddTBString(WaiXieHuiAttr.LianXiDianHua, null, "联系电话", true, false, 0, 100, 40);
               map.AddTBString(WaiXieHuiAttr.ChuanZhen, null, "传真", true, false, 0, 100, 40);
               map.AddTBString(WaiXieHuiAttr.Email, null, "Email", true, false, 0, 100, 40);
               map.AddTBStringDoc(WaiXieHuiAttr.BeiZhu, null, "备注", true, false, true);
               this._enMap = map;
               return this._enMap;
           }
       }

   }
   /// <summary>
   /// 投标信息
   /// </summary>
   public class WaiXieHuis : SimpleNoNames
   {
       /// <summary>
       /// 汽车信息s
       /// </summary>
       public WaiXieHuis() { }
       /// <summary>
       /// 得到它的 Entity 
       /// </summary>
       public override Entity GetNewEntity
       {
           get
           {
               return new WaiXieHui();
           }
       }
   }
}
