﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.DA;
using BP.En;
using BP.Port;
namespace BP.OA.Market
{
   public class ShouKuanJiHuaAttr: EntityOIDAttr
    {
        /// <summary>
        /// 计划编号
        /// </summary>
       public const string JiHuaBianHao = "JiHuaBianHao";
        /// <summary>
        /// 计划人
        /// </summary>
       public const string JiHuaRen = "JiHuaRen";
        /// <summary>
        /// 计划日期
        /// </summary>
       public const string JuHuaDate = "JuHuaDate";
       
        /// <summary>
        /// 备注
        /// </summary>
        public const string BeiZhu = "BeiZhu";
    }



   /// <summary>
   /// 市场收款计划实体类
   /// </summary>
   public class ShouKuanJiHua : EntityOID
   {
       #region 属性
       /// <summary>
       /// 计划编号
       /// </summary>
       public string JiHuaBianHao
       {
           get
           {
               return this.GetValStrByKey(ShouKuanJiHuaAttr.JiHuaBianHao);
           }
           set
           {
               this.SetValByKey(ShouKuanJiHuaAttr.JiHuaBianHao, value);
           }
       }
       /// <summary>
       /// 计划人
       /// </summary>
       public string JiHuaRen
       {
           get
           {
               return this.GetValStrByKey(ShouKuanJiHuaAttr.JiHuaRen);
           }
           set
           {
               this.SetValByKey(ShouKuanJiHuaAttr.JiHuaRen, value);
           }
       }
       
       /// <summary>
       ///  计划日期
       /// </summary>
       public DateTime JingBanDate
       {
           get
           {
               return this.GetValDateTime(ShouKuanJiHuaAttr.JuHuaDate);
           }
           set
           {
               this.SetValByKey(ShouKuanJiHuaAttr.JuHuaDate, value);
           }
       }

       /// <summary>
       /// 备注
       /// </summary>
       public string BeiZhu
       {
           get
           {
               return this.GetValStrByKey(ShouKuanJiHuaAttr.BeiZhu);
           }
           set
           {
               this.SetValByKey(ShouKuanJiHuaAttr.BeiZhu, value);
           }
       }
       #endregion 属性

       #region 权限控制属性.

       #endregion 权限控制属性.

       #region 构造方法
       /// <summary>
       /// 投标信息
       /// </summary>
       public ShouKuanJiHua()
       {
       }
       /// <summary>
       /// 投标信息
       /// </summary>
       /// <param name="_No"></param>
       public ShouKuanJiHua(int oid) : base(oid) { }
       #endregion

       #region 重写方法
       protected override bool beforeInsert()
       {

           return base.beforeInsert();
       }

       protected override bool beforeUpdate()
       {

           return base.beforeUpdate();
       }
       #endregion 重写方法

       /// <summary>
       /// 投标信息Map
       /// </summary>
       public override Map EnMap
       {
           get
           {
               if (this._enMap != null)
                   return this._enMap;

               Map map = new Map("OA_ShouKuanJiHua");
               map.EnDesc = "市场收款计划";
               //map.IsAutoGenerNo = true;
               map.CodeStruct = "3"; //三位编号001开始
               //增加OID主键字段。
               map.AddTBIntPKOID();

               map.AddTBString(ShouKuanJiHuaAttr.JiHuaBianHao, null, "计划编号", true, false, 0, 100, 40);
             
               map.AddTBString(ShouKuanJiHuaAttr.JiHuaRen, null, "计划人", true, false, 0, 100, 40);

               
               map.AddTBDate(ShouKuanJiHuaAttr.JuHuaDate, "计划日期", true, false);
               map.AddTBStringDoc(ShouKuanJiHuaAttr.BeiZhu, null, "备注", true, false, true);
               this._enMap = map;
               return this._enMap;
           }
       }

   }
   /// <summary>
   /// 投标信息
   /// </summary>
   public class ShouKuanJiHuas : SimpleNoNames
   {
       /// <summary>
       /// 汽车信息s
       /// </summary>
       public ShouKuanJiHuas() { }
       /// <summary>
       /// 得到它的 Entity 
       /// </summary>
       public override Entity GetNewEntity
       {
           get
           {
               return new ShouKuanJiHua();
           }
       }
   }
}
