﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.DA;
using BP.En;
using BP.Port;
namespace BP.OA.Market
{
    public class ShouJuDengJiAttr : EntityOIDAttr
    {
        /// <summary>
        /// 合同编号
        /// </summary>
        public const string HeTongBianHao = "HeTongBianHao";
        /// <summary>
        /// 合同金额
        /// </summary>
        public const string HeTongJE = "HeTongJE";
        /// <summary>
        /// 收据号
        /// </summary>
        public const string ShouJuHao = "ShouJuHao";
        /// <summary>
        ///项目名称
        /// </summary>
        public const string XiangMuMingCheng = "XiangMuMingCheng";
        /// <summary>
        ///收据金额
        /// </summary>
        public const string ShouJuJE = "ShouJuJE";
        /// <summary>
        /// 财务实收金额
        /// </summary>
        public const string CaiWuShiShouJE = "CaiWuShiShouJE";
        /// <summary>
        /// 经办人
        /// </summary>
        public const string JingBanRen = "JingBanRen";
        /// <summary>
        /// 客户名称
        /// </summary>
        public const string KeHuMingCheng = "KeHuMingCheng";
        /// <summary>
        /// 经办日期
        /// </summary>
        public const string JingBanDate = "JingBanDate";
        /// <summary>
        /// 备注
        /// </summary>
        public const string BeiZhu = "BeiZhu";
    }



    /// <summary>
    /// 公司资质实体类
    /// </summary>
    public class ShouJuDengJi : EntityOID
    {
        #region 属性
        /// <summary>
        /// 合同编号
        /// </summary>
        public string HeTongBianHao
        {
            get
            {
                return this.GetValStrByKey(ShouJuDengJiAttr.HeTongBianHao);
            }
            set
            {
                this.SetValByKey(ShouJuDengJiAttr.HeTongBianHao, value);
            }
        }
        /// <summary>
        /// 合同金额
        /// </summary>
        public decimal HeTongJE
        {
            get
            {
                return this.GetValDecimalByKey(ShouJuDengJiAttr.HeTongJE);
            }
            set
            {
                this.SetValByKey(ShouJuDengJiAttr.HeTongJE, value);
            }
        }
        /// <summary>
        /// 收据号
        /// </summary>
        public string ShouJuHao
        {
            get
            {
                return this.GetValStrByKey(ShouJuDengJiAttr.ShouJuHao);
            }
            set
            {
                this.SetValByKey(ShouJuDengJiAttr.ShouJuHao, value);
            }
        }
        /// <summary>
        /// 项目名称
        /// </summary>
        public string LianXiDiZhi
        {
            get
            {
                return this.GetValStrByKey(ShouJuDengJiAttr.XiangMuMingCheng);
            }
            set
            {
                this.SetValByKey(ShouJuDengJiAttr.XiangMuMingCheng, value);
            }
        }
        /// <summary>
        /// 收据金额
        /// </summary>
        public decimal ShouJuJE
        {
            get
            {
                return this.GetValDecimalByKey(ShouJuDengJiAttr.ShouJuJE);
            }
            set
            {
                this.SetValByKey(ShouJuDengJiAttr.ShouJuJE, value);
            }
        }
        /// <summary>
        /// 财务实收金额
        /// </summary>
        public decimal CaiWuShiShouJE
        {
            get
            {
                return this.GetValDecimalByKey(ShouJuDengJiAttr.CaiWuShiShouJE);
            }
            set
            {
                this.SetValByKey(ShouJuDengJiAttr.CaiWuShiShouJE, value);
            }
        }
      
            /// <summary>
           ///   经办人
           /// </summary>
        public string JingBanRen
        {
            get
            {
                return this.GetValStrByKey(ShouJuDengJiAttr.JingBanRen);
            }
            set
            {
                this.SetValByKey(ShouJuDengJiAttr.JingBanRen, value);
            }
        }
          /// <summary>
           ///   客户名称
           /// </summary>
        public string KeHuMingCheng
        {
            get
            {
                return this.GetValStrByKey(ShouJuDengJiAttr.KeHuMingCheng);
            }
            set
            {
                this.SetValByKey(ShouJuDengJiAttr.KeHuMingCheng, value);
            }
        }
          /// <summary>
           ///  经办日期
           /// </summary>
        public DateTime JingBanDate
        {
            get
            {
                return this.GetValDateTime(ShouJuDengJiAttr.JingBanDate);
            }
            set
            {
                this.SetValByKey(ShouJuDengJiAttr.JingBanDate, value);
            }
        }

        /// <summary>
        /// 备注
        /// </summary>
        public string BeiZhu
        {
            get
            {
                return this.GetValStrByKey(ShouJuDengJiAttr.BeiZhu);
            }
            set
            {
                this.SetValByKey(ShouJuDengJiAttr.BeiZhu, value);
            }
        }
        #endregion 属性
        #region 权限控制属性.

        #endregion 权限控制属性.

        #region 构造方法
        /// <summary>
        /// 投标信息
        /// </summary>
        public ShouJuDengJi()
        {
        }
        /// <summary>
        /// 投标信息
        /// </summary>
        /// <param name="_No"></param>
        public ShouJuDengJi(int oid) : base(oid) { }
        #endregion

        #region 重写方法
        protected override bool beforeInsert()
        {

            return base.beforeInsert();
        }

        protected override bool beforeUpdate()
        {

            return base.beforeUpdate();
        }
        #endregion 重写方法

        /// <summary>
        /// 投标信息Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map("OA_ShouJuDengJi");
                map.EnDesc = "市场收据登记";
                //map.IsAutoGenerNo = true;
                map.CodeStruct = "3"; //三位编号001开始
                //增加OID主键字段。
                map.AddTBIntPKOID();
               
                map.AddTBString(ShouJuDengJiAttr.HeTongBianHao, null, "合同编号", true, false, 0, 100, 40);
                map.AddTBMoney(ShouJuDengJiAttr.HeTongJE, 0, "合同金额", true, false);
                map.AddTBString(ShouJuDengJiAttr.ShouJuHao, null, "收据号", true, false, 0, 100, 40);
                
                map.AddTBString(ShouJuDengJiAttr.XiangMuMingCheng, null, "项目名称", true, false, 0, 100, 40);
                map.AddTBMoney(ShouJuDengJiAttr.ShouJuJE, 0, "收据金额", true, false);
                map.AddTBMoney(ShouJuDengJiAttr.CaiWuShiShouJE, 0, "财务实收金额", true, false);
                map.AddTBString(ShouJuDengJiAttr.JingBanRen, null, "经办人", true, false, 0, 100, 40);
                map.AddTBString(ShouJuDengJiAttr.KeHuMingCheng, null, "客户名称", true, false, 0, 100, 40);
                map.AddTBDate(ShouJuDengJiAttr.JingBanDate, "经办日期", true, false);
                map.AddTBStringDoc(ShouJuDengJiAttr.BeiZhu, null, "备注", true, false, true);
                this._enMap = map;
                return this._enMap;
            }
        }

    }
    /// <summary>
    /// 投标信息
    /// </summary>
    public class ShouJuDengJis : SimpleNoNames
    {
        /// <summary>
        /// 汽车信息s
        /// </summary>
        public ShouJuDengJis() { }
        /// <summary>
        /// 得到它的 Entity 
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new ShouJuDengJi();
            }
        }
    }
}
