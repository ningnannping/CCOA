﻿using BP.DA;
using BP.En;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.Sys;

namespace BP.OA.Operate
{
    /// <summary>
    /// 采购类型
    /// </summary>
    public enum Type
    {
        /// <summary>
        /// 未通过
        /// </summary>
        NotPass,
        /// <summary>
        /// 通过
        /// </summary>
        Pass,
        /// <summary>
        /// 审核中
        /// </summary>
        GoOn
    }
    /// <summary>
    /// 采购计划
    /// </summary>
    public class ProcurementPlanAttr : EntityNoNameAttr
    {
        /// <summary>
        /// 计划状态
        /// </summary>
        public const string JHType = "JHType";
        /// <summary>
        /// 项目
        /// </summary>
        public const string FK_Project = "FK_Project";
        /// <summary>
        /// 计划人
        /// </summary>
        public const string JHPerson = "JHPerson";
        /// <summary>
        /// 计划日期
        /// </summary>
        public const string JHDate = "JHDate";
        /// <summary>
        /// 备注 
        /// </summary>
        public const string Note = "Note";

        public const string FK_NY = "FK_NY";
    }
    /// <summary>
    /// 采购计划
    /// </summary>
    public class ProcurementPlan : EntityNoName
    {
        #region 属性


        public Type JHType
        {
            get 
            {
                return (Type)this.GetValIntByKey(ProcurementPlanAttr.JHType);
            }
            set 
            {
                this.SetValByKey(ProcurementPlanAttr.JHType,(int)value);
            }
        }

        public string FK_Project
        {
            get 
            {
                return this.GetValStrByKey(ProcurementPlanAttr.FK_Project);
            }
        }


        #endregion

        #region 构造方法

        public ProcurementPlan(){ }

        public ProcurementPlan(string _No) : base(_No) { }

        #endregion

        public override Map EnMap
        {
            get 
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map("OA_ProcurementPlan");
                map.EnDesc = "采购计划";
                map.CodeStruct = "3";

                map.DepositaryOfEntity = Depositary.Application;
                map.DepositaryOfMap = Depositary.Application;

                map.AddTBStringPK(ProcurementPlanAttr.No, null, "编号",false,true, 3, 3, 3);
                map.AddTBString(ProcurementPlanAttr.Name, null, "计划单号", true, false,0,100, 30);
                map.AddDDLSysEnum(ProcurementPlanAttr.JHType, 0, "计划状态", true, true, ProcurementPlanAttr.JHType, "@0=审核未通过@1=审核通过@2=审核中");
                map.AddDDLEntities(ProcurementPlanAttr.FK_Project,null,"项目名称",new Projects(),true);
                map.AddTBDate(ProcurementPlanAttr.JHDate,"计划日期",true,false);
                map.AddTBStringDoc(ProcurementPlanAttr.Note, null, "备注", true, false, true);
                map.AddDDLEntities(ProcurementPlanAttr.FK_NY, null, "隶属年月", new BP.Pub.NYs(),true);
                map.AddSearchAttr(ProcurementPlanAttr.JHType);
                map.AddSearchAttr(ProcurementPlanAttr.FK_NY);


                RefMethod rm = new RefMethod();
                rm.Title = "采购物品";
                rm.ClassMethodName = this.ToString() + ".DoGoods";
                map.AddRefMethod(rm);

                


                this._enMap = map;
                return this._enMap;
            }
        }
        public string DoGoods()
        {
            return "/WF/Comm/SearchBS.htm?EnsName=BP.OA.Operate.Goods&FK_PP=" + this.No;
        }
    }
    /// <summary>
    /// 采购计划
    /// </summary>
    public class ProcurementPlans : SimpleNoNames
    {
        public ProcurementPlans() { }

        public override Entity GetNewEntity
        {
            get 
            {
                return new ProcurementPlan();
            }
        }
    }

}
