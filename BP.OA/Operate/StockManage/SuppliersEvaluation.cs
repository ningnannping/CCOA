﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.DA;
using BP.En;
using BP.Port;
using System.Data;

namespace BP.OA.Operate.StockManage
{
     public class SuppliersEvaluationAttr : EntityNoNameAttr
    {

      
        /// <summary>
        /// 从父类继承的Name属性，用于外键显示
        /// </summary>
        public const string Name = "Name";
         /// <summary>
         /// 外键
         /// </summary>
        public const string FK_Suppliers = "FK_Suppliers";
        ///
        ///登记人
        ///
        public const string RegistName = "RegistrName";
        ///
        ///登记日期
        ///
        public const string RegistDate = "RegistDate";
        ///
        ///评价内容
        ///
        public const string Evaluation = "Evaluation";
    }

     public class SuppliersEvaluation : EntityOIDName
        {
        #region  属性
            public string FK_Suppliers
            {
                get
                {
                    return this.GetValStringByKey(SuppliersEvaluationAttr.FK_Suppliers);
                }
                set
                {
                    this.SetValByKey(SuppliersEvaluationAttr.FK_Suppliers, value);
                }
            }
            public string RegistName
            {
                get
                {
                    return this.GetValStringByKey(SuppliersEvaluationAttr.RegistName);
                }
                set
                {
                    this.SetValByKey(SuppliersEvaluationAttr.RegistName,value);
                }
            }
            public  string RegistDate {
                get {
                    return this.GetValStringByKey(SuppliersEvaluationAttr.RegistDate);
                }
                set {
                    this.SetValByKey(SuppliersEvaluationAttr.RegistDate,value);
                }
            }
            public string Evaluation {
               get {
                    return this.GetValStringByKey(SuppliersEvaluationAttr.Evaluation);
                }
                set {
                    this.SetValByKey(SuppliersEvaluationAttr.Evaluation,value);
                }
            }
        
        #endregion
        #region 权限控制
        #endregion 权限控制

        #region 构造方法
        /// <summary>
        /// 印章
        /// </summary>
        public SuppliersEvaluation()
        { }
        /// <summary>
        /// 供应商评价
        /// </summary>
        /// <param name="oid"></param>
        public SuppliersEvaluation(int oid) : base(oid) { }
        #endregion 构造方法

        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;
                Map map = new Map("OA_SuppliersEvaluation");
                map.EnDesc = "供应商评价";
                map.CodeStruct = "3";
                map.DepositaryOfEntity = Depositary.Application;
                map.DepositaryOfMap = Depositary.Application;

                map.AddTBIntPKOID();

                //map.AddTBStringPK(SuppliersManageAttr.No, null, "编号", true, true, 3, 3, 3);
                map.AddTBStringPK(SuppliersEvaluationAttr.Name, null, "", false, true, 0, 100, 30);
                map.AddDDLEntities(SuppliersEvaluationAttr.FK_Suppliers,null,"供应商名称",new SuppliersManages(),true);

                map.AddTBString(SuppliersEvaluationAttr.RegistName,null,"登记人",true,false,0,100,30);
                map.AddTBDate(SuppliersEvaluationAttr.RegistDate, "登记日期", true, true);
                //map.AddTBDate(SuppliersEvaluationAttr.RegistDate, System.DateTime.Now.ToString,"登记日期", true, true);
                map.AddTBString(SuppliersEvaluationAttr.Evaluation,null,"评价内容",true,false,0,100,30);

               
                //查询条件
                map.AddSearchAttr(SuppliersEvaluationAttr.FK_Suppliers);
                //map.AddSearchAttr(SuppliersEvaluationAttr.RegistDate);
                //map.AddSearchAttr(SuppliersEvaluationAttr.Evaluation);
             
                this._enMap = map;
                return this._enMap;
            }
        }
        #region 重写方法
        protected override bool beforeInsert()
        {
            return base.beforeInsert();
        }

        protected override bool beforeUpdate()
        {
            return base.beforeUpdate();
        }
        protected override bool beforeUpdateInsertAction()
        {
            this.Name = this.RegistName;
            return base.beforeUpdateInsertAction();
        }
        #endregion 重写方法
    }
     public class SuppliersEvaluations : EntitiesOID
        {
            /// <summary>
            /// SuppliersEvaluations
            /// </summary>
            public SuppliersEvaluations() { }
            /// <summary>
            /// 得到它的 Entity 
            /// </summary>
            public override Entity GetNewEntity
            {
                get
                {
                    return new SuppliersEvaluation();
                }
            }
        }
}
