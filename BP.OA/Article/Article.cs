﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.DA;
using BP.En;
using BP.Web;

namespace BP.OA
{
    /// <summary>
    /// 通知类别属性
    /// </summary>
    public class ArticleAttr : EntityOIDAttr
    {
        public const String FK_UserNo = "FK_UserNo";
        public const String FK_ArticleCatagory = "FK_ArticleCatagory";
        public const String AddTime = "AddTime";
        public const String Title = "Title";
        public const String KeyWords = "KeyWords";
        public const String ArticleSource = "ArticleSource";
        public const String Doc = "Doc";
        public const String BrowseCount = "BrowseCount";
        public const String AttachFile = "AttachFile";
        public const String SetTop = "SetTop";
    }
    /// <summary>
    /// 通知类别
    /// </summary>
    public partial class Article : EntityOID
    {
        #region 属性
        public string FK_UserNo
        {
            get { return this.GetValStrByKey(ArticleAttr.FK_UserNo); }
            set { this.SetValByKey(ArticleAttr.FK_UserNo, value); }
        }
        public string FK_ArticleCatagory
        {
            get { return this.GetValStrByKey(ArticleAttr.FK_ArticleCatagory); }
            set { this.SetValByKey(ArticleAttr.FK_ArticleCatagory, value); }
        }
        public string ArticleCatagoryText
        {
            get { return this.GetValRefTextByKey(ArticleAttr.FK_ArticleCatagory); }
        }
        /// <summary>
        /// 增加时间
        /// </summary>
        public string AddTime
        {
            get { return this.GetValStrByKey(ArticleAttr.AddTime); }
            set { this.SetValByKey(ArticleAttr.AddTime, value); }
        }
        public String Title
        {
            get { return this.GetValStrByKey(ArticleAttr.Title); }
            set { this.SetValByKey(ArticleAttr.Title, value); }
        }
        public String ArticleSource
        {
            get { return this.GetValStrByKey(ArticleAttr.ArticleSource); }
            set { this.SetValByKey(ArticleAttr.ArticleSource, value); }
        }
        public String KeyWords
        {
            get { return this.GetValStrByKey(ArticleAttr.KeyWords); }
            set { this.SetValByKey(ArticleAttr.KeyWords, value); }
        }
        public string Doc
        {
            get {//新闻内容不能为空的前提
                string doc = this.GetValStrByKey(ArticleAttr.Doc);
                if (doc=="" || doc.Length == 0 )
                    return this.GetBigTextFromDB("DocFile");
                else
                    return doc;
                 }
            set {
                if (value.Length > 2000)
                {
                    this.SaveBigTxtToDB("DocFile", value);
                    this.SetValByKey(ArticleAttr.Doc, "");
                }
                else
                {
                    this.SetValByKey(ArticleAttr.Doc, value);
                }

                }
        }
        public int BrowseCount
        {
            get { return this.GetValIntByKey(ArticleAttr.BrowseCount); }
            set { this.SetValByKey(ArticleAttr.BrowseCount, value); }
        }
        public string AttachFile
        {
            get { return this.GetValStrByKey(ArticleAttr.AttachFile); }
            set { this.SetValByKey(ArticleAttr.AttachFile, value); }
        }
        public DateTime SetTop
        {
            get { return this.GetValDateTime(ArticleAttr.SetTop); }
            set { this.SetValByKey(ArticleAttr.SetTop, value); }
        }
        #endregion

        #region 构造函数
        /// <summary>
        /// 通知类别
        /// </summary>
        public Article() { }
        /// <summary>
        /// 通知类别
        /// </summary>
        /// <param name="no">编号</param>
        public Article(int no) : base(no) { }
        #endregion

        #region 重写方法
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                uac.OpenForSysAdmin();
                return uac;
            }
        }
        /// <summary>
        /// Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map();
                map.PhysicsTable = "OA_Article";
                map.EnDesc = "新闻文章";                   // 实体的描述.
                map.CodeStruct = "2";
                //map.DepositaryOfEntity = Depositary.Application; //实体map的存放位置.
                //map.DepositaryOfMap = Depositary.Application;    // Map 的存放位置.
                map.AddTBIntPKOID();
                map.AddTBString(ArticleAttr.FK_UserNo, null, "发布人", true, false, 2, 50, 20);
                map.AddDDLEntities(ArticleAttr.FK_ArticleCatagory, "0", "新闻目录", new ArticleCatagorys(), true);
                map.AddTBDateTime(ArticleAttr.AddTime, null, "添加时间", true, false);
                map.AddTBString(ArticleAttr.Title, null, "新闻标题", true, false, 0, 100, 50);
                map.AddTBString(ArticleAttr.KeyWords, null, "新闻关键字", true, false, 0, 100, 50);
                map.AddTBString(ArticleAttr.ArticleSource, null, "新闻来源", true, false, 0, 100, 50);
                map.AddTBInt(ArticleAttr.BrowseCount, 0, "新闻浏览量", true, false);
                map.AddTBString(ArticleAttr.AttachFile, null, "新闻附件", true, false, 0, 4000, 50);
                map.AddTBStringDoc(ArticleAttr.Doc, null, "新闻内容", true, false);
                map.AddTBDateTime(ArticleAttr.SetTop, "置顶", false, false);
                //map.AddTBString(ArticleAttr.ParentNo, null, "父知识树No", true, false, 0, 100, 30);
                //map.AddTBString(ArticleAttr.FK_Unit, null, "所在单位", true, false, 0, 50, 30);

                this._enMap = map;
                return this._enMap;
            }
        }
        #endregion

        #region 自主开发
        public System.Data.DataTable GetNewsDetail(int id)
        {
            string sSql = String.Format("select Title,KeyWords,B.Name as ArticleCatagory,AddTime,FK_UserNo,Doc,ArticleSource,BrowseCount,AttachFile from OA_Article A"
                             + " inner join OA_ArticleCatagory B on A.FK_ArticleCatagory=B.No Where A.OID={0}", this.OID);
            return BP.DA.DBAccess.RunSQLReturnTable(sSql);
        }
        #endregion
    }
    /// <summary>
    ///得到集合
    /// </summary>
    public class Articles : EntitiesOID
    {
        /// <summary>
        /// 得到一个新实体
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new Article();
            }
        }
        /// <summary>
        /// 通知类别集合
        /// </summary>
        public Articles()
        {
        }
    }
}
