using System;
using System.Data;
using BP.DA;
using BP.En;
using BP.Web;
using BP.Sys;

namespace BP.OA
{
    /// <summary>
    /// 通知属性
    /// </summary>
    public class NoticeAttr : EntityOIDAttr
    {
        #region 1.基本信息(No,Title,Doc,File,FK_NoticeCategory)
        /// <summary>
        /// 标题
        /// </summary>
        public const string Title = "Title";
        /// <summary>
        /// 内容
        /// </summary>
        public const string Doc = "Doc";
        /// <summary>
        /// 上传文件
        /// </summary>
        public const string AttachFile = "AttachFile";
        /// <summary>
        /// 类别
        /// </summary>
        public const string FK_NoticeCategory = "FK_NoticeCategory";
        #endregion

        #region 2.创建信息(Rec,RDT,FK_Dept)
        /// <summary>
        /// 创建者
        /// </summary>
        public const string Rec = "Rec";
        /// <summary>
        /// 发布日期
        /// </summary>
        public const string RDT = "RDT";
        /// <summary>
        /// 发布部门
        /// </summary>
        public const string FK_Dept = "FK_Dept";
        /// <summary>
        /// 发给部分人
        /// </summary>
        public const string SendToPart = "SendToParts";
        /// <summary>
        /// 发往人员
        /// </summary>
        public const string SendToUsers = "SendToUsers";
        /// <summary>
        /// 发往部门
        /// </summary>
        public const string SendToDepts = "SendToDepts";
        /// <summary>
        /// 发往职位
        /// </summary>
        public const string SendToStations = "SendToStations";
        #endregion

        #region 3.展示设置(SetTop,Importance)
        /// <summary>
        /// 置顶时间
        /// </summary>
        public const string SetTop = "SetTop";
        /// <summary>
        /// 重要级别
        /// </summary>
        public const string Importance = "Importance";
        #endregion

        #region 4.反馈意见(GetAdvices,AdviceDesc,AdviceItems)
        /// <summary>
        /// 是否接受反馈意见
        /// </summary>
        public const string GetAdvices = "GetAdvices";
        /// <summary>
        /// 反馈意见说明
        /// </summary>
        public const string AdviceDesc = "AdviceDesc";
        /// <summary>
        /// 意见项
        /// </summary>
        public const string AdviceItems = "AdviceItems";
        #endregion

        #region 5.其它信息(NoticeSta,Starttime,Stoptime,SetMes)
        /// <summary>
        /// 状态(自动，开启，关闭)
        /// </summary>
        public const string NoticeSta = "NoticeSta";
        /// <summary>
        /// 开始时间
        /// </summary>
        public const string StartTime = "StartTime";
        /// <summary>
        /// 结束时间
        /// </summary>
        public const string StopTime = "StopTime";
        /// <summary>
        /// 是否设置消息推送
        /// </summary>
        public const string SetMes = "SetMes";
        #endregion

        /// <summary>
        /// 谁可以查看？
        /// </summary>
        public const string WhoCanView = "WhoCanView";

    }
    /// <summary>
    /// 通知
    /// </summary>
    public partial class Notice : EntityOID
    {
        #region 1.基本属性
        /// <summary>
        /// 标题
        /// </summary>
        public string Title
        {
            get { return this.GetValStrByKey(NoticeAttr.Title); }
            set { this.SetValByKey(NoticeAttr.Title, value); }
        }
        /// <summary>
        /// 内容
        /// </summary>
        public string Doc
        {
            get
            {
                try
                {//公告内容不许为空 前提
                    string doc = this.GetValStrByKey(NoticeAttr.Doc);
                    if (doc == "" || doc.Length == 0)
                        return this.GetBigTextFromDB("DocFile");
                    else
                        return doc;
                }
                catch
                {
                    return "";
                }
            }
            set
            {
                if (value.Length > 2000)
                {
                    this.SaveBigTxtToDB("DocFile", value);
                    this.SetValByKey(NoticeAttr.Doc, "");
                }
                else
                {
                    this.SetValByKey(NoticeAttr.Doc, value);
                }
            }
        }
        /// <summary>
        /// 文件
        /// </summary>
        public string AttachFile
        {
            get { return this.GetValStringByKey(NoticeAttr.AttachFile); }
            set { this.SetValByKey(NoticeAttr.AttachFile, value); }
        }
        /// <summary>
        /// 类型
        /// </summary>
        public String FK_NoticeCategory
        {
            get { return this.GetValStringByKey(NoticeAttr.FK_NoticeCategory); }
            set { this.SetValByKey(NoticeAttr.FK_NoticeCategory, value); }
        }

        #endregion

        #region 2.创建信息
        /// <summary>
        /// 用户编号
        /// </summary>
        public string Rec
        {
            get { return this.GetValStrByKey(NoticeAttr.Rec); }
            set { this.SetValByKey(NoticeAttr.Rec, value); }
        }
        /// <summary>
        /// 发布日期
        /// </summary>
        public DateTime RDT
        {
            get { return this.GetValDateTime(NoticeAttr.RDT); }
            set { this.SetValByKey(NoticeAttr.RDT, value); }
        }
        /// <summary>
        /// 发布部门
        /// </summary>
        public string FK_Dept
        {
            get { return this.GetValStrByKey(NoticeAttr.FK_Dept); }
            set { this.SetValByKey(NoticeAttr.FK_Dept, value); }
        }
        /// <summary>
        /// 发往用户
        /// </summary>
        public bool SendToPart
        {
            get { return this.GetValBooleanByKey(NoticeAttr.SendToPart); }
            set { this.SetValByKey(NoticeAttr.SendToPart, value); }
        }
        /// <summary>
        /// 发往用户
        /// </summary>
        public string SendToUsers
        {
            get { return this.GetValStrByKey(NoticeAttr.SendToUsers); }
            set { this.SetValByKey(NoticeAttr.SendToUsers, value); }
        }
        /// <summary>
        /// 发往部门
        /// </summary>
        public string SendToDepts
        {
            get { return this.GetValStrByKey(NoticeAttr.SendToDepts); }
            set { this.SetValByKey(NoticeAttr.SendToDepts, value); }
        }        /// <summary>
        /// 发往职位
        /// </summary>
        public string SendToStations
        {
            get { return this.GetValStrByKey(NoticeAttr.SendToStations); }
            set { this.SetValByKey(NoticeAttr.SendToStations, value); }
        }
        #endregion

        #region 3.显示设置
        public DateTime SetTop
        {
            get { return this.GetValDateTime(NoticeAttr.SetTop); }
            set { this.SetValByKey(NoticeAttr.SetTop, value); }
        }
        public int Importance
        {
            get { return this.GetValIntByKey(NoticeAttr.Importance); }
            set { this.SetValByKey(NoticeAttr.Importance, value); }
        }
        #endregion

        #region 4.反馈设置
        public bool GetAdvices
        {
            get { return this.GetValBooleanByKey(NoticeAttr.GetAdvices); }
            set { this.SetValByKey(NoticeAttr.GetAdvices, value); }
        }
        public String AdviceDesc
        {
            get { return this.GetValStrByKey(NoticeAttr.AdviceDesc); }
            set { this.SetValByKey(NoticeAttr.AdviceDesc, value); }
        }
        public String AdviceItems
        {
            get { return this.GetValStrByKey(NoticeAttr.AdviceItems); }
            set { this.SetValByKey(NoticeAttr.AdviceItems, value); }
        }
        #endregion

        #region 5.其它信息
        public int NoticeSta
        {
            get { return this.GetValIntByKey(NoticeAttr.NoticeSta); }
            set { this.SetValByKey(NoticeAttr.NoticeSta, value); }
        }
        public DateTime StartTime
        {
            get { return this.GetValDateTime(NoticeAttr.StartTime); }
            set { this.SetValByKey(NoticeAttr.StartTime, value); }
        }
        public DateTime StopTime
        {
            get { return this.GetValDateTime(NoticeAttr.StopTime); }
            set { this.SetValByKey(NoticeAttr.StopTime, value); }
        }
        public int SetMes
        {
            get { return this.GetValIntByKey(NoticeAttr.SetMes); }
            set { this.SetValByKey(NoticeAttr.SetMes, value); }
        }
        #endregion

        #region 构造函数
        /// <summary>
        /// 通知
        /// </summary>
        public Notice() { }
        /// <summary>
        /// 通知
        /// </summary>
        /// <param name="no">编号</param>
        public Notice(int oid) : base(oid) { }
        #endregion

        #region 重写方法
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                uac.OpenForSysAdmin();
                return uac;
            }
        }
        /// <summary>
        /// Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map();
                map.PhysicsTable = "OA_Notice";
                map.EnDesc = "通知";
                // 实体的描述.
                //map.DepositaryOfEntity = Depositary.Application; //实体map的存放位置.
                //map.DepositaryOfMap = Depositary.Application;    // Map 的存放位置.

                //基本信息
                map.AddTBIntPKOID();
                map.AddTBString(NoticeAttr.Title, null, "标题", true, false, 0, 300, 220, true);
                map.AddTBStringDoc(NoticeAttr.Doc, null, "内容", true, false, 0,4000,200,30);

                //map.AddTBString(NoticeAttr.AttachFile, null, "上传附件", false, false, 0, 4000, 30, true);
                //map.AddMyFile("资料","rar");

                map.AddDDLEntities(NoticeAttr.FK_NoticeCategory, null, "类别", new NoticeCategorys(), true);

                ////创建信息
                map.AddDDLEntities(NoticeAttr.FK_Dept, null, "发布部门", new BP.Port.Depts(), false);
                map.AddDDLEntities(NoticeAttr.Rec, null, "发布人", new BP.Port.Emps(), false);

                //map.AddBoolean(NoticeAttr.SendToPart, true, "是否发往部门个人", false, false);
                //map.AddTBStringDoc(NoticeAttr.SendToUsers, null, "发往用户", false, false);
                //map.AddTBStringDoc(NoticeAttr.SendToDepts, null, "发往部门", false, false);
                //map.AddTBStringDoc(NoticeAttr.SendToStations, null, "发往职位", false, false);

                //展示设置.
                map.AddTBDateTime(NoticeAttr.SetTop, "置顶", true, true);

                map.AddDDLSysEnum(NoticeAttr.Importance, 0, "重要程度", true, true, NoticeAttr.Importance,
                    "0=普通@1=比较重要@2=很重要");

                map.AddDDLSysEnum(NoticeAttr.WhoCanView, 0, "谁可以查看?", true, true, NoticeAttr.Importance,
                "0=所有人都可以查看@1=指定部门与人员范围");

                map.AddTBDate(NoticeAttr.RDT, null, "发布日期", true, false);

                ////反馈信息
                //map.AddBoolean(NoticeAttr.GetAdvices, false, "是否反馈", false, true);
                //map.AddTBStringDoc(NoticeAttr.AdviceDesc, null, "反馈说明", false, false, true);
                //map.AddTBStringDoc(NoticeAttr.AdviceItems, null, "反馈项", false, false, true);

                //其它信息
                map.AddDDLSysEnum(NoticeAttr.NoticeSta, 0, "状态", false, true, NoticeAttr.NoticeSta, "@0=自动@1=打开@2=关闭");
                map.AddTBDateTime(NoticeAttr.StartTime, "时间", true, false);
                map.AddTBDateTime(NoticeAttr.StopTime, "结束时间", true, false);
                map.AddDDLSysEnum(NoticeAttr.SetMes, 0, "是否发布消息", false, true, NoticeAttr.SetMes, "@0=不推送@1=站内消息");
                //增加查询条件。
                map.AddSearchAttr(NoticeAttr.FK_Dept);
                map.AddSearchAttr(NoticeAttr.FK_NoticeCategory);
                map.AddSearchAttr(NoticeAttr.NoticeSta);


                //节点绑定人员. 使用树杆与叶子的模式绑定.
                map.AttrsOfOneVSM.AddBranchesAndLeaf(new BP.OA.NoticeEmps(), new BP.Port.Emps(),
                   BP.OA.NoticeEmpAttr.FK_Notice,
                   BP.OA.NoticeEmpAttr.FK_Emp, "查看的人员", BP.Port.EmpAttr.FK_Dept, BP.Port.EmpAttr.Name, BP.Port.EmpAttr.No,
                   "@WebUser.FK_Dept");

                //节点绑定部门. 节点绑定部门.
                map.AttrsOfOneVSM.AddBranches(new BP.OA.NoticeDepts(), new BP.Port.Depts(),
                   BP.OA.NoticeDeptAttr.FK_Notice,
                   BP.OA.NoticeDeptAttr.FK_Dept, "查看的部门", BP.Port.EmpAttr.Name, BP.Port.EmpAttr.No, "@WebUser.FK_Dept");

                //附件.
                map.AddMyFileS("附件");

                //RefMethod rm = new RefMethod();
                //rm.Title = "打开";
                //rm.ClassMethodName = this.ToString() + ".DoOpen";
                //map.AddRefMethod(rm);

                //map.AddTBString(NoticeAttr.ParentNo, null, "父节点No", true, false, 0, 100, 30);
                //map.AddTBString(NoticeAttr.FK_Unit, null, "所在单位", true, false, 0, 50, 30);

                this._enMap = map;
                return this._enMap;
            }
        }
        #endregion

    }
    /// <summary>
    ///得到集合
    /// </summary>
    public class Notices : EntitiesOID
    {
        /// <summary>
        /// 得到一个新实体
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new Notice();
            }
        }
        /// <summary>
        /// 通知集合
        /// </summary>
        public Notices()
        {
        }
        /// <summary>
        /// 获得我的可以读取的公告
        /// </summary>
        /// <returns></returns>
        public string GenerMyNotices()
        {
            string sql = "SELECT OID FROM OA_Notice WHERE WhoCanView=0 ";
            sql += " UNION ";
            sql += " SELECT OID FROM OA_Notice A,OA_NoticeDept B, Port_Emp C WHERE WhoCanView=1 AND A.OID=B.FK_Notice AND B.FK_Dept=C.FK_Dept AND C.No='"+BP.Web.WebUser.No+"' ";
            sql += " UNION ";
            sql += " SELECT OID FROM OA_Notice A,OA_NoticeEmp B, Port_Emp C WHERE WhoCanView=1 AND A.OID=B.FK_Notice AND B.FK_Emp=C.No AND C.No='" + BP.Web.WebUser.No + "' ";

            Notices ens = new Notices();
            ens.RetrieveInSQL(sql);

            return ens.ToJson();
        }
    }
}
