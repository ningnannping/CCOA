using System;
using System.Data;
using BP.DA;
using BP.En;
using BP.Web;

namespace BP.OA
{
    /// <summary>
    /// 通知类别属性
    /// </summary>
    public class NoticeReaderAttr : EntityMyPKAttr
    {
        public const String FK_UserNo = "FK_UserNo";
        public const String FK_NoticeNo = "FK_NoticeNo";
        public const String AddTime = "AddTime";
        public const String ReadTime = "ReadTime";
        public const String AdviceValue = "AdviceValue";
        public const String AdviceRemark = "AdviceRemark";
    }
    /// <summary>
    /// 通知类别
    /// </summary>
    public class NoticeReader : EntityMyPK
    {
        #region 属性
        public string FK_UserNo
        {
            get { return this.GetValStrByKey(NoticeReaderAttr.FK_UserNo); }
            set { this.SetValByKey(NoticeReaderAttr.FK_UserNo, value); }
        }
        public string FK_NoticeNo
        {
            get { return this.GetValStrByKey(NoticeReaderAttr.FK_NoticeNo); }
            set { this.SetValByKey(NoticeReaderAttr.FK_NoticeNo, value); }
        }
        public string AddTime
        {
            get { return this.GetValStrByKey(NoticeReaderAttr.AddTime); }
            set { this.SetValByKey(NoticeReaderAttr.AddTime, value); }
        }
        public string ReadTime
        {
            get { return this.GetValStrByKey(NoticeReaderAttr.ReadTime); }
            set { this.SetValByKey(NoticeReaderAttr.ReadTime, value); }
        }
        public string AdviceValue
        {
            get { return this.GetValStrByKey(NoticeReaderAttr.AdviceValue); }
            set { this.SetValByKey(NoticeReaderAttr.AdviceValue, value); }
        }
        public string AdviceRemark
        {
            get { return this.GetValStrByKey(NoticeReaderAttr.AdviceRemark); }
            set { this.SetValByKey(NoticeReaderAttr.AdviceRemark, value); }
        }        
        #endregion
        #region 构造函数
        /// <summary>
        /// 通知类别
        /// </summary>
        public NoticeReader() { }
        /// <summary>
        /// 通知类别
        /// </summary>
        /// <param name="no">编号</param>
        public NoticeReader(string no) : base(no) { }
        #endregion

        #region 重写方法
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                uac.OpenForSysAdmin();
                return uac;
            }
        }
        /// <summary>
        /// Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map();                 
                map.PhysicsTable = "OA_NoticeReader"; 
                map.EnDesc = "通知阅读";                   // 实体的描述.
                map.CodeStruct = "2";
                //map.DepositaryOfEntity = Depositary.Application; //实体map的存放位置.
                //map.DepositaryOfMap = Depositary.Application;    // Map 的存放位置.
                map.AddMyPK();
                map.AddTBString(NoticeReaderAttr.FK_UserNo, null, "用户编号", true, false, 2, 50, 20);
                map.AddTBString(NoticeReaderAttr.FK_NoticeNo, null, "通知编号", true, false, 2, 50, 20);
                map.AddTBDateTime(NoticeReaderAttr.AddTime, null, "发布时间", true, false);
                map.AddTBDateTime(NoticeReaderAttr.ReadTime, null, "阅读时间", true, false);
                map.AddTBString(NoticeReaderAttr.AdviceValue, null, "建议选择", true, false, 0, 50, 50);
                map.AddTBStringDoc(NoticeReaderAttr.AdviceRemark, null, "建议内容", true, false, 0, 500, 600, 5);

                //map.AddTBString(NoticeReaderAttr.ParentNo, null, "父节点No", true, false, 0, 100, 30);
                //map.AddTBString(NoticeReaderAttr.FK_Unit, null, "所在单位", true, false, 0, 50, 30);

                this._enMap = map;
                return this._enMap;
            }
        }
        #endregion
    }
    /// <summary>
    ///得到集合
    /// </summary>
    public class NoticeReaders : EntitiesMyPK
    {
        /// <summary>
        /// 得到一个新实体
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new NoticeReader();
            }
        }
        /// <summary>
        /// 通知类别集合
        /// </summary>
        public NoticeReaders()
        {
        }
    }
}
