﻿using System;
using System.Collections;
using BP.DA;
using BP.En;
using BP.En;
using BP.WF.Port;

namespace BP.OA
{
	/// <summary>
	/// 节点人员属性
	/// </summary>
	public class NoticeEmpAttr
	{
		/// <summary>
		/// 节点
		/// </summary>
		public const string FK_Notice="FK_Notice";
		/// <summary>
		/// 人员
		/// </summary>
		public const string FK_Emp="FK_Emp";
	}
	/// <summary>
	/// 节点人员
	/// 节点的到人员有两部分组成.	 
	/// 记录了从一个节点到其他的多个节点.
	/// 也记录了到这个节点的其他的节点.
	/// </summary>
	public class NoticeEmp :EntityMM
	{
		#region 基本属性
		/// <summary>
		///节点
		/// </summary>
		public int  FK_Notice
		{
			get
			{
				return this.GetValIntByKey(NoticeEmpAttr.FK_Notice);
			}
			set
			{
				this.SetValByKey(NoticeEmpAttr.FK_Notice,value);
			}
		}
		/// <summary>
		/// 到人员
		/// </summary>
		public string FK_Emp
		{
			get
			{
				return this.GetValStringByKey(NoticeEmpAttr.FK_Emp);
			}
			set
			{
				this.SetValByKey(NoticeEmpAttr.FK_Emp,value);
			}
		}
        public string FK_EmpT
        {
            get
            {
                return this.GetValRefTextByKey(NoticeEmpAttr.FK_Emp);
            }
        }
		#endregion 

		#region 构造方法
		/// <summary>
		/// 节点人员
		/// </summary>
		public NoticeEmp()
        {
        }
		/// <summary>
		/// 重写基类方法
		/// </summary>
		public override Map EnMap
		{
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map("OA_NoticeEmp", "节点人员");

                map.AddTBIntPK(NoticeEmpAttr.FK_Notice,0,"Notice",true,true );
                map.AddDDLEntitiesPK(NoticeEmpAttr.FK_Emp, null, "到人员", new Emps(), true);

                this._enMap = map;
                return this._enMap;
            }
		}
		#endregion
	}
	/// <summary>
	/// 节点人员
	/// </summary>
    public class NoticeEmps : EntitiesMM
    {
        /// 得到它的 Entity 
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new NoticeEmp();
            }
        }

        public NoticeEmps()
        {

        }

        #region 为了适应自动翻译成java的需要,把实体转换成List.
        /// <summary>
        /// 转化成 java list,C#不能调用.
        /// </summary>
        /// <returns>List</returns>
        public System.Collections.Generic.IList<NoticeEmp> ToJavaList()
        {
            return (System.Collections.Generic.IList<NoticeEmp>)this;
        }
        /// <summary>
        /// 转化成list
        /// </summary>
        /// <returns>List</returns>
        public System.Collections.Generic.List<NoticeEmp> Tolist()
        {
            System.Collections.Generic.List<NoticeEmp> list = new System.Collections.Generic.List<NoticeEmp>();
            for (int i = 0; i < this.Count; i++)
            {
                list.Add((NoticeEmp)this[i]);
            }
            return list;
        }
        #endregion 为了适应自动翻译成java的需要,把实体转换成List.
    }
}
