using System;
using System.Data;
using System.Collections;
using BP.DA;
using BP.En;
using BP.Port;

namespace BP.DS
{
    /// <summary>
    /// 办公用品领用审核状态(枚举)
    /// </summary>
    public enum DsTakeZT
    {
        /// <summary>
        /// 审核中
        /// </summary>
        Free,
        /// <summary>
        /// 通过
        /// </summary>
        Pass,
        /// <summary>
        /// 未通过
        /// </summary>
        NoPass
    }
    /// <summary>
    /// 领取台帐 属性
    /// </summary>
    public class DSTakeAttr : EntityOIDAttr
    {
        /// <summary>
        /// 物品
        /// </summary>
        public const string FK_DSMain = "FK_DSMain";
        /// <summary>
        /// 类别
        /// </summary>
        public const string FK_Sort = "FK_Sort";
        /// <summary>
        /// 单位
        /// </summary>
        public const string Unit = "Unit";
        /// <summary>
        /// 规格型号
        /// </summary>
        public const string Spec = "Spec";
        /// <summary>
        /// 申请数量
        /// </summary>
        public const string NumOfWant = "NumOfWant";
        /// <summary>
        /// 批准数量
        /// </summary>
        public const string NumOfCan = "NumOfCan";
        /// <summary>
        /// 领取数量
        /// </summary>
        public const string NumOfTake = "NumOfTake";
        /// <summary>
        /// 领取人
        /// </summary>
        public const string Taker = "Taker";
        /// <summary>
        /// 部门
        /// </summary>
        public const string TakerDep = "TakerDep";
        /// <summary>
        /// 隶属年月
        /// </summary>
        public const string FK_NY = "FK_NY";
        /// <summary>
        /// 经办人
        /// </summary>
        public const string DoIt = "DoIt";
        /// <summary>
        /// 领取日期
        /// </summary>
        public const string RDT = "RDT";
        /// <summary>
        /// 状态
        /// </summary>
        public const string DsTakeZT = "DsTakeZT";
        /// <summary>
        /// 业务编号
        /// </summary>
        public const string WorkID = "WorkID";
        /// <summary>
        /// 用途
        /// </summary>
        public const string Note = "Note";
    }
    public class DSTake : EntityOID
    {
        #region 属性
        /// <summary>
        /// 物品名
        /// </summary>
        public string FK_DSMain
        {
            get
            {
                return this.GetValStringByKey(DSTakeAttr.FK_DSMain);
            }
            set
            {
                this.SetValByKey(DSTakeAttr.FK_DSMain, value);
            }
        }
        /// <summary>
        /// 类别
        /// </summary>
        public string FK_Sort
        {
            get
            {
                return this.GetValStringByKey(DSTakeAttr.FK_Sort);
            }
            set
            {
                this.SetValByKey(DSTakeAttr.FK_Sort, value);
            }
        }
        /// <summary>
        /// 单位
        /// </summary>
        public string Unit
        {
            get
            {
                return this.GetValStringByKey(DSTakeAttr.Unit);
            }
            set
            {
                this.SetValByKey(DSTakeAttr.Unit, value);
            }
        }
        /// <summary>
        /// 申请数量
        /// </summary>
        public decimal NumOfWant
        {
            get
            {
                return this.GetValDecimalByKey(DSTakeAttr.NumOfWant);
            }
            set
            {
                this.SetValByKey(DSTakeAttr.NumOfWant, value);
            }
        }
        /// <summary>
        /// 批准数量
        /// </summary>
        public decimal NumOfCan
        {
            get
            {
                return this.GetValDecimalByKey(DSTakeAttr.NumOfCan);
            }
            set
            {
                this.SetValByKey(DSTakeAttr.NumOfCan, value);
            }
        }
        /// <summary>
        /// 出库数量
        /// </summary>
        public decimal NumOfTake
        {
            get
            {
                return this.GetValDecimalByKey(DSTakeAttr.NumOfTake);
            }
            set
            {
                this.SetValByKey(DSTakeAttr.NumOfTake, value);
            }
        }
        /// <summary>
        /// 领取人
        /// </summary>
        public string Taker
        {
            get
            {
                return this.GetValStringByKey(DSTakeAttr.Taker);
            }
            set
            {
                this.SetValByKey(DSTakeAttr.Taker, value);
            }
        }
        /// <summary>
        /// 隶属部门
        /// </summary>
        public string TakerDep
        {
            get
            {
                return this.GetValStringByKey(DSTakeAttr.TakerDep);
            }
            set
            {
                this.SetValByKey(DSTakeAttr.TakerDep, value);
            }
        }
        /// <summary>
        /// 出库人
        /// </summary>
        public string DoIt
        {
            get
            {
                return this.GetValStringByKey(DSTakeAttr.DoIt);
            }
            set
            {
                this.SetValByKey(DSTakeAttr.DoIt, value);
            }
        }
        /// <summary>
        /// 领取时间
        /// </summary>
        public string RDT
        {
            get
            {
                return this.GetValStringByKey(DSTakeAttr.RDT);
            }
            set
            {
                this.SetValByKey(DSTakeAttr.RDT, value);
            }
        }
        /// <summary>
        /// 隶属年月
        /// </summary>
        public string FK_NY
        {
            get
            {
                return this.GetValStringByKey(DSTakeAttr.FK_NY);
            }
            set
            {
                this.SetValByKey(DSTakeAttr.FK_NY, value);
            }
        }
        /// <summary>
        /// 规格型号
        /// </summary>
        public string Spec
        {
            get
            {
                return this.GetValStringByKey(DSTakeAttr.Spec);
            }
            set
            {
                this.SetValByKey(DSTakeAttr.Spec, value);
            }
        }
        /// <summary>
        /// 审核状态
        /// </summary>
        public string DsTakeZT
        {
            get
            {
                return this.GetValStringByKey(DSTakeAttr.DsTakeZT);
            }
            set
            {
                this.SetValByKey(DSTakeAttr.DsTakeZT, value);
            }
        }
        /// <summary>
        /// 用途
        /// </summary>
        public string Note
        {
            get
            {
                return this.GetValStringByKey(DSTakeAttr.Note);
            }
            set
            {
                this.SetValByKey(DSTakeAttr.Note, value);
            }
        }
        /// <summary>
        /// 业务编号
        /// </summary>
        public string WorkID
        {
            get
            {
                return this.GetValStringByKey(DSTakeAttr.WorkID);
            }
            set
            {
                this.SetValByKey(DSTakeAttr.WorkID, value);
            }
        }
        #endregion 属性

        #region 权限控制属性.
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                uac.IsDelete = false;
                uac.IsInsert = false;
                uac.IsUpdate = false;
                uac.IsView = true;
                return uac;
            }
        }
        #endregion 权限控制属性.

        #region 构造方法
        /// <summary>
        /// 领取台帐
        /// </summary>
        public DSTake()
        {
        }
        /// <summary>
        /// 领取台帐
        /// </summary>
        /// <param name="oid"></param>
        public DSTake(int oid) : base(oid) { }
        #endregion
       
        /// <summary>
        /// 领取台帐Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map("OA_DSTake");
                map.EnDesc = "领取台帐";

                map.DepositaryOfEntity = Depositary.None;
                map.DepositaryOfMap = Depositary.Application;

                map.AddTBIntPKOID();
                map.AddDDLEntities(DSTakeAttr.FK_DSMain, null, "物品名", new DSMains(), true);
                map.AddDDLEntities(DSTakeAttr.FK_Sort, null, "类别", new DSSorts(), false);
                map.AddTBString(DSTakeAttr.Unit, null, "单位", true, true, 1, 20, 30, false);
                map.AddTBString(DSTakeAttr.Spec, null, "规格型号", true, true, 1, 100, 50, false);
                map.AddTBFloat(DSTakeAttr.NumOfWant, 0, "申请数量", true, false);
                map.AddTBFloat(DSTakeAttr.NumOfCan, 0, "批准数量", true, false);
                map.AddTBFloat(DSTakeAttr.NumOfTake, 0, "领取数量", true, false);
                map.AddTBDate(DSTakeAttr.RDT, null, "领取时间", true, false);
                map.AddTBString(DSTakeAttr.Taker, null, "领取人", true, false, 1, 500, 100, true);
                map.AddTBString(DSTakeAttr.TakerDep, null, "隶属部门", true, false, 1, 500, 100, true);
                map.AddTBInt(DSTakeAttr.WorkID, 0, "业务编号", false, true);
                map.AddTBString(DSTakeAttr.DoIt, null, "经办人", true, false, 0, 500, 100, true);
                map.AddDDLSysEnum(DSTakeAttr.DsTakeZT, 0, "状态", true, true, DSTakeAttr.DsTakeZT, "@0=审核中@1=通过@2=未通过");
                map.AddDDLEntities(DSBuyAttr.FK_NY, null, "隶属年月", new BP.Pub.NYs(), true);
                map.AddTBStringDoc(DSTakeAttr.Note, null, "用途", true, false, true);

                //查询条件.
                map.AddSearchAttr(DSTakeAttr.FK_Sort);
                map.AddSearchAttr(DSTakeAttr.FK_NY);
                map.AddSearchAttr(DSTakeAttr.FK_DSMain);


                this._enMap = map;
                return this._enMap;
            }
        }

        #region 重写方法
        protected override bool beforeUpdateInsertAction()
        {
            if (this.RDT == "")
                this.RDT = DataType.CurrentData;

            //给年月赋值.System.DateTime.Now.ToString("yyyy-MM");
            this.FK_NY = System.DateTime.Now.ToString("yyyy-MM");

            //复制主表的属性.
            DSMain ds = new DSMain(this.FK_DSMain);

            this.FK_Sort = ds.FK_Sort;
            this.Unit = ds.Unit;
            this.Spec = ds.Spec;

            return base.beforeUpdateInsertAction();
        }
        protected override void afterInsertUpdateAction()
        {

            string sql = "select No from OA_DSMain where No=" + this.FK_DSMain;
            DataTable dTable = DBAccess.RunSQLReturnTable(sql);
            string number = dTable.Rows[0]["No"].ToString();
            if (dTable.Rows.Count == 0)
            {
                throw new Exception("@没有此项库存");
            }
            else
            {
                DSMain ds = new DSMain(number);
                DSBuy db = new DSBuy();
                DSTake dt = new DSTake();
                ds.NumOfTake = DBAccess.RunSQLReturnValFloat("SELECT SUM(" + DSTakeAttr.NumOfTake + ") FROM " + this.EnMap.PhysicsTable + " WHERE " + DSTakeAttr.FK_DSMain + "='" + this.FK_DSMain + "'", 0);

                //my 添加求和查询
                ds.NumOfBuy = DBAccess.RunSQLReturnValFloat("SELECT SUM(" + DSBuyAttr.NumOfBuy + ") FROM " + db.EnMap.PhysicsTable + " WHERE " + DSBuyAttr.FK_DSMain + "='" + this.FK_DSMain + "'", 0);
                ds.NumOfLeft = ds.NumOfBuy - ds.NumOfTake;

                if (ds.NumOfLeft < 0)
                {
                    throw new Exception("@不合法的数据状态");
                }

                ds.Update();
            }
            base.afterInsertUpdateAction();
        }
        #endregion 重写方法
    }
    /// <summary>
    /// 领取台帐
    /// </summary>
    public class DSTakes : BP.En.EntitiesOID
    {
        /// <summary>
        /// 领取台帐
        /// </summary>
        public DSTakes() { }
        /// <summary>
        /// 得到它的 Entity 
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new DSTake();
            }
        }
    }
}
