using System;
using System.Data;
using System.Collections;
using BP.DA;
using BP.En;
using BP.Port;

namespace BP.DS
{
    /// <summary>
    /// 办公用品采购审核状态(枚举)
    /// </summary>
    public enum DsBuyZT
    {
        /// <summary>
        /// 审核中
        /// </summary>
        Free,
        /// <summary>
        /// 通过
        /// </summary>
        Pass,
        /// <summary>
        /// 未通过
        /// </summary>
        NoPass
    }
    /// <summary>
    /// 采购台帐 属性
    /// </summary>
    public class DSBuyAttr : EntityOIDAttr
    {
        /// <summary>
        /// 物品名
        /// </summary>
        public const string FK_DSMain = "FK_DSMain";
        /// <summary>
        /// 类别
        /// </summary>
        public const string FK_Sort = "FK_Sort";
        /// <summary>
        /// 单位
        /// </summary>
        public const string Unit = "Unit";
        /// <summary>
        /// 计划数量
        /// </summary>
        public const string NumOfPlan = "NumOfPlan";
        /// <summary>
        /// 批准数量
        /// </summary>
        public const string NumOfBuy = "NumOfBuy";
        /// <summary>
        /// 单价
        /// </summary>
        public const string UnitPrice = "UnitPrice";
        /// <summary>
        /// 建议单价
        /// </summary>
        public const string UnitPYCan = "UnitPYCan";
        /// <summary>
        /// 预算金额
        /// </summary>
        public const string JE = "JE";
        /// <summary>
        /// 实际金额
        /// </summary>
        public const string LasTJE = "LasTJE";
        /// <summary>
        /// 申请人
        /// </summary>
        public const string WhoWant = "WhoWant";
        /// <summary>
        /// 采购人
        /// </summary>
        public const string Buyer = "Buyer";
        /// <summary>
        /// 采购日期
        /// </summary>
        public const string RDT = "RDT";
        /// <summary>
        /// 备注
        /// </summary>
        public const string Note = "Note";
        /// <summary>
        /// 隶属年月
        /// </summary>
        public const string FK_NY = "FK_NY";
        /// <summary>
        /// 规格型号
        /// </summary>
        public const string Spec = "Spec";
        /// <summary>
        /// 状态
        /// </summary>
        public const string DsBuyZT = "DsBuyZT";
        /// <summary>
        /// 业务编号
        /// </summary>
        public const string WorkID = "WorkID";
    }
    /// <summary>
    ///  采购台帐
    /// </summary>
    public class DSBuy : EntityOID
    {
        #region 属性
        /// <summary>
        /// 物品名
        /// </summary>
        public string FK_DSMain
        {
            get
            {
                return this.GetValStringByKey(DSBuyAttr.FK_DSMain);
            }
            set
            {
                this.SetValByKey(DSBuyAttr.FK_DSMain, value);
            }
        }
        /// <summary>
        /// 类别
        /// </summary>
        public string FK_Sort
        {
            get
            {
                return this.GetValStringByKey(DSBuyAttr.FK_Sort);
            }
            set
            {
                this.SetValByKey(DSBuyAttr.FK_Sort, value);
            }
        }
        /// <summary>
        /// 单位
        /// </summary>
        public string Unit
        {
            get
            {
                return this.GetValStringByKey(DSBuyAttr.Unit);
            }
            set
            {
                this.SetValByKey(DSBuyAttr.Unit, value);
            }
        }
        /// <summary>
        /// 备注
        /// </summary>
        public string Note
        {
            get
            {
                return this.GetValStringByKey(DSBuyAttr.Note);
            }
            set
            {
                this.SetValByKey(DSBuyAttr.Note, value);
            }
        }
        /// <summary>
        /// 计划数量
        /// </summary>
        public decimal NumOfPlan
        {
            get
            {
                return this.GetValDecimalByKey(DSBuyAttr.NumOfPlan);
            }
            set
            {
                this.SetValByKey(DSBuyAttr.NumOfPlan, value);
            }
        }
        /// <summary>
        /// 批准数量
        /// </summary>
        public decimal NumOfBuy
        {
            get
            {
                return this.GetValDecimalByKey(DSBuyAttr.NumOfBuy);
            }
            set
            {
                this.SetValByKey(DSBuyAttr.NumOfBuy, value);
            }
        }
        /// <summary>
        /// 单价
        /// </summary>
        public decimal UnitPrice
        {
            get
            {
                return this.GetValDecimalByKey(DSBuyAttr.UnitPrice);
            }
            set
            {
                this.SetValByKey(DSBuyAttr.UnitPrice, value);
            }
        }
        /// <summary>
        /// 建议单价
        /// </summary>
        public decimal UnitPYCan
        {
            get
            {
                return this.GetValDecimalByKey(DSBuyAttr.UnitPYCan);
            }
            set
            {
                this.SetValByKey(DSBuyAttr.UnitPYCan, value);
            }
        }
        /// <summary>
        /// 预算金额
        /// </summary>
        public decimal JE
        {
            get
            {
                return this.GetValDecimalByKey(DSBuyAttr.JE);
            }
            set
            {
                this.SetValByKey(DSBuyAttr.JE, value);
            }
        }
        /// <summary>
        /// 实际金额
        /// </summary>
        public decimal LasTJE
        {
            get
            {
                return this.GetValDecimalByKey(DSBuyAttr.LasTJE);
            }
            set
            {
                this.SetValByKey(DSBuyAttr.LasTJE, value);
            }
        }
        /// <summary>
        /// 规格型号
        /// </summary>
        public string Spec
        {
            get
            {
                return this.GetValStringByKey(DSBuyAttr.Spec);
            }
            set
            {
                this.SetValByKey(DSBuyAttr.Spec, value);
            }
        }
        /// <summary>
        /// 申请人
        /// </summary>
        public string WhoWant
        {
            get
            {
                return this.GetValStringByKey(DSBuyAttr.WhoWant);
            }
            set
            {
                this.SetValByKey(DSBuyAttr.WhoWant, value);
            }
        }
        /// <summary>
        /// 采购人
        /// </summary>
        public string Buyer
        {
            get
            {
                return this.GetValStringByKey(DSBuyAttr.Buyer);
            }
            set
            {
                this.SetValByKey(DSBuyAttr.Buyer, value);
            }
        }
        /// <summary>
        /// 隶属年月
        /// </summary>
        public string FK_NY
        {
            get
            {
                return this.GetValStringByKey(DSBuyAttr.FK_NY);
            }
            set
            {
                this.SetValByKey(DSBuyAttr.FK_NY, value);
            }
        }
        /// <summary>
        /// 采购日期
        /// </summary>
        public string RDT
        {
            get
            {
                return this.GetValStringByKey(DSBuyAttr.RDT);
            }
            set
            {
                this.SetValByKey(DSBuyAttr.RDT, value);
            }
        }
        /// <summary>
        /// 审核状态
        /// </summary>
        public string DsBuyZT
        {
            get
            {
                return this.GetValStringByKey(DSBuyAttr.DsBuyZT);
            }
            set
            {
                this.SetValByKey(DSBuyAttr.DsBuyZT, value);
            }
        }
        /// <summary>
        /// 业务编号
        /// </summary>
        public string WorkID
        {
            get
            {
                return this.GetValStringByKey(DSBuyAttr.WorkID);
            }
            set
            {
                this.SetValByKey(DSBuyAttr.WorkID, value);
            }
        }
        #endregion 属性

        #region 权限控制属性.
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                uac.IsDelete = false;
                uac.IsInsert = false;
                uac.IsUpdate = false;
                uac.IsView = true;
                return uac;
            }
        }
        #endregion 权限控制属性.
       
        #region 构造方法
        /// <summary>
        /// 采购台帐
        /// </summary>
        public DSBuy()
        {
        }
        /// <summary>
        /// 采购台帐
        /// </summary>
        /// <param name="oid"></param>
        public DSBuy(int oid) : base(oid) { }
        #endregion
     
        /// <summary>
        /// 采购台帐Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map("OA_DSBuy");
                map.EnDesc = "采购台帐";

                map.DepositaryOfEntity = Depositary.None;
                map.DepositaryOfMap = Depositary.Application;

                map.AddTBIntPKOID();

                map.AddDDLEntities(DSBuyAttr.FK_DSMain, null, "物品名", new DSMains(), true);
                map.AddDDLEntities(DSBuyAttr.FK_Sort, null, "类别", new DSSorts(), false);
                map.AddTBString(DSBuyAttr.Unit, null, "单位", true, false, 1, 20, 30, false);
                map.AddTBString(DSBuyAttr.Spec, null, "规格型号", true, false, 0, 100, 30, true);
                map.AddTBFloat(DSBuyAttr.NumOfPlan, 0, "计划数量", true, false);
                map.AddTBFloat(DSBuyAttr.NumOfBuy, 0, "批准数量", true, false);
                map.AddTBMoney(DSBuyAttr.UnitPrice, 0, "单价", true, true);
                map.AddTBMoney(DSBuyAttr.UnitPYCan, 0, "建议单价(元)", true, true);
                map.AddTBMoney(DSBuyAttr.JE, 0, "预算金额(元)", true, false);
                map.AddTBMoney(DSBuyAttr.LasTJE, 0, "实际金额(元)", true, false);
                map.AddTBString(DSBuyAttr.WhoWant, null, "申请人", true, false, 0, 500, 100);
                map.AddTBString(DSBuyAttr.Buyer, null, "采购人", true, false, 0, 500, 100);
                map.AddTBDate(DSBuyAttr.RDT, null, "采购时间", true, false);
                map.AddDDLSysEnum(DSBuyAttr.DsBuyZT, 0, "状态", true, true, DSBuyAttr.DsBuyZT, "@0=审核中@1=通过@2=未通过");
                map.AddDDLEntities(DSBuyAttr.FK_NY, null, "隶属年月", new BP.Pub.NYs(),true);
                map.AddTBStringDoc(DSBuyAttr.Note, null, "备注", true, false, true);
                map.AddTBInt(DSBuyAttr.WorkID, 0, "业务编号", false, true);


                //查询. 
                map.AddSearchAttr(DSBuyAttr.FK_Sort);
                map.AddSearchAttr(DSBuyAttr.FK_DSMain);
                map.AddSearchAttr(DSBuyAttr.FK_NY);

                #region
                //供应商信息查询
                //RefMethod RmInf = new RefMethod();
                //RmInf.Title = "供应商信息";
                //RmInf.ClassMethodName = this.ToString() + ".DoInf";
                //map.AddRefMethod(RmInf);

                //供应商评价查询
                //RefMethod RmAppSup = new RefMethod();
                //RmAppSup.Title = "供应商评价";
                //RmAppSup.ClassMethodName = this.ToString() + ".DoAppSup";
                //map.AddRefMethod(RmAppSup);
                #endregion
              
                this._enMap = map;
                return this._enMap;
            }
        }
        //public string DoInf()
        //{
        //    //return "UIEn.aspx?EnsName=BP.OA.DS.DSBuys&FK_DSMain=" + this.No + "&EnName=BP.OA.DS.DSBuy", 900, 600);
        //    return "UIEn.aspx?EnsName=BP.OA.DS.DSSupplys&No=" + this.Supply, 700, 400);
        //    return null;
        //}
        //public string DoAppSup()
        //{
        //    return "SearchBS.htm?EnsName=BP.OA.DS.DSAppSups&GongYSNo=" + this.Supply, 700, 400);
        //    return null;
        //}


        #region 重写方法
        protected override bool beforeUpdateInsertAction()
        {
            if (this.RDT == "")
                this.RDT = DataType.CurrentData;

            //时间判定限制 my
            if (Convert.ToDateTime(this.RDT) > Convert.ToDateTime(DataType.CurrentData))
            {
                throw new Exception("@购买日期不可大于当前日期.");
            }

            //给年月赋值.System.DateTime.Now.ToString("yyyy-MM");
            
           this.FK_NY = System.DateTime.Now.ToString("yyyy-MM");


            DSMain ds = new DSMain(this.FK_DSMain);

            //复制主表的属性.
            this.FK_Sort = ds.FK_Sort;
            this.Unit = ds.Unit;
            this.Spec = ds.Spec;

            return base.beforeUpdateInsertAction();
        }
        protected override void afterInsertUpdateAction()
        {
            //初始化   消除main台账不确定因素  有则更新  无则添加
            try
            {
                //是否存在
                DSMain dsm = new DSMain();
                dsm.CheckPhysicsTable();
                string DsSql = "select * from " + dsm.EnMap.PhysicsTable + " where  No=" + this.FK_DSMain;
                DataTable Dsdtl = DBAccess.RunSQLReturnTable(DsSql);

                //是否存在
                DSTake dt = new DSTake();
                dt.CheckPhysicsTable();
                string DtSql = "select * from " + dt.EnMap.PhysicsTable + " where  FK_DSMain=" + this.FK_DSMain;
                DataTable Dsdt2 = DBAccess.RunSQLReturnTable(DtSql);
                if (Dsdtl.Rows.Count == 0 && Dsdt2.Rows.Count == 0)
                {
                    dsm.Name = this.FK_DSMain;
                    dsm.UnitPrice = (float)this.UnitPrice;
                    dsm.FK_Sort = this.FK_Sort;
                    dsm.Unit = this.Unit;
                    dsm.Spec = this.Spec;
                    dsm.NumOfBuy = (float)this.NumOfBuy;
                    dsm.NumOfTake = 0;
                    dsm.NumOfLeft = dsm.NumOfBuy - dsm.NumOfTake;
                    dsm.UnitPrice = (float)this.UnitPrice;
                    dsm.Insert();
                }
                if (Dsdtl.Rows.Count != 0 && Dsdt2.Rows.Count == 0)
                {
                    DSMain dsN = new DSMain(this.FK_DSMain);
                    dsN.NumOfBuy = DBAccess.RunSQLReturnValFloat("SELECT SUM(" + DSBuyAttr.NumOfBuy + ") FROM " + this.EnMap.PhysicsTable + " WHERE " + DSBuyAttr.FK_DSMain + "='" + this.FK_DSMain + "'", 0);
                    dsN.NumOfTake = 0;
                    dsN.NumOfLeft = dsN.NumOfBuy - dsN.NumOfTake;
                    dsN.UnitPrice = (float)this.UnitPrice;
                    dsN.Spec = this.Spec;
                    dsN.Update();
                }
                if (Dsdtl.Rows.Count == 0 && Dsdt2.Rows.Count != 0)
                {
                    throw new Exception("@错误的数据状态");
                }
                if (Dsdtl.Rows.Count != 0 && Dsdt2.Rows.Count != 0)
                {
                    DSMain dsN = new DSMain(this.FK_DSMain);
                    dsN.NumOfBuy = DBAccess.RunSQLReturnValFloat("SELECT SUM(" + DSBuyAttr.NumOfBuy + ") FROM " + this.EnMap.PhysicsTable + " WHERE " + DSBuyAttr.FK_DSMain + "='" + this.FK_DSMain + "'", 0);
                    dsN.NumOfTake = DBAccess.RunSQLReturnValFloat("SELECT SUM(" + DSTakeAttr.NumOfTake + ") FROM " + dt.EnMap.PhysicsTable + " WHERE " + DSTakeAttr.FK_DSMain + "='" + this.FK_DSMain + "'", 0);
                    dsN.NumOfLeft = dsN.NumOfBuy - dsN.NumOfTake;
                    dsN.Update();
                }

            }
            catch { }
            finally
            {
                base.afterInsertUpdateAction();
            }

        }
        #endregion 重写方法
    }
    /// <summary>
    /// 采购台帐
    /// </summary>
    public class DSBuys : BP.En.EntitiesOID
    {
        /// <summary>
        /// 采购台帐
        /// </summary>
        public DSBuys() { }
        /// <summary>
        /// 得到它的 Entity 
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new DSBuy();
            }
        }
    }
}
