﻿using System;
using System.Collections;
using System.Data;
using BP.Sys;
using BP.DA;
using BP.En;
using BP.GPM;
using BP.Port;

namespace BP.OA
{
    public class Glo
    {
        #region 会议室预定
        /// <summary>
        /// 会议室预定
        /// </summary>
        /// <param name="fk_room">会议室编号</param>
        /// <param name="orderNo">订单编号</param>
        /// <param name="dtFrom">时间从</param>
        /// <param name="dtTo">到</param>
        public static bool Meeting_Order(string fk_room, string orderNo, string meetingNae, string dtFrom, string dtTo, string note)
        {
            //检查是否可以预定.
            //Meeting_IsCanOrder(fk_room, dtFrom, dtTo);

            ////转化日期.
            //DateTime dFrom = DataType.ParseSysDateTime2DateTime(dtFrom);
            //DateTime dTo = DataType.ParseSysDateTime2DateTime(dtTo);

            ////执行预定 , 这里需要根据时间写入多条数据。
            //BP.Meeting.RoomOrding en = new Meeting.RoomOrding();
            //en.Name = meetingNae;
            //en.OrderNo = orderNo;
            //en.DTFrom = dtFrom;
            //en.DTTo = dtTo;
            //en.Doc = note;
            //en.Insert();

            return true;
        }
        /// <summary>
        /// 是否可以预定？
        /// </summary>
        /// <param name="fk_room">会议室编号</param>
        /// <param name="dtFrom">时间从</param>
        /// <param name="dtTo">到</param>
        /// <returns>可以预定就返回true, 否则抛出异常.</returns>
        public static bool Meeting_IsCanOrder(string fk_room, string dtFrom, string dtTo)
        {
            return true;
        }
        /// <summary>
        /// 取消预定
        /// </summary>
        /// <param name="orderNo">订单号</param>
        public static void Meeting_CancelOrder(string orderNo)
        {
            //BP.Meeting.RoomOrding en = new Meeting.RoomOrding();
            //en.Delete(BP.Meeting.RoomOrdingAttr.OrderNo, orderNo);
        }
        #endregion 会议室预定。

        /// <summary>
        /// 安装包
        /// </summary>
        public static void DoInstallDataBase()
        {
            ArrayList al = null;
            string info = "BP.En.Entity";
            al = ClassFactory.GetObjects(info);

            #region 1, 修复表
            foreach (Object obj in al)
            {
                Entity en = null;
                en = obj as Entity;
                if (en == null)
                    continue;

                string table = null;
                try
                {
                    table = en.EnMap.PhysicsTable;
                    if (table == null)
                        continue;
                }
                catch
                {
                    continue;
                }

                switch (table)
                {
                    case "WF_EmpWorks":
                    case "WF_GenerEmpWorkDtls":
                    case "WF_GenerEmpWorks":
                    case "WF_NodeExt":
                    case "V_FlowData":
                        continue;
                    case "Sys_Enum":
                        en.CheckPhysicsTable();
                        break;
                    default:
                        en.CheckPhysicsTable();
                        break;
                }
                en.PKVal = "123";
                try
                {
                    en.RetrieveFromDBSources();
                }
                catch (Exception ex)
                {
                    Log.DebugWriteWarning(ex.Message);
                    en.CheckPhysicsTable();
                }
            }
            //创建视图.
            if (DBAccess.IsExitsObject("Port_EmpDept") == false)
            {
                string sql = "create view Port_EmpDept as select No as FK_Emp, FK_Dept from Port_Emp";
                BP.DA.DBAccess.RunSQL(sql);
            }
            #endregion 修复
 
            #region 1, 执行基本的 sql
            string sqlscript = SystemConfig.PathOfWebApp + "\\App\\Admin\\SQLScript\\InitOAData.sql";
            BP.DA.DBAccess.RunSQLScript(sqlscript);
            #endregion 执行基本的

            #region 2.通讯录.
            BP.Port.Emps emps = new BP.Port.Emps();
            emps.RetrieveAll();
            foreach (BP.Port.Emp myemp in emps)
            {
                foreach (BP.Port.Emp item in emps)
                {
                    //个人通讯录
                    ALEmp alemp = new ALEmp();
                    alemp.FK_Emp = myemp.No; //处理人

                    alemp.Name = item.Name;
                    alemp.Email = item.No + "@ccflow.org";
                    alemp.Tel = "13823483927";
                    alemp.faxNo = "";
                    alemp.Address = "济南-高新开发区-齐鲁软件园.";
                    alemp.Birthday = DateTime.Now.ToString("yyyy-MM-dd");
                    alemp.MyHomePage = "http://www.ccflow.org";
                    alemp.Organization = "党员";
                    alemp.Dept = item.FK_DeptText;
                    alemp.PosiTion = "经理";
                    alemp.Role = "经理";
                    alemp.Remarks = "爱好ccflow";
                    alemp.Insert();
                }
            }
            #endregion 通讯录.

            #region 初始化权限.
            //查询出来系统
            Apps apps = new Apps();
            apps.RetrieveAll();

            //查询出来人员.
            BP.GPM.Emps myemps = new BP.GPM.Emps();
            myemps.RetrieveAllFromDBSource();
            //查询出来菜单
            BP.GPM.Menus menus = new BP.GPM.Menus();
            menus.RetrieveAllFromDBSource();

            //删除数据.
            BP.DA.DBAccess.RunSQL("DELETE FROM GPM_EmpApp");
            BP.DA.DBAccess.RunSQL("DELETE FROM GPM_EmpMenu");

            foreach (BP.GPM.Emp emp in myemps)
            {
                #region 初始化系统访问权限.
                foreach (BP.GPM.App app in apps)
                {
                    EmpApp me = new EmpApp();
                    me.Copy(app);
                    me.FK_Emp = emp.No;
                    me.FK_App = app.No;
                    me.Name = app.Name;
                    me.MyPK = app.No + "_" + me.FK_Emp;
                    me.Insert();
                }
                #endregion 初始化系统访问权限.

                #region 初始化人员菜单权限
                foreach (BP.GPM.Menu menu in menus)
                {
                    EmpMenu em = new EmpMenu();
                    em.Copy(menu);
                    em.FK_Emp = emp.No;
                    em.FK_App = menu.FK_App;
                    em.FK_Menu = menu.No;
                    em.IsChecked = true;
                    //    em.MyPK = menu.No + "_" + emp.No;
                    em.Insert();
                }
                #endregion
            }
            #endregion 初始化权限.

            #region 初始化HR数据.
            BP.OA.HR.HRRecord en1 = new BP.OA.HR.HRRecord();
            en1.CheckPhysicsTable();

            BP.Port.Emps emps123 = new BP.Port.Emps();
            emps123.RetrieveAll();
            foreach (BP.Port.Emp emp in emps123)
            {
                BP.OA.HR.HRRecord en = new BP.OA.HR.HRRecord();
                en.Copy(emp);
                en.Insert();
            }
            #endregion 初始化权限.

            #region 初始化汽车司机数据.
            foreach (BP.Port.Emp emp in emps123)
            {
                BP.OA.Car.Driver en = new BP.OA.Car.Driver();
                en.ZJCX = "汽车";
                en.LZRQ = "2018-09-09";
                en.JZDQR = "2020-01-01";
                en.Copy(emp);
                en.Insert();
            }
            #endregion 初始化汽车司机数据.

        }
    }
}
