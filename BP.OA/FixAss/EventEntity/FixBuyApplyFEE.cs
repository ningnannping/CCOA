﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.DA;
using BP.DTS;
using BP.En;
using BP.Web;
using BP.Sys;
using BP.WF;
using BP.Port;

namespace BP.OA.FixAss.EventEntity
{
    public class FixBuyApplyFEE : BP.WF.FlowEventBase
    {
        public override string FlowMark
        {
            get { return "FixBuyApply"; }
        }

        public override string FlowOverAfter()
        {
            BP.OA.FixAss.API.Fix_BuyApply(this.GetValStr("WPMC"), this.GetValStr("WPZDBH"), this.GetValStr("GGXH"), this.GetValInt("GMSL"), this.GetValDecimal("YJDJ"), this.GetValInt("SFYSNGM") == 1 ? true : false, this.GetValStr("GMYY"), this.GetValStr("SQR"), this.GetValStr("SQBM"), this.GetValStr("SQRQ"));
            return "写入成功...";
        }
        public override string AfterFlowDel()
        {
            return null;
        }
        public override string BeforeFlowDel()
        {
            return null;
        }
        public override string FlowOverBefore()
        {
            return null;
        }
    }
}
