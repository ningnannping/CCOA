﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.DA;
using BP.DTS;
using BP.En;
using BP.Web;
using BP.Sys;
using BP.WF;
using BP.Port;

namespace BP.OA.FixAss.EventEntity
{
    /// <summary>
    /// 资产变卖流程事件
    /// </summary>
    public class FixBianMaiFEE : BP.WF.FlowEventBase
    {
        public override string FlowMark
        {
            get { return "FixBianMai"; }
        }

        public override string FlowOverAfter()
        {
            BP.OA.FixAss.API.Fix_BianMai(this.GetValStr("WPMC"), this.GetValStr("WPBH"), this.GetValStr("GGXH"), this.GetValStr("SZBM"), this.GetValStr("GMRQ"), this.GetValStr("GMJG"), this.GetValStr("BMJG"), this.GetValStr("BMRQ"), this.GetValStr("SQR"), this.GetValStr("BMLY"));
            return "写入成功...";
        }
        public override string AfterFlowDel()
        {
            return null;
        }
        public override string BeforeFlowDel()
        {
            return null;
        }
        public override string FlowOverBefore()
        {
            return null;
        }
    }
}
