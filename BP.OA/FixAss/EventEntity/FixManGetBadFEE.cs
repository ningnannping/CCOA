using System;
using System.Collections;
using System.Data;
using BP.DA;
using BP.DTS;
using BP.En;
using BP.Web;
using BP.Sys;
using BP.WF;
using BP.Port;

namespace BP.FixAss
{
    /// <summary>
    /// 固定资产报废流程事件实体
    /// </summary>
    public class FixManGetBadFEE : BP.WF.FlowEventBase
    {
        #region 构造.
        /// <summary>
        /// 固定资产报废流程事件实体
        /// </summary>
        public FixManGetBadFEE()
        {
        }
        #endregion 构造.

        #region 重写属性.
        public override string FlowMark
        {
            get { return "FixManGetBad"; }
        }
        #endregion 重写属性.

        #region 重写节点运动事件.
        /// <summary>
        /// 删除后
        /// </summary>
        /// <returns></returns>
        public override string AfterFlowDel()
        {
            return null;
        }
        /// <summary>
        /// 删除前
        /// </summary>
        /// <returns></returns>
        public override string BeforeFlowDel()
        {
            return null;
        }
        /// <summary>
        /// 结束后
        /// </summary>
        /// <returns></returns>
        public override string FlowOverAfter()
        {
            string sql = "select * from  ND1601dtl1 where  RefPK=" + this.WorkID;

            DataTable dt = DBAccess.RunSQLReturnTable(sql);

            foreach (DataRow dr in dt.Rows)
            {
                BP.FixAss.API.FixMan_GetBad(
                    dr["OA_FixMan"].ToString(),
                    dr["OA_FixAssBigCatagory"].ToString(),
                    dr["OA_FixAssBigCatSon"].ToString(),
                    dr["GuiGe"].ToString(),
                    this.GetValStr("TianXieRen"),
                    this.GetValStr("LiShuBuMen"),
                  this.GetValStr("ShiJian"),
                  dr["BeiZhu"].ToString(),
                  this.GetValStr("SHZT"));
            }
            return "写入成功....";
        }
        /// <summary>
        /// 结束前
        /// </summary>
        /// <returns></returns>
        public override string FlowOverBefore()
        {
            return null;
        }
        #endregion 重写事件，完成业务逻辑.
    }
}
