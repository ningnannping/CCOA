using System;
using System.Collections;
using System.Data;
using BP.DA;
using BP.DTS;
using BP.En;
using BP.Web;
using BP.Sys;
using BP.WF;
using BP.Port;

namespace BP.FixAss
{
    /// <summary>
    /// 固定资产领用 流程事件实体
    /// </summary>
    public class FixManGetUseFEE : BP.WF.FlowEventBase
    {
        #region 构造.
        /// <summary>
        /// 固定资产领用 流程事件实体
        /// </summary>
        public FixManGetUseFEE()
        {
        }
        #endregion 构造.

        #region 重写属性.
        public override string FlowMark
        {
            get { return "FixManGetUse"; }
        }
        #endregion 重写属性.

        #region 重写节点运动事件.
        /// <summary>
        /// 删除后
        /// </summary>
        /// <returns></returns>
        public override string AfterFlowDel()
        {
            return null;
        }
        /// <summary>
        /// 删除前
        /// </summary>
        /// <returns></returns>
        public override string BeforeFlowDel()
        {
            return null;
        }
        public override string SendWhen()
        {
            int curnode = this.HisNode.NodeID;
            string uu = this.Nos;
            return base.SendWhen();
        }
        /// <summary>
        /// 结束后
        /// </summary>
        /// <returns></returns>
        public override string FlowOverAfter()
        {
            string sql = "select * from  ND1401dtl1 where  RefPK=" + this.WorkID;
            DataTable dt = DBAccess.RunSQLReturnTable(sql);


            if (this.GetValStr("SHZT") == "0")
            {
                foreach (DataRow dr in dt.Rows)
                {
                    BP.FixAss.API.FixMan_GetUse(
                        dr["OA_FixMan"].ToString(),
                        this.GetValStr("ShenQingRen"),
                        this.GetValStr("LiShuBuMen"),
                      "1");
                }
            }
            //else
            //{
            //    ZCZT = "0";
            //    foreach (DataRow dr in dt.Rows)
            //    {
            //        BP.FixAss.API.FixMan_GetUse(
            //            dr["OA_FixMan"].ToString(),
            //            "",
            //            "",
            //          ZCZT);
            //    }  
            //}

            //foreach (DataRow dr in dt.Rows)
            //{
            //    BP.FixAss.API.FixMan_GetUse(
            //        dr["OA_FixMan"].ToString(),
            //        this.GetValStr("ShenQingRen"),
            //        this.GetValStr("LiShuBuMen"),
            //      GetUseZT);
            //}
            return "写入成功....";
        }
        /// <summary>
        /// 结束前
        /// </summary>
        /// <returns></returns>
        public override string FlowOverBefore()
        {
            return null;
        }
        #endregion 重写事件，完成业务逻辑.
    }
}
