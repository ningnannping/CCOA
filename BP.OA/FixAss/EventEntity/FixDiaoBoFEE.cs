﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.DA;
using BP.DTS;
using BP.En;
using BP.Web;
using BP.Sys;
using BP.WF;
using BP.Port;

namespace BP.OA.FixAss.EventEntity
{
    public class FixDiaoBoFEE : BP.WF.FlowEventBase
    {
        public override string FlowMark
        {
            get { return "FixDiaoBo"; }
        }

        public override string FlowOverAfter()
        {
            BP.OA.FixAss.API.Fix_DiaoBo(this.GetValStr("WPMC"), this.GetValStr("WPBH"), this.GetValStr("FromDept"), this.GetValStr("BaoGuanRen"), this.GetValStr("DBM"), this.GetValStr("DRY"), this.GetValStr("DBRQ"), this.GetValStr("SQR"), this.GetValStr("DBLY"));
            return "写入成功...";
        }
        public override string AfterFlowDel()
        {
            return null;
        }
        public override string BeforeFlowDel()
        {
            return null;
        }
        public override string FlowOverBefore()
        {
            return null;
        }
    }
}
