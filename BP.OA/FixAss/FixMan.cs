﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.DA;
using BP.DTS;
using BP.En;
using BP.Web;
using BP.Sys;
using BP.WF;
using BP.Port;
using System.Data;

namespace BP.FixAss
{
    /// <summary>
    /// 资产状态(枚举)
    /// </summary>
    public enum ZCZT
    {
        /// <summary>
        /// 闲置
        /// </summary>
        Free,
        /// <summary>
        /// 使用中
        /// </summary>
        Using,
        /// <summary>
        /// 报废
        /// </summary>
        Bad,
        /// <summary>
        /// 维修中
        /// </summary>
        Repair
    }
    /// <summary>
    /// 固定资产台账属性
    /// </summary>
    public class FixManAttr : EntityNoNameAttr
    {
        /// <summary>
        /// 所属大类
        /// </summary>
        public const string FK_FixAssBigCatagory = "FK_FixAssBigCatagory";
        /// <summary>
        /// 所属小类
        /// </summary>
        public const string FK_FixAssBigCatSon = "FK_FixAssBigCatSon";
        /// <summary>
        /// 凭证号
        /// </summary>
        public const string CertifiNum = "CertifiNum";
        /// <summary>
        /// 规格型号
        /// </summary>
        public const string ZipSpec = "ZipSpec";
        /// <summary>
        /// 发票金额
        /// </summary>
        public const string InvoicePri = "InvoicePri";
        /// <summary>
        /// 发票日期
        /// </summary>
        public const string InvoiceDat = "InvoiceDat";
        /// <summary>
        /// 折旧年限
        /// </summary>
        public const string Year = "Year";
        /// <summary>
        /// 折旧开始时间
        /// </summary>
        public const string DepreBeginTime = "DepreBeginTime";
        /// <summary>
        /// 残值率(%)
        /// </summary>
        public const string HowManyGetOld = "HowManyGetOld";
        /// <summary>
        /// 资产状态
        /// </summary>
        public const string ZCZT = "ZCZT";
        /// <summary>
        /// 资产使用人
        /// </summary>
        public const string WhoUse = "WhoUse";
        /// <summary>
        /// 资产所在位置
        /// </summary>
        public const string WhereIt = "WhereIt";
        /// <summary>
        /// 备注
        /// </summary>
        public const string BeiZhu = "BeiZhu";
    }
    public class FixMan : EntityNoName
    {
        #region 属性
        /// <summary>
        /// 所属大类
        /// </summary>
        public string FK_FixAssBigCatagory
        {
            get
            {
                return this.GetValStringByKey(FixManAttr.FK_FixAssBigCatagory);
            }
            set
            {
                this.SetValByKey(FixManAttr.FK_FixAssBigCatagory, value);
            }
        }
        /// <summary>
        /// 规格型号
        /// </summary>
        public string ZipSpec
        {
            get
            {
                return this.GetValStringByKey(FixManAttr.ZipSpec);
            }
            set
            {
                this.SetValByKey(FixManAttr.ZipSpec, value);
            }
        }
        /// <summary>
        /// 所属小类
        /// </summary>
        public string FK_FixAssBigCatSon
        {
            get
            {
                return this.GetValStringByKey(FixManAttr.FK_FixAssBigCatSon);
            }
            set
            {
                this.SetValByKey(FixManAttr.FK_FixAssBigCatSon, value);
            }
        }
        /// <summary>
        /// 使用人
        /// </summary>
        public string WhoUse
        {
            get
            {
                return this.GetValStringByKey(FixManAttr.WhoUse);
            }
            set
            {
                this.SetValByKey(FixManAttr.WhoUse, value);
            }
        }
        /// <summary>
        /// 所在位置
        /// </summary>
        public string WhereIt
        {
            get
            {
                return this.GetValStringByKey(FixManAttr.WhereIt);
            }
            set
            {
                this.SetValByKey(FixManAttr.WhereIt, value);
            }
        }
        /// <summary>
        /// 资产状态
        /// </summary>
        public string ZCZT
        {
            get
            {
                return this.GetValStringByKey(FixManAttr.ZCZT);
            }
            set
            {
                this.SetValByKey(FixManAttr.ZCZT, value);
            }
        }
        #endregion
        #region 构造函数
        /// <summary>
        /// 固定资产台帐
        /// </summary>
        public FixMan() { }
        /// <summary>
        /// 固定资产台帐
        /// </summary>
        /// <param name="no">编号</param>
        public FixMan(string no) : base(no) { }
        #endregion

        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                uac.OpenAll();
                return uac;
            }
        }
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                {
                    return this._enMap;
                }
                Map map = new Map("OA_FixMan");
                map.EnDesc = "固定资产";

                map.CodeStruct = "4";
                map.IsAutoGenerNo = true;

                map.AddTBStringPK("No", null, "编号", true, true, 4, 4, 4);
                map.AddTBString(FixManAttr.Name, null, "名称", true, false, 1, 100, 30, true);
                map.AddDDLEntities(FixManAttr.FK_FixAssBigCatagory, null, "所属大类", new FixAssBigCatagorys(), true);
                map.AddDDLEntities(FixManAttr.FK_FixAssBigCatSon, null, "所属小类", new FixAssBigCatSons(), true);
                map.AddTBString(FixManAttr.CertifiNum, null, "凭证号", true, false, 1, 100, 30, true);
                map.AddTBString(FixManAttr.ZipSpec, null, "规格型号", true, false, 0, 100, 30, false);
                map.AddTBMoney(FixManAttr.InvoicePri, 0, "发票金额(元)", true, false);
                map.AddTBDate(FixManAttr.InvoiceDat, "发票日期", true, false);
                map.AddTBString(FixManAttr.Year, null, "折旧年限", true, false, 0, 100, 30, false);
                map.AddTBDate(FixManAttr.DepreBeginTime, "折旧开始时间", true, false);
                map.AddTBFloat(FixManAttr.HowManyGetOld, 0, "残值率(%)", true, false);
                map.AddDDLSysEnum(FixManAttr.ZCZT, 0, "资产状态", true, true, FixManAttr.ZCZT,
                    "@0=闲置@1=使用中@2=报废@3=维修中");
                map.AddTBString(FixManAttr.WhoUse, null, "使用人", true, true, 0, 100, 30, true);
                map.AddTBString(FixManAttr.WhereIt, null, "所在位置", true, true, 0, 100, 30, true);
                map.AddTBString(FixManAttr.BeiZhu, null, "备注", true, false, 0, 100, 30, true);

                map.AddSearchAttr(FixManAttr.FK_FixAssBigCatagory);
                map.AddSearchAttr(FixManAttr.FK_FixAssBigCatSon);


                this._enMap = map;
                return this._enMap;
            }
        }

        #region 重写方法
        /// <summary>
        /// 设置实现大/小类别联动
        /// </summary>
        /// <returns></returns>

        protected override bool beforeUpdateInsertAction()
        {

            return base.beforeUpdateInsertAction();
        }

        protected override void afterInsertUpdateAction()
        {


            base.afterInsertUpdateAction();
        }

        #endregion
    }
    public class FixMans : EntitiesNoName
    {
        /// <summary>
        /// 类别
        /// </summary>
        public FixMans() { }
        /// <summary>
        /// 得到它的 Entity 
        /// </summary>
        public override Entity GetNewEntity
        {
            get { return new FixMan(); }
        }
    }
}
