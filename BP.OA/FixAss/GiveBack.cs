﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.DA;
using BP.DTS;
using BP.En;
using BP.Web;
using BP.Sys;
using BP.WF;
using BP.Port;
using System.Data;

namespace BP.FixAss
{ 
    /// <summary>
    /// 审核状态(枚举)
    /// </summary>
    public enum GiveBackZT
    {
        /// <summary>
        /// 处理中
        /// </summary>
        Free,
        /// <summary>
        /// 通过
        /// </summary>
        Using,
        /// <summary>
        /// 未通过
        /// </summary>
        Ording
    }
    /// <summary>
    /// 固定资产报废属性
    /// </summary>
    public class GiveBackAttr : EntityOIDAttr
    {
        // <summary>
        /// 资产编号
        /// </summary>
        public const string FixNum = "FixNum";
        /// <summary>
        /// 资产名称
        /// </summary>
        public const string FK_FixMan = "FK_FixMan";
        /// <summary>
        /// 所属大类
        /// </summary>
        public const string FK_FixAssBigCatagory = "FK_FixAssBigCatagory";
        /// <summary>
        /// 所属小类
        /// </summary>
        public const string FK_FixAssBigCatSon = "FK_FixAssBigCatSon";
        /// <summary>
        /// 规格
        /// </summary>
        public const string ZipSpec = "ZipSpec";
        /// <summary>
        /// 归还人
        /// </summary>
        public const string WhoGiveBack = "WhoGiveBack";
        /// <summary>
        /// 隶属部门
        /// </summary>
        public const string GiveBackDept = "GiveBackDept";
        /// <summary>
        /// 归还日期
        /// </summary>
        public const string GiveBackDate = "GiveBackDate";
        /// <summary>
        /// 审批状态
        /// </summary>
        public const string GiveBackZT = "GiveBackZT";
        /// <summary>
        /// 备注
        /// </summary>
        public const string GiveBackNote = "GiveBackNote";
        /// <summary>
        /// 业务编号
        /// </summary>
        public const string WorkID = "WorkID";
        
    }
    /// <summary>
    /// 固定资产报废台帐
    /// </summary>
    public class GiveBack : EntityOIDName
    {
        #region 属性
        /// <summary>
        /// 资产编号
        /// </summary>
        public string FixNum
        {
            get
            {
                return this.GetValStringByKey(GiveBackAttr.FixNum);
            }
            set
            {
                this.SetValByKey(GiveBackAttr.FixNum, value);
            }
        }
        /// <summary>
        /// 物品名
        /// </summary>
        public string FK_FixMan
        {
            get
            {
                return this.GetValStringByKey(GiveBackAttr.FK_FixMan);
            }
            set
            {
                this.SetValByKey(GiveBackAttr.FK_FixMan, value);
            }
        }
        /// <summary>
        /// 所属大类
        /// </summary>
        public string FK_FixAssBigCatagory
        {
            get
            {
                return this.GetValStringByKey(GiveBackAttr.FK_FixAssBigCatagory);
            }
            set
            {
                this.SetValByKey(GiveBackAttr.FK_FixAssBigCatagory, value);
            }
        }
        /// <summary>
        /// 所属小类
        /// </summary>
        public string FK_FixAssBigCatSon
        {
            get
            {
                return this.GetValStringByKey(GiveBackAttr.FK_FixAssBigCatSon);
            }
            set
            {
                this.SetValByKey(GiveBackAttr.FK_FixAssBigCatSon, value);
            }
        }
        /// <summary>
        /// 规格型号
        /// </summary>
        public string ZipSpec
        {
            get
            {
                return this.GetValStringByKey(GiveBackAttr.ZipSpec);
            }
            set
            {
                this.SetValByKey(GiveBackAttr.ZipSpec, value);
            }
        }
        /// <summary>
        /// 归还人
        /// </summary>
        public string WhoGiveBack
        {
            get
            {
                return this.GetValStringByKey(GiveBackAttr.WhoGiveBack);
            }
            set
            {
                this.SetValByKey(GiveBackAttr.WhoGiveBack, value);
            }
        }
        /// <summary>
        /// 隶属部门
        /// </summary>
        public string GiveBackDept
        {
            get
            {
                return this.GetValStringByKey(GiveBackAttr.GiveBackDept);
            }
            set
            {
                this.SetValByKey(GiveBackAttr.GiveBackDept, value);
            }
        }
        /// <summary>
        /// 日期
        /// </summary>
        public string GiveBackDate
        {
            get
            {
                return this.GetValStringByKey(GiveBackAttr.GiveBackDate);
            }
            set
            {
                this.SetValByKey(GiveBackAttr.GiveBackDate, value);
            }
        }
        /// <summary>
        /// 审核状态
        /// </summary>
        public string GiveBackZT
        {
            get
            {
                return this.GetValStringByKey(GiveBackAttr.GiveBackZT);
            }
            set
            {
                this.SetValByKey(GiveBackAttr.GiveBackZT, value);
            }
        }
        /// <summary>
        /// 备注
        /// </summary>
        public string GiveBackNote
        {
            get
            {
                return this.GetValStringByKey(GiveBackAttr.GiveBackNote);
            }
            set
            {
                this.SetValByKey(GiveBackAttr.GiveBackNote, value);
            }
        }
        /// <summary>
        /// 业务编号
        /// </summary>
        public string WorkID
        {
            get
            {
                return this.GetValStringByKey(GiveBackAttr.WorkID);
            }
            set
            {
                this.SetValByKey(GiveBackAttr.WorkID, value);
            }
        }
        #endregion

        #region 构造函数
        public GiveBack() { }
        public GiveBack(int oid) : base(oid) { }
        #endregion

        #region 重写方法
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                uac.IsDelete = false;
                uac.IsInsert = false;
                uac.IsUpdate = false;
                uac.IsView = true;
                return uac;
            }
        }
        /// <summary>
        /// Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map();
                map.PhysicsTable = "OA_GiveBack";
                map.EnDesc = "固定资产归还台帐";
                map.CodeStruct = "5";
                map.IsAutoGenerNo = true;

                map.AddTBIntPKOID();

                map.AddTBString(GiveBackAttr.FixNum, null, "资产编号", true, true, 0, 100, 50);
                map.AddDDLEntities(GiveBackAttr.FK_FixMan, null, "物品名", new FixMans(), true);
                map.AddDDLEntities(GiveBackAttr.FK_FixAssBigCatagory, null, "所属大类", new FixAssBigCatagorys(), true);
                map.AddDDLEntities(GiveBackAttr.FK_FixAssBigCatSon, null, "所属小类", new FixAssBigCatSons(), true);
                map.AddTBString(GiveBackAttr.ZipSpec, null, "规格型号", true, true, 1, 100, 50);
                map.AddTBString(GiveBackAttr.WhoGiveBack, "", "归还人", true, false, 1, 100, 50);
                map.AddTBString(GiveBackAttr.GiveBackDept, "", "隶属部门", true, false, 1, 100, 50);
                map.AddTBDate(GiveBackAttr.GiveBackDate, "归还日期", true, false);
                map.AddDDLSysEnum(GiveBackAttr.GiveBackZT, 0, "状态", true, true, GiveBackAttr.GiveBackZT,
                  "@0=审核中@1=通过@2=未通过");
                map.AddTBString(GiveBackAttr.GiveBackNote, null, "备注", true, false, 1, 100, 50);
                map.AddTBInt(GiveBackAttr.WorkID, 0, "业务编号", false, true);


                map.AddSearchAttr(GiveBackAttr.FK_FixMan);

                this._enMap = map;
                return this._enMap;
            }
        }
        #endregion

        protected override bool beforeUpdateInsertAction()
        {
            if (this.GiveBackDate == "")
                this.GiveBackDate = DataType.CurrentData;

            FixMan ds = new FixMan(this.FK_FixMan);
            //复制主表的属性.
            this.FixNum = ds.No;
            this.ZipSpec = ds.ZipSpec;
            this.FK_FixAssBigCatagory = ds.FK_FixAssBigCatagory;
            this.FK_FixAssBigCatSon = ds.FK_FixAssBigCatSon;
            return base.beforeUpdateInsertAction();
        }
        protected override bool beforeInsert()
        {
            return base.beforeInsert();
        }
        protected override void afterInsert()
        {
            base.afterInsert();
        }
    }
    /// <summary>
    ///固定资产报废台帐s
    /// </summary>
    public class GiveBacks : EntitiesOIDName
    {
        public GiveBacks() { }
        public override Entity GetNewEntity
        {
            get
            {
                return new GiveBack();
            }
        }
    }
}
