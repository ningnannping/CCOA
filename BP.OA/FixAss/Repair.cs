﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BP.DA;
using BP.DTS;
using BP.En;
using BP.Web;
using BP.Sys;
using BP.WF;
using BP.Port;
using System.Data;

namespace BP.FixAss
{
    /// <summary>
    /// 审核状态(枚举)
    /// </summary>
    public enum RepairZT
    {
        /// <summary>
        /// 审核中
        /// </summary>
        Free,
        /// <summary>
        /// 通过
        /// </summary>
        Using,
        /// <summary>
        /// 未通过
        /// </summary>
        Ording
    }
    /// <summary>
    /// 固定资产维修属性
    /// </summary>
    public class RepairAttr : EntityOIDAttr
    {
        // <summary>
        /// 资产编号
        /// </summary>
        public const string FixNum = "FixNum";
        /// <summary>
        /// 资产名称
        /// </summary>
        public const string FK_FixMan = "FK_FixMan";
        /// <summary>
        /// 所属大类
        /// </summary>
        public const string FK_FixAssBigCatagory = "FK_FixAssBigCatagory";
        /// <summary>
        /// 所属小类
        /// </summary>
        public const string FK_FixAssBigCatSon = "FK_FixAssBigCatSon";
        /// <summary>
        /// 规格
        /// </summary>
        public const string ZipSpec = "ZipSpec";
        /// <summary>
        /// 维修负责人
        /// </summary>
        public const string WhoRes = "WhoRes";
        /// <summary>
        /// 维修费用
        /// </summary>
        public const string RepMney = "RepMney";
        /// <summary>
        /// 申请人
        /// </summary>
        public const string WhoWantRep = "WhoWantRep";
        /// <summary>
        /// 隶属部门
        /// </summary>
        public const string FixManRepairDept = "FixManRepairDept";
        /// <summary>
        /// 申请日期
        /// </summary>
        public const string FixManRepairDate = "FixManRepairDate";
        /// <summary>
        /// 原因
        /// </summary>
        public const string WhyRepair = "WhyRepair";
        /// <summary>
        /// 审批状态
        /// </summary>
        public const string RepairZT = "RepairZT";
        /// <summary>
        /// 业务编号
        /// </summary>
        public const string WorkID = "WorkID";
    }
    /// <summary>
    /// 固定资产维修台帐
    /// </summary>
    public class Repair : EntityOIDName
    {
        #region 属性
        /// <summary>
        /// 资产编号
        /// </summary>
        public string FixNum
        {
            get
            {
                return this.GetValStringByKey(RepairAttr.FixNum);
            }
            set
            {
                this.SetValByKey(RepairAttr.FixNum, value);
            }
        }
        /// <summary>
        /// 物品名
        /// </summary>
        public string FK_FixMan
        {
            get
            {
                return this.GetValStringByKey(RepairAttr.FK_FixMan);
            }
            set
            {
                this.SetValByKey(RepairAttr.FK_FixMan, value);
            }
        }
        /// <summary>
        /// 所属大类
        /// </summary>
        public string FK_FixAssBigCatagory
        {
            get
            {
                return this.GetValStringByKey(RepairAttr.FK_FixAssBigCatagory);
            }
            set
            {
                this.SetValByKey(RepairAttr.FK_FixAssBigCatagory, value);
            }
        }
        /// <summary>
        /// 所属小类
        /// </summary>
        public string FK_FixAssBigCatSon
        {
            get
            {
                return this.GetValStringByKey(RepairAttr.FK_FixAssBigCatSon);
            }
            set
            {
                this.SetValByKey(RepairAttr.FK_FixAssBigCatSon, value);
            }
        }
        /// <summary>
        /// 规格型号
        /// </summary>
        public string ZipSpec
        {
            get
            {
                return this.GetValStringByKey(RepairAttr.ZipSpec);
            }
            set
            {
                this.SetValByKey(RepairAttr.ZipSpec, value);
            }
        }
        /// <summary>
        /// 维修负责人
        /// </summary>
        public string WhoRes
        {
            get
            {
                return this.GetValStringByKey(RepairAttr.WhoRes);
            }
            set
            {
                this.SetValByKey(RepairAttr.WhoRes, value);
            }
        }
        /// <summary>
        /// 费用
        /// </summary>
        public string RepMney
        {
            get
            {
                return this.GetValStringByKey(RepairAttr.RepMney);
            }
            set
            {
                this.SetValByKey(RepairAttr.RepMney, value);
            }
        }
        /// <summary>
        /// 申请人
        /// </summary>
        public string WhoWantRep
        {
            get
            {
                return this.GetValStringByKey(RepairAttr.WhoWantRep);
            }
            set
            {
                this.SetValByKey(RepairAttr.WhoWantRep, value);
            }
        }
        /// <summary>
        /// =隶属部门
        /// </summary>
        public string FixManRepairDept
        {
            get
            {
                return this.GetValStringByKey(RepairAttr.FixManRepairDept);
            }
            set
            {
                this.SetValByKey(RepairAttr.FixManRepairDept, value);
            }
        }
        /// <summary>
        /// 日期
        /// </summary>
        public string FixManRepairDate
        {
            get
            {
                return this.GetValStringByKey(RepairAttr.FixManRepairDate);
            }
            set
            {
                this.SetValByKey(RepairAttr.FixManRepairDate, value);
            }
        }
        /// <summary>
        /// 审核状态
        /// </summary>
        public string RepairZT
        {
            get
            {
                return this.GetValStringByKey(RepairAttr.RepairZT);
            }
            set
            {
                this.SetValByKey(RepairAttr.RepairZT, value);
            }
        }
        /// <summary>
        /// 原因
        /// </summary>
        public string WhyRepair
        {
            get
            {
                return this.GetValStringByKey(RepairAttr.WhyRepair);
            }
            set
            {
                this.SetValByKey(RepairAttr.WhyRepair, value);
            }
        }
        /// <summary>
        /// 业务编号
        /// </summary>
        public string WorkID
        {
            get
            {
                return this.GetValStringByKey(RepairAttr.WorkID);
            }
            set
            {
                this.SetValByKey(RepairAttr.WorkID, value);
            }
        }


        #endregion

        #region 构造函数
        public Repair() { }
        public Repair(int oid) : base(oid) { }
        #endregion

        #region 重写方法
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                uac.IsDelete = false;
                uac.IsInsert = false;
                uac.IsUpdate = false;
                uac.IsView = true;
                return uac;
            }
        }
        /// <summary>
        /// Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map();
                map.PhysicsTable = "OA_Repair";
                map.EnDesc = "固定资产维修台帐";
                map.CodeStruct = "5";
                map.IsAutoGenerNo = true;

                map.AddTBIntPKOID();

                map.AddTBString(RepairAttr.FixNum, null, "资产编号", true, true, 0, 100, 50);
                map.AddDDLEntities(RepairAttr.FK_FixMan, null, "物品名", new FixMans(), true);
                map.AddDDLEntities(RepairAttr.FK_FixAssBigCatagory, null, "所属大类", new FixAssBigCatagorys(), true);
                map.AddDDLEntities(RepairAttr.FK_FixAssBigCatSon, null, "所属小类", new FixAssBigCatSons(), true);
                map.AddTBString(RepairAttr.ZipSpec, null, "规格型号", true, true, 1, 100, 50);
                map.AddTBString(RepairAttr.WhoRes, null, "维修负责人", true, false, 0, 100, 50);
                map.AddTBMoney(RepairAttr.RepMney, 0, "费用(元)", true, false);
                map.AddTBString(RepairAttr.WhoWantRep, null, "申请人", true, false, 1, 100, 50);
                map.AddTBString(RepairAttr.FixManRepairDept, "", "隶属部门", true, false, 1, 100, 50);
                map.AddTBDate(RepairAttr.FixManRepairDate, "申请日期", true, false);
                map.AddDDLSysEnum(RepairAttr.RepairZT, 0, "状态", true, true, RepairAttr.RepairZT,
                  "@0=审核中@1=通过@2=未通过");
                map.AddTBString(RepairAttr.WhyRepair, null, "原因", true, false, 1, 100, 50);
                map.AddTBInt(RepairAttr.WorkID, 0, "业务编号", false, true);

                this._enMap = map;
                return this._enMap;
            }
        }
        #endregion

        protected override bool beforeUpdateInsertAction()
        {
            if (this.FixManRepairDate == "")
                this.FixManRepairDate = DataType.CurrentData;

            FixMan ds = new FixMan(this.FK_FixMan);
            //复制主表的属性.
            this.FixNum = ds.No;
            this.ZipSpec = ds.ZipSpec;
            this.FK_FixAssBigCatagory = ds.FK_FixAssBigCatagory;
            this.FK_FixAssBigCatSon = ds.FK_FixAssBigCatSon;
            return base.beforeUpdateInsertAction();
        }
        protected override bool beforeInsert()
        {
            return base.beforeInsert();
        }
        protected override void afterInsert()
        {
            base.afterInsert();
        }
    }
    /// <summary>
    ///固定资产报废台帐s
    /// </summary>
    public class Repairs : EntitiesOIDName
    {
        public Repairs() { }
        public override Entity GetNewEntity
        {
            get
            {
                return new Repair();
            }
        }
    }
}
