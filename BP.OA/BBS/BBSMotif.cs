﻿using System;
using System.Collections.Generic;
using System.Text;

using BP.DA;
using BP.En;
using System.Data;
using BP.Port;

namespace BP.OA.BBS
{
    //论坛主题
    public class BBSMotifAttr : EntityNoNameAttr
    {
        /// <summary>
        /// 板块编号
        /// </summary>
        public const String FK_Class = "FK_Class";
        /// <summary>
        /// 内容
        /// </summary>
        public const String Contents = "Contents";
        /// <summary>
        /// 新建人员
        /// </summary>
        public const String FK_Emp = "FK_Emp";
        /// <summary>
        /// 记录日期
        /// </summary>
        public const String RDT = "RDT";
        /// <summary>
        /// 浏览次数
        /// </summary>
        public const String Hits = "Hits";
        /// <summary>
        /// 回复次数
        /// </summary>
        public const String Replays = "Replays";
        /// <summary>
        /// 最后回复时间
        /// </summary>
        public const String LastReTime = "LastReTime";
        /// <summary>
        /// 最后回复人
        /// </summary>
        public const String LastReEmp = "LastReEmp";
        /// <summary>
        /// 总是置顶
        /// </summary>
        public const String IsAllTop = "IsAllTop";
        /// <summary>
        /// 置顶
        /// </summary>
        public const String IsTop = "IsTop";
        /// <summary>
        /// 精华帖
        /// </summary>
        public const String IsWonderful = "IsWonderful";
    }
    /// <summary>
    /// 论坛主题
    /// </summary>
    public partial class BBSMotif : EntityNoName
    {
        #region 属性
        /// <summary>
        /// 板块编号
        /// </summary>
        public string FK_Class
        {
            get
            {
                return this.GetValStrByKey(BBSMotifAttr.FK_Class);
            }
            set
            {
                this.SetValByKey(BBSMotifAttr.FK_Class, value);
            }
        }
        /// <summary>
        /// 内容
        /// </summary>
        public string Contents
        {
            get 
            {
                string contents = this.GetValStrByKey(BBSMotifAttr.Contents);
                if (contents.Length <= 10)
                    return this.GetBigTextFromDB("DocFile");
                else
                    return contents;
            }
            set {
                if (value.Length > 2000)
                {
                    this.SaveBigTxtToDB("DocFile", value);
                    this.SetValByKey(BBSMotifAttr.Contents, "");
                }
                else 
                {
                    this.SetValByKey(BBSMotifAttr.Contents, value);
                }
                 }
        }
        /// <summary>
        /// 新建人员
        /// </summary>
        public string FK_Emp
        {
            get
            {
                return this.GetValStrByKey(BBSMotifAttr.FK_Emp);
            }
            set
            {
                this.SetValByKey(BBSMotifAttr.FK_Emp, value);
            }
        }
        /// <summary>
        /// 新建人员名称
        /// </summary>
        public string FK_EmpText
        {
            get
            {
                return this.GetValRefTextByKey(BBSMotifAttr.FK_Emp);
            }
        }
        /// <summary>
        /// 记录日期
        /// </summary>
        public DateTime RDT
        {
            get
            {
                return this.GetValDateTime(BBSMotifAttr.RDT);
            }
            set
            {
                this.SetValByKey(BBSMotifAttr.RDT, value);
            }
        }
        /// <summary>
        /// 浏览次数
        /// </summary>
        public int Hits
        {
            get
            {
                return this.GetValIntByKey(BBSMotifAttr.Hits);
            }
            set
            {
                this.SetValByKey(BBSMotifAttr.Hits, value);
            }
        }
        /// <summary>
        /// 回复次数
        /// </summary>
        public int Replays
        {
            get
            {
                return this.GetValIntByKey(BBSMotifAttr.Replays);
            }
            set
            {
                this.SetValByKey(BBSMotifAttr.Replays, value);
            }
        }
        /// <summary>
        /// 最后回复时间
        /// </summary>
        public DateTime LastReTime
        {
            get
            {
                return this.GetValDateTime(BBSMotifAttr.LastReTime);
            }
            set
            {
                this.SetValByKey(BBSMotifAttr.LastReTime, value);
            }
        }
        /// <summary>
        /// 最后回复人
        /// </summary>
        public string LastReEmp
        {
            get
            {
                return this.GetValStrByKey(BBSMotifAttr.LastReEmp);
            }
            set
            {
                this.SetValByKey(BBSMotifAttr.LastReEmp, value);
            }
        }
        /// <summary>
        /// 最后回复人名称
        /// </summary>
        public string LastReEmpText
        {
            get
            {
                return this.GetValRefTextByKey(BBSMotifAttr.LastReEmp);
            }
        }
        /// <summary>
        /// 总是置顶
        /// </summary>
        public int IsAllTop
        {
            get
            {
                return this.GetValIntByKey(BBSMotifAttr.IsAllTop);
            }
            set
            {
                this.SetValByKey(BBSMotifAttr.IsAllTop, value);
            }
        }
        /// <summary>
        /// 置顶
        /// </summary>
        public int IsTop
        {
            get
            {
                return this.GetValIntByKey(BBSMotifAttr.IsTop);
            }
            set
            {
                this.SetValByKey(BBSMotifAttr.IsTop, value);
            }
        }
        /// <summary>
        /// 精华帖
        /// </summary>
        public int IsWonderful
        {
            get
            {
                return this.GetValIntByKey(BBSMotifAttr.IsWonderful);
            }
            set
            {
                this.SetValByKey(BBSMotifAttr.IsWonderful, value);
            }
        }
        /// <summary>
        /// 发帖状态
        /// </summary>
        public string MotifState
        {
            get
            {

                string returnStr = "";
                if (this.IsAllTop == 1)
                {
                    returnStr += "[总置顶]";
                }
                else
                {
                    if (this.IsTop == 1)
                    {
                        returnStr += "[置顶]";
                    }
                }
                if (this.IsWonderful == 1)
                {
                    returnStr += "[精华]";
                }
                return returnStr;
            }
        }
        #endregion

        #region 构造函数
        /// <summary>
        /// 论坛主题
        /// </summary>
        public BBSMotif() { }
        /// <summary>
        /// 论坛主题
        /// </summary>
        /// <param name="_No">编号</param>
        public BBSMotif(string _No) : base(_No) { }
        #endregion

        #region 重写方法
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                uac.OpenAll();
                return uac;
            }
        }
        /// <summary>
        /// Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map();
                map.PhysicsTable = "OA_BBSMotif";
                map.EnDesc = "论坛主题";
                map.DepositaryOfEntity = Depositary.Application;
                map.DepositaryOfMap = Depositary.Application;
                map.CodeStruct = "2";
                map.AddTBStringPK(BBSMotifAttr.No, null, "编号", true, true, 2, 2, 2);
                map.AddTBString(BBSMotifAttr.Name, null, "名称", true, false, 0, 1000, 300);
                map.AddDDLEntities(BBSMotifAttr.FK_Class, null, "板块编号", new BBSClasss(), true);
                map.AddTBString(BBSMotifAttr.Contents, null, "内容", true, false, 0, 4000, 300);
                map.AddDDLEntities(BBSMotifAttr.FK_Emp, null, "新建人员", new Emps(), true);
                map.AddTBDateTime(BBSMotifAttr.RDT, null, "记录日期", true, false);
                map.AddTBInt(BBSMotifAttr.Hits, 0, "浏览次数", true, false);
                map.AddTBInt(BBSMotifAttr.Replays, 0, "回复次数", true, false);
                map.AddTBDateTime(BBSMotifAttr.LastReTime, null, "最后回复时间", true, false);
                map.AddDDLEntities(BBSMotifAttr.LastReEmp, null, "最后回复人", new Emps(), true);
                map.AddBoolean(BBSMotifAttr.IsAllTop, false, "总是置顶", true, false);
                map.AddBoolean(BBSMotifAttr.IsTop, false, "置顶", true, false);
                map.AddBoolean(BBSMotifAttr.IsWonderful, false, "精华帖", true, false);

                this._enMap = map;
                return this._enMap;
            }
        }

        /// <summary>
        /// 新增后事件
        /// </summary>
        protected override void afterInsert()
        {
            string sql = "SELECT COUNT(No) Num FROM OA_BBSMotif where FK_Class = '" + this.FK_Class + "'";
            //版块增加发帖量
            BBSClass bbsClass = new BBSClass(this.FK_Class);
            bbsClass.ThemeCount = BP.DA.DBAccess.RunSQLReturnValInt(sql);
            bbsClass.Update();

            //系统配置参数, 怎么参数是 0 ？
            BBSConfig config = new BBSConfig(0);

            //用户经验值
            BBSUserInfo bbsUser = new BBSUserInfo(this.FK_Emp);
            bbsUser.Exp = Convert.ToInt32(config.F_exp) + bbsUser.Exp;
            bbsUser.Integral = Convert.ToInt32(config.F_integral) + bbsUser.Integral;
            bbsUser.Gold = Convert.ToInt32(config.F_gold) + bbsUser.Gold;
            bbsUser.ThemeCount = bbsUser.ThemeCount + 1;
            bbsUser.Update();

            base.afterInsert();
        }

        protected override void afterDelete()
        {
            string sql = "SELECT COUNT(No) Num FROM OA_BBSMotif where FK_Class = '" + this.FK_Class + "'";
            //修改版块发帖量
            BBSClass bbsClass = new BBSClass(this.FK_Class);
            bbsClass.ThemeCount = BP.DA.DBAccess.RunSQLReturnValInt(sql);
            bbsClass.Update();

            base.afterDelete();
        }

        #endregion
    }
    /// <summary>
    ///论坛主题集合
    /// </summary>
    public class BBSMotifs : EntitiesNoName
    {
        /// <summary>
        /// 得到一个新实体
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new BBSMotif();
            }
        }
        /// <summary>
        /// 论坛主题集合
        /// </summary>
        public BBSMotifs()
        {
        }
    }
}
