﻿using System;
using System.Collections.Generic;
using System.Text;

using BP.DA;
using BP.En;

namespace BP.OA.Desktop
{
    /// <summary>
    /// 岗位配置属性
    /// </summary>
    public class DesktopStationAttr : EntityOIDAttr
    {
        /// <summary>
        /// 所属人员
        /// </summary>
        public const string FK_Station = "FK_Station";
        /// <summary>
        /// 加载顺序
        /// </summary>
        public const string Idx = "Idx";
        /// <summary>
        /// 模块编号
        /// </summary>
        public const string FK_PID = "FK_PID";
    }
    /// <summary>
    ///  岗位配置
    /// </summary>
    public class DesktopStation : EntityOID
    {
        #region 属性
        /// <summary>
        /// 所属人员
        /// </summary>
        public string FK_Station
        {
            get
            {
                return this.GetValStrByKey(DesktopStationAttr.FK_Station);
            }
            set
            {
                this.SetValByKey(DesktopStationAttr.FK_Station, value);
            }
        }
        /// <summary>
        /// 模块编号
        /// </summary>
        public string FK_PID
        {
            get
            {
                return this.GetValStrByKey(DesktopStationAttr.FK_PID);
            }
            set
            {
                this.SetValByKey(DesktopStationAttr.FK_PID, value);
            }
        }
        /// <summary>
        /// 加载顺序
        /// </summary>
        public int Idx
        {
            get
            {
                return this.GetValIntByKey(DesktopStationAttr.Idx);
            }
            set
            {
                this.SetValByKey(DesktopStationAttr.Idx, value);
            }
        }
        #endregion 属性

        #region 构造方法
        /// <summary>
        /// 岗位配置
        /// </summary>
        public DesktopStation()
        {
        }

        #endregion

        /// <summary>
        /// 岗位配置Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;
                Map map = new Map();
                map.PhysicsTable = "OA_DesktopStation"; //物理表。
                map.EnDesc = "岗位配置";
                map.DepositaryOfEntity = Depositary.Application;
                map.DepositaryOfMap = Depositary.Application;
                map.CodeStruct = "2";

                map.AddTBStringPK(DesktopStationAttr.OID, null, "编号", true, true, 10, 10, 10);
                map.AddDDLEntities(DesktopStationAttr.FK_Station, null, "FK_Station", new BP.Port.Stations(), true);
                map.AddTBString(DesktopStationAttr.FK_PID, null, "模块编号", true, false, 0, 200, 300, false);
                map.AddTBInt(DesktopStationAttr.Idx, 0, "加载顺序", true, false);
                this._enMap = map;
                return this._enMap;
            }
        }
    }
    /// <summary>
    /// 岗位配置
    /// </summary>
    public class DesktopStations : EntitiesOID
    {
        /// <summary>
        /// 岗位配置s
        /// </summary>
        public DesktopStations() { }
        /// <summary>
        /// 得到它的 Entity 
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new DesktopStation();
            }
        }
    }
}
