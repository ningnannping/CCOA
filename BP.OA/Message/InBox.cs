﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using BP.DA;
using BP.En;
using BP.Web;
using BP.Sys;

namespace BP.OA.Message
{
    /// <summary>
    /// 收件箱属性
    /// </summary>
    public class InBoxAttr : EntityMyPKAttr
    {
        #region 关联的发邮件信息
        /// <summary>
        /// 关联的发邮件ID
        /// </summary>
        public const String RefMsg = "RefMsg";
        /// <summary>
        /// 发件人
        /// </summary>
        public const String Sender = "Sender";
        public const String SenderName = "SenderName";
        /// <summary>
        /// 接受者
        /// </summary>
        public const String Receiver = "Receiver";
        /// <summary>
        /// 抄送者
        /// </summary>
        public const String CCers = "CCers";
        /// <summary>
        /// 发送时间
        /// </summary>
        public const String SendTime = "SendTime";
        /// <summary>
        /// 标题
        /// </summary>
        public const String Title = "Title";
        /// <summary>
        /// 发送内容
        /// </summary>
        public const String Docs = "Docs";
        #endregion

        #region 收件箱信息
        /// <summary>
        /// 接收人
        /// </summary> 
        public const String UserNo = "UserNo";
        /// <summary>
        /// 阅读时间
        /// </summary>
        public const String OpenTime = "OpenTime";
       /// <summary>
       /// 邮件类型 0、发邮件 1、抄送的
       /// </summary>
        public const String MsgType = "MsgType";
        /// <summary>
        /// 邮件状态 0 未读 1 已读 2 删除
        /// </summary>
        public const String InBoxState = "InBoxState";
        #endregion

    }
    /// <summary>
    /// 收件箱
    /// </summary>
    public partial class InBox : EntityMyPK
    {
        #region 属性
        /// <summary>
        /// 消息类型 0=接受者 1=抄送者.
        /// </summary>
        public int MsgType
        {
            get { return this.GetValIntByKey(InBoxAttr.MsgType); }
            set { this.SetValByKey(InBoxAttr.MsgType, value); }
        }
        public string RefMsg
        {
            get { return this.GetValStrByKey(InBoxAttr.RefMsg); }
            set { this.SetValByKey(InBoxAttr.RefMsg, value); }
        }
        public string UserNo
        {
            get { return this.GetValStrByKey(InBoxAttr.UserNo); }
            set { this.SetValByKey(InBoxAttr.UserNo, value); }
        }
       
        public DateTime OpenTime
        {
            get { return this.GetValDateTime(InBoxAttr.OpenTime); }
            set { this.SetValByKey(InBoxAttr.OpenTime, value); }
        }
        #endregion

        #region 构造函数
        /// <summary>
        /// 收件箱
        /// </summary>
        public InBox() { }
        /// <summary>
        /// 收件箱
        /// </summary>
        /// <param name="mypk">主键</param>
        public InBox(string mypk) : base(mypk) { }
        #endregion

        #region 重写方法
        public override UAC HisUAC
        {
            get
            {
                UAC uac = new UAC();
                uac.OpenAllForStation("");
                return uac;
            }
        }
        /// <summary>
        /// Map
        /// </summary>
        public override Map EnMap
        {
            get
            {
                if (this._enMap != null)
                    return this._enMap;

                Map map = new Map();
                map.PhysicsTable = "OA_MessageInBox";
                map.EnDesc = "收件箱";  // 实体的描述.
                
                map.AddMyPK();

                //关联的发件箱信息
                map.AddTBString(InBoxAttr.RefMsg, null, "关联的消息ID", true, false, 0, 50, 20);
                map.AddTBString(InBoxAttr.Sender, null, "发件人", true, false, 0, 1000, 20);
                map.AddTBString(InBoxAttr.SenderName, null, "发件人姓名", true, false, 0, 1000, 20);
                map.AddTBString(InBoxAttr.Receiver, null, "接收者", true, false, 0, 1000, 20);
                map.AddTBString(InBoxAttr.CCers, null, "抄送给", true, false, 0, 1000, 20);
                map.AddTBDateTime(InBoxAttr.SendTime, null, "发送日期", true, false);
                map.AddTBString(InBoxAttr.Title, null, "标题", true, false, 2, 50, 20);
                map.AddTBStringDoc(InBoxAttr.Docs, null, "内容", true, false);

                //收件箱信息
                map.AddTBString(InBoxAttr.UserNo, null, "接收者", true, false, 0, 100, 20);
                //类型  0=接受者, 1=抄送者.
                map.AddTBInt(InBoxAttr.MsgType, 0, "状态", true, false);
                map.AddTBDateTime(InBoxAttr.OpenTime, null, "打开日期", true, false);
                //@0=未读@1=已@2=删除
                map.AddTBInt(InBoxAttr.InBoxState, 0, "状态", true, false);

                this._enMap = map;
                return this._enMap;
            }
        }

        protected override bool beforeUpdateInsertAction()
        {
           // this.MyPK = this.RefMsg + "_" + this.UserNo;
            return base.beforeUpdateInsertAction();
        }
        #endregion
    }
    /// <summary>
    ///得到集合
    /// </summary>
    public class InBoxs : EntitiesOID
    {
        /// <summary>
        /// 得到一个新实体
        /// </summary>
        public override Entity GetNewEntity
        {
            get
            {
                return new InBox();
            }
        }
        /// <summary>
        /// 收件箱集合
        /// </summary>
        public InBoxs()
        {
        }

        public string RetrieveData(string ensName, object[] paras)
        {
            InBoxs boxs = new InBoxs();
            Int32 pageIdx = Int32.Parse(paras[0].ToString().Split('=')[1]);
            string searchKey = paras[1].ToString().Split('=')[1];
            QueryObject qo = new QueryObject(boxs);
            qo.AddWhere(InBoxAttr.UserNo, WebUser.No);
            qo.addAnd();
            qo.AddWhere(InBoxAttr.InBoxState, "<=", 1);
            qo.addAnd();
            qo.addLeftBracket();
            if (SystemConfig.AppCenterDBVarStr == "@" || SystemConfig.AppCenterDBVarStr == "?")
                qo.AddWhere(InBoxAttr.Title, " LIKE ", SystemConfig.AppCenterDBType == DBType.MySQL ? (" CONCAT('%'," + SystemConfig.AppCenterDBVarStr + "SearchKey,'%')") : (" '%'+" + SystemConfig.AppCenterDBVarStr + "SearchKey+'%'"));
            else
                qo.AddWhere(InBoxAttr.Title, " LIKE ", " '%'||" + SystemConfig.AppCenterDBVarStr + "SearchKey||'%'");
            qo.MyParas.Add("SearchKey", searchKey);

            qo.DoQuery(InBoxAttr.MyPK, 15, pageIdx);

            return boxs.ToJson();
        }

        /// <summary>
        /// 获取收件箱总邮件数和未读邮件
        /// </summary>
        /// <returns></returns>
        public string RetrieveCountData()
        {
            InBoxs boxs = new InBoxs();
            QueryObject qo = new QueryObject(boxs);
            qo.AddWhere(InBoxAttr.UserNo, WebUser.No);
            qo.addAnd();
            qo.AddWhere(InBoxAttr.InBoxState, "<=", 1);
            //总条数
            Int32 totalCount = qo.GetCount();

            //获取未读的条数
            qo.clear();
            qo.AddWhere(InBoxAttr.UserNo, WebUser.No);
            qo.addAnd();
            qo.AddWhere(InBoxAttr.InBoxState, 0);
            //总条数
            Int32 unReadCount = qo.GetCount();

            Hashtable ht = new Hashtable();
            ht.Add("TotalCount", totalCount);
            ht.Add("UnReadCount", unReadCount);

            return BP.Tools.Json.ToJson(ht);
             
        }

        /// <summary>
        /// 获取垃圾箱中邮件的总邮件数
        /// </summary>
        /// <returns></returns>
        public string RecycleCountData()
        {
            InBoxs boxs = new InBoxs();
            QueryObject qo = new QueryObject(boxs);
            qo.AddWhere(InBoxAttr.UserNo, WebUser.No);
            qo.addAnd();
            qo.AddWhere(InBoxAttr.InBoxState, "=", 2);
            Int32 count = qo.GetCount();
            //总条数
            return count.ToString();
        }

        //设置全部已读
        public void SetMsgRead()
        {
            string sql = "UPDATE OA_MessageInBox Set InBoxState = 1 WHERE InBoxState = 0 AND UserNo = '" + WebUser.No+"'";
            DBAccess.RunSQL(sql);
        }


    }
}
